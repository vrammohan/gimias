/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvRenderingTree.h"
#include "msvMultiRenderWindow.h"
#include "coreDataContainer.h"

//!
msvRenderingTree::msvRenderingTree() { m_RenderWindow = NULL; }

//!
msvRenderingTree::~msvRenderingTree(void) {}

void msvRenderingTree::SetMultiRenderWindow(msvMultiRenderWindow *renderWindow) {
  m_RenderWindow = renderWindow;
}

boost::any msvRenderingTree::Add(Core::DataEntity::Pointer dataEntity,
                                 bool bShow /*= true*/,
                                 bool bInitializeViews /*= true*/) {
  m_RenderWindow->GetButtonsManager()->ShowVTKButton(dataEntity);

  return RenderingTreeMITK::Add(dataEntity, bShow, bInitializeViews);
}

bool msvRenderingTree::Remove(mitk::DataTreeNode::Pointer node,
                              bool bInitializeViews /*= true*/) {
  // Use raw pointer to avoid increasing reference count
  Core::DataEntity *nodeDataEntity = GetDataEntity(node);
  if (nodeDataEntity != NULL) {
    m_RenderWindow->GetButtonsManager()->ShowVTKButton(nodeDataEntity, false);
  }

  return RenderingTreeMITK::Remove(node, bInitializeViews);
}

void msvRenderingTree::Show(Core::DataEntity::Pointer dataEntity, bool bShow,
                            bool initializeViews /*= true*/) {
  m_RenderWindow->GetButtonsManager()->ShowVTKButton(dataEntity, bShow);

  RenderingTreeMITK::Show(dataEntity, bShow, initializeViews);
}
