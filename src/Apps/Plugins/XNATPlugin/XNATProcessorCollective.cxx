/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATProcessorCollective.h"
#include "coreDataEntityReader.h"
#include "coreBaseDataEntityReader.h"

#include "coreDownloadXMLReader.h"
#include "coreUploadXMLReader.h"

XNATProcessorCollective::XNATProcessorCollective() {
  Core::IO::DataEntityReader::RegisterFormatReader(
      Core::IO::BaseDataEntityReader::Pointer(Core::IO::DownloadXMLReader::New()));
  Core::IO::DataEntityReader::RegisterFormatReader(
      Core::IO::BaseDataEntityReader::Pointer(Core::IO::UploadXMLReader::New()));
}

XNATProcessorCollective::~XNATProcessorCollective() {
  Core::IO::DataEntityReader::UnRegisterFormatReader(
      Core::IO::DataEntityReader::GetRegisteredReader(
          Core::IO::DownloadXMLReader::GetNameClass()));

  Core::IO::DataEntityReader::UnRegisterFormatReader(
      Core::IO::DataEntityReader::GetRegisteredReader(
          Core::IO::UploadXMLReader::GetNameClass()));
}
