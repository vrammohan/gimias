# Used to configure mitkPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmItkDataEntityIO = api.CreateCompiledProject("gmItkDataEntityIO", "dll")
gmItkDataEntityIO.AddProjects([itk, vtk, gmDataHandling])
gmItkDataEntityIO.AddSources(["itkDataEntityIO/*.cxx" ], checkExists = 0)
gmItkDataEntityIO.AddSources(["itkDataEntityIO/*.txx" ], checkExists = 0)
gmItkDataEntityIO.AddSources(["itkDataEntityIO/*.h" ], checkExists = 0)
gmItkDataEntityIO.SetPrecompiledHeader("gmitkDataEntityIOPCH.h")
gmItkDataEntityIO.AddIncludeFolders(["itkDataEntityIO"])
if api.GetCompiler().TargetIsWindows():
    gmItkDataEntityIO.AddDefinitions(["-D_CRT_SECURE_NO_WARNINGS"], private = 1)

mitkPlugin = GimiasPluginProject("MITKPlugin", api)

projects = [
	mitk,
	tpExtLibMITK,
	wxMitk,
    gmCoreLight, 
    guiBridgeLibWxWidgets,
	gmItkDataEntityIO,
	dcmAPI,
	cgns,
	hdf5
]
mitkPlugin.AddProjects(projects)

mitkPlugin.AddSources(["*.cxx", "*.h"])
mitkPlugin.AddSources(["processors/*.cxx", "processors/*.h", "processors/*.txx"], checkExists = 0)
mitkPlugin.AddIncludeFolders(["processors"])

ioFolders = mitkPlugin.Glob("processors/*")
for ioFolder in ioFolders:
    if os.path.isdir(ioFolder):
        mitkPlugin.AddSources(["%s/*.cpp" % ioFolder], checkExists = 0)
        mitkPlugin.AddSources(["%s/*.cxx" % ioFolder], checkExists = 0)
        mitkPlugin.AddSources(["%s/*.txx" % ioFolder], checkExists = 0)
        mitkPlugin.AddSources(["%s/*.h" % ioFolder], checkExists = 0)
        mitkPlugin.AddIncludeFolders([ioFolder])


widgetModules = [
	"CroppingWidget",
	"BoundingBoxWidget",
	"DataInformation",
	"ImageInfoWidget",
	"Interactors",
	"LandmarkSelectorWidget",
	"MeasurementWidget",
	"PointsTableWidget",
	"RenderingTree",
	"RenderWindow",
	"Toolbars",
	"VisualProps",
	"CGNSWriterDialog",
	"DICOMWriterWidget",
	"Propagate",
	"Preview"
]
mitkPlugin.AddWidgetModules(widgetModules, _useQt = 0)

mitkPlugin.SetPrecompiledHeader("MITKPluginPCH.h")
if api.GetCompiler().TargetIsWindows():
    mitkPlugin.AddDefinitions(["/bigobj"], private = 1)
    mitkPlugin.AddDefinitions(["-D_SCL_SECURE_NO_WARNINGS"], private = 1)

mitkPlugin.AddFilesToInstall(mitkPlugin.Glob("widgets/Interactors/GimiasStateMachines.xml"), "resource")
mitkPlugin.AddFilesToInstall(mitkPlugin.Glob("resource"), "resource")

