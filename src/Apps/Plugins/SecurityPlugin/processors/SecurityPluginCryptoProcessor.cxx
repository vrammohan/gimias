/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SecurityPluginCryptoProcessor.h"

//----------------------------------------------------------------------
void SecurityPluginCryptoProcessor::Init()
//----------------------------------------------------------------------
{
  // initialites members
  this->m_salt = "dlvashvsynttdddm(***tqems";
  this->m_MasterKeyHolder = Core::DataHolder< std::string >::New();
  this->m_MasterKeyHolder->SetSubject("");
  m_Encripted = false;
}

//----------------------------------------------------------------------
bool SecurityPluginCryptoProcessor::Find(std::string strKey)
//----------------------------------------------------------------------
{
  std::map< std::string, std::string >::iterator it = this->m_RegisterPasswords.begin();

  bool retVal = false;

  while (it != this->m_RegisterPasswords.end()) {
    if (this->Decrypt(it->second).length())
      retVal = true;
    it++;
  }

  return retVal;
}

//----------------------------------------------------------------------
void SecurityPluginCryptoProcessor::UpdateMasterKey(std::string newMasterPassword)
//----------------------------------------------------------------------
{
  if (!newMasterPassword.empty()) {
    std::map< std::string, std::string >::iterator it = this->m_RegisterPasswords.begin();

    while (it != this->m_RegisterPasswords.end()) {
      std::string decr = this->Decrypt(it->second, this->m_MasterKeyHolder->GetSubject());
      if (decr.length())
        this->Encrypt(it->first, decr, newMasterPassword, true);
      it++;
    }
    this->m_Encripted = true;

    this->m_MasterKeyHolder->SetSubject(newMasterPassword);
  }
}

//----------------------------------------------------------------------
std::string SecurityPluginCryptoProcessor::Encrypt(std::string name, std::string password,
                                                   std::string keyword, bool exists)
//----------------------------------------------------------------------
{
  std::string encr;

  Poco::Crypto::CipherFactory &factory = Poco::Crypto::CipherFactory::defaultFactory();

  if (keyword.empty())
    keyword = this->m_MasterKeyHolder->GetSubject();

  Poco::Crypto::CipherKey key("des3", keyword, this->m_salt);

  if (this->m_Encripted)
    encr = factory.createCipher(key)
               ->encryptString(password, Poco::Crypto::Cipher::ENC_BASE64);
  else
    encr = password;

  this->RegisterPassword(name, encr, exists);

  return encr;
}

//----------------------------------------------------------------------
std::string SecurityPluginCryptoProcessor::DecryptFromList(std::string name)
//----------------------------------------------------------------------
{

  Poco::Crypto::CipherFactory &factory = Poco::Crypto::CipherFactory::defaultFactory();

  Poco::Crypto::CipherKey key("des3", this->m_MasterKeyHolder->GetSubject(),
                              this->m_salt);

  std::string decr;

  std::map< std::string, std::string >::iterator it =
      this->m_RegisterPasswords.find(name);

  if (it == this->m_RegisterPasswords.end()) {
    throw std::runtime_error(std::string("Security Plugin"
                                         " Password not define."));
  } else {
    try {

      decr = factory.createCipher(key)
                 ->decryptString(it->second.c_str(), Poco::Crypto::Cipher::ENC_BASE64);

    } catch (...) {
      decr = it->second.c_str();
    }
  }
  return decr;
}

//----------------------------------------------------------------------
std::string SecurityPluginCryptoProcessor::Decrypt(std::string name, std::string keyword)
//----------------------------------------------------------------------
{
  Poco::Crypto::CipherFactory &factory = Poco::Crypto::CipherFactory::defaultFactory();

  if (keyword.empty())
    keyword = this->m_MasterKeyHolder->GetSubject();

  Poco::Crypto::CipherKey key("des3", keyword, this->m_salt);

  std::string decr;

  try {

    decr =
        factory.createCipher(key)->decryptString(name, Poco::Crypto::Cipher::ENC_BASE64);

  } catch (...) {
    decr = "";
  }

  return decr;
}

//----------------------------------------------------------------------
void SecurityPluginCryptoProcessor::RegisterPassword(std::string name,
                                                     std::string password, bool exists)
//----------------------------------------------------------------------
{
  std::map< std::string, std::string >::iterator it =
      this->m_RegisterPasswords.find(name);

  if (it == this->m_RegisterPasswords.end())
    this->m_RegisterPasswords.insert(
        std::pair< std::string, std::string >(name, password));
  else if (exists) {
    it->second = password;
    // m_RegisterPasswords[name] = password;
  } else {
    throw std::runtime_error(std::string("Security Plugin"
                                         " Password Already exists."));
  }
}

//----------------------------------------------------------------------
void SecurityPluginCryptoProcessor::ResetMasterKey()
//----------------------------------------------------------------------
{
  this->m_MasterKeyHolder->SetSubject("");
  this->m_Encripted = false;
}

//----------------------------------------------------------------------
void SecurityPluginCryptoProcessor::SetMasterKey(std::string masterKey)
//----------------------------------------------------------------------
{
  if (!masterKey.empty()) {
    this->m_MasterKeyHolder->SetSubject(masterKey);
    this->m_Encripted = true;
  }
}

//----------------------------------------------------------------------
bool SecurityPluginCryptoProcessor::CheckMasterKey(std::string str)
//----------------------------------------------------------------------
{
  return (this->m_MasterKeyHolder->GetSubject() == str);
}

//----------------------------------------------------------------------
bool SecurityPluginCryptoProcessor::GetBoolEncrypt()
//----------------------------------------------------------------------+
{
  return m_Encripted;
}

//----------------------------------------------------------------------
std::string SecurityPluginCryptoProcessor::GetPassword(std::string reg)
//----------------------------------------------------------------------
{
  std::map< std::string, std::string >::iterator it = this->m_RegisterPasswords.find(reg);

  std::string ret;

  if (!(it == this->m_RegisterPasswords.end()))
    ret = it->second.c_str();
  else
    ret = "";

  return ret;
}

//----------------------------------------------------------------------
Core::DataHolder< std::string >::Pointer
SecurityPluginCryptoProcessor::GetMasterKeyHolder()
//----------------------------------------------------------------------
{
  return this->m_MasterKeyHolder;
}