/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef msvVTKBtnEditView_H
#define msvVTKBtnEditView_H

#include "msvVTKBtnEditViewUI.h"
#include "msvViewManager.h"

/**
\brief Edit View

\ingroup msvPlugin
\author Xavi Planes
\date Nov 2012
*/
class msvVTKBtnEditView : public msvVTKBtnEditViewUI {
public:
  //!
  msvVTKBtnEditView(wxWindow *parent, int id, const wxPoint &pos = wxDefaultPosition,
                    const wxSize &size = wxDefaultSize, long style = 0);

  //!
  void SetSelectedViewHolder(ViewHolderType::Pointer viewHolder);

  //!
  void SetViewManager(msvViewManager::Pointer viewManager);

private:
  //!
  virtual void OnCancel(wxCommandEvent &event);

  //!
  virtual void OnCaptureView(wxCommandEvent &event);

  //!
  virtual void OnRemoveView(wxCommandEvent &event);

  //!
  void OnLoadSnapshot(wxCommandEvent &event);

  //!
  void OnSelectedViewModified();

  //!
  void UpdateWidget();

  //!
  void UpdateData();

private:
  //!
  msvViewManager::Pointer m_ViewManager;
};

#endif // msvVTKBtnEditView_H
