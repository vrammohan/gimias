/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvWxVTKButtonsManager.h"
#include "msvMultiRenderWindow.h"
#include "msvWxVTKButtons.h"
#include "msvMultiRenderWindow.h"

#include "coreDataContainer.h"
#include "coreDataEntityTreeCtrl.h"
#include "coreDataEntityListBrowser.h"

#include "wxUnicode.h"
#include "blTextUtils.h"

#include "vtkCamera.h"
#include "vtkMath.h"

//!
msvWxVTKButtonsManager::msvWxVTKButtonsManager() {}

//!
msvWxVTKButtonsManager::~msvWxVTKButtonsManager(void) {}

void msvWxVTKButtonsManager::SetRenderWindow(msvMultiRenderWindow *renderWindow) {
  m_RenderWindow = renderWindow;
}

msvWxVTKButtons *msvWxVTKButtonsManager::GetButton(int id) {
  std::map< unsigned int, msvWxVTKButtons * >::iterator itSelected;
  itSelected = m_Buttons.find(id);
  if (itSelected == m_Buttons.end()) {
    return NULL;
  }

  return itSelected->second;
}

void msvWxVTKButtonsManager::SelectButton(int id) {
  if (!m_RenderWindow->Get3D())
    return;

  // Find selected button
  msvWxVTKButtons *button = GetButton(id);
  if (button == NULL)
    return;

  // Get selected data
  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  Core::DataEntityList::Pointer dataEntityList = dataContainer->GetDataEntityList();
  Core::DataEntity::Pointer selected = dataEntityList->GetDataEntity(id);

  // Get list of session data and children
  Core::DataEntity::Pointer currentSession;
  currentSession = dataEntityList->FindRelative(selected, Core::SessionTypeId);
  Core::DataEntity::ChildrenListType visibleList =
      m_RenderWindow->GetListOfDataToLoad(currentSession);
  Core::DataEntity::ChildrenListType children = selected->GetChildrenList();
  visibleList.insert(visibleList.end(), children.begin(), children.end());

  // Restore all icon sizes to 16 and show selected data
  std::map< unsigned int, msvWxVTKButtons * >::iterator it;
  for (it = m_Buttons.begin(); it != m_Buttons.end(); it++) {
    // Size
    it->second->setIconSize(16);

    // Set visibility
    bool visible = false;
    if (dataEntityList->IsInTheList(it->first)) {
      // Check if item is visible
      Core::DataEntity::Pointer current = dataEntityList->GetDataEntity(it->first);
      Core::DataEntity::ChildrenListType::iterator itVisible;
      itVisible = std::find(visibleList.begin(), visibleList.end(), current);
      visible = itVisible != visibleList.end();

      // Check bounds
      visible &= it->second->getAnimateCamera()->checkValidBounds();

      // Collapse child session nodes
      wxTreeItemId item =
          m_RenderWindow->GetListBrowser()->GetTreeControl()->GetItem(current);
      if (!m_RenderWindow->IsParent(current, selected) && !visible) {
        m_RenderWindow->GetListBrowser()->GetTreeControl()->Collapse(item);
      } else if (visible) {
        m_RenderWindow->GetListBrowser()->GetTreeControl()->Expand(item);
      }
    }
    it->second->setShowButton(visible);
    it->second->update();
  }

  // Change icon size
  if (button) {
    button->setIconSize(32);
  }
}

void msvWxVTKButtonsManager::AddVTKButton(Core::DataEntity::Pointer dataEntity) {
  if (!m_RenderWindow->Get3D()) {
    return;
  }

  Core::DataEntityMetadata::Pointer metadata = dataEntity->GetMetadata();

  std::string desc;
  if (metadata->GetTag("Description").IsNotNull()) {
    desc = metadata->GetTag("Description")->GetValueAsString();
  }
  std::string name = metadata->GetName();

  // Set preview folder
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  dataEntity->GetPreview()->SetTempFolder(settings->GetProjectHomePath() + "/Temp");

  // Set current render
  vtkSmartPointer< vtkRenderer > vtkrenderer;
  vtkRenderWindow *renWin = m_RenderWindow->Get3D()->GetVtkRenderWindow();
  vtkrenderer = mitk::BaseRenderer::GetInstance(renWin)->GetVtkRenderer();

  // Create new VTK button
  std::map< unsigned int, msvWxVTKButtons * >::iterator it;
  it = m_Buttons.find(dataEntity->GetId());
  msvWxVTKButtons *toolButton;
  if (it == m_Buttons.end()) {
    toolButton = new msvWxVTKButtons(m_RenderWindow, dataEntity->GetId());
    m_Buttons[dataEntity->GetId()] = toolButton;
    wxString iconFileName =
        _U(Core::Runtime::Kernel::GetApplicationSettings()->GetCoreResourceForFile(
            "buttonIcon.png"));
    toolButton->setIconFileName(iconFileName);
    toolButton->setShowButton(true);
    toolButton->SetName(name);
    toolButton->setLabel(name);
    toolButton->setDescription(desc);
    toolButton->setCurrentRenderer(vtkrenderer);
  } else {
    toolButton = it->second;
  }

  // Update button properties
  UpdateVTKButtonProperties(toolButton->getId());

  UpdateDataObject(dataEntity);
  UpdatePreview(dataEntity);

  // If non selected -> select data and zoom it
  if (dataEntity->GetId() != 1)
    m_RenderWindow->AnimateFirstData();
}

vtkDataSet *msvWxVTKButtonsManager::GetVTKObject(Core::DataEntity::Pointer dataEntity) {
  if (dataEntity.IsNull()) {
    return NULL;
  }

  // get vtk object
  vtkDataSet *object = NULL;
  Core::vtkPolyDataPtr inputSurfaceVtk;
  Core::vtkImageDataPtr inputImageVtk;
  Core::vtkUnstructuredGridPtr inputVolumeVtk;
  if (dataEntity->GetProcessingData(inputSurfaceVtk)) {
    object = inputSurfaceVtk;
  } else if (dataEntity->GetProcessingData(inputImageVtk)) {
    object = inputImageVtk;
  } else if (dataEntity->GetProcessingData(inputVolumeVtk)) {
    object = inputVolumeVtk;
  }

  return object;
}

void msvWxVTKButtonsManager::UpdateDataObject(Core::DataEntity::Pointer dataEntity) {
  if (dataEntity.IsNull() || m_RenderWindow->Get3D() == NULL) {
    return;
  }

  std::map< unsigned int, msvWxVTKButtons * >::iterator it;
  it = m_Buttons.find(dataEntity->GetId());
  if (it == m_Buttons.end()) {
    return;
  }
  msvWxVTKButtons *toolButton = it->second;

  // Retrieve bounds form dataEntity meta data or current vtk object
  double bounds[6] = { 0, 0, 0, 0, 0, 0 };
  if (!GetMetadataTag(dataEntity->GetMetadata().GetPointer(), "Bounds", bounds, 6)) {
    vtkDataSet *object = GetVTKObject(dataEntity);
    if (object) {
      object->GetBounds(bounds);
    }
  }
  toolButton->setBounds(bounds); // this must be set after ther setCurrentRenderer call

  // Update button properties
  if (dataEntity->GetNumberOfTimeSteps() == 0) {
    toolButton->setShowButton(false);
  }
  toolButton->update();
}

void msvWxVTKButtonsManager::UpdateCameraParameters(Core::DataEntity::Pointer dataEntity,
                                                    blTagMap::Pointer view) {
  if (dataEntity.IsNull())
    return;

  // Get button
  msvWxVTKButtons *toolButton = GetButton(dataEntity->GetId());
  if (toolButton == NULL)
    return;

  // Get view tag properties from dataEntity or from session (view)
  if (view.IsNull())
    return;

  // Get multirenderwindow properties
  blTagMap::Pointer multiRenderWindow;
  multiRenderWindow =
      view->GetTagValue< blTagMap::Pointer >(m_RenderWindow->GetName().c_str());
  if (multiRenderWindow.IsNull())
    return;

  // Get 3D render window properties
  blTagMap::Pointer renderWindow;
  renderWindow = multiRenderWindow->GetTagValue< blTagMap::Pointer >(
      m_RenderWindow->Get3D()->GetName().ToStdString());
  if (renderWindow.IsNull())
    return;

  // Get camera params
  blTagMap::Pointer cameraParams =
      renderWindow->GetTagValue< blTagMap::Pointer >("Camera");
  if (cameraParams.IsNull())
    return;

  // Pass camera parameters to button
  double position[3];
  if (GetMetadataTag(cameraParams, "Position", position, 3)) {
    toolButton->getAnimateCamera()->setCameraPosition(position);
  }
  double parallelScale;
  if (GetMetadataTag(cameraParams, "ParallelScale", &parallelScale, 1)) {
    toolButton->getAnimateCamera()->setCameraParallelScale(parallelScale);
  }
  double viewUp[3];
  if (GetMetadataTag(cameraParams, "ViewUp", viewUp, 3)) {
    toolButton->getAnimateCamera()->setCameraViewUp(viewUp);
  }
  double clippingRange[2];
  if (GetMetadataTag(cameraParams, "ClippingRange", clippingRange, 2)) {
    toolButton->getAnimateCamera()->setCameraClippingRange(clippingRange);
  }
  double focalPoint[3];
  if (GetMetadataTag(cameraParams, "FocalPoint", focalPoint, 3)) {
    toolButton->getAnimateCamera()->setCameraFocalPoint(focalPoint);
  }
}

void msvWxVTKButtonsManager::UpdatePreview(Core::DataEntity::Pointer dataEntity) {
  if (dataEntity.IsNull()) {
    return;
  }

  std::map< unsigned int, msvWxVTKButtons * >::iterator it;
  it = m_Buttons.find(dataEntity->GetId());
  if (it == m_Buttons.end()) {
    return;
  }
  msvWxVTKButtons *toolButton = it->second;

  // Preview
  Core::DataEntityPreview::Pointer preview = dataEntity->GetPreview();
  toolButton->setPreviewFName(preview->GetTmpFileName());
  if (preview->IsOk()) {
    // Save temporary filename to be used by HTML
    preview->SaveTempFile();
  } else {
    // Build VTK preview
    vtkDataSet *object = GetVTKObject(dataEntity);
    if (toolButton->buildPreview(object, 80, 80)) {
      wxImage image;
      image.LoadFile(preview->GetTmpFileName());
      preview->SetImage(image);
    }
  }
}

void msvWxVTKButtonsManager::RemoveVTKButton(Core::DataEntity::Pointer dataEntity) {
  if (dataEntity.IsNull()) {
    return;
  }

  std::map< unsigned int, msvWxVTKButtons * >::iterator it;
  it = m_Buttons.find(dataEntity->GetId());
  if (it == m_Buttons.end()) {
    return;
  }
  msvWxVTKButtons *button = it->second;

  m_Buttons.erase(it);
  button->Destroy();
}

void msvWxVTKButtonsManager::ShowButtons(bool show,
                                         std::map< unsigned int, bool > &backupShow) {
  if (!show) {
    // Hide all buttons
    std::map< unsigned int, msvWxVTKButtons * >::iterator it;
    for (it = m_Buttons.begin(); it != m_Buttons.end(); it++) {
      backupShow[it->first] = it->second->showButton();
      it->second->setShowButton(false);
      it->second->update();
    }

    // Refresh render window
    mitk::RenderingManager::GetInstance()->ForceImmediateUpdateAll();

    // disables the user input to all program windows
    wxSafeYield();
  } else {
    // Show again the buttons
    std::map< unsigned int, msvWxVTKButtons * >::iterator it;
    for (it = m_Buttons.begin(); it != m_Buttons.end(); it++) {
      it->second->setShowButton(backupShow[it->first]);
      it->second->update();
    }

    // Refresh render window
    mitk::RenderingManager::GetInstance()->RequestUpdateAll();
  }
}

void msvWxVTKButtonsManager::ShowVTKButton(Core::DataEntity *dataEntity, bool show) {
  if (dataEntity == NULL) {
    return;
  }

  std::map< unsigned int, msvWxVTKButtons * >::iterator itSelected;
  itSelected = m_Buttons.find(dataEntity->GetId());
  if (itSelected == m_Buttons.end()) {
    return;
  }

  itSelected->second->setShowButton(show);
}

bool msvWxVTKButtonsManager::GetMetadataTag(blTagMap::Pointer tagMap,
                                            const std::string &name, double *array,
                                            int size) {
  blTag::Pointer tag = tagMap->GetTag(name);
  if (tag.IsNull()) {
    return false;
  }

  std::list< std::string > data;
  blTextUtils::ParseLine(tag->GetValueAsString(), ';', data);
  std::list< std::string >::iterator it;
  int pos = 0;
  for (it = data.begin(); it != data.end() && pos < size; it++) {
    array[pos] = atof(it->c_str());
    pos++;
  }
  return true;
}

void msvWxVTKButtonsManager::GetCameraParameters(blTagMap::Pointer cameraParams) {
  if (m_RenderWindow->Get3D() == NULL)
    return;

  // Get camera
  vtkSmartPointer< vtkRenderer > vtkrenderer;
  vtkRenderWindow *renWin = m_RenderWindow->Get3D()->GetVtkRenderWindow();
  vtkrenderer = mitk::BaseRenderer::GetInstance(renWin)->GetVtkRenderer();

  // Get camera parameters
  vtkCamera *camera = vtkrenderer->GetActiveCamera();
  double viewUp[3];
  camera->GetViewUp(viewUp);
  double focalPoint[3];
  camera->GetFocalPoint(focalPoint);
  double position[3];
  camera->GetPosition(position);
  double clippingRange[2];
  camera->GetClippingRange(clippingRange);

  // Camera parameters for current and render window (3D)
  std::stringstream stream1;
  for (int i = 0; i < 3; i++)
    stream1 << position[i] << ";";
  cameraParams->AddTag("Position", stream1.str());
  cameraParams->AddTag("ParallelScale", camera->GetParallelScale());
  std::stringstream stream2;
  for (int i = 0; i < 3; i++)
    stream2 << viewUp[i] << ";";
  cameraParams->AddTag("ViewUp", stream2.str());
  std::stringstream stream3;
  for (int i = 0; i < 2; i++)
    stream3 << clippingRange[i] << ";";
  cameraParams->AddTag("ClippingRange", stream3.str());
  std::stringstream stream4;
  for (int i = 0; i < 3; i++)
    stream4 << focalPoint[i] << ";";
  cameraParams->AddTag("FocalPoint", stream4.str());
}

void msvWxVTKButtonsManager::GetCameraBounds(blTagMap::Pointer metadata) {
  if (m_RenderWindow->Get3D() == NULL)
    return;

  // Compute bounds
  vtkSmartPointer< vtkRenderer > vtkrenderer;
  vtkRenderWindow *renWin = m_RenderWindow->Get3D()->GetVtkRenderWindow();
  vtkrenderer = mitk::BaseRenderer::GetInstance(renWin)->GetVtkRenderer();

  vtkCamera *camera = vtkrenderer->GetActiveCamera();
  double viewUp[3];
  camera->GetViewUp(viewUp);
  double focalPoint[3];
  camera->GetFocalPoint(focalPoint);
  double position[3];
  camera->GetPosition(position);
  double clippingRange[2];
  camera->GetClippingRange(clippingRange);
  double angle = vtkMath::RadiansFromDegrees(camera->GetViewAngle());
  double distance[3];
  for (int i = 0; i < 3; i++)
    distance[i] = focalPoint[i] - position[i];
  double bounds[6];
  bounds[0] = focalPoint[0] - distance[0] * tan(angle / 2);
  bounds[1] = focalPoint[0] + distance[0] * tan(angle / 2);
  bounds[2] = focalPoint[1] - distance[1] * tan(angle / 2);
  bounds[3] = focalPoint[1] + distance[1] * tan(angle / 2);
  bounds[4] = focalPoint[2] - distance[2] * tan(angle / 2);
  bounds[5] = focalPoint[2] + distance[2] * tan(angle / 2);

  // Update Metadata
  std::stringstream stream;
  for (int i = 0; i < 6; i++)
    stream << bounds[i] << ";";
  metadata->AddTag("Bounds", stream.str());
  std::stringstream stream1;
}

std::string msvWxVTKButtonsManager::GetScaleLevel(Core::DataEntity::Pointer dataEntity) {
  if (dataEntity.IsNull()) {
    return "Undefined";
  }

  double bounds[6] = { 0, 0, 0, 0, 0, 0 };
  if (!GetMetadataTag(dataEntity->GetMetadata().GetPointer(), "Bounds", bounds, 6)) {
    return "Undefined";
  }

  // Get max bounds range
  float maxRange = 0;
  float rangeX = bounds[1] - bounds[0];
  float rangeY = bounds[3] - bounds[2];
  float rangeZ = bounds[5] - bounds[4];
  if (maxRange < rangeX)
    maxRange = rangeX;
  if (maxRange < rangeY)
    maxRange = rangeY;
  if (maxRange < rangeZ)
    maxRange = rangeZ;

  // Check spatial scale
  std::string level;
  if (maxRange > 500) {
    level = "Body";
  }
  // From 500 mm to 5 cm
  else if (maxRange > 50 && maxRange <= 500) {
    level = "Organ";
  } else if (maxRange > 1 && maxRange <= 50) {
    level = "Tissue";
  }

  return level;
}

void msvWxVTKButtonsManager::UpdateVTKButtonProperties(int id) {
  msvWxVTKButtons *button = GetButton(id);
  if (button == NULL)
    return;

  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  // Get properties
  std::string showStr;
  settings->GetPluginProperty("MSV", "Show", showStr);
  bool show = showStr == "false" ? false : true;

  std::string showLabelsStr;
  settings->GetPluginProperty("MSV", "ShowLabels", showLabelsStr);
  bool showLabels = showLabelsStr == "true" ? true : false;

  std::string flyToStr;
  settings->GetPluginProperty("MSV", "FlyTo", flyToStr);
  bool flyTo = flyToStr == "false" ? false : true;

  std::string centerStr;
  settings->GetPluginProperty("MSV", "Center", centerStr);
  bool center = centerStr == "false" ? false : true;

  std::string flyToNStepsStr = "1000";
  settings->GetPluginProperty("MSV", "FlyToNSteps", flyToNStepsStr);
  int flyToNSteps = atoi(flyToNStepsStr.c_str());

  button->setShowButton(show);
  button->setShowLabel(showLabels);
  button->setFlyTo(flyTo);
  button->setOnCenter(center);
  if (flyTo)
    button->getAnimateCamera()->setFlyToNSteps(flyToNSteps);
  else
    button->getAnimateCamera()->setFlyToNSteps(1);

  button->update();
}
