import os.path
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")

# Third Parties -------------------------------------------------------------------------

# this flag determines whether the vtk() function returns the "static libraries" or
# "dynamic libraries" version VTK. By default, dynamic libraries are used.
# once the client calls the vtk() function and the "dynamic libraries" version
# is return, this flag is set automatically to False
useStaticVTK = None

# Sets useStaticVTK to True. Pre: useStaticVTK may not be False (because that
# indicates that the client is sometimes trying to use the static version and other times the dynamic version.
def UseVTKAsStaticLibrary():
    global useStaticVTK
    assert (useStaticVTK is None) or (useStaticVTK), "UseVTKAsStaticLibrary: vtk is already used as a dll"
    useStaticVTK = True

def mtk():
    return api.LoadThirdPartyModule('MTK', 'csnMTK').mtk

def itk():
    return api.LoadThirdPartyModule('ITK-3.20', 'csnITK').itk
    #return api.LoadThirdPartyModule('ITK-4.6', 'csnITK').itk

def vmtk():
    return api.LoadThirdPartyModule('VMTK-0.9', 'csnVMTK').vmtk

def vtkStatic():
    return api.LoadThirdPartyModule('VTK-5.10.1', 'csnVTK').vtkStatic

def vtkDynamic():
    return api.LoadThirdPartyModule('VTK-5.10.1', 'csnVTK').vtk

def vtk():
    global useStaticVTK
    if useStaticVTK is None:
        useStaticVTK = False

    if useStaticVTK:
        print "Using static VTK\n"
        return vtkStatic()
    else:
        return vtkDynamic()

def netGen():
    return api.LoadThirdPartyModule('NETGEN-4.9.5', 'csnNetGen').netGen

def libxml2():
    return api.LoadThirdPartyModule( 'VTK-5.10.1', 'csnLIBXML2').libxml2

def cxxTest():
    return api.LoadThirdPartyModule('CXXTEST', 'csnCxxTest').cxxTest

def dcmtk():
    return api.LoadThirdPartyModule('DCMTK-3.6.0', 'csnDCMTK').dcmtk

def matlab():
    return api.LoadThirdPartyModule('MATLAB-7.3', 'csnMATLAB').matlab

def torch():
    return api.LoadThirdPartyModule('Torch', 'csnTorch').torch

def ann():
    return api.LoadThirdPartyModule('ANN', 'csnANN').ann

def boost():
    return api.LoadThirdPartyModule('BOOST-1.45.0', 'csnBoost').boost

def zlib():
    return api.LoadThirdPartyModule('VTK-5.10.1', 'csnZLIB').zlib

def hdf5():
    return api.LoadThirdPartyModule('VTK-5.10.1', 'csnHDF5').hdf5


def curl():
    return api.LoadThirdPartyModule('Curl', 'csnCurl').curl


def mitk():
    return api.LoadThirdPartyModule('MITK_SVN2', 'csnMITK').mitk


def libsendspaceapi():
    return api.LoadThirdPartyModule('Libsendspaceapi',
                                    'csnLibsendspaceapi').libsendspaceapi


def poco():
    return api.LoadThirdPartyModule('MITK_SVN2', 'csnPoco').poco


def cgns():
    return api.LoadThirdPartyModule('CGNS', 'csnCGNS').cgns

def ga():
    return api.LoadThirdPartyModule('GA', 'csnGA').ga

def tinyXml():
    import TinyXml.csnTinyXml
    return TinyXml.csnTinyXml.tinyXml

def slicer():
    return api.LoadThirdPartyModule('SLICER', 'csnSlicer').slicer

def generateClp():
    return api.LoadThirdPartyModule('SLICER', 'csnSlicer').generateClp

def tclap():
    return api.LoadThirdPartyModule('SLICER', 'csnSlicer').tclap

def log4cplus():
    return api.LoadThirdPartyModule('LOG4CPLUS', 'csnLOG4CPLUS').log4cplus

def clapack():
    return api.LoadThirdPartyModule('CLAPACK-3.2.1', 'csnCLapack').clapack

def tubetk():
	return api.LoadThirdPartyModule('TUBETK-0.9', 'csnTUBETK').tubetk

def openssl():
    return api.LoadThirdPartyModule('OpenSSL', 'csnOpenSSL').openssl

def libssh():
    return api.LoadThirdPartyModule('Libssh', 'csnLibssh').libssh

def axis2C():
    return api.LoadThirdPartyModule('AXIS2C', 'csnAXIS2C').axis2C

def wxpdfdoc():
    return api.LoadThirdPartyModule('WXPDFDOC', 'csnWXPDFDoc').wxpdfdoc

# Modules -------------------------------------------------------------------------------

def baseLib():
    import Modules.BaseLib.csnBaseLib
    return Modules.BaseLib.csnBaseLib.baseLib

def baseLibApps():
    import Modules.BaseLib.csnBaseLibApps
    return Modules.BaseLib.csnBaseLibApps.baseLibApps

def baseLibCLPs():
    import Modules.BaseLib.csnBaselibCLP
    return Modules.BaseLib.csnBaselibCLP.EveryCLP

def baseLibITK():
    import Modules.BaseLib.csnBaseLib
    return Modules.BaseLib.csnBaseLib.baseLibITK

def baseLibVTK():
    import Modules.BaseLib.csnBaseLib
    return Modules.BaseLib.csnBaseLib.baseLibVTK

def baseLibSignal():
    import Modules.BaseLib.csnBaseLib
    return Modules.BaseLib.csnBaseLib.baseLibSignal

def baseLibNumericData():
    import Modules.BaseLib.csnBaseLib
    return Modules.BaseLib.csnBaseLib.baseLibNumericData

def baseLibMATLAB():
    import Modules.BaseLib.csnBaseLib
    return Modules.BaseLib.csnBaseLib.baseLibMATLAB

def baseLibCxxTest():
    import Modules.BaseLib.csnBaseLib
    return Modules.BaseLib.csnBaseLib.baseLibCxxTest

def baseLibTetgen():
    import Modules.BaseLib.csnBaseLib
    return Modules.BaseLib.csnBaseLib.baseLibTetgen

def cilabMacros():
    import Modules.CILabMacros.csnCILabMacros
    return Modules.CILabMacros.csnCILabMacros.cilabMacros

def dcmAPI():
    import Modules.DcmAPI.csnDcmAPI
    return Modules.DcmAPI.csnDcmAPI.dcmAPI

def dcmAPIData():
    import Modules.DcmAPI.csnDcmAPI
    return Modules.DcmAPI.csnDcmAPI.dcmAPIData

def dcmAPIIO():
    import Modules.DcmAPI.csnDcmAPI
    return Modules.DcmAPI.csnDcmAPI.dcmAPIIO

def meshLib():
    import Modules.MeshLib.csnMeshLib
    return Modules.MeshLib.csnMeshLib.meshLib

def pacsAPI():
    import Modules.PacsAPI.csnPacsAPI
    return Modules.PacsAPI.csnPacsAPI.pacsAPI

def securityLib():
    import Modules.SecurityLib.csnSecurityLib
    return Modules.SecurityLib.csnSecurityLib.securityLib

def sshAPI():
    import Modules.SshAPI.csnSshAPI
    return Modules.SshAPI.csnSshAPI.sshAPI

def sshAPIApps():
    import Modules.SshAPI.csnSshAPIApps
    return Modules.SshAPI.csnSshAPIApps.sshAPIApps

def xnat():
    import Modules.XNAT.csnXNAT
    return Modules.XNAT.csnXNAT.xNAT

def xnatApps():
    import Modules.XNAT.csnXNATApps
    return Modules.XNAT.csnXNATApps.xNATApps

def aheAPI():
    import Modules.AheAPI.csnAheAPI
    return Modules.AheAPI.csnAheAPI.aheAPI
    
# Toolkit header
toolkit = api.CreateCompiledProject("CISTIBToolkit", "library")
toolkit.CreateHeader("CISTIBToolkit.h", None, "CISTIB_TOOLKIT")


# Command line plugin
def CommandLinePlugin(_name, _holderProject=None):
    """ Create a command line plugin project. """
    _sourceRootFolder = api.FindSourceRootFolder(1)

    # command line lib
    projectLibName = "%sLib" % _name
    projectLib = api.CreateCompiledProject(projectLibName, "dll",
                                           _sourceRootFolder)
    projectLib.AddDefinitions(["-Dmain=ModuleEntryPoint"], private=1 )
    projectLib.SetBuildResultsSubFolder("commandLinePlugins")
    projectLib.AddCMakeInsertBeforeTarget(_CreateCMakeCLPPre)
    projectLib.AddCMakeInsertAfterTarget(_CreateCMakeCLPPost)

    # command line executable
    projectAppName = _name
    projectApp = api.CreateCompiledProject(projectAppName, "executable",
                                           _sourceRootFolder)
    projectApp.AddProjects([projectLib, generateClp, mtk])
    projectApp.SetBuildResultsSubFolder("commandLinePlugins")
    # wrapper for shared libraries
    wrapperSourceFile = api.RewrapProject(slicer).Glob("Slicer3/Applications/CLI/Templates/CommandLineSharedLibraryWrapper.cxx")
    if (not wrapperSourceFile) or (not os.path.isfile(wrapperSourceFile[0])):
        raise Exception("Could not find Slicer template in your thirdParty folders.")
    projectApp.AddSources([wrapperSourceFile[0]])

    # force the creation of the application project
    projectLib.AddProjects([projectApp], dependency=0)

    if not (_holderProject is None):
        _holderProject.AddProjects([projectLib])

    return projectLib


def _CreateCMakeCLPPre(project):
    """ CLP Cmake specific, prefix to main cmake section. """
    from csnAPIPublic import GetAPI
    project = GetAPI("2.5.0").RewrapProject(project)
    cmakelistsCode = ""
    if len(project.GetSources()) > 0:
        cmakelistsCode += "SET( CLP ${PROJECT_NAME}CLP )\n"
        cmakelistsCode += "SET( ${CLP}_SOURCE \"%s\" )\n" % project.GetSources()[0]
        cmakelistsCode += "GET_FILENAME_COMPONENT( TMP_FILENAME ${${CLP}_SOURCE} NAME_WE )\n"
        cmakelistsCode += "SET( ${CLP}_INCLUDE_FILE ${CMAKE_CURRENT_BINARY_DIR}/${TMP_FILENAME}CLP.h )\n"
        project.AddSources( ['${${CLP}_INCLUDE_FILE}'], checkExists = 0, forceAdd = 1 )
    return cmakelistsCode


def _CreateCMakeCLPPost(project):
    """ CLP Cmake specific, postfix to main cmake section. """
    from csnAPIPublic import GetAPI
    project = GetAPI("2.5.0").RewrapProject(project)
    cmakelistsCode = ""
    if len( project.GetSources() ) > 0:
        sourceFile = project.GetSources()[0]
        if sourceFile.endswith(".cxx") or sourceFile.endswith(".cpp"):
            xmlFile = sourceFile[0:-3]+"xml"
        else:
            raise Exception("Unsupported file extension: %s." % sourceFile)
        # GenerateCLP should be a dependency of the project, no need to find package
        cmakelistsCode += "GENERATECLP( ${CLP}_SOURCE \"%s\" )\n" % xmlFile
    return cmakelistsCode
