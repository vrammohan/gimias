/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntity.h"
#include "gmItkDataEntityIOWin32Header.h"
#include "itkCreateObjectFunction.h"
#include "itkImageIOBase.h"
#include "itkMetaDataObject.h"
#include "itkObjectFactoryBase.h"
#include "itkVersion.h"
#include "vtkDataArray.h"
#include "vtkPointData.h"
#include <string>
