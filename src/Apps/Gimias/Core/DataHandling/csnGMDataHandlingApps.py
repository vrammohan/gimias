from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

csnGMDataHandlingApps = api.CreateStandardModuleProject("csnGMDataHandlingApps", "library")
csnGMDataHandlingApps.AddSources([api.GetDummyCppFilename()])
csnGMDataHandlingApps.AddProjects([gmDataHandling])

