/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvMultiRenderWindow.h"

#include "coreSettings.h"
#include "coreVTKImageDataHolder.h"
#include "coreVTKUnstructuredGridHolder.h"
#include "coreSelectionToolboxWidget.h"
#include "corePluginTabFactory.h"
#include "coreDataContainer.h"
#include "coreDataEntityList.txx"
#include "coreDataEntityReader.h"
#include "coreDataEntityListBrowser.h"
#include "coreDataEntityTreeCtrl.h"
#include "coreRenderWindowContainer.h"
#include "coreProcessorManager.h"

#include "wxHtmlTooltipDialog.h"
#include "wxDataNavigationWidget.h"
#include "msvVTKBtnEdit.h"
#include "msvVTKBtnEditView.h"

#include "itksys/SystemTools.hxx"

#include "wxUnicode.h"
#include "wxCaptureWindow.h"

#include "vtkCamera.h"
#include "vtkMath.h"

DEFINE_EVENT_TYPE(wxID_DATA_ENTITY_SELECTED)
DEFINE_EVENT_TYPE(wxID_DATA_ENTITY_MODIFIED)
DEFINE_EVENT_TYPE(wxID_ANIMATE)
DEFINE_EVENT_TYPE(wxID_END_ANIMATION)

BEGIN_EVENT_TABLE(msvMultiRenderWindow, Core::Widgets::MultiRenderWindowMITK)
EVT_COMMAND(wxID_ANY, wxID_DATA_ENTITY_SELECTED, msvMultiRenderWindow::OnSelectDataEntity)
EVT_COMMAND(wxID_ANY, wxID_DATA_ENTITY_MODIFIED,
            msvMultiRenderWindow::OnModifiedDataEntity)
EVT_COMMAND(wxID_ANY, wxID_ANIMATE, msvMultiRenderWindow::OnAnimate)
EVT_COMMAND(wxID_ANY, wxID_END_ANIMATION, msvMultiRenderWindow::OnEndAnimation)
END_EVENT_TABLE();

//!
msvMultiRenderWindow::msvMultiRenderWindow(wxWindow *parent, wxWindowID id,
                                           const wxPoint &pos, const wxSize &size,
                                           long style, const wxString &name)
    : Core::Widgets::MultiRenderWindowMITK(parent, id, pos, size, style, name) {
  msvRenderingTree::Pointer tree = msvRenderingTree::New();
  tree->SetMultiRenderWindow(this);
  SetPrivateRenderingTree(tree.GetPointer());

  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  m_DataEntityList = dataContainer->GetDataEntityList();

  m_ButtonsManager = msvWxVTKButtonsManager::New();
  m_ButtonsManager->SetRenderWindow(this);

  m_DlgTooltip = new wxHtmlTooltipDialog(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
                                         wxBORDER_NONE);
  m_DlgTooltip->SetRenderWindow(this);
  m_DlgInfo = NULL;

  m_IsReading = false;
}

//!
msvMultiRenderWindow::~msvMultiRenderWindow(void) {
  // Remove observers
  if (m_ConnOnAdd.connected())
    m_ConnOnAdd.disconnect();
  if (m_ConnOnRemove.connected())
    m_ConnOnRemove.disconnect();
  m_DataEntityList->GetSelectedDataEntityHolder()->RemoveObserver1(
      this, &msvMultiRenderWindow::OnSelectedDataEntity);
}

void msvMultiRenderWindow::Init(Core::DataEntityHolder::Pointer selectedDataEntity,
                                bool enablePlanes /*= true*/) {
  Core::Widgets::MultiRenderWindowMITK::Init(selectedDataEntity, enablePlanes);

  GetPluginTab()->GetWidget< wxDataNavigationWidget >(wxID_TOOLTIP_WINDOW, m_DlgInfo);
  if (m_DlgInfo) {
    m_DlgInfo->SetButtonsManager(m_ButtonsManager);
    m_DlgInfo->SetRenderWindow(this);
  }

  // Initialize VTK buttons
  Core::DataEntityList::iterator it;
  for (it = m_DataEntityList->Begin(); it != m_DataEntityList->End(); it++) {
    m_ButtonsManager->AddVTKButton(m_DataEntityList->Get(it));
  }

  AnimateFirstData();

  // Add observers to DataEntityList
  m_DataEntityList->GetSelectedDataEntityHolder()->AddObserver1(
      this, &msvMultiRenderWindow::OnSelectedDataEntity);
  m_ConnOnAdd = m_DataEntityList->AddObserverOnAddDataEntity< msvMultiRenderWindow >(
      this, &msvMultiRenderWindow::OnDataEntityAddedToList);
  m_ConnOnRemove =
      m_DataEntityList->AddObserverOnRemoveDataEntity< msvMultiRenderWindow >(
          this, &msvMultiRenderWindow::OnDataEntityRemovedFromList);
}

msvWxVTKButtonsManager::Pointer msvMultiRenderWindow::GetButtonsManager() {
  return m_ButtonsManager;
}

//------------------------------------------------------------------------------
void msvMultiRenderWindow::ShowTooltip(msvWxVTKButtons *button, bool show) {
  if (m_DlgInfo->IsShown() && m_DataEntityList->GetSelectedDataEntity().IsNotNull() &&
      m_DataEntityList->GetSelectedDataEntity()->GetId() == button->getId()) {
    return;
  }

  if (show) {
    // show tooltip near the current mouse position
    wxPoint mousePos = wxGetMousePosition();
    m_DlgTooltip->SetPosition(mousePos + wxPoint(10, 10));
    m_DlgTooltip->SetButton(button);
    m_DlgTooltip->Show();
  } else {
    m_DlgTooltip->Hide();
  }
}

//------------------------------------------------------------------------------
void msvMultiRenderWindow::ShowInformation(msvWxVTKButtons *button, bool show) {
  if (show) {
    SelectDataEntity(button->getId());

    m_DlgInfo->SetMode(msvWxHtmlWindow::MODE_DESCRIPTION);

    // show tooltip near the current mouse position
    if (GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).IsFloating() ||
        !GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).IsShown()) {
      wxPoint mousePos = wxGetMousePosition();
      GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).FloatingSize(wxSize(400, 320));
      GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).Float();
      GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).FloatingPosition(mousePos +
                                                                 wxPoint(10, 10));
    }

    m_DlgTooltip->Hide();
  }

  GetPluginTab()->ShowWindow(m_DlgInfo->GetId());
}

bool msvMultiRenderWindow::BeginAnimation(bool isFlight, unsigned int btnId) {
  bool dataToLoad = false;

  // Get DataEntity
  Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(btnId);

  // Lock camera to avoid reseting view but initialize axis
  LockCamera(true);

  try {
    Core::DataEntity::Pointer sessionNode;
    sessionNode = m_DataEntityList->FindRelative(dataEntity, Core::SessionTypeId);
    Core::DataEntity::ChildrenListType list = GetListOfDataToLoad(sessionNode);

    // Update rendering tree properties from current view to DataEntity metadata
    blTagMap::Pointer view = m_DlgInfo->GetViewManager()->GetSelectedView();
    std::string workingAreaName = m_DlgInfo->GetViewManager()->GetWorkingAreaName(view);

    // If current working area is not MSV, change it to MSV and animate later
    if (view.IsNotNull() && GetCurrentWorkingAreaName() != "MSV") {
      ChangeWorkingArea(workingAreaName);
    }

    // if data is not loaded -> load it and update default preview and bounds
    if (dataEntity->GetNumberOfTimeSteps() == 0 ||
        dataEntity->GetType() == Core::SessionTypeId) {
      // Load data into memory
      Core::DataEntity::ChildrenListType::iterator it;
      for (it = list.begin(); it != list.end(); it++) {
        if (AddToPendingReader((*it)->GetId())) {
          dataToLoad = true;
        }
      }
    }

    // Execute readers for pending data to load or
    // just add events for animation
    if (dataToLoad) {
      ProcessPendingReaderList();
    } else {
      wxCommandEvent animateEvent;
      animateEvent.SetInt(btnId);
      animateEvent.SetEventType(wxID_ANIMATE);
      AddPendingEvent(animateEvent);

      wxCommandEvent endAnimateEvent;
      endAnimateEvent.SetInt(btnId);
      endAnimateEvent.SetEventType(wxID_END_ANIMATION);
      AddPendingEvent(endAnimateEvent);
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(BeginAnimation)

      // Unlock axis
      LockCamera(false);

  return dataToLoad;
}

void msvMultiRenderWindow::EndAnimation(bool isFlight, unsigned int btnId) {
  LockCamera(true);

  // Get list of Data objects to show
  Core::DataEntity::Pointer sessionNode;
  sessionNode =
      m_DataEntityList->FindRelative(m_DlgInfo->GetDataEntity(), Core::SessionTypeId);
  Core::DataEntity::ChildrenListType list = GetListOfDataToLoad(sessionNode);

  // Unload all other Dataentities
  Core::DataEntityList::iterator it;
  for (it = m_DataEntityList->Begin(); it != m_DataEntityList->End(); it++) {
    Core::DataEntity::Pointer data = m_DataEntityList->Get(it);
    Core::DataEntity::ChildrenListType::iterator it;
    it = std::find(list.begin(), list.end(), data);
    if (it == list.end()) {
      UnloadDataEntity(data->GetId());
    }
  }

  // Update list browser
  GetPrivateRenderingTree()->Modified();

  LockCamera(false);

  // Update rendering
  mitk::RenderingManager::GetInstance()->RequestUpdateAll();

  // Change working area
  blTagMap::Pointer view = m_DlgInfo->GetViewManager()->GetSelectedView();
  if (view.IsNotNull()) {
    std::string workingAreaName = m_DlgInfo->GetViewManager()->GetWorkingAreaName(view);
    ChangeWorkingArea(workingAreaName);
  }

  // Sometimes it gets hidden
  SelectDataEntity(btnId);
}

void msvMultiRenderWindow::OnModifiedMetadata() {
  Core::Widgets::MultiRenderWindowMITK::OnModifiedMetadata();

  // This function is called when enabling the working area
  Core::DataEntityList::iterator it;
  for (it = m_DataEntityList->Begin(); it != m_DataEntityList->End(); it++) {
    m_ButtonsManager->UpdateVTKButtonProperties(m_DataEntityList->Get(it)->GetId());
  }

  // Update selected button
  if (m_DlgInfo && m_DlgInfo->GetButton())
    m_ButtonsManager->SelectButton(m_DlgInfo->GetButton()->getId());

  // Refresh render window
  mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void msvMultiRenderWindow::OnSelectedDataEntity(Core::DataEntity::Pointer de) {
  if (de.IsNull())
    return;

  // When a child is registered, the code is called in the secondary thread of the reader
  wxCommandEvent ce;
  ce.SetEventType(wxID_DATA_ENTITY_SELECTED);
  ce.SetInt(de->GetId());
  if (wxIsMainThread()) {
    ProcessEvent(ce);
  } else {
    AddPendingEvent(ce);
  }
}

void msvMultiRenderWindow::OnModifiedDataEntity(Core::SmartPointerObject *object) {
  Core::DataEntity *de = dynamic_cast< Core::DataEntity * >(object);
  if (de == NULL)
    return;

  // When a child is registered, the code is called in the secondary thread of the reader
  wxCommandEvent ce;
  ce.SetEventType(wxID_DATA_ENTITY_MODIFIED);
  ce.SetInt(de->GetId());
  if (wxIsMainThread()) {
    ProcessEvent(ce);
  } else {
    AddPendingEvent(ce);
  }
}

void msvMultiRenderWindow::OnModifiedDataEntity(wxCommandEvent &event) {
  Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(event.GetInt());
  if (dataEntity.IsNull())
    return;

  m_ButtonsManager->UpdateDataObject(dataEntity);
}

void msvMultiRenderWindow::OnSelectDataEntity(wxCommandEvent &event) {
  RefreshSelectedDataEntity();
}

void msvMultiRenderWindow::SelectDataEntity(int id) {
  if (m_DataEntityList->IsInTheList(id)) {
    Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(id);
    m_DataEntityList->GetSelectedDataEntityHolder()->SetSubject(dataEntity);
  }
}

void msvMultiRenderWindow::RefreshSelectedDataEntity() {
  // Update selected DataEntity
  // Get selected data
  Core::DataEntity::Pointer selected;
  selected = m_DataEntityList->GetSelectedDataEntityHolder()->GetSubject();

  if (selected.IsNull()) {
    m_ButtonsManager->SelectButton(-1);
    m_DlgInfo->SetButton(NULL);
    return;
  }

  m_ButtonsManager->SelectButton(selected->GetId());

  // Set active button
  m_DlgInfo->SetButton(m_ButtonsManager->GetButton(selected->GetId()));

  // Refresh navigation window
  if (m_DlgInfo->GetMode() == msvWxHtmlWindow::MODE_DESCRIPTION ||
      m_DlgInfo->GetMode() == msvWxHtmlWindow::MODE_COMPLETE_INFORMATION ||
      m_DlgInfo->GetMode() == msvWxHtmlWindow::MODE_NAVIGATION) {
    m_DlgInfo->Show(m_DlgInfo->IsShown());
  }

  // Update view
  vtkSmartPointer< vtkRenderer > vtkrenderer;
  vtkRenderWindow *renWin = Get3D()->GetVtkRenderWindow();
  vtkrenderer = mitk::BaseRenderer::GetInstance(renWin)->GetVtkRenderer();
  vtkrenderer->GetRenderWindow()->Render();
}

void msvMultiRenderWindow::CaptureSnapshot() {
  if (GetPluginTab()->GetCurrentWorkingArea() == NULL || m_DlgInfo->GetButton() == NULL ||
      Get3D() == NULL)
    return;

  // Get selected DataEntity
  Core::DataEntity::Pointer dataEntity;
  dataEntity = m_DataEntityList->GetDataEntity(m_DlgInfo->GetButton()->getId());
  if (dataEntity.IsNull())
    return;

  // Hide dialog
  msvWxHtmlWindow::MODE_TYPE mode = m_DlgInfo->GetMode();
  if (GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).IsFloating())
    GetPluginTab()->ShowWindow(m_DlgInfo->GetId(), false);

  // Hide buttons
  std::map< unsigned int, bool > backupShow;
  m_ButtonsManager->ShowButtons(false, backupShow);

  // Capture window
  wxWindow *win = dynamic_cast< wxWindow * >(GetPluginTab()->GetCurrentWorkingArea());
  // Save temp filename
  Core::DataEntityPreview::Pointer preview = dataEntity->GetPreview();
  wxCaptureWindowThumbnail(win, preview->GetTmpFileName(), 80, 80, true, 50);
  wxImage image;
  image.LoadFile(preview->GetTmpFileName());
  preview->SetImage(image);

  // Update view
  std::string name = dataEntity->GetMetadata()->GetName();
  blTagMap::Pointer view = m_DlgInfo->GetViewManager()->GetView(name);
  if (view.IsNotNull()) {
    view->AddTag("Image", image);

    // Current working area
    view->AddTag("WorkingArea", GetCurrentWorkingAreaName());
    view->AddTag("WorkingAreaProps", GetWorkingAreaProps());

    // Get Camera Parameters
    if (view->GetTagValue< bool >("SaveCamera")) {
      blTagMap::Pointer cameraParams;
      cameraParams = m_DlgInfo->GetViewManager()->GetProps(
          view, GetName(), Get3D()->GetName().ToStdString(), "Camera");

      m_ButtonsManager->GetCameraParameters(cameraParams);
    }

    // Get Camera Parameters
    if (view->GetTagValue< bool >("SaveRenderingTree"))
      GetAllRenderingTreeProperties(view);
  }

  // Update bounds
  m_ButtonsManager->GetCameraBounds(dataEntity->GetMetadata().GetPointer());

  // Update VTK Button bounds
  m_ButtonsManager->UpdateDataObject(dataEntity);

  // Refresh editing window
  dataEntity->Modified();

  if (GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).IsFloating())
    GetPluginTab()->ShowWindow(m_DlgInfo->GetId(), true);
  m_ButtonsManager->ShowButtons(true, backupShow);
}

void msvMultiRenderWindow::ProcessPendingReaderList() {
  // boost::lock_guard<boost::mutex> lock_g(m_MutexPendingReader);

  bool multiThreading = false;
  std::map< Core::DataEntity::Pointer, Core::ProcessorThread::Pointer >::iterator it;
  for (it = m_PendingReader.begin(); it != m_PendingReader.end(); it++) {
    std::string filepath =
        it->first->GetMetadata()->GetTag("FilePath")->GetValueAsString();

    // Read data
    Core::IO::DataEntityReader::Pointer reader;
    reader = Core::IO::DataEntityReader::New();
    // Session data should not be loaded because it loads all children
    reader->SetLightRead(it->first->GetType() == Core::SessionTypeId);
    reader->SetFileName(filepath);
    reader->AddObserver1(this, &msvMultiRenderWindow::OnModifiedReader);
    reader->SetMultithreading(multiThreading);
    Core::ProcessorThread::Pointer thread;
    thread = Core::Runtime::Kernel::GetProcessorManager()->Execute(reader.GetPointer());
    it->second = thread;

    // Configure observer for processor thread status to clean observers
    if (thread.IsNotNull()) {
      // Observer for information messages
      thread->GetUpdateCallback()->AddObserver1(
          this, &msvMultiRenderWindow::OnReaderThreadStatus);
    }
  }

  if (!multiThreading) {
    // Main GUI thread
    m_PendingReader.clear();
    m_IsReading = !m_PendingReader.empty();
    SendAnimateEvents();
  }
}

bool msvMultiRenderWindow::AddToPendingReader(int id) {
  // boost::lock_guard<boost::mutex> lock_g(m_MutexPendingReader);

  if (!m_DataEntityList->IsInTheList(id))
    return false;

  // Check if FilePath is present
  Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(id);
  if (dataEntity->GetMetadata()->GetTag("FilePath").IsNull())
    return false;
  std::string filepath =
      dataEntity->GetMetadata()->GetTag("FilePath")->GetValueAsString();

  if (dataEntity->GetNumberOfTimeSteps() > 0)
    return false;

  // Add data entity to pending to read list
  m_PendingReader[dataEntity] = NULL;
  m_IsReading = !m_PendingReader.empty();
  return true;
}

bool msvMultiRenderWindow::LoadDataEntity(int id) {
  if (!m_DataEntityList->IsInTheList(id))
    return false;

  // Check if FilePath is present
  Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(id);
  if (dataEntity->GetMetadata()->GetTag("FilePath").IsNull())
    return false;
  std::string filepath =
      dataEntity->GetMetadata()->GetTag("FilePath")->GetValueAsString();

  if (dataEntity->GetNumberOfTimeSteps() > 0)
    return false;

  // Read data
  Core::IO::DataEntityReader::Pointer reader;
  reader = Core::IO::DataEntityReader::New();
  // Session data should not be loaded because it loads all children
  reader->SetLightRead(dataEntity->GetType() == Core::SessionTypeId);
  reader->SetFileName(filepath);
  reader->Update();

  // Check valid output data
  if (reader->GetNumberOfOutputs() == 0 || reader->GetOutputDataEntity(0).IsNull()) {
    return false;
  }

  // Copy output data
  Core::DataEntityImpl::Pointer impl = dataEntity->GetTimeStep(-1);
  dataEntity->MoveDataImpl(reader->GetOutputDataEntity(0));

  // Add the current DataEntity to Rendering Tree
  GetPrivateRenderingTree()->Add(dataEntity);
  return true;
}

void msvMultiRenderWindow::OnModifiedReader(Core::SmartPointerObject *object) {
  Core::IO::DataEntityReader *reader =
      dynamic_cast< Core::IO::DataEntityReader * >(object);
  if (reader == NULL)
    return;

  // Add observer for each output
  for (unsigned i = 0; i < reader->GetNumberOfOutputs(); i++) {
    reader->GetOutputDataEntityHolder(i)
        ->AddObserver1(this, &msvMultiRenderWindow::OnModifiedReaderOutput);
  }
}

void msvMultiRenderWindow::OnModifiedReaderOutput(Core::DataEntity::Pointer dataEntity) {
  // boost::lock_guard<boost::mutex> lock_g(m_MutexPendingReader);

  if (dataEntity.IsNull()) {
    return;
  }

  // Check pending reader
  Core::DataEntity::Pointer pendingDataEntity;
  std::map< Core::DataEntity::Pointer, Core::ProcessorThread::Pointer >::iterator it;
  it = m_PendingReader.begin();
  while (pendingDataEntity.IsNull() && it != m_PendingReader.end()) {
    std::string pendingFilepath =
        it->first->GetMetadata()->GetTag("FilePath")->GetValueAsString();
    std::string filepath =
        dataEntity->GetMetadata()->GetTag("FilePath")->GetValueAsString();
    if (filepath == pendingFilepath) {
      pendingDataEntity = it->first;
    }
    it++;
  }
  if (pendingDataEntity.IsNull())
    return;

  // m_PendingReader.erase( pendingDataEntity );
  m_IsReading = !m_PendingReader.empty();

  // Copy output data
  Core::DataEntityImpl::Pointer impl = pendingDataEntity->GetTimeStep(-1);
  pendingDataEntity->MoveDataImpl(dataEntity);

  // Update data
  m_ButtonsManager->UpdateDataObject(pendingDataEntity);
  m_ButtonsManager->UpdatePreview(pendingDataEntity);

  SendAnimateEvents();
}

void msvMultiRenderWindow::OnReaderThreadStatus(Core::SmartPointerObject *object) {
  Core::UpdateCallback::Pointer callback = dynamic_cast< Core::UpdateCallback * >(object);
  if (callback.IsNull())
    return;

  Core::ProcessorThread::Pointer thread;
  thread = Core::Runtime::Kernel::GetProcessorManager()->GetProcessorThread(
      callback->GetProcessorThreadID());
  if (thread.IsNull()) {
    return;
  }

  if (thread->GetState() == Core::ProcessorThread::STATE_FINISHED) {
    // boost::lock_guard<boost::mutex> lock_g(m_MutexPendingReader);

    // Find iterator to the thread
    std::map< Core::DataEntity::Pointer, Core::ProcessorThread::Pointer >::iterator
        itReader;
    itReader = m_PendingReader.begin();
    while (itReader != m_PendingReader.end()) {
      if (thread == itReader->second)
        break;
      itReader++;
    }

    if (itReader != m_PendingReader.end()) {
      m_PendingReader.erase(itReader);
      m_IsReading = !m_PendingReader.empty();
      SendAnimateEvents();
    }
  }
}

void msvMultiRenderWindow::SendAnimateEvents() {
  // When all data has been loaded -> Animate
  if (m_PendingReader.empty() && m_DlgInfo->GetButton()) {
    wxCommandEvent ce;
    ce.SetInt(m_DlgInfo->GetButton()->getId());

    ce.SetEventType(wxID_ANIMATE);
    AddPendingEvent(ce);

    ce.SetEventType(wxID_END_ANIMATION);
    AddPendingEvent(ce);
  }
}

void msvMultiRenderWindow::OnAnimate(wxCommandEvent &event) {
  msvWxVTKButtons *button = m_ButtonsManager->GetButton(event.GetInt());
  if (button == NULL)
    return;

  // Get data to load
  Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(button->getId());
  Core::DataEntity::Pointer sessionNode;
  sessionNode = m_DataEntityList->FindRelative(dataEntity, Core::SessionTypeId);
  Core::DataEntity::ChildrenListType list = GetListOfDataToLoad(sessionNode);

  // Get view and working area name
  blTagMap::Pointer view = m_DlgInfo->GetViewManager()->GetSelectedView();
  std::string workingAreaName = m_DlgInfo->GetViewManager()->GetWorkingAreaName(view);

  // Lock axis
  LockCamera(true);

  // Transfer rendering properties from view to DataEntity
  if (view.IsNotNull() && workingAreaName == GetCurrentWorkingAreaName()) {
    Core::DataEntity::ChildrenListType::iterator it;
    for (it = list.begin(); it != list.end(); it++) {
      UpdateRenderingPropsFromView(*it);
    }
  }

  // Update rendering tree using view props when data is already loaded
  if (view.IsNotNull() && workingAreaName == GetCurrentWorkingAreaName()) {
    Core::DataEntity::ChildrenListType::iterator it;
    for (it = list.begin(); it != list.end(); it++) {
      GetPrivateRenderingTree()->UpdateRenderingProperties(*it);
    }
  }

  // Force Update rendering
  mitk::RenderingManager::GetInstance()->ForceImmediateUpdateAll();

  // Unlock axis
  LockCamera(false);

  // Animate
  if (!button->getAnimateCamera()->isFlight() &&
      button->getAnimateCamera()->getCurrentRenderer() != NULL) {
    button->getAnimateCamera()->flyTo();
  }
}

void msvMultiRenderWindow::OnEndAnimation(wxCommandEvent &event) {
  msvWxVTKButtons *button = m_ButtonsManager->GetButton(event.GetInt());
  if (button == NULL)
    return;

  button->endAnimation();
  // Refresh
  m_DlgInfo->Show();
}

void msvMultiRenderWindow::UnloadDataEntity(int id) {
  Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(id);

  // Never unload session because it contains the views
  if (dataEntity->GetType() == Core::SessionTypeId)
    return;

  // Clean data
  dataEntity->Clean();
}

void msvMultiRenderWindow::OnDataEntityAddedToList(Core::DataEntity::Pointer dataEntity) {
  m_ButtonsManager->AddVTKButton(dataEntity);

  // Add observer to update modifications from transform box widget
  dataEntity->AddObserver1(this, &msvMultiRenderWindow::OnModifiedDataEntity);
}

void msvMultiRenderWindow::OnDataEntityRemovedFromList(
    Core::DataEntity::Pointer dataEntity) {
  if (m_DlgInfo->GetButton() == m_ButtonsManager->GetButton(dataEntity->GetId())) {
    m_DlgInfo->SetButton(NULL);
  }

  m_ButtonsManager->RemoveVTKButton(dataEntity);

  dataEntity->RemoveObserver1(this, &msvMultiRenderWindow::OnModifiedDataEntity);
}

bool msvMultiRenderWindow::IsParent(Core::DataEntity::Pointer dataEntity1,
                                    Core::DataEntity::Pointer dataEntity2) {
  while (dataEntity1 && dataEntity2 && dataEntity2->GetFather() != dataEntity1) {
    dataEntity2 = dataEntity2->GetFather();
  }

  return dataEntity2.IsNotNull();
}

Core::DataEntity::ChildrenListType
msvMultiRenderWindow::GetListOfDataToLoad(Core::DataEntity::Pointer dataEntity) {
  Core::DataEntity::ChildrenListType loadList;
  if (dataEntity.IsNull()) {
    return loadList;
  }

  loadList.push_back(dataEntity);

  Core::DataEntity::ChildrenListType children = dataEntity->GetChildrenList();
  for (int i = 0; i < children.size(); i++) {
    if (children[i]->GetType() != Core::SessionTypeId) {
      Core::DataEntity::ChildrenListType list = GetListOfDataToLoad(children[i]);
      loadList.insert(loadList.end(), list.begin(), list.end());
    }
  }

  return loadList;
}

bool msvMultiRenderWindow::IsZoomable(Core::DataEntity::Pointer dataEntity) {
  Core::DataEntity::ChildrenListType list = GetListOfDataToLoad(dataEntity);
  Core::DataEntity::ChildrenListType::iterator it;
  for (it = list.begin(); it != list.end(); it++) {
    if ((*it)->GetType() != Core::SessionTypeId) {
      return true;
    }
  }

  return false;
}

bool msvMultiRenderWindow::IsLoaded(Core::DataEntity::Pointer dataEntity) {
  if (dataEntity.IsNull()) {
    return false;
  }

  return dataEntity->GetNumberOfTimeSteps() > 0;
}
void msvMultiRenderWindow::CaptureView() {
  if (GetPluginTab()->GetCurrentWorkingArea() == NULL || m_DlgInfo->GetButton() == NULL ||
      Get3D() == NULL)
    return;

  // Get selected DataEntity
  Core::DataEntity::Pointer dataEntity;
  dataEntity = m_DataEntityList->GetDataEntity(m_DlgInfo->GetButton()->getId());
  if (dataEntity.IsNull())
    return;

  // Hide buttons
  msvWxHtmlWindow::MODE_TYPE mode = m_DlgInfo->GetMode();
  if (GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).IsFloating())
    GetPluginTab()->ShowWindow(m_DlgInfo->GetId(), false);
  std::map< unsigned int, bool > backupShow;
  m_ButtonsManager->ShowButtons(false, backupShow);

  // Capture window
  wxWindow *win = dynamic_cast< wxWindow * >(GetPluginTab()->GetCurrentWorkingArea());
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string tempFilname = settings->GetProjectHomePath() + "/Temp/Temp.png";
  wxCaptureWindowThumbnail(win, tempFilname, 80, 80, true, 50);

  // Load image
  wxImage image;
  image.LoadFile(tempFilname);
  blTagMap::Pointer view = m_DlgInfo->GetViewManager()->GetSelectedView();
  view->AddTag("Image", image);

  // Current working area
  view->AddTag("WorkingArea", GetCurrentWorkingAreaName());
  view->AddTag("WorkingAreaProps", GetWorkingAreaProps());

  // Get Camera Parameters
  if (view->GetTagValue< bool >("SaveCamera")) {
    blTagMap::Pointer cameraParams;
    cameraParams = m_DlgInfo->GetViewManager()->GetProps(
        view, GetName(), Get3D()->GetName().ToStdString(), "Camera");

    m_ButtonsManager->GetCameraParameters(cameraParams);
  }

  // Get Camera Parameters
  if (view->GetTagValue< bool >("SaveRenderingTree")) {
    GetAllRenderingTreeProperties(view);
  }

  // Update view
  m_DlgInfo->GetViewManager()->GetSelectedViewHolder()->NotifyObservers();

  // Show buttons and dialog
  if (GetPluginTab()->GetAuiPaneInfo(m_DlgInfo).IsFloating())
    GetPluginTab()->ShowWindow(m_DlgInfo->GetId(), true);
  m_ButtonsManager->ShowButtons(true, backupShow);
}

void msvMultiRenderWindow::GetAllRenderingTreeProperties(blTagMap::Pointer view) {
  if (m_DlgInfo->GetDataEntity().IsNull() || view.IsNull())
    return;

  // Get active working area
  Core::Widgets::RenderWindowContainer *workingAreaWindow;
  workingAreaWindow = dynamic_cast< Core::Widgets::RenderWindowContainer * >(
      GetPluginTab()->GetCurrentWorkingArea());
  if (workingAreaWindow == NULL)
    return;

  std::string workingAreaName = workingAreaWindow->GetName().ToStdString();

  // Iterate for each View
  for (int iWin = 0; iWin < workingAreaWindow->GetNumberOfWindows(); iWin++) {
    std::string viewName = workingAreaWindow->GetWindow(iWin)->GetName();

    // Get tag map to put the values
    blTagMap::Pointer renderingParams;
    renderingParams =
        m_DlgInfo->GetViewManager()->GetProps(view, viewName, "", "Rendering");

    // Get list of data for current session
    Core::DataEntity::Pointer session;
    session =
        m_DataEntityList->FindRelative(m_DlgInfo->GetDataEntity(), Core::SessionTypeId);
    Core::DataEntity::ChildrenListType list = GetListOfDataToLoad(session);

    // Get rendering properties
    Core::DataEntity::ChildrenListType::iterator it;
    for (it = list.begin(); it != list.end(); it++) {
      // Update metadata
      workingAreaWindow->GetWindow(iWin)->GetPrivateRenderingTree()->UpdateMetadata(*it);

      // Get rendering tag
      blTagMap::Pointer renderingData;
      renderingData = (*it)->GetMetadata()->GetTagValue< blTagMap::Pointer >("Rendering");
      if (renderingData.IsNull())
        continue;

      // Create a copy
      blTagMap::Pointer newRenderingData = blTagMap::New();
      newRenderingData->AddTags(renderingData);

      // Add it
      renderingParams->AddTag((*it)->GetMetadata()->GetName(), newRenderingData);
    }
  }
}

void msvMultiRenderWindow::UpdateRenderingPropsFromView(
    Core::DataEntity::Pointer dataEntity) {
  if (dataEntity.IsNull())
    return;

  blTagMap::Pointer view = m_DlgInfo->GetViewManager()->GetSelectedView();
  if (view.IsNull())
    return;

  // Get active working area
  Core::Widgets::RenderWindowContainer *workingAreaWindow;
  workingAreaWindow = dynamic_cast< Core::Widgets::RenderWindowContainer * >(
      GetPluginTab()->GetCurrentWorkingArea());
  if (workingAreaWindow == NULL)
    return;

  std::string workingAreaName = workingAreaWindow->GetName().ToStdString();

  // Iterate for each View
  for (int iWin = 0; iWin < workingAreaWindow->GetNumberOfWindows(); iWin++) {
    std::string viewName = workingAreaWindow->GetWindow(iWin)->GetName();

    // Get rendering props
    blTagMap::Pointer renderingParams;
    renderingParams =
        m_DlgInfo->GetViewManager()->GetProps(view, viewName, "", "Rendering");

    // Get rendering props for current object
    blTagMap::Pointer renderingDataProps;
    renderingDataProps = renderingParams->GetTagValue< blTagMap::Pointer >(
        dataEntity->GetMetadata()->GetName());
    if (renderingDataProps.IsNull())
      return;

    dataEntity->GetMetadata()->AddTag("Rendering", renderingDataProps);

    // Add to rendering tree
    if (!workingAreaWindow->GetWindow(iWin)
             ->GetPrivateRenderingTree()
             ->IsDataEntityRendered(dataEntity))
      workingAreaWindow->GetWindow(iWin)->GetPrivateRenderingTree()->Add(dataEntity,
                                                                         false);

    // Update rendering tree
    workingAreaWindow->GetWindow(iWin)
        ->GetPrivateRenderingTree()
        ->UpdateRenderingProperties(dataEntity);
  }
}

void msvMultiRenderWindow::AnimateFirstData() {
  Core::DataEntityHolder::Pointer selectedHolder =
      m_DataEntityList->GetSelectedDataEntityHolder();

  if (m_DataEntityList->GetCount() <= 1)
    return;

  if (m_DlgInfo->GetButton() != NULL && m_DlgInfo->GetDataEntity()->GetId() != 1)
    return;

  // Select first data
  if (selectedHolder->GetSubject().IsNull() ||
      selectedHolder->GetSubject()->GetId() == 1) {
    Core::DataEntityList::iterator it = m_DataEntityList->Begin();
    it++;
    selectedHolder->SetSubject(m_DataEntityList->Get(it));
  }

  // Zoom selected data
  if (selectedHolder->GetSubject().IsNotNull()) {
    std::stringstream sstream;
    sstream << selectedHolder->GetSubject()->GetId();
    wxHtmlLinkEvent link(wxID_ANY, wxHtmlLinkInfo("image:" + sstream.str()));
    wxPostEvent(m_DlgInfo->GetEventHandler(), link);
  }
}

std::string msvMultiRenderWindow::GetCurrentWorkingAreaName() {
  wxWindow *workingAreaWindow =
      dynamic_cast< wxWindow * >(GetPluginTab()->GetCurrentWorkingArea());
  if (workingAreaWindow == NULL)
    return "";

  return workingAreaWindow->GetName().ToStdString();
}

void msvMultiRenderWindow::ChangeWorkingArea(const std::string &workingAreaName) {
  // Check if working area is available
  wxWindow *win =
      GetPluginTab()->GetWorkingAreaManager()->GetWorkingArea(workingAreaName);
  if (!win || GetCurrentWorkingAreaName() == workingAreaName)
    return;

  // Check if view is available
  blTagMap::Pointer view = m_DlgInfo->GetViewManager()->GetSelectedView();
  if (view.IsNull())
    return;

  // Change working area
  GetPluginTab()->GetWorkingAreaManager()->SetActiveWorkingArea(win->GetId());
  if (workingAreaName != GetCurrentWorkingAreaName())
    return;

  // Update working area properties
  blTagMap::Pointer waProps = view->GetTagValue< blTagMap::Pointer >("WorkingAreaProps");
  if (waProps.IsNotNull()) {
    Core::BaseWindow *workingArea = GetPluginTab()->GetCurrentWorkingArea();
    if (workingArea) {
      workingArea->SetProperties(waProps);
    }
  }

  // Get list of data to load from current session
  Core::DataEntity::Pointer sessionNode;
  sessionNode =
      m_DataEntityList->FindRelative(m_DlgInfo->GetDataEntity(), Core::SessionTypeId);
  Core::DataEntity::ChildrenListType list = GetListOfDataToLoad(sessionNode);

  // Update rendering tree properties from current view to DataEntity metadata
  Core::DataEntity::ChildrenListType::iterator it;
  for (it = list.begin(); it != list.end(); it++) {
    UpdateRenderingPropsFromView(*it);
  }
}

blTagMap::Pointer msvMultiRenderWindow::GetWorkingAreaProps() {
  blTagMap::Pointer waProps = blTagMap::New();

  // true state is for just saving state of planes, layout, ... and not all configuration
  waProps->AddTag("State", true);

  Core::BaseWindow *workingArea = GetPluginTab()->GetCurrentWorkingArea();
  if (!workingArea)
    return waProps;

  workingArea->GetProperties(waProps);

  // Make a copy
  blTagMap::Pointer newProps = blTagMap::New();
  newProps->AddTags(waProps);

  return newProps;
}

bool msvMultiRenderWindow::IsReading() { return m_IsReading; }
