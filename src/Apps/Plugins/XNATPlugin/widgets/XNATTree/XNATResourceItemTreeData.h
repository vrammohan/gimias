/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* University of Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef XNATResourceItemTreeData_H
#define XNATResourceItemTreeData_H

#include <wx/treectrl.h>

#include "XNATPluginUtilities.h"

/**
A class that holds information about XNAT experiment resources in a tree item

\ingroup XNATPlugin
\author Shreyansh Jain
\date 09 May 2013
*/

class ResourceItemTreeData : public wxTreeItemData {
public:
  //!
  ResourceItemTreeData(int id, int subjectId,
                       // int experimentId,
                       // std::string& experimentLabel,
                       std::string &subjectLabel,
                       XNAT::Utils::SubjectResourceFile &resource)
      : m_id(id), m_subjectId(subjectId),
        // m_experimentId(experimentId),
        // m_experimentLabel(experimentLabel),
        m_subjectLabel(subjectLabel), m_resource(resource) {}

  //!
  ~ResourceItemTreeData() {}

  //!
  int GetId() const { return m_id; }
  ////!
  // int GetExperimentId() const { return m_experimentId; }
  //!
  int GetSubjectId() const { return m_subjectId; }
  ////!
  // std::string GetExperimentLabel() const { return m_experimentLabel; }
  //!
  std::string GetSubjectlabel() const { return m_subjectLabel; }
  //!
  XNAT::Utils::SubjectResourceFile GetResource() const { return m_resource; }

private:
  //!
  int m_id;
  //!
  int m_subjectId;
  ////!
  // int m_experimentId;
  ////!
  // std::string m_experimentLabel;
  //!
  std::string m_subjectLabel;
  //!
  XNAT::Utils::SubjectResourceFile m_resource;
};

class ResourceFolderTreeData : public wxTreeItemData {
public:
  //!
  ResourceFolderTreeData(int id, int subjectId,
                         // int experimentId,
                         // std::string& experimentLabel,
                         std::string &subjectLabel, XNAT::Utils::Resource &resource)
      : m_id(id), m_subjectId(subjectId),
        // m_experimentId(experimentId),
        // m_experimentLabel(experimentLabel),
        m_subjectLabel(subjectLabel), m_resource(resource) {}

  //!
  ~ResourceFolderTreeData() {}

  //!
  int GetId() const { return m_id; }
  ////!
  // int GetExperimentId() const { return m_experimentId; }
  //!
  int GetSubjectId() const { return m_subjectId; }
  ////!
  // std::string GetExperimentLabel() const { return m_experimentLabel; }
  //!
  std::string GetSubjectlabel() const { return m_subjectLabel; }
  //!
  XNAT::Utils::Resource GetResource() const { return m_resource; }

private:
  //!
  int m_id;
  //!
  int m_subjectId;
  ////!
  // int m_experimentId;
  ////!
  // std::string m_experimentLabel;
  //!
  std::string m_subjectLabel;
  //!
  XNAT::Utils::Resource m_resource;
};

#endif // XNATResourceItemTreeData_H
