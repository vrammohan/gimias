/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATPluginConnectionPanelWidget.h"
#include "XNATPluginEditConfigurationDialog.h"

#include "SecurityPluginUtilities.h"

XNATPluginConnectionPanelWidget::XNATPluginConnectionPanelWidget(
    XNATPluginManageConnections::Pointer connManager, wxWindow *parent, int id,
    const wxPoint &pos /*=wxDefaultPosition*/, const wxSize &size /*=wxDefaultSize*/,
    long style /*=0*/)
    : XNATListConnectionsDialogUI(parent, id) //, title), pos, size, style )
{
  // initializates the value of connection manager
  this->m_ConnectionsManager = connManager;

  this->OnInit();
}

void XNATPluginConnectionPanelWidget::OnInit() {
  this->m_ConnectionsManager->InitConnections(XNATPluginUtilities::ReadConfigFile());

  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();

  manager->GetMasterKeyHolder()->AddObserver(
      this, &XNATPluginConnectionPanelWidget::UpdateWidget);
}

void XNATPluginConnectionPanelWidget::OnAdd(wxCommandEvent &event) {
  try {
    // returns password manager and checks if encripted option is set
    SecurityPluginCryptoProcessor::Pointer manager =
        SecurityPluginUtilities::GetMPManager();

    XnatPluginEditConfigurationDialog dialog =
        XnatPluginEditConfigurationDialog(this, wxID_ANY, "Add Node");
    dialog.Center();

    if (dialog.ShowModal() == wxID_BTN_OK) {
      XNATPluginDefineConetions node;
      // defines node
      node.SetName(dialog.GetConnectionName());
      node.SetURL(dialog.GetURL());
      node.SetUser(dialog.GetUser());

      if (manager) {
        node.SetEncripted(manager->GetBoolEncrypt());
        // encrypts node and replace it if exists in node manager
        std::string registrationName =
            dialog.GetConnectionName() + dialog.GetUser() + dialog.GetURL();
        node.SetPassword(
            manager->Encrypt(registrationName, dialog.GetPassword().c_str(), "", true));
      } else {
        node.SetEncripted(false);
        node.SetPassword(dialog.GetPassword().c_str());
      }
      // add node to XNat node manager
      m_ConnectionsManager->AddNode(node);
      m_listNodes->Append(node.GetName());
    }
    this->UpdateData();
  }
  coreCatchExceptionsReportAndNoThrowMacro(XNATPluginConnectionPanelWidget::OnAdd)
}

void XNATPluginConnectionPanelWidget::OnRemove(wxCommandEvent &event) {
  try {
    int num = m_listNodes->GetCount();
    int count = 0;
    for (int i = 0; i < num; i++) {
      if (m_listNodes->IsSelected(i))
        count = count + 1;
    }
    if (count) {
      int selected = m_listNodes->GetSelection();
      std::string selectedNodeName(m_listNodes->GetString(selected).mb_str());
      m_ConnectionsManager->RemoveNode(selectedNodeName);
      m_listNodes->Delete(selected);
    }
    this->UpdateData();
  }
  coreCatchExceptionsReportAndNoThrowMacro(XNATPluginConnectionPanelWidget::OnRemove)
}

void XNATPluginConnectionPanelWidget::OnEdit(wxCommandEvent &event) {
  try {
    int num = m_listNodes->GetCount();
    int count = 0;
    for (int i = 0; i < num; i++) {
      if (m_listNodes->IsSelected(i))
        count = count + 1;
    }
    if (count) {
      int selected = m_listNodes->GetSelection();
      std::string selectedNodeName(m_listNodes->GetString(selected).mb_str());

      XNATPluginDefineConetions node;
      std::map< std::string, XNATPluginDefineConetions >::iterator it;

      it = m_ConnectionsManager->GetConnectionsHolder()->GetSubject().find(
          selectedNodeName);

      if (it == m_ConnectionsManager->GetConnectionsHolder()->GetSubject().end()) {
        throw std::runtime_error(std::string("XNATPluginConnections"
                                             "Connection doesn't exists."));
      }

      node = m_ConnectionsManager->GetConnectionsHolder()->GetSubject()[selectedNodeName];

      XnatPluginEditConfigurationDialog dialog =
          XnatPluginEditConfigurationDialog(this, wxID_ANY, "Edit Node");
      dialog.Center();

      dialog.SetConnectionName(node.GetName());
      dialog.SetURL(node.GetURL());
      dialog.SetUser(node.GetUser());
      dialog.SetPassword(node.GetPassword());

      std::string oldNodename = node.GetName();

      if (dialog.ShowModal() == wxID_BTN_OK) {
        node.SetName(dialog.GetConnectionName());
        // 131106 AMB: remove "/" at the end of the URL
        //             because of problems down the road
        std::string newURL = dialog.GetURL();
        if ((*newURL.rbegin()) == '/') {
          newURL.erase(newURL.size() - 1);
        }
        node.SetURL(newURL);
        node.SetUser(dialog.GetUser());
        node.SetPassword(dialog.GetPassword());

        m_ConnectionsManager->RemoveNode(oldNodename);
        m_ConnectionsManager->AddNode(node);
        m_listNodes->SetString(selected, dialog.GetConnectionName().c_str());
      }
    }
    this->UpdateData();
  }
  coreCatchExceptionsReportAndNoThrowMacro(XNATPluginConnectionPanelWidget::OnAdd)
}

void XNATPluginConnectionPanelWidget::UpdateData() {
  XNATPluginUtilities::OverwriteConfigFile(
      m_ConnectionsManager->GetConnectionsHolder()->GetSubject());

  m_ConnectionsManager->InitConnections(XNATPluginUtilities::ReadConfigFile());
}

void XNATPluginConnectionPanelWidget::UpdateWidget() {
  // only shows available elements
  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();

  // load connections
  m_listNodes->Clear();
  std::map< std::string, XNATPluginDefineConetions >::iterator it;
  for (it = m_ConnectionsManager->GetConnectionsHolder()->GetSubject().begin();
       it != m_ConnectionsManager->GetConnectionsHolder()->GetSubject().end(); it++) {

    std::string enc;
    if (it->second.GetEncripted()) {
      if (!manager) {
        m_listNodes->Append(it->second.GetName());
        return;
      }

      enc = manager->Decrypt(it->second.GetPassword());

      if (enc.size() > 0) {
        m_listNodes->Append(it->second.GetName());
      }
    } else {
      m_listNodes->Append(it->second.GetName());
    }
  }
}
