/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "coreDownloadXMLReader.h"

// xml
#include "tinyxml.h"

#include "xnatClient.h"
#include "Core.h"
#include "Utils.h"

#include "coreFile.h"

#include "coreDataEntityWriter.h"
#include "coreStringHelper.h"

#include "itksys/SystemTools.hxx"

Core::IO::DownloadXMLReader::DownloadXMLReader(void) {
  m_ValidExtensionsList.push_back(".dxml");

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string projectHomePath = settings->GetProjectHomePath();

  Core::IO::Directory::Pointer dir = Core::IO::Directory::New();
  dir->SetDirNameFullPath(projectHomePath);

  if (!dir->Exists()) {
    dir->Create();
  }

  m_TmpDir = projectHomePath;
}

Core::IO::DownloadXMLReader::~DownloadXMLReader(void) {}

std::string Core::IO::DownloadXMLReader::IntegerToString(int i) {
  if (i == 0)
    return "0";

  std::string ret = "";

  int temp = i;
  while (temp > 0) {
    ret = std::string(1, (char)('0' + (temp % 10))) + ret;
    temp /= 10;
  }

  return ret;
}

void Core::IO::DownloadXMLReader::ReadData() {
  if (m_Filenames.size() == 0) {
    return;
  }

  std::string filename = m_Filenames[0];
  TiXmlDocument *doc = new TiXmlDocument(filename);

  if (!doc->LoadFile()) {
    std::stringstream sstream;
    sstream << "Cannot open file: " << filename;
    throw Core::Exceptions::Exception("DownloadXMLReader::ReadData",
                                      sstream.str().c_str());
  }

  TiXmlHandle *hDoc = new TiXmlHandle(doc);
  TiXmlElement *Downloads;
  TiXmlHandle hRoot(0);

  Downloads = hDoc->FirstChildElement().Element();

  if (!Downloads) {
    return;
  }

  TiXmlElement *Element;

  std::vector< DownloadElement > downloadElements;

  for (Element = Downloads->FirstChildElement(); Element;
       Element = Element->NextSiblingElement()) {
    std::string ElementType = Element->Attribute("type");
    if (ElementType == "XNAT") {
      std::string ElementURL = "";
      std::string ElementFolder = "";
      std::string ElementFilename = "";
      std::string ElementUsr = "";
      std::string ElementPwd = "";

      ElementURL = Element->Attribute("url");
      ElementFolder = Element->Attribute("folder");
      ElementFilename = Element->Attribute("filename");
      ElementUsr = Element->Attribute("usr");
      ElementPwd = Element->Attribute("pwd");

      DownloadElement de;
      de.URL = ElementURL;
      de.DestinationFolder = ElementFolder;
      de.Filename = ElementFilename;
      de.usr = ElementUsr;
      de.pwd = ElementPwd;

      downloadElements.push_back(de);
    }
  }

  std::vector< Core::DataEntity::Pointer > outputDataEntities;
  std::vector< std::string > outputDataEntitiesFilenames;

  int NumberOfOutputs = 0;
  for (int i = 0; i < downloadElements.size(); i++) {
    // Write a temporal file
    Core::Runtime::Settings::Pointer settings =
        Core::Runtime::Kernel::GetApplicationSettings();
    std::string projectHomePath = settings->GetProjectHomePath();

    std::ostringstream fnameString;
    fnameString << this << time(NULL);
    std::string fname =
        "xml_" + Core::StringHelper::ConvertDigitsToCharacters(fnameString);
    Core::IO::Directory::Pointer tempDir = Core::IO::Directory::New();
    tempDir->SetDirNameFullPath(projectHomePath + Core::IO::SlashChar + fname);
    if (!tempDir->Exists()) {
      tempDir->Create();
    }

    //! \todo Use proper extension
    std::string tempFile =
        tempDir->GetFullPathDirName() + Core::IO::SlashChar + "temp.zip";

    // Download file from XNAT
    XNAT::Core client;
    client.New(downloadElements[i].usr, downloadElements[i].pwd);
    std::string filename =
        downloadElements[i].DestinationFolder + downloadElements[i].Filename;
    client.retrieve(downloadElements[i].URL, tempFile);

    // Read downloaded file
    Core::IO::DataEntityReader::Pointer reader;
    reader = Core::IO::DataEntityReader::New();
    reader->SetFileName(tempFile);
    reader->Update();

    // Remove temporal directory
    if (!tempDir->GetFullPathDirName().empty() &&
        itksys::SystemTools::FileIsDirectory(tempDir->GetFullPathDirName().c_str())) {
      itksys::SystemTools::RemoveADirectory(tempDir->GetFullPathDirName().c_str());
    }

    // Create output filenames
    for (int j = 0; j < reader->GetNumberOfOutputs(); j++) {
      Core::DataEntity::Pointer de = reader->GetOutputDataEntity(j);
      outputDataEntities.push_back(de);

      outputDataEntitiesFilenames.push_back(
          downloadElements[i].DestinationFolder +
          /* IntegerToString(outputDataEntities.size()) + "_" */ downloadElements[i]
              .Filename);
      NumberOfOutputs++;
    }
  }

  // Write files
  SetNumberOfOutputs(NumberOfOutputs);
  for (int i = 0; i < NumberOfOutputs; i++) {
    GetOutputDataEntityHolder(i)->SetSubject(outputDataEntities[i]);
  }

  for (int i = 0; i < NumberOfOutputs; i++) {
    Core::IO::DataEntityWriter::Pointer writer = Core::IO::DataEntityWriter::New();
    writer->SetInputDataEntity(i, GetOutputDataEntity(i));
    writer->SetFileName(outputDataEntitiesFilenames[i]);
    writer->Update();
  }
}

boost::any Core::IO::DownloadXMLReader::ReadSingleTimeStep(int iTimeStep,
                                                           const std::string &filename) {
  blTagMap::Pointer output;
  return output;
}
