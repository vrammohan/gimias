/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef XNATExperimentItemTreeData_H
#define XNATExperimentItemTreeData_H

#include <wx/treectrl.h>

/**
A class that holds some information about experiment in the tree item

\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class ExperimentItemTreeData : public wxTreeItemData {
public:
  //!
  ExperimentItemTreeData(int id, int subjectId, std::string &experimentLabel,
                         std::string &experimentName, std::string &experimentUrl)
      : m_id(id), m_subjectId(subjectId), m_experimentLabel(experimentLabel),
        m_experimentType(experimentName), m_experimentUrl(experimentUrl) {}

  //!
  ~ExperimentItemTreeData() {}

  //!
  int GetId() const { return m_id; }
  //!
  int GetSubjectId() const { return m_subjectId; }
  //!
  std::string GetExperimentLabel() const { return m_experimentLabel; }
  //!
  std::string GetExperimentType() const { return m_experimentType; }
  //!
  std::string GetExperimentUrl() const { return m_experimentUrl; }

private:
  //!
  int m_id;
  //!
  int m_subjectId;
  //!
  std::string m_experimentLabel;
  //!
  std::string m_experimentType;
  //!
  std::string m_experimentUrl;
};

#endif // XNATExperimentItemTreeData_H
