/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATTree.h"
#include "blMitkUnicode.h"
#include "wx/imaglist.h"

#include "coreReportExceptionMacros.h"
#include "coreKernel.h"
#include "coreSettings.h"

BEGIN_EVENT_TABLE(XNATTree, wxTreeCtrl)
END_EVENT_TABLE()

XNATTree::XNATTree(wxWindow *parent, wxWindowID id /*= wxID_ANY*/,
                   const wxPoint &pos /*= wxDefaultPosition*/,
                   const wxSize &size /*= wxDefaultSize*/,
                   long style /*= wxTR_HAS_BUTTONS | wxTR_LINES_AT_ROOT*/,
                   const wxValidator &validator /*= wxDefaultValidator*/,
                   const wxString &name /*= wxTreeCtrlNameStr*/)
    : wxTreeCtrl(parent, id, pos, size, style, validator, name) {
  m_currentSubject = "";
  m_currentExperiment = "";
  m_folderNumber = 0;

  SetIcons();
}

// assign icons for items in tree
void XNATTree::SetIcons() {
  // set tree icons
  wxImageList *imageList = new wxImageList(16, 16);
  imageList->Add(GetIcon("patients_list_16.ico"));
  imageList->Add(GetIcon("patient_16.ico"));
  imageList->Add(GetIcon("series_16.ico"));
  imageList->Add(GetIcon("slice_16.ico"));

  AssignImageList(imageList);
}

void XNATTree::LoadScansIntoTree(int experimentId, XNATSubject subject, int subjectId,
                                 std::string experimentLabel,
                                 const wxTreeItemId &experimentItem) {
  try {
    // Display the scans.
    std::vector< XNAT::Utils::Scan > scans;
    scans = subject.scans[experimentLabel];

    for (unsigned i = 0; i < scans.size(); i++) {
      XNAT::Utils::Scan scan = scans[i];

      std::string id = scan.ID;
      std::string type = scan.type;

      wxTreeItemId scanItem = AppendItem(
          experimentItem, _U("Scan: " + id + " (Type: " + type + ")"),

          3, 3, new ScanItemTreeData(i, subjectId, experimentId, experimentLabel,
                                     subject.attributes["label"], scan));
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(XNATTree::LoadScansIntoTree)
}

void XNATTree::LoadExperimentsIntoTree(int subjectId, XNATSubject subject,
                                       const wxTreeItemId &subjectItem) {
  try {
    std::map< std::string, std::vector< XNAT::Utils::Experiment > > experiments;
    experiments = subject.experiments;

    std::map< std::string, std::vector< XNAT::Utils::Experiment > >::iterator it;

    for (it = experiments.begin(); it != experiments.end(); it++) {
      std::string projectName = it->first;
      std::vector< XNAT::Utils::Experiment > projectExperiments = it->second;

      for (unsigned i = 0; i < projectExperiments.size(); i++, m_folderNumber++) {
        XNAT::Utils::Experiment experiment = projectExperiments[i];

        std::string label = experiment.label;
        std::string type = experiment.type;
        std::string uri = experiment.uri;

        wxTreeItemId experimentItem = AppendItem(
            subjectItem, _U("Experiment: " + label + " (Project: " + projectName + ")"),
            2, 2,
            new ExperimentItemTreeData(m_folderNumber, subjectId, label, type, uri));

        LoadScansIntoTree(m_folderNumber, subject, subjectId, label, experimentItem);
      }
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(XNATTree::LoadExperimentsIntoTree)
}

void XNATTree::LoadSubjectResourceItemsIntoTree(int subjectId, XNATSubject subject,
                                                const wxTreeItemId &folderItem,
                                                std::string resourceCollection) {
  try {
    std::vector< XNAT::Utils::SubjectResourceFile > resourceFiles;
    resourceFiles = subject.subjectResourceFiles[resourceCollection];

    for (unsigned j = 0; j < resourceFiles.size(); j++) {
      XNAT::Utils::SubjectResourceFile resFile = resourceFiles[j];

      std::string label = resFile.label;
      std::string collection = resFile.collection;

      wxTreeItemId resourceItem = AppendItem(
          folderItem, _U("File: " + label), 3, 3,
          new ResourceItemTreeData(j, subjectId, subject.attributes["label"], resFile));
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(XNATTree::LoadSubjctResourceItemsIntoTree)
}

void XNATTree::LoadSubjectResourcesIntoTree(int subjectId, XNATSubject subject,
                                            const wxTreeItemId &subjectItem,
                                            std::string resName) {
  try {
    std::vector< XNAT::Utils::SubjectResourceFile > resourceFiles;
    resourceFiles = subject.subjectResourceFiles[resName];
    int size = resourceFiles.size();
    XNAT::Utils::Resource myResource = { resName, "", resName, size, "", "" };

    if (size > 0) {
      wxTreeItemId resourceFolder =
          AppendItem(subjectItem, _U("Resource: " + resName), 2, 2,
                     new ResourceFolderTreeData(m_folderNumber, subjectId,
                                                subject.attributes["label"], myResource));
      LoadSubjectResourceItemsIntoTree(subjectId, subject, resourceFolder, resName);
      m_folderNumber++;
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(XNATTree::LoadSubjctResourceItemsIntoTree)
}

void XNATTree::LoadSubjectsIntoTree(std::vector< XNATSubject > subjects) {
  // first clean the tree
  DeleteAllItems();

  std::stringstream ss;
  ss << subjects.size();
  std::string nSubjects = ss.str();

  wxTreeItemId rootPatients =
      AddRoot(wxT("Subject list (" + nSubjects + " found)"), 0, 0);

  // reading patients
  for (unsigned i = 0; i < subjects.size(); i++) {
    XNATSubject subject = subjects[i];
    m_folderNumber = 0;

    std::string subjectID = subject.attributes["ID"];
    std::string subjectName = subject.attributes["label"];
    std::string subjectYOB = subject.attributes["yob"];
    std::string subjectGender = subject.attributes["gender"];

    wxTreeItemId subjectItem = AppendItem(
        rootPatients, _U("Subject: " + subjectName), 1, 1,
        // new wxTreeItemData()
        new SubjectItemTreeData(i, subjectID, subjectName, subjectYOB, subjectGender));

    // reading studies for the patient
    LoadExperimentsIntoTree(i, subject, subjectItem);

    // Now display the resources at subject level
    for (std::map< std::string,
                   std::vector< XNAT::Utils::SubjectResourceFile > >::iterator it =
             subject.subjectResourceFiles.begin();
         it != subject.subjectResourceFiles.end(); it++) {
      LoadSubjectResourcesIntoTree(i, subject, subjectItem, it->first);
    }
  }

  ExpandAll();
}

wxIcon XNATTree::GetIcon(const std::string &filename) {
  std::string fullPath =
      Core::Runtime::Kernel::GetApplicationSettings()->GetCoreResourceForFile(filename);
  return wxIcon(_U(fullPath), wxBITMAP_TYPE_ANY);
}
