/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "TavernaPluginWidgetCollective.h"
#include "TavernaPluginWorkflowPanelWidget.h"
#include "TavernaPluginModuleExecutionTaverna.h"
#include "TavernaPluginDataTransferTaverna.h"
#include "TavernaPluginWorkingArea.h"
#include "TavernaWorkingArea.xpm"
#include "TavernaPluginConfigurationPanelWidget.h"
#include "TavernaPluginToolbar.h"
#include "TavernaPluginWorkflowPanelWidgetFactory.h"

#include "coreFactoryManager.h"

TavernaPlugin::WidgetCollective::WidgetCollective() {
  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

  // Register Taverna components
  graphicalIface->RegisterImpl("TavernaModule", ModuleExecutionTaverna::Factory::New());

  graphicalIface->RegisterFactory(Core::DynDataTransferBase::GetNameClass(),
                                  DataTransferTaverna::Factory::New());

  // Scan workflows folder
  WorkflowPanelWidgetFactory::Scan();

// WorkingArea
#ifdef _WIN32
  graphicalIface->RegisterFactory(WorkingArea::Factory::NewBase(),
                                  Core::WindowConfig()
                                      .WorkingArea()
                                      .Caption("Taverna working area")
                                      .Bitmap(tavernaworkingarea_xpm)
                                      .Id(wxNewId()));
#endif

  // Configuration panel
  graphicalIface->RegisterFactory(
      ConfigurationPanelWidget::Factory::NewBase(),
      Core::WindowConfig().Preferences().Caption("Taverna configuration"));

  graphicalIface->RegisterFactory(Toolbar::Factory::NewBase(),
                                  Core::WindowConfig()
                                      .Toolbar()
                                      .Top()
                                      .Show()
                                      .Caption("Taverna Toolbar")
                                      .Id(wxNewId()));
}

TavernaPlugin::WidgetCollective::~WidgetCollective() {}
