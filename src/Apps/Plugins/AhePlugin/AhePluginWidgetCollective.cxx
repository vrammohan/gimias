/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "AhePluginWidgetCollective.h"

#include "AhePluginEditConfigurationDialog.h"
const long wxID_AhePluginEditConfigurationDialog = wxNewId();

#include "AhePluginConfigurationPanelWidget.h"
const long wxID_ConfigurationPanelWidget = wxNewId();

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTab.h"

namespace ahePlugin {

WidgetCollective::WidgetCollective() {
  // Configure the widget to be attached to the Preferences dialog
  Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
      ConfigurationPanelWidget::Factory::NewBase(),
      Core::WindowConfig().Preferences().Caption(
          AHE_CONF_PANEL_NAME)); // wxT("AHE configuration")
}

} // namespace ahePlugin
