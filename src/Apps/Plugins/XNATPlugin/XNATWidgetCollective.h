/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _XNATWidgetCollective_H
#define _XNATWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

/**

\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class XNATWidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(XNATWidgetCollective, Core::WidgetCollective);

  //!
  void Init();

private:
  XNATWidgetCollective();

private:
};

#endif //_XNATWidgetCollective_H
