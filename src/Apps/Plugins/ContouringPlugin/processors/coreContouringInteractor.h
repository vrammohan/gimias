/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef coreContouringInteractor_H
#define coreContouringInteractor_H

// MITK
#include "mitkDataTree.h"
#include "mitkContourInteractor.h"
#include "ManualContouringTool.h"
#include "mitkToolManager.h"

namespace Core {

/**
\brief Adapted version of class ContourInteractor in ManualSegmentationPlugin
\ingroup ContouringPlugin
\author Ernesto Coto
\date Nov 2013
*/
class ContouringInteractor : public Core::SmartPointerObject {

  // PUBLIC OPERATIONS
public:
  enum INTERACTOR_STATE { INTERACTOR_DISABLED, INTERACTOR_ENABLED };

  typedef Core::DataHolder< INTERACTOR_STATE > InteractorStateHolderType;

  //! type of inputs for the processor
  typedef enum { INVALID_TOOL = -1, MANUALCONTOURTOOL, NUMBER_OF_TOOLS } TOOLS_TYPE;

  //!
  coreDeclareSmartPointerClassMacro4Param(Core::ContouringInteractor,
                                          Core::SmartPointerObject,
                                          Core::RenderingTree::Pointer,
                                          Core::DataEntityHolder::Pointer,
                                          Core::DataEntityHolder::Pointer, TOOLS_TYPE);

  //! Connect the interactor to the selectedData tree node
  void ConnectToDataTreeNode();

  //! Disconnect the interactor from the rendering tree
  void DisconnectFromDataTreeNode();

  //!
  Core::RenderingTree::Pointer GetRenderingTree() const;

  //!
  void SetRenderingTree(Core::RenderingTree::Pointer val);

  //!
  Core::DataEntityHolder::Pointer GetSelectedContourHolder() const;

  //!
  void SetSelectedContourHolder(Core::DataEntityHolder::Pointer val);

  //!
  Core::DataEntityHolder::Pointer GetSelectedData() const;

  //!
  void SetSelectedData(Core::DataEntityHolder::Pointer val);

  //!
  bool IsConnectedToRenderingTree(Core::DataEntity::Pointer dataEntity);

  //!
  void SetToolType(TOOLS_TYPE val);

  //!
  TOOLS_TYPE GetToolType();

  //
  InteractorStateHolderType::Pointer GetInteractorStateHolder() const;

  //!
  mitk::Tool *GetContourTool();

  // PRIVATE OPERATIONS
private:
  //!
  ContouringInteractor(Core::RenderingTree::Pointer renderingTree,
                       Core::DataEntityHolder::Pointer selectedContour,
                       Core::DataEntityHolder::Pointer selectedData,
                       TOOLS_TYPE contourType);

  virtual ~ContouringInteractor();

  //! Connect m_pointSetNode to the m_renderingTree
  void ConnectNodeToTree();

  //! Disconnect m_pointSetNode to the m_renderingTree
  void DisconnectNodeFromTree();

  //! Connect the interactors to the global instance
  void ConnectInteractors();

  //! Disconnect all interactors to the global instance
  void DisconnectInteractors();

  //! Redefined
  void CreateInteractor();

  //! Redefined
  void DestroyInteractor();

  //! Redefined
  void OnInteractorConnected();

  //!
  Core::DataEntity::Pointer GetSelectedContourDataEntity();

  //!
  Core::DataEntity::Pointer GetSelectedDataEntity();

  //!
  mitk::DataTreeNode::Pointer GetSelectedContourNode();

  //!
  mitk::Image::Pointer GetSelectedContourRenderingData();

  //!
  mitk::DataTreeNode::Pointer GetSelectedDataEntityNode();

  //!
  void OnContourModified();

  //!
  int GetCurrentToolId();

  // ATTRIBUTES
private:
  //! Rendering data tree.
  Core::RenderingTree::Pointer m_renderingTree;

  //! Current selected point of m_selectedData
  Core::DataEntityHolder::Pointer m_selectedContourHolder;

  //! Surface mesh of the selected point
  Core::DataEntityHolder::Pointer m_selectedData;

  //!
  mitk::ToolManager::Pointer m_toolManager;

  //
  TOOLS_TYPE m_toolType;

  //! State of the interactor to update the views
  InteractorStateHolderType::Pointer m_InteractorStateHolder;
};

} // Core

#endif // coreContouringInteractor_H
