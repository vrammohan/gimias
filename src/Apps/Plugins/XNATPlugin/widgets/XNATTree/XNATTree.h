/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef XNATTree_H
#define XNATTree_H

#include <wx/wx.h>
#include <wx/treectrl.h>

#include "XNATPluginUtilities.h"

#include "XNATSubjectItemTreeData.h"
#include "XNATExperimentItemTreeData.h"
#include "XNATScanItemTreeData.h"
#include "XNATResourceItemTreeData.h"

/**
XNAT Tree with icons

\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class XNATTree : public wxTreeCtrl {
public:
  enum TreeItemType { SUBJECT, EXPERIMENT };

  XNATTree(wxWindow *parent, wxWindowID id = wxID_ANY,
           const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize,
           long style = wxTR_HAS_BUTTONS | wxTR_LINES_AT_ROOT | wxTR_MULTIPLE,
           const wxValidator &validator = wxDefaultValidator,
           const wxString &name = wxTreeCtrlNameStr);

  //! Fill the tree with the image scans
  void LoadScansIntoTree(int experimentId, XNATSubject subject, int subjectId,
                         std::string experimentLabel, const wxTreeItemId &experimentItem);

  //! Fill the tree with the experiments info
  void LoadExperimentsIntoTree(int subjectId, XNATSubject subject,
                               const wxTreeItemId &subjectItem);

  //! Fill the tree with the subjects info
  void LoadSubjectsIntoTree(std::vector< XNATSubject > subjects);

  //! Fill the tree with the resources info at subject level
  void LoadSubjectResourcesIntoTree(int subjectId, XNATSubject subject,
                                    const wxTreeItemId &subjectItem,
                                    std::string resourceCollection);

  //! Fill the tree with the resource item info at subject level
  void LoadSubjectResourceItemsIntoTree(int subjectId, XNATSubject subject,
                                        const wxTreeItemId &subjectItem,
                                        std::string resourceCollection);

private:
  //! sets the icons for each tree level
  void SetIcons();

  //! Returns an icon based on the GIMIAS resource path and \a filename
  wxIcon GetIcon(const std::string &filename);

  //! Find the child of the treeItemId using the index
  wxTreeItemId FindChildItemByIndex(wxTreeItemId treeItemId, unsigned int index);

private:
  //!
  std::string m_currentSubject;

  //!
  std::string m_currentExperiment;

  //!
  int m_folderNumber;

  DECLARE_EVENT_TABLE()
};

#endif // XNATTree_H
