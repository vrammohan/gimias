/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "TableView.h"

#include "blMitkUnicode.h"
#include "blTextUtils.h"

#include "coreWxMitkCoreMainWindow.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreDirectory.h"
#include "coreSettings.h"
#include "coreKernel.h"
#include "corePluginTab.h"
#include "coreMainMenu.h"
#include "coreToolbarIO.h"
#include "corePluginTab.h"
#include "corePluginTabFactory.h"
#include "coreProcessorManager.h"

#include <wx/event.h>
#include <wx/process.h>
#include <wx/dir.h>
#include <wx/zipstrm.h>
#include <wx/wfstream.h>

using namespace Core::Widgets;

TableView::TableView(wxWindow *parent, int id, const wxPoint &pos, const wxSize &size,
                     long style)
    : TableViewUI(parent, id, pos, size, style) {
  m_TagMap = NULL;

  UpdateWidget();
}

void TableView::UpdateWidget() {
  if (m_TagMap.IsNotNull()) {
    FillGrid();
  }
}

void TableView::SetTagMap(blTagMap::Pointer tagMag) {
  m_TagMap = tagMag;

  UpdateWidget();
}

void TableView::FillGrid() {
  if (m_TagMap.IsNotNull()) {
    blTagMap::TagMapType tagMap = m_TagMap->GetTagMap();
    blTagMap::TagMapType::iterator it;

    // Preprocessing
    std::vector< std::string > columns;

    for (it = tagMap->begin(); it != tagMap->end(); it++) {
      std::string key = it->first;
      blTag::Pointer tag = it->second;

      if (tag->GetValue().type() == typeid(blTagMap::Pointer)) {
        blTagMap::Pointer childTagMap;
        tag->GetValue< blTagMap::Pointer >(childTagMap);

        blTagMap::TagMapType::iterator itChild;
        for (itChild = childTagMap->begin(); itChild != childTagMap->end(); itChild++) {
          std::string keyChild = it->first;

          if (std::find(columns.begin(), columns.end(), key) == columns.end()) {
            columns.push_back(it->first);
          }
        }
      }
    }

    std::vector< std::vector< std::string > > valuesMatrix;

    for (it = tagMap->begin(); it != tagMap->end(); it++) {
      std::vector< std::string > valuesRow(columns.size());

      std::string key = it->first;
      blTag::Pointer tag = it->second;

      if (tag->GetValue().type() == typeid(blTagMap::Pointer)) {
        blTagMap::Pointer childTagMap;
        tag->GetValue< blTagMap::Pointer >(childTagMap);

        blTagMap::TagMapType::iterator itChild;
        for (itChild = childTagMap->begin(); itChild != childTagMap->end(); itChild++) {
          std::string keyChild = it->first;
          blTag::Pointer tagChild = it->second;

          std::vector< std::string >::iterator keyPosition;
          keyPosition = std::find(columns.begin(), columns.end(), keyChild);

          if (keyPosition != columns.end()) {
            int pos = (keyPosition - columns.begin());
            valuesRow[pos] = tagChild->GetValueAsString();
          }
        }
      }

      valuesMatrix.push_back(valuesRow);
    }

    // Print content

    cout << "Print Content:" << endl;

    for (int i = 0; i < columns.size(); i++) {
      cout << columns[i] << "\t";
    }
    cout << endl;

    for (int i = 0; i < valuesMatrix.size(); i++) {
      for (int j = 0; j < columns.size(); j++) {
        cout << valuesMatrix[i][j] << "\t";
      }
      cout << endl;
    }
    cout << endl;
  }
}
