#if defined(_MSC_VER)
#pragma warning(disable : 4786)
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif

#include <stdexcept>

#include "aheHost.h"
#include "aheApp.h"
#include "aheAppInst.h"
#include "aheExecute.h"
#include "itkPluginUtilities.h"
#include "blTextUtils.h"
#include "AheCLPCLP.h"
#include <boost/thread/thread.hpp>
#include "tinyxml.h"

/**
 *  Extracts an attribute from the element <app_instance> within the AHE input xml.
 */
std::string GetAppInstXMLAttribute(std::string xml, std::string attribute) {

  TiXmlDocument doc;
  doc.Parse(xml.c_str());
  if (doc.Error()) {
    throw std::runtime_error(doc.ErrorDesc());
  }
  TiXmlNode *node = doc.FirstChild()->FirstChild(); // get children of <ahe> tag
  while (node != NULL &&
         std::string(node->Value()) != "app_instance") // search for <app_instance> tag
  {
    node = node->NextSibling();
  }
  if (node != NULL && node->ToElement()->Attribute(attribute.c_str())) {
    return node->ToElement()->Attribute(attribute.c_str());
  }

  return "";
}

/**
 *  Main method.
 */
int main(int argc, char *argv[]) {
  PARSE_ARGS;

  try {
    aheHost::Pointer host = aheHost::New();
    aheApp::Pointer app = aheApp::New();
    aheAppInst::Pointer appInst = aheAppInst::New();

    // configure host
    host->SetHostname(hostName);
    host->SetPort(hostPort);
    host->SetUsername(hostUser);
    host->SetPassword(hostPassword);

    // configure app
    app->SetName(appName);
    app->SetResourceName(appResourceName);

    // configure appInst
    appInst->SetHost(host);
    appInst->SetApp(app);

    // parse parameters list (semicolon-separated) for creation of the app instance
    std::list< std::string > appParameters;
    blTextUtils::ParseLine(appInstParameterList, ';', appParameters);
    for (std::list< std::string >::iterator paramItr = appParameters.begin();
         paramItr != appParameters.end(); paramItr++) {
      std::list< std::string > paramPair;
      blTextUtils::ParseLine(*paramItr, '=', paramPair);
      std::string paramName = paramPair.front();
      // trim paramName
      int strBegin = paramName.find_first_not_of(" ");
      if (strBegin != std::string::npos) {
        int strEnd = paramName.find_last_not_of(" ");
        int strRange = strEnd - strBegin + 1;
        paramName = paramName.substr(strBegin, strRange);
      }
      // trim paramValue
      std::string paramValue = paramPair.back();
      strBegin = paramValue.find_first_not_of(" ");
      if (strBegin != std::string::npos) {
        int strEnd = paramValue.find_last_not_of(" ");
        int strRange = strEnd - strBegin + 1;
        paramValue = paramValue.substr(strBegin, strRange);
      }

      appInst->SetParameter(paramName, paramValue);
    }

    // initiate app instance and if this is successful, run it
    if ((appInst->Initiate()).first == "Initiated") {

      // configure instance execution
      aheExecute::Pointer exec = aheExecute::New();
      exec->SetExecAppInst(appInst);
      exec->SetParameter("resource_name", app->GetResourceName());

      // parse parameters list (semicolon-separated) for execution of the app instance
      appParameters.clear();
      blTextUtils::ParseLine(appInstExecParameterList, ';', appParameters);
      for (std::list< std::string >::iterator paramItr = appParameters.begin();
           paramItr != appParameters.end(); paramItr++) {
        std::list< std::string > paramPair;
        blTextUtils::ParseLine(*paramItr, '=', paramPair);
        std::string paramName = paramPair.front();
        // trim paramName
        int strBegin = paramName.find_first_not_of(" ");
        if (strBegin != std::string::npos) {
          int strEnd = paramName.find_last_not_of(" ");
          int strRange = strEnd - strBegin + 1;
          paramName = paramName.substr(strBegin, strRange);
        }
        // trim paramValue
        std::string paramValue = paramPair.back();
        strBegin = paramValue.find_first_not_of(" ");
        if (strBegin != std::string::npos) {
          int strEnd = paramValue.find_last_not_of(" ");
          int strRange = strEnd - strBegin + 1;
          paramValue = paramValue.substr(strBegin, strRange);
        }

        exec->SetParameter(paramName, paramValue);
      }

      // run the app instance
      if ((exec->Run()).first == "Running") {

        // if we need to wait until the instance is finished. Check the status
        // periodically
        // until it finished or triggers an error.
        if (waitUntilFinished) {
          std::string status;
          std::string appInstStatusXML;
          do {
            appInstStatusXML = exec->Status(); // get status xml response
            status = GetAppInstXMLAttribute(appInstStatusXML,
                                            "status"); // and extract the status attribute
            if (status != "" && status != "Finished" && status != "Error_Polling") {
              boost::this_thread::sleep(boost::posix_time::seconds(3)); // wait 3 seconds
            }
            // std::cout << appInstStatusXML;
          } while (status != "" && status != "Finished" && status != "Error_Polling");

          if (status == "Error_Polling" || status == "") {
            std::cerr << "AHE execution failed: " + appInstStatusXML << std::endl;
            return EXIT_FAILURE;
          }
        } else {
          std::string status = "AHE job submitted - instance ";
          status = status + appInst->GetInstanceId();
          std::cout << status;
        }
      }
    }
  } catch (itk::ExceptionObject &excep) {
    std::cerr << argv[0] << ": exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
