/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATToolbar.h"
#include "coreKernel.h"
#include "corePluginTab.h"
#include "coreSettings.h"
#include "coreDirectory.h"

//#include ".xpm"
#include "xnat24.xpm"

#include "XNATWorkingArea.h"

// xml
#include "tinyxml.h"

// Boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

#define wxID_OPEN_XNAT_WORKING_AREA wxID("wxID_OPEN_XNAT_WORKING_AREA")

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::XNATToolbar, Core::Widgets::ToolbarBase)
EVT_TOOL(wxID_OPEN_XNAT_WORKING_AREA, Core::Widgets::XNATToolbar::OpenXNATWorkingArea)
END_EVENT_TABLE()

Core::Widgets::XNATToolbar::XNATToolbar(wxWindow *parent, int id, const wxPoint &pos,
                                        const wxSize &size, long style,
                                        const wxString &name)
    : Core::Widgets::ToolbarBase(parent, id, pos, size, style, name) {
  wxBitmap bitmapXNATWorkingArea;

  bitmapXNATWorkingArea = wxBitmap(xnat24_xpm);

  AddTool(wxID_OPEN_XNAT_WORKING_AREA, _T("Open XNAT Working Area"),
          bitmapXNATWorkingArea, _T("Open XNAT Working Area"), wxITEM_NORMAL);

  Realize();
}

void Core::Widgets::XNATToolbar::OpenXNATWorkingArea(wxCommandEvent &event) {
  // GetPluginTab()->ShowWindow("XNAT Working Area");
  // OnSelectedTool(event);

  bool shown = GetPluginTab()->IsWindowShown("XNAT Working Area");

  if (!shown) {
    GetPluginTab()->ShowWindow("XNAT Working Area", true);
  }
}

Core::BaseProcessor::Pointer Core::Widgets::XNATToolbar::GetProcessor() { return NULL; }
