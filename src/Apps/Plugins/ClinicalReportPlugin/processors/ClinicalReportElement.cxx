/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportElement.h"

ClinicalReportElement::ClinicalReportElement() {}

bool ClinicalReportElement::operator<(ClinicalReportElement &second) {
  return this->GetPosition() < second.GetPosition();
}

bool ClinicalReportElementSorter(ClinicalReportElement first,
                                 ClinicalReportElement second) {
  return first.GetPosition() < second.GetPosition();
}

std::string ClinicalReportElement::GetComments() { return m_Comments; }

void ClinicalReportElement::SetComments(std::string comments) { m_Comments = comments; }

std::string ClinicalReportElement::GetTitle() { return m_Title; }

void ClinicalReportElement::SetTitle(std::string title) { m_Title = title; }

std::string ClinicalReportElement::GetThumbnailSmall() { return m_ThumbnailSmall; }

void ClinicalReportElement::SetThumbnailSmall(std::string thumbnailPath) {
  m_ThumbnailSmall = thumbnailPath;
}

std::string ClinicalReportElement::GetThumbnailMedium() { return m_ThumbnailMedium; }

void ClinicalReportElement::SetThumbnailMedium(std::string thumbnailPath) {
  m_ThumbnailMedium = thumbnailPath;
}

std::string ClinicalReportElement::GetScreenshot() { return m_Screenshot; }

void ClinicalReportElement::SetScreenshot(std::string screenshotPath) {
  m_Screenshot = screenshotPath;
}

std::string ClinicalReportElement::GetType() { return m_Type; }

void ClinicalReportElement::SetType(std::string type) { m_Type = type; }

int ClinicalReportElement::GetPosition() { return m_Position; }

void ClinicalReportElement::SetPosition(int position) { m_Position = position; }

bool ClinicalReportElement::GetSelected() { return m_Selected; }

void ClinicalReportElement::SetSelected(bool selected) { m_Selected = selected; }

std::string ClinicalReportElement::GetTable() { return m_Table; }

void ClinicalReportElement::SetTable(std::string table) { m_Table = table; }