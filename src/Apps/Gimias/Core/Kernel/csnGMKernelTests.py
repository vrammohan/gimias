# Used to configure coreIO
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmKernel = api.RewrapProject(gmKernel)
gmKernel.AddTests(["Tests/*.*"], cxxTest)

