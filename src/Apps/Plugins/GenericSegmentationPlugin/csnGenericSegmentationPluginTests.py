# Used to configure GenericSegmentationPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

genericSegmentationPlugin = api.RewrapProject(genericSegmentationPlugin())
genericSegmentationPlugin.AddTests(["tests/Applications/*.h", "tests/src/*.cxx"], cxxTest, enableWxWidgets = True, dependencies = [gimiasHeader])
genericSegmentationPlugin.testProject.AddIncludeFolders(["tests/include"])
genericSegmentationPlugin.testProject.AddSources(["tests/include/*.h"])
genericSegmentationPlugin.testProject.SetPrecompiledHeader("GenericSegmentationPluginPCH.h")
