/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvViewManager.h"
#include "coreDataContainer.h"

msvViewManager::msvViewManager() {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string tempViewsDir = settings->GetProjectHomePath() + "/Temp";
  itksys::SystemTools::MakeDirectory(tempViewsDir.c_str());

  m_SelectedViewHolder = Core::DataHolder< blTagMap::Pointer >::New();
}

msvViewManager::~msvViewManager() {}

void msvViewManager::SetDataEntityHolder(
    Core::DataEntityHolder::Pointer dataEntityHolder) {
  m_DataEntityHolder = dataEntityHolder;
}

blTagMap::Pointer msvViewManager::GetViews() {
  if (m_DataEntityHolder->GetSubject().IsNull())
    return NULL;

  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();

  Core::DataEntity::Pointer session;
  session = list->FindRelative(m_DataEntityHolder->GetSubject(), Core::SessionTypeId);
  if (session.IsNull())
    return NULL;

  blTagMap::Pointer sessionTagMap;
  session->GetProcessingData(sessionTagMap);
  if (sessionTagMap.IsNull())
    return NULL;

  // Get views tags
  blTagMap::Pointer views;
  views = sessionTagMap->GetTagValue< blTagMap::Pointer >("Views");
  if (views.IsNull()) {
    views = blTagMap::New();
    sessionTagMap->AddTag("Views", views);
  }

  return views;
}

wxImage msvViewManager::GetImage(blTagMap::Pointer view) {
  if (view.IsNull())
    return wxNullImage;

  wxImage image = wxNullImage;
  if (view->GetTag("Image").IsNotNull())
    image = view->GetTag("Image")->GetValueCasted< wxImage >();
  return image;
}

std::string msvViewManager::GetName(blTagMap::Pointer view) {
  if (view.IsNull())
    return "";

  std::string name;
  if (view->GetTag("Name").IsNotNull())
    name = view->GetTag("Name")->GetValueAsString();
  return name;
}
std::string msvViewManager::SaveTempPreview(blTagMap::Pointer view) {
  // Create preview
  Core::DataEntityPreview::Pointer preview;
  Core::BaseFactory::Pointer factory =
      Core::FactoryManager::Find(Core::DataEntityPreview::GetNameClass());
  if (factory.IsNotNull())
    preview =
        dynamic_cast< Core::DataEntityPreview * >(factory->CreateInstance().GetPointer());
  if (preview.IsNull())
    return "";

  // Delete old preview
  m_PreviewMap[GetName(view)] = NULL;

  // Configure parameteres
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  preview->SetName("View" + GetName(view));
  preview->SetTempFolder(settings->GetProjectHomePath() + "/Temp");
  preview->SetImage(GetImage(view));
  preview->SaveTempFile();

  m_PreviewMap[GetName(view)] = preview;

  return preview->GetTmpFileName();
}

void msvViewManager::SetSelectedView(blTagMap::Pointer view) {
  m_SelectedViewHolder->SetSubject(view);
}

blTagMap::Pointer msvViewManager::GetSelectedView() {
  return m_SelectedViewHolder->GetSubject();
}

blTagMap::Pointer msvViewManager::GetView(const std::string &viewName) {
  blTagMap::Pointer views = GetViews();
  if (views.IsNull())
    return NULL;

  blTagMap::Iterator itView = views->GetIteratorBegin();
  while (itView != views->GetIteratorEnd()) {
    blTagMap::Pointer view = itView->second->GetValueCasted< blTagMap::Pointer >();

    std::string name;
    if (view->GetTag("Name").IsNotNull()) {
      name = view->GetTag("Name")->GetValueAsString();
    }
    if (name == viewName) {
      return view;
    }
    itView++;
  }

  return NULL;
}

void msvViewManager::RemoveView(blTagMap::Pointer viewToRemove) {
  blTagMap::Pointer views = GetViews();
  if (views.IsNull())
    return;

  // Find tag
  blTagMap::Iterator itView = views->GetIteratorBegin();
  while (itView != views->GetIteratorEnd()) {
    blTagMap::Pointer view = itView->second->GetValueCasted< blTagMap::Pointer >();

    if (view == viewToRemove) {
      views->RemoveTag(itView->second);
      break;
    }
    itView++;
  }

  if (GetSelectedView() = viewToRemove) {
    SetSelectedView(NULL);
  }
}

Core::DataHolder< blTagMap::Pointer >::Pointer msvViewManager::GetSelectedViewHolder() {
  return m_SelectedViewHolder;
}

blTagMap::Pointer msvViewManager::GetProps(blTagMap::Pointer view,
                                           const std::string &multiRenderWindowName,
                                           const std::string &renderWindowName,
                                           const std::string &name) {
  // Container of views
  blTagMap::Pointer multiRenderWindow =
      view->GetTagValue< blTagMap::Pointer >(multiRenderWindowName);
  if (multiRenderWindow.IsNull()) {
    multiRenderWindow = blTagMap::New();
    view->AddTag(multiRenderWindowName, multiRenderWindow);
  }

  // Current render window
  blTagMap::Pointer renderWindow;
  if (!renderWindowName.empty()) {
    renderWindow = multiRenderWindow->GetTagValue< blTagMap::Pointer >(renderWindowName);
    if (renderWindow.IsNull()) {
      renderWindow = blTagMap::New();
      multiRenderWindow->AddTag(renderWindowName, renderWindow);
    }
  } else {
    renderWindow = multiRenderWindow;
  }

  // Camera
  blTagMap::Pointer cameraParams = renderWindow->GetTagValue< blTagMap::Pointer >(name);
  if (cameraParams.IsNull()) {
    cameraParams = blTagMap::New();
    renderWindow->AddTag(name, cameraParams);
  }

  return cameraParams;
}

blTagMap::Pointer msvViewManager::AddView() {
  // Get views tags
  blTagMap::Pointer views = GetViews();

  // Create default view
  std::stringstream stream;
  stream << "View" << views->GetLength();
  blTagMap::Pointer newView = blTagMap::New();
  newView->AddTag("Name", stream.str());
  newView->AddTag("Visible", true);
  newView->AddTag("SaveRenderingTree", true);
  newView->AddTag("SaveCamera", true);
  views->AddTag(stream.str(), newView);

  return newView;
}

std::string msvViewManager::GetWorkingAreaName(blTagMap::Pointer view) {
  if (view.IsNull())
    return "";

  blTag::Pointer workingAreaTag = view->GetTag("WorkingArea");
  if (workingAreaTag.IsNull())
    return "";

  return workingAreaTag->GetValueAsString();
}
