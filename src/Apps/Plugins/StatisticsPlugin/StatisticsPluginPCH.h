/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _StatisticsPluginPCH_H
#define _StatisticsPluginPCH_H

// baselib
#include "blMITKUtils.h"

// bridgelib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

// core
#include "coreAssert.h"
#include "coreBaseProcessor.h"
#include "coreBaseWindow.h"
#include "coreCommonDataTypes.h"
#include "coreDataContainer.h"
#include "coreDataEntity.h"
#include "coreDataEntityImplFactory.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHolder.h"
#include "coreDataEntityList.h"
#include "coreDataEntityList.txx"
#include "coreDataEntityListBrowser.h"
#include "coreDataHolder.h"
#include "coreDataTreeHelper.h"
#include "coreEnvironment.h"
#include "coreException.h"
#include "coreFrontEndPlugin.h"
#include "coreKernel.h"
#include "coreLogger.h"
#include "coreObject.h"
#include "corePluginMacros.h"
#include "corePluginTab.h"
#include "corePluginTabFactory.h"
#include "coreProcessorInputWidget.h"
#include "coreProfile.h"
#include "coreRenderingTree.h"
#include "coreReportExceptionMacros.h"
#include "coreSimpleProcessingWidget.h"
#include "coreSmartPointerMacros.h"
#include "coreUserHelperWidget.h"
#include "coreVTKImageDataHolder.h"
#include "coreVTKImageDataRenDataBuilder.h"
#include "coreVTKPolyDataHolder.h"
#include "coreWidgetCollective.h"
#include "coreWxMitkGraphicalInterface.h"

// gm
//#include "gmInteractorsWin32Header.h"
//#include "gmProcessorsWin32Header.h"

// stl
#include <algorithm>
#include <cmath>
#include <limits>
#include <map>
#include <sstream>
#include <vector>

// wx

#include "wxID.h"
#include <wx/image.h>
#include <wx/spinctrl.h>
#include <wx/tglbtn.h>
#include <wx/wx.h>
#include <wx/wxprec.h>

#endif //_StatisticsPluginPCH_H
