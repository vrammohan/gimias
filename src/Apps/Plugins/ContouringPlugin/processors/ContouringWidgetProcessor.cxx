/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "ContouringWidgetProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataHolder.h"

#include "vtkSmartPointer.h"

namespace contouringPlugin {

ContouringWidgetProcessor::ContouringWidgetProcessor() {
  SetName("ContouringWidgetProcessor");

  BaseProcessor::SetNumberOfInputs(NUMBER_OF_INPUTS);
  GetInputPort(REFERENCE_IMAGE)->SetName("Reference Image");
  GetInputPort(REFERENCE_IMAGE)->SetDataEntityType(Core::ImageTypeId);

  BaseProcessor::SetNumberOfOutputs(OUTPUTS_NUMBER);

  GetOutputPort(CONTOUR)->SetName("Contour");
  GetOutputPort(CONTOUR)
      ->SetDataEntityType(Core::MeasurementTypeId | Core::PointSetTypeId);
}

ContouringWidgetProcessor::~ContouringWidgetProcessor() {}

void ContouringWidgetProcessor::Update() {

  /*** Automatically generated code ***

  ImageType::Pointer itkInputImage;
  GetProcessingData(INPUT_0), itkInputImage);

  Core::vtkPolyDataPtr vtkInput;
  GetProcessingData(INPUT_1, vtkInput );

  Core::vtkImageDataPtr vtkInputImage;
  GetProcessingData(INPUT_0, vtkInputImage );

  // here goes the filter or the functions that determine the processor
  // the output should go in the update functions

  // Set the output to the output of this processor
  UpdateOutput( 0, itkInputImage, "ContouringWidgetProcessorImage");
  UpdateOutput( 1, vtkInput, "ContouringWidgetProcessorSurface");
  UpdateOutput( 2, vtkInputImage, "ContouringWidgetProcessorImageVTK");*/

  /*
  // Get the first image
  ImageType::Pointer itkInputImage;
  GetProcessingData(REFERENCE_IMAGE, itkInputImage);

  //Core::vtkPolyDataPtr vtkInput;
  //GetProcessingData(INPUT_CONTOUR, vtkInput );

  Core::vtkImageDataPtr vtkInputImage;
  GetProcessingData(REFERENCE_IMAGE, vtkInputImage );

  // here goes the filter or the functions that determine the processor
  // the output should go in the update functions

  // Set the output to the output of this processor
  mitk::Contour::Pointer outputContour= mitk::Contour::New( );
  outputContour->Initialize( );
  GetOutputPort( 0 )->UpdateOutput(outputContour,0, NULL);*/
}

} // namespace contouringPlugin
