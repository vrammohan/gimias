/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _NewClinicalReportImageDialog_H
#define _NewClinicalReportImageDialog_H

#include "NewClinicalReportImageDialogUI.h"
#include "coreBaseWindow.h"
#include <string>

#include "ClinicalReportProcessor.h"

// xml
#include "tinyxml.h"

namespace Core {
namespace Widgets {

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 25 Oct 2010
*/

#define wxID_NewClinicalReportImageDialog wxID("wxID_NewClinicalReportImageDialog")

  class NewClinicalReportImageDialog : public NewClinicalReportImageDialogUI,
                                       public Core::BaseWindow {
    // OPERATIONS
  public:
    //!
    NewClinicalReportImageDialog();

    //!
    ~NewClinicalReportImageDialog();

    //!
    virtual Core::BaseProcessor::Pointer GetProcessor();

    //!
    void AddImage(std::string thumbnailPath);

    //!
    std::vector< std::string > GetComments();

    //!
    std::vector< std::string > GetTitles();

    //!
    std::vector< bool > GetSelected();

    //!
    bool GetMergeAllInOne();

    //!
    void LoadReports();

    //!
    void OnAddReport(wxCommandEvent &event); // wxGlade: <event_handler>

    //!
    void OnEditReport(wxCommandEvent &event); // wxGlade: <event_handler>

    //!
    void SetProcessor(ClinicalReportProcessor::Pointer processor);

    // ATTRIBUTES
  public:
    std::string m_ReportName;
    std::string m_ReportAuthors;
    std::string m_ReportPath;

  private:
    DECLARE_EVENT_TABLE()

    //!
    void OnChangeReport(wxCommandEvent &event);

    //!
    void LoadReportInfo(std::string reportName);

    // ATTRIBUTES
  private:
    std::vector< std::string > m_CommentsVector;
    std::vector< std::string > m_TitlesVector;
    std::vector< std::string > m_ThumbnailVector; // paths

    std::vector< wxCheckBox * > m_CheckBoxVector;
    std::vector< wxTextCtrl * > m_TitlesTxtVector;
    std::vector< wxTextCtrl * > m_CommentsTxtVector;
    std::vector< wxStaticBitmap * > m_BitmapVector;

    std::string m_BaseDir;

    //! processor
    ClinicalReportProcessor::Pointer m_Processor;
  };

} // namespace Widgets
} // namespace Core

#endif //_NewClinicalReportImageDialog_H
