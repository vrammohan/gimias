/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "StatisticsToolbar.h"
#include "coreKernel.h"
#include "corePluginTab.h"
#include "coreSettings.h"
#include "coreDirectory.h"

//#include ".xpm"
#include "Statistics24.xpm"

#include "StatisticsWorkingArea.h"

// xml
#include "tinyxml.h"

// Boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

#define wxID_OPEN_Statistics_WORKING_AREA wxID("wxID_OPEN_Statistics_WORKING_AREA")

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::StatisticsToolbar, wxToolBar)
EVT_TOOL(wxID_OPEN_Statistics_WORKING_AREA,
         Core::Widgets::StatisticsToolbar::OpenStatisticsWorkingArea)
END_EVENT_TABLE()

Core::Widgets::StatisticsToolbar::StatisticsToolbar(wxWindow *parent, int id,
                                                    const wxPoint &pos,
                                                    const wxSize &size, long style,
                                                    const wxString &name)
    : wxToolBar(parent, id, pos, size, style, name) {
  wxBitmap bitmapStatisticsWorkingArea;

  bitmapStatisticsWorkingArea = wxBitmap(statistics24_xpm);

  AddTool(wxID_OPEN_Statistics_WORKING_AREA, _T("Open Statistics Working Area"),
          bitmapStatisticsWorkingArea, wxNullBitmap, wxITEM_NORMAL,
          _T("Open Statistics Working Area"), _T("Open Statistics Working Area"));

  Realize();
}

void Core::Widgets::StatisticsToolbar::OpenStatisticsWorkingArea(wxCommandEvent &event) {
  std::cout << "OpenStatisticsWorkingArea" << std::endl;

  GetPluginTab()->ShowWindow("Statistics Working Area");
}

Core::BaseProcessor::Pointer Core::Widgets::StatisticsToolbar::GetProcessor() {
  return NULL;
}
