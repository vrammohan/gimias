/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SecurityPlugin_H
#define _SecurityPlugin_H

#include "SecurityPluginProcessorCollective.h"
#include "SecurityPluginWidgetCollective.h"

// CoreLib
#include "coreFrontEndPlugin.h"

namespace SecurityPlugin {

/**
\brief Creates all objects of the plug-in and connect them.

\note Nobody can get access to this class. This class is only for
initialization of all components.

\note Try to make all processors reusable for other plug-ins. Be aware
of creating a dependency between the processor and any class of the rest
of the plug-in.

\ingroup SecurityPlugin
\author H�ctor Fernandez
\date Jan 2012
*/
class PLUGIN_EXPORT SecurityPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
  // TYPE DEFINITIONS
public:
  coreDeclareSmartPointerClassMacro(SecurityPlugin, Core::FrontEndPlugin::FrontEndPlugin);

  // OPERATIONS
protected:
  //!
  SecurityPlugin(void);

  //!
  virtual ~SecurityPlugin(void);

private:
  //! Purposely not implemented
  SecurityPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  ProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  WidgetCollective::Pointer m_Widgets;
};

} // namespace SecurityPlugin

#endif // SecurityPlugin_H
