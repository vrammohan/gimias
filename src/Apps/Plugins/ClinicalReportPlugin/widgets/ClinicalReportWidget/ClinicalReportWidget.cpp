/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportWidget.h"
#include "ClinicalReportDocument.h"
#include "ShowTableDialog.h"
#include "ClinicalReportPluginUtilities.h"

// bridgelib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"

// core
#include "coreReportExceptionMacros.h"
#include "coreBaseExceptions.h"
#include "corePluginTab.h"
#include "coreProcessorInputWidget.h"
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreDirectory.h"
#include "coreDataEntityReader.h"
#include "coreProcessorManager.h"

// std
#include <limits>
#include <cmath>
#include <algorithm>
#include <sstream>
#include <time.h>

// ms?
#include "wx/event.h"

// wxpdf
#include "wx/pdfdoc.h"
#include "wx/pdffontmanager.h"
#include <wx/wupdlock.h>

// boost
#include "boost/filesystem.hpp"

#include "itksys/SystemTools.hxx"

#include "adobe_pdf_icon.xpm"
#include "Add-icon16.xpm"
#include "Remove-icon16.xpm"
#include "Delete-icon16.xpm"
#include "Stock-Index-Up-icon.xpm"
#include "Stock-Index-Down-icon.xpm"

using namespace boost::filesystem;

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(ClinicalReportWidget, ClinicalReportWidgetUI)
EVT_COMBOBOX(wxID_COMBO_REPORTS, ClinicalReportWidget::OnChangeReport)
END_EVENT_TABLE();

RunCommand::RunCommand(std::string command) { m_Command = command; }

void RunCommand::operator()() {
  // run the command
  system(m_Command.c_str());
}

ClinicalReportWidget::ClinicalReportWidget()
    : ClinicalReportWidgetUI(NULL, wxID_ANY, "", wxPoint(380, 150)) {
  try {
    // load icons from the resource path
    m_CreateReportBtn->SetBitmapLabel(wxBitmap(adobe_pdf_icon_xpm));
    m_DeleteBtn->SetBitmapLabel(wxBitmap(Remove_icon16_xpm));
    m_AddBtn->SetBitmapLabel(wxBitmap(Add_icon16_xpm));

    // initializations
    m_ReportAddedAndNotSaved = "";
    m_SelectedReportBeforeAddReport = "";
    m_Logo = "";
  }
  coreCatchExceptionsReportAndNoThrowMacro("ClinicalReportWidget::ClinicalReportWidget");
}

ClinicalReportWidget::~ClinicalReportWidget() {}

void ClinicalReportWidget::OnInit() {
  LoadReports();

  // select the last selected
  std::string lastSelected = ClinicalReportPluginUtilities::GetLastSelectedFromConfig();
  LoadReportInfo(lastSelected);

  int position = 0;

  for (unsigned int i = 0; i < m_ComboReports->GetCount(); i++) {
    if (m_ComboReports->GetString(i) == lastSelected) {
      position = i;
      break;
    }
  }

  m_ComboReports->Select(position);
}

Core::BaseProcessor::Pointer ClinicalReportWidget::GetProcessor() {
  return Core::BaseProcessor::Pointer(m_Processor);
}

bool ClinicalReportWidget::Enable(bool enable) {
  bool bReturn = ClinicalReportWidgetUI::Enable(enable);

  if (enable) {
  } else {
  }
  return bReturn;
}

void ClinicalReportWidget::OnClose(wxCommandEvent &event) { Save(); }

void ClinicalReportWidget::OnSave(wxCommandEvent &event) {
  Save();
  Close();
}

bool ClinicalReportWidget::CheckFields() {
  if (m_PatientId == "")
    return false;
  if (m_PatientName == "")
    return false;
  if (!m_PatientSexFemale->GetValue() && !m_PatientSexMale->GetValue())
    return false;

  return true;
}

void ClinicalReportWidget::CreatePDFReport() {
  Save();

  // gather the document parameters
  ClinicalReportPatientInformation patientInfo;
  patientInfo.SetId(m_PatientIdTxt->GetValue());
  patientInfo.SetName(m_PatientNameTxt->GetValue());

  // compute patient's age
  int age;
  wxDateTime now = wxDateTime::GetTimeNow();
  wxTimeSpan diff = now.Subtract(m_BirthDatePicker->GetValue());
  age =
      (int)((double)diff.GetHours() / (double)(8765.81277)); // 1 year = 8 765.81277 hours

  patientInfo.SetAge(age);
  patientInfo.SetSex(m_PatientSexFemale->GetValue());
  patientInfo.SetAdquisitionDate(m_AdquisitionDatePicker->GetValue());
  patientInfo.SetBirthDate(m_BirthDatePicker->GetValue());

  // pass all the parameters to the processor
  m_Processor->SetParameters(patientInfo, m_ClinicalReportElements,
                             m_ReportTitleTxt->GetValue(), m_ReportAuthorsTxt->GetValue(),
                             m_Comments, m_ReportsFolder, m_ComboReports->GetValue(),
                             m_Logo);

  // the processor will generate the pdf file
  GetProcessor()->SetMultithreading(false);
  Core::Runtime::Kernel::GetProcessorManager()->Execute(GetProcessor(), "Default");

  // open pdf file in a separate thread
  std::string pdfFile = m_Processor->GetReportFilename();
#ifdef __WXMAC__
  std::string command = std::string("open ") + pdfFile;
#else  //__WXMAC__
  std::string command = std::string("\"") + pdfFile + std::string("\"");
#endif //__WXMAC__
  RunCommand runCommand(command);
  boost::thread commandThread(runCommand);
}

void ClinicalReportWidget::OnCreateReportBtn(wxCommandEvent &event) { CreatePDFReport(); }

void ClinicalReportWidget::AddReportElement(ClinicalReportElement clinicalReportElement) {
  m_ClinicalReportElements.push_back(clinicalReportElement);
  UpdateWidget();
}

void ClinicalReportWidget::ClearReportElements() {
  m_ClinicalReportElements.clear();
  m_TextControls.clear();
  m_TextTitleControls.clear();
  m_SpinCtrls.clear();
  m_StaticBitMaps.clear();
  m_Buttons.clear();
  m_ButtonsUp.clear();
  m_ButtonsDown.clear();

  UpdateWidget();
}

void ClinicalReportWidget::UpdateWidget() {
  wxWindowUpdateLocker noUpdates(this);

  // update report elements panel
  wxBoxSizer *globalsizer = new wxBoxSizer(wxVERTICAL);
  m_PanelReportElements->GetSizer()->Clear(true);
  m_PanelReportElements->SetSizer(globalsizer, true);

  // cleanup
  m_TextControls.clear();
  m_TextTitleControls.clear();
  m_SpinCtrls.clear();
  m_StaticBitMaps.clear();
  m_Buttons.clear();
  m_ButtonsUp.clear();
  m_ButtonsDown.clear();

  // sort the clinical report elements in the same order they will appear in the pdf
  std::sort(m_ClinicalReportElements.begin(), m_ClinicalReportElements.end(),
            ClinicalReportElementSorter);

  for (int i = 0; i < m_ClinicalReportElements.size(); i++) {
    wxBoxSizer *elementsizer = new wxBoxSizer(wxHORIZONTAL);

    int idTextCtrl = REPORT_ELEMENT_BASE_ID + i * 9 + 1;
    int idBitmap = REPORT_ELEMENT_BASE_ID + i * 9 + 2;
    int idDeleteBtn = REPORT_ELEMENT_BASE_ID + i * 9 + 4;
    int idTableButton = REPORT_ELEMENT_BASE_ID + i * 9 + 5;
    int idTextTitleCtrl = REPORT_ELEMENT_BASE_ID + i * 9 + 6;
    int idBtnUp = REPORT_ELEMENT_BASE_ID + i * 9 + 7;
    int idBtnDown = REPORT_ELEMENT_BASE_ID + i * 9 + 8;

    bool addThumbnail = m_ClinicalReportElements[i].GetThumbnailSmall() != "";
    bool addButton = !addThumbnail && m_ClinicalReportElements[i].GetType() == "table";

    wxTextCtrl *textctrl =
        new wxTextCtrl(m_PanelReportElements, idTextCtrl, wxEmptyString,
                       wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
    wxTextCtrl *textTitleCtrl =
        new wxTextCtrl(m_PanelReportElements, idTextTitleCtrl, wxEmptyString,
                       wxDefaultPosition, wxDefaultSize);
    wxStaticBitmap *bitmap = NULL;
    wxButton *button = NULL;

    if (addThumbnail) {
      bitmap = new wxStaticBitmap(
          m_PanelReportElements, idBitmap,
          wxBitmap(m_ClinicalReportElements[i].GetThumbnailSmall(), wxBITMAP_TYPE_ANY));
    }
    if (addButton) {
      button = new wxButton(m_PanelReportElements, idTableButton, "Show table");
    }

    wxBitmapButton *deleteBtn = new wxBitmapButton(m_PanelReportElements, idDeleteBtn,
                                                   wxBitmap(Delete_icon16_xpm));

    deleteBtn->SetToolTip("Delete item");
    textctrl->SetValue(m_ClinicalReportElements[i].GetComments());
    textTitleCtrl->SetValue(m_ClinicalReportElements[i].GetTitle());
    textctrl->Enable(m_ClinicalReportElements[i].GetSelected());
    textTitleCtrl->Enable(m_ClinicalReportElements[i].GetSelected());

    wxBitmapButton *buttonUp = new wxBitmapButton(m_PanelReportElements, idBtnUp,
                                                  wxBitmap(Stock_Index_Up_icon_xpm));
    wxBitmapButton *buttonDown = new wxBitmapButton(m_PanelReportElements, idBtnDown,
                                                    wxBitmap(Stock_Index_Down_icon_xpm));
    wxBoxSizer *sizerArrows = new wxBoxSizer(wxHORIZONTAL);

    sizerArrows->Add(buttonUp, 0, 0, 0);
    sizerArrows->Add(buttonDown, 0, 0, 0);
    elementsizer->Add(sizerArrows, 0, wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL,
                      0);

    if (addThumbnail) {
      elementsizer->Add(10, 20, 0, 0, 0);
      elementsizer->Add(textTitleCtrl, 1,
                        wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 0);
      elementsizer->Add(32, 20, 0, 0, 0);
      elementsizer->Add(bitmap, 0, wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL,
                        0);
    } else {
      if (addButton) {
        elementsizer->Add(10, 20, 0, 0, 0);
        elementsizer->Add(textTitleCtrl, 1,
                          wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 0);
        elementsizer->Add(20, 20, 0, 0, 0);
        elementsizer->Add(button, 0, wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL,
                          0);
      } else {
        elementsizer->Add(textTitleCtrl, 1,
                          wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 0);
        textTitleCtrl->Hide();
      }
    }
    elementsizer->Add(10, 20, 0, 0, 0);
    elementsizer->Add(textctrl, 1, wxEXPAND, 0);
    elementsizer->Add(20, 20, 0, 0, 0);
    elementsizer->Add(deleteBtn, 0, wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL,
                      0);
    elementsizer->Add(20, 20, 0, 0, 0);

    // save newly created widgets (to access them in the events)
    m_TextControls.push_back(textctrl);
    m_TextTitleControls.push_back(textTitleCtrl);
    m_StaticBitMaps.push_back(bitmap);
    m_Buttons.push_back(button);
    m_ButtonsUp.push_back(buttonUp);
    m_ButtonsDown.push_back(buttonDown);

    globalsizer->Add(elementsizer, 0, wxEXPAND, 0);
    globalsizer->Add(5, 5, 0, 0, 0);
  }

  // connect widgets to observers in this class
  for (int i = 0; i < m_ClinicalReportElements.size(); i++) {
    int idTextCtrl = REPORT_ELEMENT_BASE_ID + i * 9 + 1;
    int idBitmap = REPORT_ELEMENT_BASE_ID + i * 9 + 2;
    int idDeleteBtn = REPORT_ELEMENT_BASE_ID + i * 9 + 4;
    int idTableButton = REPORT_ELEMENT_BASE_ID + i * 9 + 5;
    int idTextTitleCtrl = REPORT_ELEMENT_BASE_ID + i * 9 + 6;
    int idBtnUp = REPORT_ELEMENT_BASE_ID + i * 9 + 7;
    int idBtnDown = REPORT_ELEMENT_BASE_ID + i * 9 + 8;

    Connect(idBtnUp, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(ClinicalReportWidget::OnBtnUp));

    Connect(idBtnDown, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(ClinicalReportWidget::OnBtnDown));

    Connect(idDeleteBtn, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(ClinicalReportWidget::OnDeleteBtn));

    Connect(idTextCtrl, wxEVT_COMMAND_TEXT_UPDATED,
            wxCommandEventHandler(ClinicalReportWidget::OnElementTextUpdated));

    Connect(idTextTitleCtrl, wxEVT_COMMAND_TEXT_UPDATED,
            wxCommandEventHandler(ClinicalReportWidget::OnElementTextTitleUpdated));

    Connect(idTableButton, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(ClinicalReportWidget::OnShowTableBtn));
  }

#ifndef __WXMAC__
  // Cast resize events to avoid sizer problems
  wxSizeEvent resEventGlobal(this->GetSize(), this->GetId());
  resEventGlobal.SetEventObject(this);
  this->GetEventHandler()->ProcessEvent(resEventGlobal);

  wxSizeEvent resEvent(m_PanelReportElements->GetSize(), m_PanelReportElements->GetId());
  resEvent.SetEventObject(m_PanelReportElements);
  m_PanelReportElements->GetEventHandler()->ProcessEvent(resEvent);

#else  //__WXMAC__
  m_PanelReportElements->GetSizer()->Fit(m_PanelReportElements);
#endif //__WXMAC__
}

void ClinicalReportWidget::OnShowTableBtn(wxCommandEvent &event) {
  int pos;
  pos = (event.GetId() - REPORT_ELEMENT_BASE_ID) / 9;
  if (pos < 0 || pos >= m_ClinicalReportElements.size()) {
    return;
  }

  std::vector< std::string > header;
  std::vector< std::vector< std::string > > lines;
  std::pair< std::vector< std::string >, std::vector< std::vector< std::string > > > res;

  res = ClinicalReportPluginUtilities::GetTableFormat(
      m_ClinicalReportElements[pos].GetTable());

  header = res.first;
  lines = res.second;

  ShowTableDialog *showTable = new ShowTableDialog();
  showTable->SetTable(header, lines);
  showTable->ShowModal();
}

void ClinicalReportWidget::OnDeleteBtn(wxCommandEvent &event) {
  int pos;
  pos = (event.GetId() - REPORT_ELEMENT_BASE_ID) / 9;
  if (pos < 0 || pos >= m_ClinicalReportElements.size()) {
    return;
  }

  int DocumentPosition = m_ClinicalReportElements[pos].GetPosition();
  for (int i = 0; i < m_ClinicalReportElements.size(); i++) {
    if (i == pos)
      continue;

    int _docPos = m_ClinicalReportElements[i].GetPosition();

    if (_docPos > DocumentPosition) {
      m_ClinicalReportElements[i].SetPosition(_docPos - 1);
    }
  }

  if (m_ClinicalReportElements[pos].GetType() == "image") {
    m_ImagesToBeDeleted.push_back(m_ClinicalReportElements[pos].GetScreenshot());
    m_ImagesToBeDeleted.push_back(m_ClinicalReportElements[pos].GetThumbnailMedium());
    m_ImagesToBeDeleted.push_back(m_ClinicalReportElements[pos].GetThumbnailSmall());
  }

  m_ClinicalReportElements.erase(m_ClinicalReportElements.begin() + pos);

  UpdateWidget();
}

void ClinicalReportWidget::OnRemoveAll(wxCommandEvent &event) {
  while (m_ClinicalReportElements.size() > 0) {
    if (m_ClinicalReportElements[0].GetType() == "image") {
      m_ImagesToBeDeleted.push_back(m_ClinicalReportElements[0].GetScreenshot());
      m_ImagesToBeDeleted.push_back(m_ClinicalReportElements[0].GetThumbnailMedium());
      m_ImagesToBeDeleted.push_back(m_ClinicalReportElements[0].GetThumbnailSmall());
    }

    std::cout << "Removing... " << m_ClinicalReportElements[0].GetTitle() << std::endl;

    m_ClinicalReportElements.erase(m_ClinicalReportElements.begin());
  }

  UpdateWidget();
}

void ClinicalReportWidget::OnElementTextUpdated(wxCommandEvent &event) {
  int pos;
  pos = (event.GetId() - REPORT_ELEMENT_BASE_ID) / 9;
  if (pos < 0 || pos >= m_ClinicalReportElements.size() || pos >= m_TextControls.size()) {
    return;
  }

  m_ClinicalReportElements[pos].SetComments(std::string(m_TextControls[pos]->GetValue()));
}

void ClinicalReportWidget::OnElementTextTitleUpdated(wxCommandEvent &event) {
  int pos;
  pos = (event.GetId() - REPORT_ELEMENT_BASE_ID) / 9;
  if (pos < 0 || pos >= m_ClinicalReportElements.size() ||
      pos >= m_TextTitleControls.size()) {
    return;
  }

  m_ClinicalReportElements[pos].SetTitle(
      std::string(m_TextTitleControls[pos]->GetValue()));
}

void ClinicalReportWidget::OnBtnUp(wxCommandEvent &event) {
  int pos;
  pos = (event.GetId() - REPORT_ELEMENT_BASE_ID) / 9;
  if (pos < 0 || pos >= m_ClinicalReportElements.size()) {
    return;
  }

  if (pos == 0)
    return;

  size_t nElems = m_ClinicalReportElements.size();
  int tmp = m_ClinicalReportElements[pos].GetPosition();

  m_ClinicalReportElements[pos].SetPosition(
      m_ClinicalReportElements[(pos + nElems - 1) % nElems].GetPosition());
  m_ClinicalReportElements[(pos + nElems - 1) % nElems].SetPosition(tmp);

  UpdateWidget();
}

void ClinicalReportWidget::OnBtnDown(wxCommandEvent &event) {
  int pos;
  pos = (event.GetId() - REPORT_ELEMENT_BASE_ID) / 9;
  if (pos < 0 || pos >= m_ClinicalReportElements.size()) {
    return;
  }

  if (pos == m_ClinicalReportElements.size() - 1)
    return;

  size_t nElems = m_ClinicalReportElements.size();
  int tmp = m_ClinicalReportElements[pos].GetPosition();

  m_ClinicalReportElements[pos].SetPosition(
      m_ClinicalReportElements[(pos + 1) % nElems].GetPosition());
  m_ClinicalReportElements[(pos + 1) % nElems].SetPosition(tmp);

  UpdateWidget();
}

void ClinicalReportWidget::Save() {
  ClinicalReportPluginUtilities::SetLastSelectedToConfig(
      std::string(m_ComboReports->GetValue()));

  if (!CheckFields()) {
    Core::Runtime::Kernel::GetApplicationRuntime()->SetAppState(
        Core::Runtime::APP_STATE_IDLE);

    throw Exceptions::Exception("Clinical Report Designer",
                                "You must fill all patient information fields");
  }

  // first of all, delete images that are not needed anymore
  for (int i = 0; i < m_ImagesToBeDeleted.size(); i++) {
    remove(path(m_ImagesToBeDeleted[i]));
  }

  m_ImagesToBeDeleted.clear();

  // Now write to the xml file
  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);

  TiXmlDeclaration *decl;
  TiXmlElement *ClinicalReport;

  decl = new TiXmlDeclaration("1.0", "UTF-8", "yes");
  ClinicalReport = new TiXmlElement("ClinicalReport");

  ClinicalReport->SetAttribute("name", m_Title);
  ClinicalReport->SetAttribute("authors", m_Authors);
  ClinicalReport->SetAttribute("comments", m_Comments);
  ClinicalReport->SetAttribute("patientId", m_PatientId);
  ClinicalReport->SetAttribute("patientName", m_PatientName);
  ClinicalReport->SetAttribute("patientAge", m_PatientAge);
  ClinicalReport->SetAttribute("patientSex", m_PatientSex);
  ClinicalReport->SetAttribute("patientAdqDate", "");
  ClinicalReport->SetAttribute("patientBirthDate", "");

  for (int i = 0; i < m_ClinicalReportElements.size(); i++) {
    TiXmlElement *Element = new TiXmlElement("Element");

    ClinicalReport->LinkEndChild(Element);

    std::string _type = m_ClinicalReportElements[i].GetType();
    int posInt = m_ClinicalReportElements[i].GetPosition();
    std::stringstream ss;
    ss << posInt;
    std::string _position = ss.str();
    std::string _comments = m_ClinicalReportElements[i].GetComments();
    std::string _title = m_ClinicalReportElements[i].GetTitle();
    bool _selected = m_ClinicalReportElements[i].GetSelected();

    Element->SetAttribute("position", _position);
    Element->SetAttribute("type", _type);
    Element->SetAttribute("description", _comments);
    Element->SetAttribute("selected", _selected);
    Element->SetAttribute("title", _title);

    if (_type == "image") {
      std::string _thumbnailSmall = m_ClinicalReportElements[i].GetThumbnailSmall();
      std::string _thumbnailMedium = m_ClinicalReportElements[i].GetThumbnailMedium();
      std::string _screenshot = m_ClinicalReportElements[i].GetScreenshot();

      TiXmlElement *Screenshot = new TiXmlElement("Image");
      TiXmlElement *ThumbnailSmall = new TiXmlElement("Image");
      TiXmlElement *ThumbnailMedium = new TiXmlElement("Image");

      Element->LinkEndChild(Screenshot);
      Element->LinkEndChild(ThumbnailSmall);
      Element->LinkEndChild(ThumbnailMedium);

      Screenshot->SetAttribute("imagetype", "screenshot");
      ThumbnailSmall->SetAttribute("imagetype", "thumbnailsmall");
      ThumbnailMedium->SetAttribute("imagetype", "thumbnailmedium");

      Screenshot->SetAttribute("path", _screenshot);
      ThumbnailSmall->SetAttribute("path", _thumbnailSmall);
      ThumbnailMedium->SetAttribute("path", _thumbnailMedium);
    } else if (_type == "table") {
      std::string strTable = m_ClinicalReportElements[i].GetTable();
      TiXmlText *text = new TiXmlText(strTable.c_str());
      Element->LinkEndChild(text);
    }
  }

  doc.LinkEndChild(decl);
  doc.LinkEndChild(ClinicalReport);
  doc.SaveFile(m_ReportPath);

  std::string reportName = "";
  int count = 0;

  if (m_ReportAddedAndNotSaved != "") {
    reportName = itksys::SystemTools::GetFilenameWithoutLastExtension(m_ReportPath);

    if (reportName == m_ReportAddedAndNotSaved) {
      m_ReportAddedAndNotSaved = "";
    }
  }
}

void ClinicalReportWidget::OnChangeReport(wxCommandEvent &event) {
  m_ReportName = std::string(m_ComboReports->GetValue().mb_str());
  LoadReportInfo(m_ReportName);
  ClinicalReportPluginUtilities::SetLastSelectedToConfig(m_ReportName);
  UpdateWidget();
}

void ClinicalReportWidget::LoadReportInfo(std::string reportName) {
  TiXmlElement *ClinicalReport;

  std::string file = m_ReportsFolder + Core::IO::SlashChar + reportName +
                     Core::IO::SlashChar + reportName + ".xml";

  try {
    ClinicalReport = ClinicalReportPluginUtilities::LoadReportXML(file);
  } catch (...) {
    return;
  }

  m_Title = ClinicalReport->Attribute("name");
  ;
  m_Authors = ClinicalReport->Attribute("authors");
  m_ReportPath = file;

  m_ReportTitleTxt->SetValue(m_Title);
  m_ReportAuthorsTxt->SetValue(m_Authors);

  m_ReportName = reportName;

  ClearReportElements();

  m_Title = ClinicalReport->Attribute("name");
  m_ReportTitleTxt->SetValue(m_Title);
  m_Authors = ClinicalReport->Attribute("authors");
  m_ReportAuthorsTxt->SetValue(m_Authors);
  m_Comments = ClinicalReport->Attribute("comments");
  m_ComentsTxt->SetValue(m_Comments);

  m_PatientId = ClinicalReport->Attribute("patientId");
  m_PatientIdTxt->SetValue(m_PatientId);
  m_PatientName = ClinicalReport->Attribute("patientName");
  m_PatientNameTxt->SetValue(m_PatientName);

  m_PatientAge = atoi(ClinicalReport->Attribute("patientAge"));

  std::string PatientSex = std::string(ClinicalReport->Attribute("patientSex"));

  if (PatientSex != "") {
    m_PatientSex = (PatientSex == "1");
    m_PatientSexFemale->SetValue(m_PatientSex);
    m_PatientSexMale->SetValue(!m_PatientSex);
  } else {
    m_PatientSexFemale->SetValue(false);
    m_PatientSexMale->SetValue(false);
  }

  int i = 0;

  TiXmlElement *Element;
  TiXmlElement *ElementTag;

  for (Element = ClinicalReport->FirstChildElement(); Element;
       Element = Element->NextSiblingElement()) {
    std::string ElementPosition = "";
    std::string ElementSelected = "";
    std::string ElementType = "";

    ElementPosition = Element->Attribute("position");
    ElementSelected = Element->Attribute("selected");
    ElementType = Element->Attribute("type");

    ClinicalReportElement reportElement;

    reportElement.SetComments(Element->Attribute("description"));
    reportElement.SetTitle(Element->Attribute("title"));

    if (ElementType == "image") {
      for (ElementTag = Element->FirstChildElement(); ElementTag;
           ElementTag = ElementTag->NextSiblingElement()) {
        if (ElementTag->Attribute("imagetype")) {
          std::string typeString = ElementTag->Attribute("imagetype");

          if (typeString == "screenshot") {
            reportElement.SetScreenshot(ElementTag->Attribute("path"));
          } else if (typeString == "thumbnailsmall") {
            reportElement.SetThumbnailSmall(ElementTag->Attribute("path"));
          } else if (typeString == "thumbnailmedium") {
            reportElement.SetThumbnailMedium(ElementTag->Attribute("path"));
          }
        }
      }
    } else if (ElementType == "table") {
      TiXmlText *TableElement = new TiXmlText(Element->GetText());
      TiXmlPrinter printer;
      TableElement->Accept(&printer);
      std::string table = printer.CStr();
      reportElement.SetTable(table);
    }

    reportElement.SetPosition(atoi(ElementPosition.c_str()));
    reportElement.SetSelected(ElementSelected == "1");
    reportElement.SetType(ElementType);

    AddReportElement(reportElement);
  }
}

std::string ClinicalReportWidget::IntegerToString(int n) {
  if (n == 0)
    return "0";

  std::string ret = "";
  while (n > 0) {
    ret = std::string(1, '0' + n % 10) + ret;
    n /= 10;
  }
  return ret;
}

std::string ClinicalReportWidget::AddReport() {
  if (m_ReportAddedAndNotSaved != "") {
    throw Exceptions::Exception("Clinical Report Designer",
                                (std::string("Cannot add a new report. Please save ") +
                                 m_ReportAddedAndNotSaved +
                                 std::string(" before creating a new report"))
                                    .c_str());
  }

  int total = m_ComboReports->GetCount() + 1;
  std::string NewReportName = "Report" + IntegerToString(total);

  int i = 1;

  while (true) {
    bool exists = false;

    path dir_path(m_ReportsFolder);

    directory_iterator end_itr; // default construction yields past-the-end

    for (directory_iterator itr(dir_path); itr != end_itr; ++itr) {
      if (is_directory(itr->status())) {
        if (itr->path().filename() == NewReportName) {
          exists = true;
          break;
        }
      }
    }

    if (!exists) // does not exist
    {
      break;
    }

    i++;
    NewReportName = "Report" + std::string(wxString::Format("%d", i).c_str());
  }

  m_ReportAddedAndNotSaved = NewReportName;
  m_SelectedReportBeforeAddReport =
      ClinicalReportPluginUtilities::GetLastSelectedFromConfig();

  // create the empty xml file and its directory
  ClinicalReportPluginUtilities::CreateEmptyReport(m_ReportsFolder, NewReportName);

  // reload reports information
  LoadReports();

  // select the new one
  LoadReportInfo(NewReportName);
  m_ComboReports->Select(m_ComboReports->GetCount() - 1);

  return NewReportName;
}

void ClinicalReportWidget::CleanNotSavedNewReports() {
  if (m_ReportAddedAndNotSaved == "")
    return;
  ClinicalReportPluginUtilities::SetLastSelectedToConfig(m_SelectedReportBeforeAddReport);
  m_SelectedReportBeforeAddReport = "";
  DeleteReport(m_ReportAddedAndNotSaved);
}

void ClinicalReportWidget::OnAddBtn(wxCommandEvent &event) { AddReport(); }

void ClinicalReportWidget::LoadReports() {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  m_ReportsFolder = settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  Core::IO::Directory::Pointer reportsDir = Core::IO::Directory::New();
  reportsDir->SetDirNameFullPath(m_ReportsFolder);

  // find subdirectories containing reports
  std::vector< std::string > subdirectories;

  try {
    subdirectories =
        ClinicalReportPluginUtilities::GetReports(m_ReportsFolder, reportsDir);
  } catch (...) {
    return;
  }

  m_ComboReports->Clear();

  for (int i = 0; i < subdirectories.size(); i++) {
    m_ComboReports->Append(subdirectories[i]);
  }

  for (int i = int(subdirectories.size()) - 1; i >= 0; i--) {
    LoadReportInfo(subdirectories[i]);
  }

  if (m_ComboReports->GetCount() > 0)
    m_ComboReports->Select(0);
  UpdateWidget();
}

void ClinicalReportWidget::OnTextTitle(wxCommandEvent &event) {
  m_Title = m_ReportTitleTxt->GetValue();
}

void ClinicalReportWidget::OnTextAuthors(wxCommandEvent &event) {
  m_Authors = m_ReportAuthorsTxt->GetValue();
}

void ClinicalReportWidget::OnTextId(wxCommandEvent &event) {
  m_PatientId = m_PatientIdTxt->GetValue();
}

void ClinicalReportWidget::OnTextName(wxCommandEvent &event) {
  m_PatientName = m_PatientNameTxt->GetValue();
}

void ClinicalReportWidget::OnFemale(wxCommandEvent &event) { m_PatientSex = true; }

void ClinicalReportWidget::OnMale(wxCommandEvent &event) { m_PatientSex = false; }

void ClinicalReportWidget::OnAddLogo(wxCommandEvent &event) {
  if (m_ChkLogo->GetValue()) {

    // Open file dialog
    std::string filetypes = Core::IO::DataEntityReader::GetFileFilterTypesForRead();
    Core::Runtime::Settings::Pointer settings;
    settings = Core::Runtime::Kernel::GetApplicationSettings();

    std::string resourcePath = settings->GetResourcePath();
    std::string dataPath;

    wxFileDialog *openFileDialog =
        new wxFileDialog(this, wxT("Select a logo"), wxT(""), wxT(""), wxT(""),
                         wxFD_OPEN | wxFD_FILE_MUST_EXIST | wxFD_MULTIPLE);
    openFileDialog->SetWildcard(_U(filetypes));

    // Default value
    openFileDialog->SetDirectory(
        resourcePath); // mitk::_U(settings->GetCurrentDataPath().c_str()));

    std::vector< std::string > completePath = std::vector< std::string >(0);

    if (openFileDialog->ShowModal() == wxID_OK) {
      wxArrayString wxFilenames;
      openFileDialog->GetFilenames(wxFilenames);
      dataPath = _U(openFileDialog->GetDirectory());

      for (unsigned i = 0; i < wxFilenames.size(); i++) {
        completePath.push_back(dataPath + Core::IO::SlashChar + _U(wxFilenames[i]));
      }

      wxTheApp->Yield(true);
    }

    if (completePath.size() > 0) {
      m_Logo = completePath[0];
    }
  } else {
    m_Logo = "";
  }
}

void ClinicalReportWidget::OnTextGeneralComments(wxCommandEvent &event) {
  m_Comments = m_ComentsTxt->GetValue();
}

void ClinicalReportWidget::DeleteReport(std::string selectedReport) {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  m_ReportsFolder = settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  path dir_path(m_ReportsFolder);

  int count = 0;
  bool deleted = false;

  directory_iterator end_itr; // default construction yields past-the-end

  try {
    for (directory_iterator itr(dir_path); itr != end_itr; ++itr) {
      if (is_directory(itr->status())) {
        count++;
        if (selectedReport == itr->path().filename()) {
          deleted = true;
          path subdir(m_ReportsFolder + "/" + itr->path().filename().string());
          remove_all(subdir);
        }
      }
    }
  } catch (...) {
    throw Exceptions::Exception("Clinical Report Designer",
                                "Cannot remove report. Please close all open documents.");
  }

  if (deleted && count == 1) {
    m_ClinicalReportElements.clear();
    m_ReportTitleTxt->SetValue("");
    m_ReportAuthorsTxt->SetValue("");
    m_PatientIdTxt->SetValue("");
    m_PatientNameTxt->SetValue("");
    m_ComentsTxt->SetValue("");
  }
}

void ClinicalReportWidget::OnDeleteReportBtn(wxCommandEvent &event) {
  std::string selectedReport = std::string(m_ComboReports->GetValue().mb_str());
  DeleteReport(selectedReport);
  LoadReports();
}

void ClinicalReportWidget::SetProcessor(ClinicalReportProcessor::Pointer processor) {
  m_Processor = processor;
}
