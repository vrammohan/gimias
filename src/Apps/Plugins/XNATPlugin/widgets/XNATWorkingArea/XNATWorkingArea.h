/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef XNATWorkingArea_H
#define XNATWorkingArea_H

#include "XNATWorkingAreaUI.h"
#include "XNATPluginUtilities.h"
#include "XNATPluginConnectionPanelWidget.h"
//#include "XNATPluginManageConnection.h"

#include "CILabNamespaceMacros.h"
#include "coreProcessingWidget.h"

#include "XNATQueryProcessor.h"
#include "XNATLoadProcessor.h"
#include "XNATDownloadProcessor.h"
#include "XNATExportProcessor.h"

#include "xnatClient.h"
#include "Core.h"
#include "Utils.h"

#include "coreBaseDataEntityReader.h"
#include "coreDataEntityWriter.h"
#include "coreSettings.h"
#include "coreKernel.h"
#include "coreDirectory.h"
#include "coreDataEntityReader.h"

/**
A class that defines all the graphical interface and user interaction for connecting to
XNAT and downloading data

\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class XNATWorkingArea : public XNATWorkingAreaUI, public Core::Widgets::ProcessingWidget {
public:
  coreDefineBaseWindowFactory1param(XNATWorkingArea, XNATPluginManageConnections::Pointer)
      // coreDefineBaseWindowFactory( XNATWorkingArea )

      //! constructor. initializes members.
      XNATWorkingArea(XNATPluginManageConnections::Pointer connManager, wxWindow *parent,
                      int id, const wxPoint &pos = wxDefaultPosition,
                      const wxSize &size = wxDefaultSize, long style = 0);

  ~XNATWorkingArea();

public:
  //!
  Core::BaseProcessor::Pointer GetProcessor();

private:
  //! retrieves the list of projects in the current XNAT node
  std::vector< std::string > GetAllProjects();

  //! updates the data structures holding the subjects and its experiments, given the
  //! subjects from the last query
  void UpdateSubjects(std::vector< std::map< std::string, std::string > > subjects);

  //! fills the project combobox with the projects in the current XNAT node
  void FillProjects();

  //! fills the XNAT connections combobox with the list of available XNAT nodes
  void FillConnections();

  //! updates the "Detailed information" area with the information of the
  //! m_SelectedSubjectIndex subject and m_SelectedExperimentIndex experiment
  void UpdateDetailedInformation();

  //! Updates the XNAT tree with the current subjects and its experiments
  void UpdateTree();

  //! Updates GUI
  void UpdateWidget();

  //! Opens a directory dialog in order to choose a new XNAT local database root folder.
  void OpenDirectoryForSavingData(wxCommandEvent &event);

  //! Event Handler. Performs a query to the selected XNAT node with the current search
  //! criteria.
  void OnQuery(wxCommandEvent &event);

  //! Event handler, updates m_SelectedSubjectIndex and m_SelectedExperimentIndex
  void OnTreeSelectionChanged(wxTreeEvent &event);

  //! Opens a dialog to Add, Remove or Edit XNAT nodes.
  void OnManageConnection(wxCommandEvent &event);

  //! Opens the XNAT local browser
  void OnBrowseLocal(wxCommandEvent &event);

  //! Event Handler, downloads the selected image experiment to the XNAT local database
  void OnDownload(wxCommandEvent &event);

  //! observer. called when XNATQueryProcessor::OUTPUT_XNAT_ALL_SUBJECTS is updated.
  //! publishes the data entity
  void OnOutputXNATAllSubjects();

  //! observer. called when XNATQueryProcessor::OUTPUT_XNAT_QUERY_RESULTS is updated.
  //! publishes the data entity and updates the tree.
  void OnOutputXNATQueryResults();

  //!
  void OnDownloadDataEntity();

  //!
  void OnExportDataEntity();

  void ClearSubjects();

  //!
  void OnConnectionItemSelect(wxCommandEvent &event);

private:
  //! current subjects (from the last query)
  std::vector< XNATSubject > m_subjects;

  //! projects in the selected database
  std::vector< std::string > m_projects;

  //! holds the currently selected subject's ID in the XNAT tree
  int m_selectedSubjectIndex;

  //! holds the last selected subject's ID in the XNAT tree
  int m_lastSelSubjectIndex;

  //! holds the last selected subject in the XNAT tree
  int m_selectedExperimentIndex;

  //! holds the last selected subject's resource file 'index' in the XNAT tree
  int m_selectedSubResourceIndex;

  //! holds the last selected subject's resource file 'collection' in the XNAT tree
  std::string m_selectedSubResourceCollection;

  //! query processor
  XNATQueryProcessor::Pointer m_QueryProcessor;

  //! load processor
  XNATLoadProcessor::Pointer m_LoadProcessor;

  //! download processor
  XNATDownloadProcessor::Pointer m_DownloadProcessor;

  //! export processor
  XNATExportProcessor::Pointer m_ExportProcessor;

  //! list of available connections, ordered by its name
  // std::map<std::string, XNATPluginDefineConetions> m_Connections;
  XNATPluginManageConnections::Pointer m_Connections;

  std::string m_SelectedConnection;

  //! folder where files are download
  std::string m_BaseFolder;

  std::string IntegerToString(int i);

  //! Holds the object of a valid selected item in the XNAT Tree
  wxTreeItemData *m_treeItemData;

  std::string FindAfter(std::string str, std::string pat);
};

#endif // XNATWorkingArea_H
