/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _WebServicePCH_H
#define _WebServicePCH_H

#include "ModuleDescription.h"
#include "axiom_node.h"
#include "axutil_env.h"
#include "coreBaseWindowFactories.h"
#include "coreDynProcessor.h"
#include "coreKernel.h"
#include "coreProcessorManager.h"
#include "coreWxMitkGraphicalInterface.h"
#include "dynModuleHelper.h"
#include <axiom_element.h>
#include <axiom_node.h>
#include <axiom_text.h>
#include <axis2_svc_skeleton.h>
#include <axutil_array_list.h>
#include <axutil_error_default.h>
#include <axutil_log_default.h>
#include <stdio.h>

#endif //_WebServicePCH_H
