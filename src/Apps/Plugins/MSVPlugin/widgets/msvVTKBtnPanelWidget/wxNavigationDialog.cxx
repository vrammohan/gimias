/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "wxNavigationDialog.h"

wxNavigationDialog::wxNavigationDialog(wxWindow *parent, wxWindowID id, wxBitmap &bitmap,
                                       const wxPoint &pos /*= wxDefaultPosition*/,
                                       const wxSize &size /*= wxDefaultSize*/,
                                       long style /*= wxBORDER_NONE*/,
                                       const wxString &name /*= "dialogBox"*/)
    : wxDialog(parent, id, "", pos, size, style, name) {
  SetExtraStyle(GetExtraStyle() & ~wxWS_EX_BLOCK_EVENTS);

  m_bmpButton = new wxBitmapButton(this, id, bitmap);

  wxBoxSizer *topsizer = new wxBoxSizer(wxVERTICAL);
  topsizer->Add(m_bmpButton, 0, wxEXPAND, 0);
  this->SetSizer(topsizer);
  Layout();
}

wxNavigationDialog::~wxNavigationDialog() {}
