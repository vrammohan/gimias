/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _XNATPluginEditConfigurationDialog_H
#define _XNATPluginEditConfigurationDialog_H

// XNAT plugin GUI
#include "XNATPluginEditConfigurationDialogUI.h"

/**
Configure a single SSH node

\ingroup XNAT Plugin
\author Xavi Planes
\date Sept 2011
*/

class XnatPluginEditConfigurationDialog : public XNATPluginEditConfigurationDialogUI {
public:
  XnatPluginEditConfigurationDialog(wxWindow *parent, int id, const wxString &title,
                                    const wxPoint &pos = wxDefaultPosition,
                                    const wxSize &size = wxDefaultSize,
                                    long style = wxDEFAULT_DIALOG_STYLE);

  //!
  void OnInit();

  //!
  std::string GetConnectionName() const;

  //!
  std::string GetURL() const;

  //!
  std::string GetUser() const;

  //!
  std::string GetPassword() const;

  void SetConnectionName(std::string s);
  void SetURL(std::string s);
  void SetUser(std::string s);
  void SetPassword(std::string s);

  std::string m_Name, m_URL, m_User, m_Password;

  bool m_Encrypted;

private:
  //! test define connection
  void OnBtnTest(wxCommandEvent &event);
  //! Add defube connection
  void OnOK(wxCommandEvent &event);
  //! close define window
  void OnCancel(wxCommandEvent &event);

  //!
  void UpdateWidget();

  //!
  void UpdateData();
};

#endif // _XNATPluginEditConfigurationDialog_H
