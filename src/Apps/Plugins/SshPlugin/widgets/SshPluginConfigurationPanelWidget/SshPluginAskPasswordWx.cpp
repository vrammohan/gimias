/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#include "SshPluginAskPasswordWx.h"
#include "coreWxGenericEvent.h"

sshPluginAskPasswordWx::sshPluginAskPasswordWx() {}

sshPluginAskPasswordWx::~sshPluginAskPasswordWx() {}

bool sshPluginAskPasswordWx::AskPassword() {
  Core::WxGenericEvent event;
  event.SetFunction(boost::bind(&sshPluginAskPasswordWx::AskPasswordInternal, this));
  if (wxIsMainThread()) {
    wxPostEvent(wxTheApp->GetTopWindow()->GetEventHandler(), event);
  } else {
    wxTheApp->GetTopWindow()->GetEventHandler()->AddPendingEvent(event);
    event.Wait();
  }

  return m_AskPasswordReturnValue;
}

void sshPluginAskPasswordWx::AskUserConfirmation() {
  Core::WxGenericEvent event;
  event.SetFunction(boost::bind(&sshPluginAskPasswordWx::AskUserConfirmation, this));
  if (wxIsMainThread()) {
    wxPostEvent(wxTheApp->GetTopWindow()->GetEventHandler(), event);
  } else {
    wxTheApp->GetTopWindow()->GetEventHandler()->AddPendingEvent(event);
    event.Wait();
  }
}

void sshPluginAskPasswordWx::AskPasswordInternal() {
  int returnCode;
  if (m_EchoPassword) {
    wxTextEntryDialog dialog(NULL, m_Prompt, m_Title);
    m_AskPasswordReturnValue = dialog.ShowModal();
    m_Answer = dialog.GetValue();
  } else {
    wxPasswordEntryDialog dialog(NULL, m_Prompt, m_Title);
    returnCode = dialog.ShowModal();
    m_Answer = dialog.GetValue();
  }

  m_AskPasswordReturnValue = (returnCode == wxID_OK);
}

void sshPluginAskPasswordWx::AskUserConfirmationInternal() {
  wxMessageDialog dialog(NULL, m_Prompt, "Confirm");
  if (dialog.ShowModal() == wxID_OK) {
    m_Answer = "yes";
  } else {
    m_Answer = "no";
  }
}
