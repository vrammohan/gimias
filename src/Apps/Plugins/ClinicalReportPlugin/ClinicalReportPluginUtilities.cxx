/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportPluginUtilities.h"
#include "ClinicalReportWidget.h"

void ClinicalReportPluginUtilities::CreateReportsConfigFile() {
  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);
  TiXmlDeclaration *decl;
  TiXmlElement *Config;

  decl = new TiXmlDeclaration("1.0", "UTF-8", "yes");
  Config = new TiXmlElement("Configuration");
  Config->SetAttribute("LastSelectedReport", "Report1");

  doc.LinkEndChild(decl);
  doc.LinkEndChild(Config);

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string reportsFolder =
      settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  doc.SaveFile(reportsFolder + "/" + "config.xml");
}

void ClinicalReportPluginUtilities::CreateEmptyReport(std::string reportsFolder,
                                                      std::string reportname) {
  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);
  TiXmlDeclaration *decl;
  TiXmlElement *ClinicalReport;

  decl = new TiXmlDeclaration("1.0", "UTF-8", "yes");
  ClinicalReport = new TiXmlElement("ClinicalReport");

  ClinicalReport->SetAttribute("name", "[Report Title Here]");
  ClinicalReport->SetAttribute("authors", "[Report Authors Here]");
  ClinicalReport->SetAttribute("comments", "[General Commnets Here]");
  ClinicalReport->SetAttribute("patientId", "[Patient Id Here]");
  ClinicalReport->SetAttribute("patientName", "[Patient Name Here]");
  ClinicalReport->SetAttribute("patientAge", "");
  ClinicalReport->SetAttribute("patientSex", "");
  ClinicalReport->SetAttribute("patientAdqDate", "");
  ClinicalReport->SetAttribute("patientBirthDate", "");

  doc.LinkEndChild(decl);
  doc.LinkEndChild(ClinicalReport);

  // create the directory
  Core::IO::Directory::Pointer reportsDir = Core::IO::Directory::New();
  reportsDir->SetDirNameFullPath(reportsFolder + "/" + reportname);
  reportsDir->Create();

  doc.SaveFile(reportsFolder + "/" + reportname + "/" + reportname + ".xml");
}

std::string ClinicalReportPluginUtilities::GetLastSelectedFromConfig() {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string reportsFolder =
      settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  std::string ConfigFilePath = reportsFolder + "/" + "config.xml";

  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);
  TiXmlElement *Config;

  doc = TiXmlDocument(ConfigFilePath);
  if (!doc.LoadFile())
    return "";

  Config = hDoc.FirstChildElement().Element();
  if (!Config)
    return "";

  return Config->Attribute("LastSelectedReport");
}

void ClinicalReportPluginUtilities::SetLastSelectedToConfig(std::string newSelected) {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string reportsFolder =
      settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  std::string ConfigFilePath = reportsFolder + "/" + "config.xml";

  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);
  TiXmlElement *Config;

  doc = TiXmlDocument(ConfigFilePath);
  if (!doc.LoadFile())
    return;

  Config = hDoc.FirstChildElement().Element();
  if (!Config)
    return;

  Config->SetAttribute("LastSelectedReport", newSelected);

  doc.SaveFile(ConfigFilePath);
}

std::vector< std::string >
ClinicalReportPluginUtilities::GetReports(std::string basedir,
                                          Core::IO::Directory::Pointer reportsdir) {
  std::vector< std::string > subdirectories;

  if (!reportsdir->Exists()) {
    reportsdir->Create();
  }

  path dir_path(basedir);

  if (!exists(dir_path)) {
    std::string message = dir_path.string() + std::string("Path doesn't exist ");
    throw(Core::Exceptions::Exception("ClinicalReportUtilities::GetReports",
                                      message.c_str()));
  }

  directory_iterator end_itr; // default construction yields past-the-end
  for (directory_iterator itr(dir_path); itr != end_itr; ++itr) {
    if (is_directory(itr->status())) {
      subdirectories.push_back(itr->path().filename().string());
    }
  }

  return subdirectories;
}

TiXmlElement *ClinicalReportPluginUtilities::LoadReportXML(std::string filename) {
  // read the attributes we want from the xml file
  TiXmlDocument *doc = new TiXmlDocument(filename);

  if (!doc->LoadFile()) {
    std::string message = ("ClinicalReportPluginUtilities::LoadReportXML: ") + filename +
                          std::string(" doesn't exist.");
    throw(Core::Exceptions::Exception("ClinicalReportUtilities::LoadReportXML",
                                      message.c_str()));
  }

  TiXmlHandle *hDoc = new TiXmlHandle(doc);
  TiXmlElement *ClinicalReport;
  TiXmlHandle hRoot(0);

  ClinicalReport = hDoc->FirstChildElement().Element();
  if (!ClinicalReport) {
    std::string message = std::string("ClinicalReportPluginUtilities::LoadReportXML: ") +
                          filename + std::string(" failed. ClinicalReport is NULL.");
    throw(Core::Exceptions::Exception("ClinicalReportUtilities::LoadReportXML",
                                      message.c_str()));
  }

  hRoot = TiXmlHandle(ClinicalReport);

  return ClinicalReport;
}

std::pair< std::vector< std::string >, std::vector< std::vector< std::string > > >
ClinicalReportPluginUtilities::GetTableFormat(std::string input) {
  std::pair< std::vector< std::string >, std::vector< std::vector< std::string > > > ret;
  std::vector< std::string > header;
  std::vector< std::vector< std::string > > lines;
  std::istringstream iss(input);

  char line[1024];
  int i = 0;

  while (iss.getline(line, 1024, ':')) {
    if (i == 0) // header
    {
      std::string lineString = std::string(line);

      std::istringstream iss2(lineString);
      char cell[1024];

      while (iss2.getline(cell, 1024, ';')) {
        header.push_back(std::string(cell));
      }

    } else // lines
    {
      std::vector< std::string > linevector;

      std::string lineString = std::string(line);

      std::istringstream iss2(lineString);

      char cell[1024];

      while (iss2.getline(cell, 1024, ';')) {
        linevector.push_back(std::string(cell));
      }

      lines.push_back(linevector);
    }

    i++;
  }

  lines.pop_back();

  ret.first = header;
  ret.second = lines;

  return ret;
}

void ClinicalReportPluginUtilities::OpenClinicalReportDesigner(
    ClinicalReportProcessor::Pointer processor, bool addReport) {
  // create the widget
  Core::Widgets::ClinicalReportWidget *ClinicalReportDialog =
      new Core::Widgets::ClinicalReportWidget();
  ClinicalReportDialog->OnInit();
  ClinicalReportDialog->SetProcessor(processor);

  if (addReport)
    ClinicalReportDialog->AddReport();

  int result = ClinicalReportDialog->ShowModal();

  if (result == wxID_OK) {
    try {
      ClinicalReportDialog->CreatePDFReport();
    } catch (...) {
      Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalInterface;
      graphicalInterface = Core::Runtime::Kernel::GetGraphicalInterface();

      if (graphicalInterface.IsNotNull()) {
        graphicalInterface->ReportMessage("You must fill all patient information fields",
                                          true);
      }

      OpenClinicalReportDesigner(processor);
    }
  } else if (result == wxID_CANCEL) {
    ClinicalReportDialog->CleanNotSavedNewReports();
  }

  // free allocated memory
  if (ClinicalReportDialog != NULL)
    delete ClinicalReportDialog;
}
