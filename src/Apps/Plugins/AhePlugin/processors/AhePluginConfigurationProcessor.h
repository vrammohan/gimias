/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _AhePluginConfigurationProcessor_H
#define _AhePluginConfigurationProcessor_H

#include "coreBaseProcessor.h"
#include "aheAppInst.h"

#include <map>

namespace ahePlugin {

/**
 * Processor for the AHE Plugin.
 *
 * Automatically loads the AHE nodes for the configuration file into a map.
 * Provides operations to manage and access the map.
 * Saves the AHE nodes into the configuration before destruction.
 *
 * \ingroup AhePlugin
 * \author Ernesto Coto
 * \date May 2014
 */
class ConfigurationProcessor : public Core::BaseProcessor {
public:
  //! Node-info map definition
  typedef std::map< std::string, aheAppInst::Pointer > NodeMapType;

public:
  //!
  coreProcessor(ConfigurationProcessor, Core::BaseProcessor);

  //! Saves the AHE nodes into the configuration.
  void Update();

  //! Adds a new node to the AHE node map
  void Add(aheAppInst::Pointer node);

  //! Returns a node in the AHE node map
  aheAppInst::Pointer Get(const std::string &key);

  //! Removes a node in the AHE node map
  void Remove(const std::string &key);

  //! Removes all nodes in the AHE node map
  void RemoveAll();

  //! Provides access to the AHE node map
  NodeMapType GetNodeMap() const;

private:
  //! Class constructor
  ConfigurationProcessor();

  //! Class destructor
  ~ConfigurationProcessor();

  //! Purposely not implemented
  ConfigurationProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

  //! AHE node map
  NodeMapType m_NodeMap;

}; // class ConfigurationProcessor

} // namespace ahePlugin

#endif //_AhePluginConfigurationProcessor_H
