/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#ifndef _SshPluginNodeManager_H
#define _SshPluginNodeManager_H

#include "sshNodeManager.h"

/**
\brief SSH Node manager that reads and stores configuration in Settings

\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/
class SshPluginNodeManager : public sshNodeManager {
public:
  //!
  coreDeclareSmartPointerTypesMacro(SshPluginNodeManager, sshNodeManager)
      blNewMacro(Self);
  coreClassNameMacro(SshPluginNodeManager)

      //!
      void UpdateSettings();

protected:
  //! Constructor: protect from instantiation.
  SshPluginNodeManager();

  //! Destructor: protect from instantiation.
  ~SshPluginNodeManager();

private:
};

#endif //_SshPluginNodeManager_H
