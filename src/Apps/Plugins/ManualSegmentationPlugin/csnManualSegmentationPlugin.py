# Used to configure ManualSegmentationPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

manualSegmentationPlugin = GimiasPluginProject("ManualSegmentationPlugin", api)

projects = [
    gmCore
]
manualSegmentationPlugin.AddProjects(projects)

manualSegmentationPlugin.AddSources(["*.cxx", "*.h"])
manualSegmentationPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
manualSegmentationPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "ManualSegmentationWidget",
  "MultiLevelROIPanelWidget",
  "InteractiveSegmPanelWidget"
  ]
manualSegmentationPlugin.AddWidgetModules(widgetModules, _useQt = 0)

manualSegmentationPlugin.SetPrecompiledHeader("ManualSegmentationPluginPCH.h")



