# Used to configure coreWorkflow
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmWorkflow = api.CreateCompiledProject("gmWorkflow", "dll")
gmWorkflow.AddProjects([
  gmCommonObjects,
  baseLib,
  baseLibNumericData
  ])

gmWorkflow.SetPrecompiledHeader("gmWorkflowPCH.h")

gmWorkflow.AddSources(["src/*.cxx" ], checkExists = 0)
gmWorkflow.AddSources(["src/*.txx" ], checkExists = 0)
gmWorkflow.AddSources(["src/*.h" ], checkExists = 0)
gmWorkflow.AddIncludeFolders(["src"])

