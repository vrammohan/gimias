/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "NewClinicalReportCommentDialog.h"
#include "ClinicalReportPluginUtilities.h"

// core
#include "coreSettings.h"
#include "coreDirectory.h"

// boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::NewClinicalReportCommentDialog,
                  NewClinicalReportCommentDialogUI)
END_EVENT_TABLE();

NewClinicalReportCommentDialog::NewClinicalReportCommentDialog()
    : NewClinicalReportCommentDialogUI(NULL, wxID_ANY, "", wxPoint(280, 180)) {
  try {
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      "NewClinicalReportCommentDialog::NewClinicalReportCommentDialog");
}

NewClinicalReportCommentDialog::~NewClinicalReportCommentDialog() {}

Core::BaseProcessor::Pointer
Core::Widgets::NewClinicalReportCommentDialog::GetProcessor() {
  return NULL;
}

std::string NewClinicalReportCommentDialog::GetComments() {
  return m_Comments->GetValue().ToStdString();
}

void NewClinicalReportCommentDialog::LoadReports() {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  m_BaseDir = settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  Core::IO::Directory::Pointer reportsDir = Core::IO::Directory::New();
  reportsDir->SetDirNameFullPath(m_BaseDir);

  // find subdirectories containing reports
  std::vector< std::string > subdirectories;

  try {
    subdirectories = ClinicalReportPluginUtilities::GetReports(m_BaseDir, reportsDir);
  } catch (...) {
    return;
  }

  m_ListReports->Clear();

  int lastSelectedPos = 0;
  std::string lastSelected = ClinicalReportPluginUtilities::GetLastSelectedFromConfig();

  for (int i = 0; i < subdirectories.size(); i++) {
    m_ListReports->Append(subdirectories[i]);

    if (lastSelected == subdirectories[i]) {
      lastSelectedPos = i;
    }
  }

  for (int i = subdirectories.size() - 1; i >= 0; i--) {
    LoadReportInfo(subdirectories[i]);
  }

  if (lastSelectedPos != 0)
    LoadReportInfo(lastSelected);
  if (m_ListReports->GetCount() > lastSelectedPos)
    m_ListReports->Select(lastSelectedPos);
}

void NewClinicalReportCommentDialog::LoadReportInfo(std::string reportName) {
  TiXmlElement *ClinicalReport;
  std::string file = m_BaseDir + Core::IO::SlashChar + reportName + Core::IO::SlashChar +
                     reportName + ".xml";

  try {
    ClinicalReport = ClinicalReportPluginUtilities::LoadReportXML(file);
  } catch (...) {
    return;
  }

  m_ReportName = ClinicalReport->Attribute("name");
  ;
  m_ReportAuthors = ClinicalReport->Attribute("authors");
  ;
  m_ReportPath = file;

  return;
}

void NewClinicalReportCommentDialog::OnChangeReport(wxCommandEvent &event) {
  m_ReportName = std::string(m_ListReports->GetStringSelection().mb_str());
  ClinicalReportPluginUtilities::SetLastSelectedToConfig(m_ReportName);
  LoadReportInfo(m_ReportName);
}

void NewClinicalReportCommentDialog::OnAddReport(wxCommandEvent &event) {
  ClinicalReportPluginUtilities::OpenClinicalReportDesigner(m_Processor, true);
  LoadReports();
}

void NewClinicalReportCommentDialog::OnEditReport(wxCommandEvent &event) {
  ClinicalReportPluginUtilities::OpenClinicalReportDesigner(m_Processor);
}

void NewClinicalReportCommentDialog::SetProcessor(
    ClinicalReportProcessor::Pointer processor) {
  m_Processor = processor;
}
