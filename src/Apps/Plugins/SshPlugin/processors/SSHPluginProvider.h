/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SSHPluginProvider_H
#define _SSHPluginProvider_H

#include "coreObject.h"
#include "ModuleFactory.h"
#include "corePluginProvider.h"
#include "SshPluginNodeManager.h"
#include "sshConnection.h"
#include "SSHPluginAttachRunningPlugins.h"

/**
\brief Loads command line plugins using SSH connection

Configuration parameters( "Configuration" tag in properties):
- Working directory: This is the remote directory where the command line plugins will be
executed
- Temporary directory: Local directory where to store local data
- Scan folder: Folder where to find remote command line plugins
- SSH node: SSH node configuration

This provider uses a cache that stores the modified time for all command line plugins.
This allows to avoid scanning the files each time the provider is executed.

\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/
class SSHPluginProvider : public Core::Runtime::PluginProvider, public ModuleFactory {
public:
  coreDeclareSmartPointerClassMacro1Param(SSHPluginProvider,
                                          Core::Runtime::PluginProvider,
                                          SshPluginNodeManager::Pointer);

  coreDefineFactoryClass1param(SSHPluginProvider, SshPluginNodeManager::Pointer)
      coreDefineFactoryClassEnd()

      //!
      std::string GetName() const;

  //! Create BaseFrontEndPlugin instances and attach them to GUI
  void LoadPlugins();

  //!
  void ScanPlugins();

  //!
  void AttachRunningPlugins();

  //! This is the current plugin name being created
  std::string GetCurrentPluginName() const;

  /// Get a module description by command.
  ModuleDescription GetModuleDescriptionByCommand(const std::string &) const;

  //!
  sshConnection::Pointer GetConnection();

protected:
  //!
  SSHPluginProvider(SshPluginNodeManager::Pointer nodeManager);
  //!
  virtual ~SSHPluginProvider(void);

  //! Compute available profiles from loaded libraries
  void InferAvailableProfilesFromPlugins(void);

  //! Load XML files for GIMIAS widgets descriptions
  void LoadXMLDescriptions(const std::string &path);

  //!
  virtual void Scan();

  /// Scan for command line modules in the module search path. Command
  /// line modules are executables that respond to a --xml
  /// argument. Returns the number of modules found (that have not
  /// already been discovered by another method).
  virtual long ScanForCommandLineModulesByExecuting();

  /// Load the module cache.
  virtual void LoadModuleCache();

  /// Save the module cache.
  virtual void SaveModuleCache();

  //!
  void LoadPlugin(const std::string &name);

  //!
  void UnLoadPlugin(const std::string &name);

private:
  //!
  coreDeclareNoCopyConstructors(SSHPluginProvider);

private:
  //!
  sshConnection::Pointer m_Connection;
  //!
  SshPluginNodeManager::Pointer m_nodeManager;
  //! Used to classify the registered factories
  std::string m_CurrentPluginName;
  //!
  SSHPluginAttachRunningPlugins::Pointer m_AttachPlugins;
};

#endif // _SSHPluginProvider_H
