/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "TavernaPluginWorkflowPanelWidgetFactory.h"
#include "coreSettings.h"
#include "wflWorkflowFactory.h"
#include "coreDirectory.h"
#include "blTextUtils.h"
#include "coreBaseWindowFactories.h"
#include "coreBaseWindowFactorySearch.h"

const std::string STR_WORKFLOW_FOLDER = "Workflows";

Core::BaseWindowFactory::Pointer
TavernaPlugin::WorkflowPanelWidgetFactory::NewBase(const wflWorkflow::Pointer workflow) {
  WorkflowPanelWidgetFactory::Pointer factory = New();
  factory->SetWorkflow(workflow);
  return factory.GetPointer();
}

Core::BaseWindow *TavernaPlugin::WorkflowPanelWidgetFactory::CreateBaseWindow() {
  WorkflowPanelWidget *window =
      new WorkflowPanelWidget(m_Workflow, GetParent(), GetWindowId());
  window->SetFactoryName(GetNameOfClass());
  if (!GetWindowName().empty()) {
    wxWindowBase *windowBase = window;
    windowBase->SetName(GetWindowName());
  }
  return window;
}

const char *TavernaPlugin::WorkflowPanelWidgetFactory::GetNameOfClass() const {
  return m_Name.c_str();
}

wflWorkflow::Pointer TavernaPlugin::WorkflowPanelWidgetFactory::GetWorkflow() const {
  return m_Workflow;
}

void TavernaPlugin::WorkflowPanelWidgetFactory::SetWorkflow(wflWorkflow::Pointer val) {
  m_Workflow = val;
  m_Name = m_Workflow->GetProperty("name") + "Factory";
}

void TavernaPlugin::WorkflowPanelWidgetFactory::Scan() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  // Scan workflows dir
  wflWorkflowFactory::Pointer factory = wflWorkflowFactory::New();
  factory->SetScanDirectory(settings->GetProjectHomePath() + Core::IO::SlashChar +
                            STR_WORKFLOW_FOLDER);
  factory->Update();
  wflWorkflowFactory::WorkflowMapType workflows = factory->GetWorkflows();

  // For each workflow -> register a factory
  wflWorkflowFactory::WorkflowMapType::iterator it;
  for (it = workflows.begin(); it != workflows.end(); it++) {
    wflWorkflow::Pointer workflow = it->second;

    Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
    graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

    // Replace "_" by " " because in GTK "_" is not valid for menu items
    std::string name = workflow->GetProperty("name");
    blTextUtils::StrSub(name, "_", " ");

    Core::BaseWindowFactorySearch::Pointer search = Core::BaseWindowFactorySearch::New();
    search->SetCaption(name);
    search->Update();
    std::list< std::string > windows = search->GetFactoriesNames();

    if (windows.empty()) {
      // Panel widgets
      Core::WindowConfig config;
      config.ProcessorObservers().Category("Taverna Workflow");
      Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
          NewBase(workflow), config.ProcessingTool().ProcessorObservers().Caption(name));
    } else {
      Core::BaseWindowFactory::Pointer factory;
      factory = graphicalIface->GetBaseWindowFactory()->FindFactory(*windows.begin());
      ((WorkflowPanelWidgetFactory *)factory.GetPointer())->SetWorkflow(workflow);
    }
  }
}
