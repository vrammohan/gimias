/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SshPluginConfigurationPanelWidget.h"
#include "SshPluginEditConfigurationDialog.h"

class wxSshNodeData : public wxClientData {
public:
  wxSshNodeData(sshNode::Pointer node) { m_Node = node; }
  //!
  sshNode::Pointer m_Node;
};

sshPluginConfigurationPanelWidget::sshPluginConfigurationPanelWidget(
    SshPluginNodeManager::Pointer nodeManager, wxWindow *parent, int id /*= wxID_ANY*/,
    const wxPoint &pos /*= wxDefaultPosition*/, const wxSize &size /*= wxDefaultSize*/,
    long style /* = 0*/)
    : SshPluginConfigurationPanelWidgetUI(parent, id, pos, size, style) {
  SetName("SSH Configuration");
  m_NodeManager = nodeManager;
}

sshPluginConfigurationPanelWidget::~sshPluginConfigurationPanelWidget() {
  // We don't need to destroy anything because all the child windows
  // of this wxWindow are destroyed automatically
}

void sshPluginConfigurationPanelWidget::UpdateWidget() {
  m_listNodes->Clear();
  sshNodeManager::NodeMapType nodes = m_NodeManager->GetNodeMap();
  sshNodeManager::NodeMapType::iterator it;
  for (it = nodes.begin(); it != nodes.end(); it++) {
    m_listNodes->Append(it->first, new wxSshNodeData(it->second));
  }
}

void sshPluginConfigurationPanelWidget::UpdateData() {
  m_NodeManager->RemoveAll();
  for (int i = 0; i < m_listNodes->GetCount(); i++) {
    wxSshNodeData *sshNodeData;
    sshNodeData = static_cast< wxSshNodeData * >(m_listNodes->GetClientObject(i));
    m_NodeManager->Add(sshNodeData->m_Node);
  }

  // Setting will be available to rest of plugins
  m_NodeManager->UpdateSettings();
}

void sshPluginConfigurationPanelWidget::OnAdd(wxCommandEvent &event) {
  try {
    SshPluginEditConfigurationDialog dialog(this, wxID_ANY, "Add new SSH node");
    dialog.Center();
    if (dialog.ShowModal() == wxID_OK) {
      if (dialog.GetNode()->GetHostname().empty()) {
        throw Core::Exceptions::Exception("sshPluginConfigurationPanelWidget::OnAdd",
                                          "Host name cannot be empty");
      }
      m_listNodes->Append(dialog.GetNode()->GetHostname(),
                          new wxSshNodeData(dialog.GetNode()));
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(sshPluginConfigurationPanelWidget::OnAdd)
}

void sshPluginConfigurationPanelWidget::OnRemove(wxCommandEvent &event) {
  try {
    if (m_listNodes->GetSelection() != -1) {
      m_listNodes->Delete(m_listNodes->GetSelection());
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(sshPluginConfigurationPanelWidget::OnRemove)
}

void sshPluginConfigurationPanelWidget::OnEdit(wxCommandEvent &event) {
  try {
    if (m_listNodes->GetSelection() != -1) {
      wxSshNodeData *sshNodeData;
      sshNodeData = static_cast< wxSshNodeData * >(
          m_listNodes->GetClientObject(m_listNodes->GetSelection()));
      SshPluginEditConfigurationDialog dialog(this, wxID_ANY, "Edit SSH node");
      dialog.SetNodeData(sshNodeData->m_Node);
      dialog.Center();
      if (dialog.ShowModal() == wxID_OK) {
        sshNodeData->m_Node = dialog.GetNode();
      }
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(sshPluginConfigurationPanelWidget::OnEdit)
}

void sshPluginConfigurationPanelWidget::OnListDClick(wxCommandEvent &event) {
  OnEdit(event);
}
