
#include "MasterPasswordDefinition.h"

//-------------------------------------------------------------------------------------
MasterPasswordDefinition::MasterPasswordDefinition(
    wxWindow *parent, int id, const wxString &title, bool enable /*=true*/,
    const wxPoint &pos /*=wxDefaultPosition*/, const wxSize &size /*=wxDefaultSize*/,
    long style /*=wxDEFAULT_DIALOG_STYLE*/)
    : MasterPasswordDefinitionUI(parent, id, title, pos, size, style)
//-------------------------------------------------------------------------------------
{
  this->m_rdBoxMasterPassChoice->SetSelection(1);
}

//-------------------------------------------------------------------------------------
void MasterPasswordDefinition::OnBtnAdd(wxCommandEvent &event)
//-------------------------------------------------------------------------------------
{
  EndModal(wxID_BTN_ADD);
}

//-------------------------------------------------------------------------------------
void MasterPasswordDefinition::OnBtnCancel(wxCommandEvent &event)
//-------------------------------------------------------------------------------------
{
  EndModal(wxID_BTN_CANCEL);
}

//--------------------------------------------------------------------------
void MasterPasswordDefinition::OnBtnRdBoxChange(wxCommandEvent &event)
//--------------------------------------------------------------------------
{
  if (!this->GetRdBoxOption()) {
    this->m_txtActiveMasterPass->Hide();
    this->m_txtDescActiveMasterPass->Hide();
  } else {
    this->m_txtActiveMasterPass->Show();
    this->m_txtDescActiveMasterPass->Show();
  }
}

//--------------------------------------------------------------------------
int MasterPasswordDefinition::GetRdBoxOption()
//--------------------------------------------------------------------------
{
  int val = this->m_rdBoxMasterPassChoice->GetSelection();

  return val;
}

//--------------------------------------------------------------------------
void MasterPasswordDefinition::HideRdBox()
//--------------------------------------------------------------------------
{
  this->m_rdBoxMasterPassChoice->Hide();
}

//--------------------------------------------------------------------------
void MasterPasswordDefinition::HideActualMP()
//--------------------------------------------------------------------------
{
  this->m_txtDescActiveMasterPass->Hide();
  this->m_txtActiveMasterPass->Hide();
}

//--------------------------------------------------------------------------
std::string MasterPasswordDefinition::GetTxtActiveMasterPassword()
//--------------------------------------------------------------------------
{
  return this->m_txtActiveMasterPass->GetValue().ToStdString().c_str();
}

//--------------------------------------------------------------------------
std::string MasterPasswordDefinition::GetTxtNewMasterPassword()
//--------------------------------------------------------------------------
{
  return this->m_txtNewMasterPass->GetValue().ToStdString().c_str();
}

//--------------------------------------------------------------------------
std::string MasterPasswordDefinition::GetTxtRepMasterPassword()
//--------------------------------------------------------------------------
{
  return this->m_txtRepMasterPass->GetValue().ToStdString().c_str();
}
