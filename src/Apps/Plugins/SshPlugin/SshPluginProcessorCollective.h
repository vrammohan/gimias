/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SshPluginProcessorCollective_H
#define _SshPluginProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

namespace SshPlugin {

/**

\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/

class ProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(SshPlugin::ProcessorCollective,
                                    Core::SmartPointerObject);

private:
  //! The constructor instantiates all the processors and connects them.
  ProcessorCollective();
};

} // namespace SshPlugin{

#endif //_SshPluginProcessorCollective_H
