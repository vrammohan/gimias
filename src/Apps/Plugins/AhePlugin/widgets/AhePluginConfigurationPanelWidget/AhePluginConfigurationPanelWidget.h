/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _AhePluginConfigurationPanelWidget_H
#define _AhePluginConfigurationPanelWidget_H

#include "AhePluginConfigurationProcessor.h"
#include "AhePluginConfigurationPanelWidgetUI.h"
#include "corePreferencesPage.h"

// Panel label definition
#define AHE_CONF_PANEL_NAME "AHE configuration"

namespace ahePlugin {

/**
 * GUI implementation class for AHE node configuration panel.
 *
 * Implements GUI components for the panel.
 *
 * \ingroup AhePlugin
 * \author Ernesto Coto
 * \date May 2014
 */
class PLUGIN_EXPORT ConfigurationPanelWidget : public AhePluginConfigurationPanelWidgetUI,
                                               public Core::Widgets::PreferencesPage {

  // OPERATIONS
public:
  //!
  coreDefineBaseWindowFactory(ConfigurationPanelWidget);

  //! Class constructor
  ConfigurationPanelWidget(wxWindow *parent, int id = wxID_ANY,
                           const wxPoint &pos = wxDefaultPosition,
                           const wxSize &size = wxDefaultSize, long style = 0);

  //! Class destructor
  ~ConfigurationPanelWidget();

  //! Loads the panel's initial content and configuration
  void OnInit();

  //! Returns the processor of this class
  Core::BaseProcessor::Pointer GetProcessor();

private:
  //! Update GUI from working data
  void UpdateWidget();

  //! Update working data from GUI
  void UpdateData();

  //! Add AHE node event handler
  void OnAdd(wxCommandEvent &event);

  //! Remove AHE node event handler.
  void OnRemove(wxCommandEvent &event);

  //! Edit AHE node event handler.
  void OnEdit(wxCommandEvent &event);

  //! Double-click AHE node list event handler.
  void OnListDClick(wxCommandEvent &event);

  // ATTRIBUTES
private:
  //! Working data of the processor

  //! Class processor
  ConfigurationProcessor::Pointer m_Processor;

}; // class ConfigurationPanelWidget

} // namespace ahePlugin

#endif //_AhePluginConfigurationPanelWidget_H
