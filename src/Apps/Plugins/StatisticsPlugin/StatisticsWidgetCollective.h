/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _StatisticsWidgetCollective_H
#define _StatisticsWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

/**

\ingroup StatisticsPlugin
\author Albert Sanchez
\date 18 Oct 2011
*/

class StatisticsWidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(StatisticsWidgetCollective, Core::WidgetCollective);

  //!
  void Init();

private:
  StatisticsWidgetCollective();

private:
};

#endif //_StatisticsWidgetCollective_H
