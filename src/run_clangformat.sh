#!/bin/bash

find -type f -name "*\.h" -o -name "*\.cpp" -o -name "*\.cxx" -o -name "*\.txx" | while read el ; do
    dos2unix $el
    clang-format-3.7 -i -style=file $el
done

