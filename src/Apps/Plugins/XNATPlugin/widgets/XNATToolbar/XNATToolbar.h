/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _XNATToolbar_H
#define _XNATToolbar_H

#include <wx/wx.h>
#include <wx/image.h>
#include "wx/toolbar.h"

#include "gmWidgetsWin32Header.h"

#include "coreBaseWindow.h"
#include "coreWorkingAreaFactory.h"
#include "coreToolbarPluginTab.h"

namespace Core {
namespace Widgets {

  /**
  \brief Toolbar for XNAT
  \ingroup XNATPlugin
  \author Albert Sanchez
  \date 19 Sep 2011
  */

  class XNATToolbar : public Core::Widgets::ToolbarBase {
  public:
    //!
    coreDefineBaseWindowFactory(XNATToolbar);

    //!
    XNATToolbar(wxWindow *parent, wxWindowID id = wxID_ANY,
                const wxPoint &pos = wxDefaultPosition,
                const wxSize &size = wxDefaultSize,
                long style = wxTB_HORIZONTAL | wxTB_FLAT,

                const wxString &name = wxPanelNameStr);

    //!
    Core::BaseProcessor::Pointer GetProcessor();

    //!
    void Init();

    //! Opens the XNAT working area.
    void OpenXNATWorkingArea(wxCommandEvent &event);

  protected:
    //!
    void UpdateState();

    DECLARE_EVENT_TABLE();

  private:
  };

} // namespace Widgets
} // namespace Core

#endif // _XNATToolbar_H
