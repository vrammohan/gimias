/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _VMTKPlugin_H
#define _VMTKPlugin_H

#include "vmtkProcessorCollective.h"
#include "vmtkWidgetCollective.h"

// CoreLib
#include "coreFrontEndPlugin.h"

/**
\brief Signal Viewer
\ingroup MeshEditorPlugin
\author Xavi Planes
\date 12 April 2010
*/
class PLUGIN_EXPORT VMTKPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
  // TYPE DEFINITIONS
public:
  coreDeclareSmartPointerClassMacro(VMTKPlugin, Core::FrontEndPlugin::FrontEndPlugin);

  // OPERATIONS
protected:
  //!
  VMTKPlugin(void);

  //!
  virtual ~VMTKPlugin(void);

private:
  //! Purposely not implemented
  VMTKPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  vmtkProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  vmtkWidgetCollective::Pointer m_Widgets;
};

#endif // VMTKPlugin_H
