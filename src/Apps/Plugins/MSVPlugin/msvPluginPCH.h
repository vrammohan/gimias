/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _msvDemoPCH_H
#define _msvDemoPCH_H

#if defined(_MSC_VER)
#define WX_HIDE_MODE_T 1
typedef unsigned short mode_t;
#endif

// WxWidgets
#include "wxID.h"
#include <wx/image.h>
#include <wx/wx.h>
#include <wx/wxprec.h>
#include <wx/spinbutt.h>

// CoreLib
#include "coreDataEntity.h"
#include "coreFrontEndPlugin.h"
#include "coreKernel.h"
#include "coreReportExceptionMacros.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreDataEntity.h"
#include "coreFrontEndPlugin.h"
#include "coreMultiRenderWindow.h"
#include "coreObject.h"
#include "corePluginTab.h"
#include "coreRenderingTree.h"
#include "coreReportExceptionMacros.h"
#include "coreSmartPointerMacros.h"
#include "coreVTKPolyDataHolder.h"
#include "coreWxMitkCoreMainWindow.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSettings.h"
#include "itksys/SystemTools.hxx"
#include "coreVTKImageDataHolder.h"
#include "coreVTKUnstructuredGridHolder.h"
#include "coreMultiRenderWindowMITK.h"

// STD
#include <iostream>
#include <limits>
#include <string>

#endif //_msvDemoPCH_H
