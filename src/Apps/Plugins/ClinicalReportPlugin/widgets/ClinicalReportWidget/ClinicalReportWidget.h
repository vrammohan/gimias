/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ClinicalReportWidget_H
#define _ClinicalReportWidget_H

#include "ClinicalReportWidgetUI.h"
#include "ClinicalReportProcessor.h"

// CoreLib
#include "coreRenderingTree.h"
#include "coreFrontEndPlugin.h"
#include "coreCommonDataTypes.h"
#include "coreBaseWindow.h"

// GuiBridgeLib
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>

#include "wx/pdfdocdef.h"

#include "ClinicalReportElement.h"
#include "ClinicalReportPatientInformation.h"

#include <boost/thread/thread.hpp>

// xml
#include "tinyxml.h"

namespace Core {
namespace Widgets {
  class AcquireDataEntityInputControl;
  class UserHelper;
  class DataEntityListBrowser;
}
}

namespace Core {
namespace Widgets {

  /**
  \ingroup ClinicalReportPlugin
  \brief Used to run a command in a separate thread
  \author Albert Sanchez
  \date 15 Nov 2010
  */

  class RunCommand {
  public:
    RunCommand(std::string command);
    void operator()();

  private:
    std::string m_Command;
  };

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 19 Oct 2010
*/

#define wxID_ClinicalReportWidget wxID("wxID_ClinicalReportWidget")
#define REPORT_ELEMENT_BASE_ID 100

  class ClinicalReportWidget : public ClinicalReportWidgetUI, public Core::BaseWindow {
    // OPERATIONS
  public:
    //!
    ClinicalReportWidget();

    //!
    ~ClinicalReportWidget();

    //!
    bool Enable(bool enable /*= true */);

    //!
    Core::BaseProcessor::Pointer GetProcessor();

    //!
    void OnInit();

    //!
    bool CheckFields();

    //!
    void OnCreateReportBtn(wxCommandEvent &event);

    //!
    void OnClose(wxCommandEvent &event);

    //!
    void OnSave(wxCommandEvent &event);

    //!
    void OnSpinCtrlUpdated(wxCommandEvent &event);

    //!
    void OnReportElementSelected(wxEvent &event);

    //!
    void Save();

    //!
    void OnChangeReport(wxCommandEvent &event);

    //!
    void OnTextTitle(wxCommandEvent &event);

    //!
    void OnTextAuthors(wxCommandEvent &event);

    //!
    void OnTextId(wxCommandEvent &event);

    //!
    void OnTextName(wxCommandEvent &event);

    //!
    void OnFemale(wxCommandEvent &event);

    //!
    void OnMale(wxCommandEvent &event);

    //!
    void OnAddLogo(wxCommandEvent &event);

    //!
    void OnTextGeneralComments(wxCommandEvent &event);

    //!
    void DeleteReport(std::string selectedReport);

    //!
    void OnDeleteReportBtn(wxCommandEvent &event);

    //!
    void OnElementTextUpdated(wxCommandEvent &event);

    //!
    void OnElementTextTitleUpdated(wxCommandEvent &event);

    //!
    void OnShowTableBtn(wxCommandEvent &event);

    //!
    void OnDeleteBtn(wxCommandEvent &event);

    //!
    void OnBtnUp(wxCommandEvent &event);

    //!
    void OnBtnDown(wxCommandEvent &event);

    //!
    void OnAddBtn(wxCommandEvent &event);

    //!
    void OnRemoveAll(wxCommandEvent &event);

    //!
    void CreatePDFReport();

    //!
    std::string AddReport();

    //!
    void CleanNotSavedNewReports();

    //!
    void SetProcessor(ClinicalReportProcessor::Pointer processor);

  private:
    DECLARE_EVENT_TABLE()

    //! adds a new report element
    void AddReportElement(ClinicalReportElement clinicalReportElement);

    //! clears all report element
    void ClearReportElements();

    //! updates GUI data
    void UpdateWidget();

    //! load all the clinical reports elements
    void LoadReports();

    //! load one clinical report in the combobox and also update its title and authors
    void LoadReportInfo(std::string reportName);

    //!
    std::string IntegerToString(int n);

    // ATTRIBUTES
  private:
    //! processor
    ClinicalReportProcessor::Pointer m_Processor;

    //! reports folder path
    std::string m_ReportsFolder;

    //! clinical report elements
    std::vector< ClinicalReportElement > m_ClinicalReportElements;

    //! comment text control vector
    std::vector< wxTextCtrl * > m_TextControls;

    //! title text control vector
    std::vector< wxTextCtrl * > m_TextTitleControls;

    //! comment text control vector
    std::vector< wxStaticBitmap * > m_StaticBitMaps;

    //! spin control vector
    std::vector< wxSpinCtrl * > m_SpinCtrls;

    //! show table buttons vector
    std::vector< wxButton * > m_Buttons;

    //! show table buttons vector
    std::vector< wxButton * > m_ButtonsUp;

    //! show table buttons vector
    std::vector< wxButton * > m_ButtonsDown;

    //! images that belong to a deleted element
    std::vector< std::string > m_ImagesToBeDeleted;

    //! report title
    std::string m_Title;

    //! report authors
    std::string m_Authors;

    //! report general comments
    std::string m_Comments;

    //! current report name
    std::string m_ReportName;

    //! current report path
    std::string m_ReportPath;

    //! Patient Information
    std::string m_PatientId;
    std::string m_PatientName;
    int m_PatientAge;
    bool m_PatientSex;
    std::string m_AdquisitionDate;
    std::string m_BirthDate;

    std::string m_ReportAddedAndNotSaved;
    std::string m_SelectedReportBeforeAddReport;

    std::string m_Logo;
  };
} // namespace Widgets
} // namespace Core

#endif //_ClinicalReportWidget_H
