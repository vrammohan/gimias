/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "coreUploadXMLReader.h"

// xml
#include "tinyxml.h"

#include "xnatClient.h"
#include "Core.h"
#include "Utils.h"

#include "coreFile.h"

#include "coreDataEntityWriter.h"

Core::IO::UploadXMLReader::UploadXMLReader(void) {
  m_ValidExtensionsList.push_back(".uxml");
}

Core::IO::UploadXMLReader::~UploadXMLReader(void) {}

std::string Core::IO::UploadXMLReader::FindAfter(std::string str, std::string pat) {
  std::string ret;
  size_t found;

  found = str.find(pat);

  if (found != std::string::npos) {
    size_t n = (int)found;
    n += pat.size();

    int count = 0;
    for (size_t i = n; i < str.size(); i++) {
      if (str[i] == '/')
        break;
      count++;
    }

    ret = str.substr(n, count);
  } else {
    ret = "";
  }

  return ret;
}

std::string Core::IO::UploadXMLReader::FindBefore(std::string str, std::string pat) {
  std::string ret;
  size_t found;

  found = str.find(pat);

  if (found != std::string::npos) {
    int n = (int)found;

    ret = str.substr(0, n);
  } else {
    ret = str;
  }

  return ret;
}

void Core::IO::UploadXMLReader::ReadData() {
  if (m_Filenames.size() > 0) {
    std::string filename = m_Filenames[0];
    TiXmlDocument *doc = new TiXmlDocument(filename);

    if (doc->LoadFile()) {
      TiXmlHandle *hDoc = new TiXmlHandle(doc);
      TiXmlElement *Uploads;
      TiXmlHandle hRoot(0);

      Uploads = hDoc->FirstChildElement().Element();

      if (Uploads) {
        TiXmlElement *Element;

        std::vector< UploadElement > UploadElements;

        for (Element = Uploads->FirstChildElement(); Element;
             Element = Element->NextSiblingElement()) {
          std::string ElementType = "";

          ElementType = Element->Attribute("type");

          if (ElementType == "XNAT") {
            std::string ElementURL = "";
            std::string ElementBaseURL = "";
            std::string ElementUsr = "";
            std::string ElementPwd = "";
            std::string ElementProject = "";
            std::string ElementSubject = "";
            std::string ElementExperiment = "";
            std::string ElementFolder = "";
            std::string ElementFilename = "";

            ElementURL = Element->Attribute("url");
            ElementBaseURL = FindBefore(ElementURL, "data/");

            ElementUsr = Element->Attribute("usr");
            ElementPwd = Element->Attribute("pwd");
            ElementProject = FindAfter(ElementURL, "projects/");
            ElementSubject = FindAfter(ElementURL, "subjects/");
            ElementExperiment = FindAfter(ElementURL, "experiments/");
            ElementFolder = Element->Attribute("folder");
            ElementFilename = Element->Attribute("filename");
            ElementExperiment += "_" + ElementFilename;

            UploadElement ue;
            ue.baseURL = ElementBaseURL;
            ue.usr = ElementUsr;
            ue.pwd = ElementPwd;
            ue.subject = ElementSubject;
            ue.project = ElementProject;
            ue.experiment = ElementExperiment;
            ue.folder = ElementFolder;
            ue.filename = ElementFilename;

            UploadElements.push_back(ue);
          }
        }
        SetNumberOfOutputs(UploadElements.size());
        for (int i = 0; i < UploadElements.size(); i++) {
          UploadElement ue = UploadElements[i];
          XNAT::Utils client(ue.usr, ue.pwd, ue.baseURL);

          // Catch exception when file doesn't exists
          try {
            client.removeResourceFileSubLvl(ue.folder + ue.filename, "outputs",
                                            ue.experiment, ue.project, ue.subject);
          } catch (...) {
          }
          client.sendResourceFileSubLvl(ue.folder + ue.filename, "outputs", ue.experiment,
                                        ue.project, ue.subject, XNAT::Utils::VTK, "",
                                        false);
          Core::DataEntity::Pointer de = Core::DataEntity::New();
          SetOutputDataEntity(i, de);
        }
      }
    }
  }
}

boost::any Core::IO::UploadXMLReader::ReadSingleTimeStep(int iTimeStep,
                                                         const std::string &filename) {
  blTagMap::Pointer output;
  return output;
}
