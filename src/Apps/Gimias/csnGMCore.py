# Used to configure gmCore
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

# Use gmCore and MITK dependency for compatibility
gmCore = api.CreateCompiledProject("gmCore", "library")
gmCore.AddProjects([gmCoreLight,mitkPlugin] )

