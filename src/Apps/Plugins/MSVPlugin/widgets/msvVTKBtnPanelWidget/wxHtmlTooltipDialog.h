/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef wxHtmlTooltipDialog_H
#define wxHtmlTooltipDialog_H

// wxWidgets
#include <wx/dialog.h>
#include <wx/timer.h>
#include <wx/html/htmlwin.h>
#include <wx/wx.h>
#include <wx/sizer.h>
#include "wxNavigationDialog.h"
#include "msvViewManager.h"
#include "msvWxVTKButtonsManager.h"

class wxHtmlEventHandler;
class msvWxVTKButtons;
class msvVTKBtnEdit;
class msvMultiRenderWindow;
class msvVTKBtnEditView;
class msvWxHtmlWindow;

/**
\brief Dialog to show tooltips and information of a VTK button. Ii also handles
the events when clicking on the window.

It uses an HTML window (msvWxHtmlWindow) to show the contents and use hyperlinks to handle
interaction.

User events are managed by two strategies:
- hyperlink on the HTML page
- Context menu

\ingroup msvKPlugin
\author Xavi Planes
\date Nov 2010
*/
class wxHtmlTooltipDialog : public wxDialog {

public:
  //!
  wxHtmlTooltipDialog(wxWindow *parent, wxWindowID id,
                      const wxPoint &pos = wxDefaultPosition,
                      const wxSize &size = wxDefaultSize,
                      long style = wxDEFAULT_DIALOG_STYLE,
                      const wxString &name = "dialogBox");

  ~wxHtmlTooltipDialog();

  //! Set active button
  void SetButton(msvWxVTKButtons *button);

  //! SET rendering tree
  void SetRenderWindow(msvMultiRenderWindow *renderWindow);

  //! Load a specific page
  void LoadPage(wxString hPage);

  //! Get internal window
  wxHtmlWindow *GetHtmlWin();

  //! Show dialog
  void Show();

  //! Hide dialog
  bool Hide();

private:
  DECLARE_EVENT_TABLE()

private:
  //! drawing elements
  msvWxHtmlWindow *m_htmlWin;

  //!
  msvWxVTKButtons *m_Button;

  //!
  msvMultiRenderWindow *m_RenderWindow;
};

#endif // wxHtmlTooltipDialog_H
