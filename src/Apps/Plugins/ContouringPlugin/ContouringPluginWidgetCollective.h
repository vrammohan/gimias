/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ContouringPluginWidgetCollective_H
#define _ContouringPluginWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

#include "ContouringPluginProcessorCollective.h"

namespace contouringPlugin {

/**
This class instantiates all widgets used in the plugin. The widgets are used to operate
the plugin processors
(see ProcessorCollective).
In the ContouringPlugin, there is currently only one widget, but when the number of
widgets grows, this class
ensures that the code remains maintainable.

\ingroup ContouringPlugin
*/

class WidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(WidgetCollective, Core::WidgetCollective);

private:
  //! The constructor instantiates all the widgets and registers them.
  WidgetCollective();
};

} // namespace contouringPlugin

#endif //_ContouringPluginWidgetCollective_H
