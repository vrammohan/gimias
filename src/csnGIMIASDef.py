from csnToolkitOpen import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

######################################################################################
# Third Party

def wxWidgets():
    return api.LoadThirdPartyModule('WXWIDGETS-3.0.2', 'csnWxWidgets').wxWidgets


def wxMathPlot():
    return api.LoadThirdPartyModule('WXMATHPLOT-0.1.2', 'csnWxMathPlot').wxMathPlot


def libElf():
    return api.LoadThirdPartyModule('LIBELF', 'csnLibElf').libElf


def expat():
    import EXPAT.csnExpat
    return EXPAT.csnExpat.exPat


def vmtk():
    return api.LoadThirdPartyModule('VMTK-0.9', 'csnVMTK').vmtk

######################################################################################
# Modules

def tpExtLibITK():
    import Modules.TpExtLib.csnTpExtLib
    return Modules.TpExtLib.csnTpExtLib.tpExtLibITK

def tpExtLibMITK():
    import Modules.TpExtLib.csnTpExtLib
    return Modules.TpExtLib.csnTpExtLib.tpExtLibMITK

def tpExtLibWxWidgets():
    import Modules.TpExtLib.csnTpExtLib
    return Modules.TpExtLib.csnTpExtLib.tpExtLibWxWidgets

def tpExtLibWxWebUpdate():
    import Modules.TpExtLib.csnTpExtLib
    return Modules.TpExtLib.csnTpExtLib.tpExtLibWxWebUpdate

def tpExtLibBoost():
    import Modules.TpExtLib.csnTpExtLib
    return Modules.TpExtLib.csnTpExtLib.tpExtLibBoost

def tpExtLibUTF():
    import Modules.TpExtLib.csnTpExtLib
    return Modules.TpExtLib.csnTpExtLib.tpExtLibUTF

def tpExtLibVTK():
    import Modules.TpExtLib.csnTpExtLib
    return Modules.TpExtLib.csnTpExtLib.tpExtLibVTK

def guiBridgeLib():
    import Modules.GuiBridgeLib.csnGuiBridgeLib
    return Modules.GuiBridgeLib.csnGuiBridgeLib.guiBridgeLib

def guiBridgeLibQt():
    import Modules.GuiBridgeLib.csnGuiBridgeLib
    return Modules.GuiBridgeLib.csnGuiBridgeLib.guiBridgeLibQt

def guiBridgeLibWxWidgets():
    import Modules.GuiBridgeLib.csnGuiBridgeLib
    return Modules.GuiBridgeLib.csnGuiBridgeLib.guiBridgeLibWxWidgets

def wxMitk():
    import Modules.wxMitk.csnWxMitk
    return Modules.wxMitk.csnWxMitk.wxMitk

def dynLib():
    import Modules.DynLib.csnDynLib
    return Modules.DynLib.csnDynLib.dynLib

def dynWxAGUILib():
    import Modules.DynLib.csnDynLib
    return Modules.DynLib.csnDynLib.dynWxAGUILib

def wflLib():
    import Modules.WflLib.csnWflLib
    return Modules.WflLib.csnWflLib.wflLib


######################################################################################
# GIMIAS

def CreateGimiasApp():
    import Apps.Gimias.csnGIMIAS
    return Apps.Gimias.csnGIMIAS.gimias

def gmCommonObjects():
    import Apps.Gimias.Core.CommonObjects.csnGMCommonObjects
    return Apps.Gimias.Core.CommonObjects.csnGMCommonObjects.gmCommonObjects

def gmDataHandling():
    import Apps.Gimias.Core.DataHandling.csnGMDataHandling
    return Apps.Gimias.Core.DataHandling.csnGMDataHandling.gmDataHandling

def gmFiltering():
    import Apps.Gimias.Core.Filtering.csnGMFiltering
    return Apps.Gimias.Core.Filtering.csnGMFiltering.gmFiltering

def gmKernel():
    import Apps.Gimias.Core.Kernel.csnGMKernel
    return Apps.Gimias.Core.Kernel.csnGMKernel.gmKernel

def gmIO():
    import Apps.Gimias.Core.IO.csnGMIO
    return Apps.Gimias.Core.IO.csnGMIO.gmIO

def gmWorkflow():
    import Apps.Gimias.Core.Workflow.csnGMWorkflow
    return Apps.Gimias.Core.Workflow.csnGMWorkflow.gmWorkflow

def gmProcessors():
    import Apps.Gimias.Core.Processors.csnGMProcessors
    return Apps.Gimias.Core.Processors.csnGMProcessors.gmProcessors

def gmWxEvents():
    import Apps.Gimias.GUI.WxEvents.csnGMWxEvents
    return Apps.Gimias.GUI.WxEvents.csnGMWxEvents.gmWxEvents

def gmWidgets():
    import Apps.Gimias.GUI.Widgets.csnGMWidgets
    return Apps.Gimias.GUI.Widgets.csnGMWidgets.gmWidgets

def gmCoreLight():
    import Apps.Gimias.csnGMCoreLight
    return Apps.Gimias.csnGMCoreLight.gmCoreLight

def gmCore():
    import Apps.Gimias.csnGMCore
    return Apps.Gimias.csnGMCore.gmCore


######################################################################################
# Plugins

def sandboxPlugin():
    import Apps.Plugins.SandboxPlugin.csnSandboxPlugin
    return Apps.Plugins.SandboxPlugin.csnSandboxPlugin.sandboxPlugin

def signalViewerPlugin():
    import Apps.Plugins.SignalViewerPlugin.csnSignalViewerPlugin
    return Apps.Plugins.SignalViewerPlugin.csnSignalViewerPlugin.signalViewerPlugin


def securityPlugin():
    import Apps.Plugins.SecurityPlugin.csnSecurityPlugin
    return Apps.Plugins.SecurityPlugin.csnSecurityPlugin.securityPlugin


def sshPlugin():
    import Apps.Plugins.SshPlugin.csnSshPlugin
    return Apps.Plugins.SshPlugin.csnSshPlugin.sshPlugin


def StatisticsPlugin():
    import Apps.Plugins.StatisticsPlugin.csnStatisticsPlugin
    return Apps.Plugins.StatisticsPlugin.csnStatisticsPlugin.StatisticsPlugin


def XNATPlugin():
    import Apps.Plugins.XNATPlugin.csnXNATPlugin
    return Apps.Plugins.XNATPlugin.csnXNATPlugin.XNATPlugin

def ahePlugin():
    import Apps.Plugins.AhePlugin.csnAhePlugin
    return Apps.Plugins.AhePlugin.csnAhePlugin.ahePlugin

def meshEditorPlugin():
    import Apps.Plugins.MeshEditorPlugin.csnMeshEditorPlugin
    return Apps.Plugins.MeshEditorPlugin.csnMeshEditorPlugin.meshEditorPlugin

def imageToolsPlugin():
    import Apps.Plugins.ImageToolsPlugin.csnImageToolsPlugin
    return Apps.Plugins.ImageToolsPlugin.csnImageToolsPlugin.imageToolsPlugin

def manualSegmentationPlugin():
    import Apps.Plugins.ManualSegmentationPlugin.csnManualSegmentationPlugin
    return Apps.Plugins.ManualSegmentationPlugin.csnManualSegmentationPlugin.manualSegmentationPlugin

def dicomPlugin():
    import Apps.Plugins.DicomPlugin.csnDicomPlugin
    return Apps.Plugins.DicomPlugin.csnDicomPlugin.dicomPlugin

def sceneViewPlugin():
    import Apps.Plugins.SceneViewPlugin.csnSceneViewPlugin
    return Apps.Plugins.SceneViewPlugin.csnSceneViewPlugin.sceneViewPlugin

def mitkPlugin():
    import Apps.Plugins.MITKPlugin.csnMITKPlugin
    return Apps.Plugins.MITKPlugin.csnMITKPlugin.mitkPlugin

def genericSegmentationPlugin():
    import Apps.Plugins.GenericSegmentationPlugin.csnGenericSegmentationPlugin
    return Apps.Plugins.GenericSegmentationPlugin.csnGenericSegmentationPlugin.genericSegmentationPlugin

def tavernaPlugin():
    import Apps.Plugins.TavernaPlugin.csnTavernaPlugin
    return Apps.Plugins.TavernaPlugin.csnTavernaPlugin.tavernaPlugin


def vmtkPlugin():
    import Apps.Plugins.VMTKPlugin.csnVMTKPlugin
    return Apps.Plugins.VMTKPlugin.csnVMTKPlugin.vmtkPlugin


def msvPlugin():
    import Apps.Plugins.MSVPlugin.csnMSVPlugin
    return Apps.Plugins.MSVPlugin.csnMSVPlugin.msvPlugin


def webServicesPlugin():
    import Apps.Plugins.WebServicesPlugin.csnWebServicesPlugin
    return Apps.Plugins.WebServicesPlugin.csnWebServicesPlugin.webServicesPlugin

def clinicalReportPlugin():
    import Apps.Plugins.ClinicalReportPlugin.csnClinicalReportPlugin
    return Apps.Plugins.ClinicalReportPlugin.csnClinicalReportPlugin.clinicalReportPlugin

def contouringPlugin():
    import Apps.Plugins.ContouringPlugin.csnContouringPlugin
    return Apps.Plugins.ContouringPlugin.csnContouringPlugin.contouringPlugin
    
######################################################################################
# Group of commandLinePlugins

def commandLinePlugins():
    import Apps.Gimias.csnGIMIASCommon
    return Apps.Gimias.csnGIMIASCommon.commandLinePlugins

def CollectCommandLinePlugins():
	import Modules.TpExtLib.csnTpExtLibApps
	import SLICERAPPS.csnSlicerApps

######################################################################################
# GIMIAS Header

gimiasHeader = api.CreateCompiledProject("GIMIASHeader", "library")
gimiasHeaderVariables = { "DATA_FOLDER" : "\"%s\"" % os.getenv("GIMIAS_DATA_FOLDER", "C:/Code/Data/gimias-trunk") }
gimiasHeader.CreateHeader("GIMIASHeader.h", gimiasHeaderVariables, "GIMIAS")

######################################################################################
# GIMIAS Plugins

def _AddWidgetModulesMemberFunction(self, _widgetModules, _holdingFolder = None, _useQt = 0):
    """
    Similar to AddCilabLibraryModules, but this time the source code in the widgets folder is added to self.
    _useQt - If true, adds build rules for the ui and moc files .
    """
    if _holdingFolder is None:
        _holdingFolder = "widgets"

    # add sources
    for widgetModule in _widgetModules:
        srcFolder = "%s/%s" % (_holdingFolder, widgetModule)
        self.AddIncludeFolders([srcFolder])
        for extension in ["cxx", "cc", "cpp"]:
            self.AddSources(["%s/*.%s" % (srcFolder, extension)], checkExists = 0, sourceGroup = "Widgets")
        if _useQt:
            self.AddSources(["%s/*.ui" % srcFolder], ui = 1, checkExists = 0, sourceGroup = "WidgetsUI")

        includeFolder = "%s/%s" % (_holdingFolder, widgetModule)
        self.AddIncludeFolders([includeFolder])
        for extension in ["h", "hpp", "txx"]:
            self.AddSources(["%s/*.%s" % (includeFolder, extension)], checkExists = 0, sourceGroup = "Widgets")

def _GetInstallSubFolder(self):
    return "plugins/%s/lib" % self.GetName()

def GimiasPluginProject(_name, callerApi, _sourceRootFolder = None):
    """
    This class is used to build a plugin coming from the CilabApps/Plugins folder. Use AddWidgetModules to add widget
    modules to the plugin.
    """
    if _sourceRootFolder is None:
        _sourceRootFolder = api.FindSourceRootFolder(1)
    pluginName = "GIMIAS%s" % _name
    project = api.CreateCompiledProject(
        _name,
        "dll",
        sourceRootFolder = _sourceRootFolder,
        categories = ["Gimias", "Plugins"]
    )
    #project.applicationsProject = None
    project.AddIncludeFolders(["."])
    #project.AddWidgetModules = new.instancemethod(_AddWidgetModulesMemberFunction, project)
    project.AddCustomMemberFunction("AddWidgetModules", _AddWidgetModulesMemberFunction)
    project.AddCustomMemberFunction("GetInstallSubFolder", _GetInstallSubFolder)

    installSubFolder = project.GetInstallSubFolder()

    project.SetBuildResultsSubFolder(installSubFolder)

    if api.GetCompiler().TargetIsWindows():
        # Windows debug
        installFolder = "%s/debug" % installSubFolder
        project.AddFilesToInstall( project.Glob( "plugin.xml" ), installFolder, debugOnly = 1)
        installFolder = installFolder + "/Filters/"
        project.AddFilesToInstall( project.Glob( "Filters/*.*" ), installFolder, debugOnly = 1)

        # Windows release
        installFolder = "%s/release" % installSubFolder
        project.AddFilesToInstall( project.Glob( "plugin.xml" ), installFolder, releaseOnly = 1)
        installFolder = installFolder + "/Filters/"
        project.AddFilesToInstall( project.Glob( "Filters/*.*" ), installFolder, releaseOnly = 1)
    else:
        # Linux
        project.AddFilesToInstall( project.Glob( "plugin.xml" ), installSubFolder)

        installFolder = installSubFolder + "/Filters"
        project.AddFilesToInstall( project.Glob( "Filters/*.*" ), installFolder)

    return callerApi.RewrapProject(project)
