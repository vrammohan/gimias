/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvWxHtmlWindow.h"
#include "wxDataNavigationWidget.h"
#include "msvWxVTKButtons.h"
#include "msvMultiRenderWindow.h"

#include "coreDataEntityInfoHelper.h"

msvWxHtmlWindow::msvWxHtmlWindow(wxWindow *parent, wxWindowID id, const wxPoint &pos,
                                 const wxSize &size, long style, const wxString &name)
    : wxHtmlWindow(parent, id, pos, size, style, name) {
  m_Dialog = dynamic_cast< wxDataNavigationWidget * >(parent);
  m_RenderWindow = NULL;
  m_Button = NULL;
}

void msvWxHtmlWindow::OnLinkClicked(const wxHtmlLinkInfo &link) {
  wxHtmlLinkEvent event(GetId(), link);
  event.SetEventObject(this);
  wxPostEvent(GetParent()->GetEventHandler(), event);
}

void msvWxHtmlWindow::OnInternalIdle() { wxHtmlWindow::OnInternalIdle(); }

void msvWxHtmlWindow::SetButton(msvWxVTKButtons *button) { m_Button = button; }

void msvWxHtmlWindow::SetRenderWindow(msvMultiRenderWindow *renderWindow) {
  m_RenderWindow = renderWindow;
}

void msvWxHtmlWindow::BuildContents(MODE_TYPE mode) {
  // Set size and HTML contents
  switch (mode) {
  case msvWxHtmlWindow::MODE_TOOLTIP:
    SetPage(BuildToolTip());
    break;
  case msvWxHtmlWindow::MODE_NAVIGATION:
    SetPage(BuildNavigation());
    break;
  case msvWxHtmlWindow::MODE_DESCRIPTION:
    SetPage(BuildDescription());
    break;
  case msvWxHtmlWindow::MODE_COMPLETE_INFORMATION:
    SetPage(BuildCompleteInformation());
    break;
  case msvWxHtmlWindow::MODE_BROWSE_VIEWS:
    SetPage(BuildViews());
    break;
  case msvWxHtmlWindow::MODE_NONE:
    break;
  }
}

std::string msvWxHtmlWindow::BuildToolTip() {
  if (m_Button == NULL) {
    return "";
  }

  wxString text;
  text.Append("<Body bgcolor=\"#FFFFFF\">");
  text.Append("<table border=\"0\"  width=\"100%\" >");

  // Title
  text.Append("<tr>");
  text.Append("<td width=\"95%\">");
  text.Append("<b>" + m_Button->GetName() + "</b>");
  text.Append("</td>");
  text.Append("</tr>");

  // Description 100 characters
  wxString desc = m_Button->getDescription().substr(0, 100);
  text.Append("<tr>");
  text.Append("<td>");
  text.Append(desc);
  text.Append("</td>");
  text.Append("</tr>");

  // Close tags
  text.Append("</table>");
  text.Append("</Body>");
  return text.ToStdString();
}

//------------------------------------------------------------------------------
std::string msvWxHtmlWindow::BuildNavigation() {
  if (m_Button == NULL) {
    return "";
  }

  wxString text;
  text.Append("<Body bgcolor=\"#E6E6FA\">");

  text.Append(BuildHeader());

  text.Append(BuildMenu());

  text.Append(BuildNavigationContent());

  text.Append("</Body>");
  return text.ToStdString();
}

//------------------------------------------------------------------------------
std::string msvWxHtmlWindow::BuildDescription() {
  if (m_Button == NULL) {
    return "";
  }

  wxString text;
  text.Append("<Body bgcolor=\"#E6E6FA\">");

  text.Append(BuildHeader());

  text.Append(BuildMenu());

  text.Append(BuildDescriptionMainContent());

  text.Append("</Body>");
  return text.ToStdString();
}

//------------------------------------------------------------------------------
std::string msvWxHtmlWindow::BuildCompleteInformation() {
  if (m_Button == NULL) {
    return "";
  }

  wxString text;
  text.Append("<Body bgcolor=\"#E6E6FA\">");

  // Header
  text.Append(BuildHeader());

  text.Append(BuildMenu());

  text.Append(BuildGenericDataInformation());

  // Description
  text.Append(m_Button->getDescription());

  text.Append("</Body>");
  return text.ToStdString();
}

//------------------------------------------------------------------------------
std::string msvWxHtmlWindow::BuildGenericDataInformation() {
  wxString text;

  Core::DataEntity::Pointer dataEntity = m_Dialog->GetDataEntity();
  if (dataEntity.IsNull())
    return "";

  // Data type
  std::string type =
      Core::DataEntityInfoHelper::GetEntityTypeAsString(dataEntity->GetType());
  text.Append("<b>Data type: </b>" + type + "<br />");
  if (dataEntity->GetType() == Core::ImageTypeId) {
    std::string modality = Core::DataEntityInfoHelper::GetModalityTypeAsString(
        dataEntity->GetMetadata()->GetModality());
    text.Append("<b>Modality: </b>" + modality + "<br />");
  }
  text.Append("<br />");

  return text.ToStdString();
}

//------------------------------------------------------------------------------
std::string msvWxHtmlWindow::BuildMenu() {
  wxString text;
  switch (m_Dialog->GetMode()) {
  case msvWxHtmlWindow::MODE_NAVIGATION:
    text.Append("<a href=\"back\">Back</a>");
    text.Append("  ");
    text.Append("<a href=\"descriptionView\">Description</a>");
    text.Append("  ");
    text.Append("<b>Browse</b>");
    text.Append("  ");
    text.Append("<a href=\"views\">Views</a>");
    text.Append("  ");
    text.Append("<a href=\"more\">More</a>");
    text.Append("<br /><br />");
    break;
  case msvWxHtmlWindow::MODE_DESCRIPTION:
    text.Append("<b>Description</b>");
    text.Append("  ");
    text.Append("<a href=\"browseView\">Browse</a>");
    text.Append("  ");
    text.Append("<a href=\"views\">Views</a>");
    text.Append("  ");
    text.Append("<a href=\"more\">More</a>");
    break;
  case msvWxHtmlWindow::MODE_COMPLETE_INFORMATION:
    text.Append("<div><a href=\"descriptionView\">Description</a></div>");
    text.Append("  ");
    text.Append("<a href=\"browseView\">Browse</a>");
    text.Append("  ");
    text.Append("<a href=\"views\">Views</a>");
    text.Append("  ");
    text.Append("<a href=\"more\">More</a>");
    text.Append("<br /><br />");
    break;
  case msvWxHtmlWindow::MODE_BROWSE_VIEWS:
    text.Append("<div><a href=\"descriptionView\">Description</a></div>");
    text.Append("  ");
    text.Append("<a href=\"browseView\">Browse</a>");
    text.Append("  ");
    text.Append("<b>Views</b>");
    text.Append("  ");
    text.Append("<a href=\"more\">More</a>");
    text.Append("<br /><br />");
    break;
  }
  return text.ToStdString();
}

//------------------------------------------------------------------------------
std::string msvWxHtmlWindow::BuildNavigationContent() {
  wxString text;
  Core::DataEntity::Pointer dataEntity = m_Dialog->GetDataEntity();
  if (dataEntity.IsNull()) {
    return "";
  }
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();

  // Get children of current Data
  text.Append("<table border=\"1\" width=\"100%\" cellspacing=\"0\">");
  Core::DataEntity::ChildrenListType children = dataEntity->GetChildrenList();
  for (int i = 0; i < children.size(); i++) {
    text.Append("<tr>");

    std::stringstream streamId;
    streamId << children[i]->GetId();

    bool isZoomable = m_RenderWindow->IsZoomable(children[i]);
    bool isSession = children[i]->GetType() == Core::SessionTypeId;
    bool isLoaded = m_RenderWindow->IsLoaded(children[i]);
    bool isShown = m_RenderWindow->GetRenderingTree()->IsDataEntityShown(children[i]);

    // Snapshot
    Core::DataEntityPreview::Pointer preview = children[i]->GetPreview();
    text.Append("<td width=\"80\">");
    if (isZoomable)
      text.Append("<a href=\"image:" + streamId.str() + "\">");
    text.Append("<img  height=\"80\" width=\"80\" src=\"" + preview->GetTmpFileName() +
                "\"/>");
    if (isZoomable)
      text.Append("</a>");
    text.Append("</td>");

    // Name + eye
    text.Append("<td>");

    text.Append("<table border=\"0\" width=\"100%\" cellspacing=\"0\">");
    text.Append("<tr>");
    text.Append("<td>");
    text.Append("<font face=\"arial\" size=\"5\">");
    if (isSession)
      text.Append("<b>");
    std::string name = children[i]->GetMetadata()->GetName();
    text.Append("<a href=\"nav:" + streamId.str() + "\">");
    text.Append(name);
    text.Append("</a>");
    if (isSession)
      text.Append("</b>");
    text.Append("</font>");
    text.Append("</td>");

    // Eye
    std::string eye = isShown ? "eye.png" : "eyeNo.png";
    std::string action = isShown ? "hide" : "show";
    text.Append("<td align=\"right\">");
    if (isLoaded)
      text.Append("<a href=\"" + action + ":" + streamId.str() + "\">");
    text.Append("<img src=\"" + settings->GetCoreResourceForFile(eye) + "\"/>");
    if (isLoaded)
      text.Append("</a>");
    text.Append("</td>");
    text.Append("</tr>");
    text.Append("</table>");

    text.Append("</td>");

    text.Append("</tr>");
  }
  text.Append("</table>");

  return text.ToStdString();
}

std::string msvWxHtmlWindow::BuildNavigationLink() {
  wxString text;
  Core::DataEntity::Pointer dataEntity = m_Dialog->GetDataEntity();
  if (dataEntity.IsNull())
    return "";

  text.Append("<font face=\"arial\" size=\"1\">");
  text.Append("<table border=\"0\"><tr><td height=\"200\">");
  wxString content;
  while (dataEntity) {
    // Only session data and no root node
    if (dataEntity->GetType() == Core::SessionTypeId && dataEntity->GetId() != 1) {
      std::string name = dataEntity->GetMetadata()->GetName();
      std::stringstream streamId;
      streamId << dataEntity->GetId();
      content = "<a href=\"nav:" + streamId.str() + "\">" + name + "</a> &gt; " + content;
    }
    dataEntity = dataEntity->GetFather();
  }
  text.Append(content);
  text.Append("</td></tr></table>");
  text.Append("</font>");

  return text.ToStdString();
}

std::string msvWxHtmlWindow::BuildDescriptionMainContent() {
  wxString text;

  // Table 2: Description + snapshot
  text.Append("<table border=\"0\"  width=\"100%\" >");

  // Description
  text.Append("<tr>");
  text.Append("<td>");
  text.Append(BuildGenericDataInformation());
  text.Append(m_Button->getDescription().substr(0, 150));
  if (m_Button->getDescription().size() > 150) {
    text.Append("...<a href=\"read more\"> Read more</a>");
  }
  text.Append("</td>");

  bool isZoomable = m_RenderWindow->IsZoomable(m_Dialog->GetDataEntity());

  // Snapshot
  text.Append("<td align=\"right\" colspan=\"2\">");

  std::stringstream streamId;
  streamId << m_Dialog->GetDataEntity()->GetId();
  if (isZoomable)
    text.Append("<a href=\"image:" + streamId.str() + "\">");
  text.Append("<img  height=\"80\" width=\"80\" src=\"" + m_Button->getPreviewFName() +
              "\"/>");
  if (isZoomable)
    text.Append("</a>");
  text.Append("</td>");
  text.Append("</tr>");

  // End Table 2
  text.Append("</table>");

  return text.ToStdString();
}

std::string msvWxHtmlWindow::BuildHeader() {
  Core::DataEntity::Pointer dataEntity = m_Dialog->GetDataEntity();
  if (m_Button == NULL || m_Dialog->GetDataEntity().IsNull()) {
    return "";
  }

  wxString text;
  text.Append(BuildNavigationLink());

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();

  // Table 1
  text.Append(
      "<table border=\"0\"  width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" >");
  text.Append("<tr>");

  bool isSession = dataEntity->GetType() == Core::SessionTypeId;
  wxString color = m_RenderWindow->IsLoaded(dataEntity) ? "#000000" : "#808080";
  bool modified = m_Dialog->CheckModified(dataEntity->GetId());

  // Small Snapshot
  std::string tempFilname = settings->GetProjectHomePath() + "/Temp/Temp.png";
  Core::DataEntityPreview::Pointer preview = dataEntity->GetPreview();
  if (preview->IsOk()) {
    wxImage image = boost::any_cast< wxImage >(preview->GetImage());
    image.Scale(40, 40, wxIMAGE_QUALITY_HIGH).SaveFile(tempFilname);
  } else {
    itksys::SystemTools::RemoveFile(tempFilname.c_str());
  }

  bool isZoomable = m_RenderWindow->IsZoomable(dataEntity);
  std::stringstream streamId;
  streamId << dataEntity->GetId();
  text.Append("<td width=\"50\">");
  if (isZoomable)
    text.Append("<a href=\"image:" + streamId.str() + "\">");
  text.Append("<img  height=\"40\" width=\"40\" src=\"" + tempFilname + "\"/>");
  if (isZoomable)
    text.Append("</a>");
  text.Append("</td>");

  // Title
  text.Append("<td width=\"95%\">");
  text.Append("<font face=\"arial\" size=\"5\" color=\"" + color + "\">");
  if (isSession)
    text.Append("<b>");
  text.Append(m_Button->GetName());
  if (modified)
    text.Append(" *");
  if (isSession)
    text.Append("</b>");
  text.Append("</font>");
  text.Append("</td>");

  // Close row 1
  text.Append("</tr>");

  // Loading
  if (m_RenderWindow->IsReading()) {
    text.Append("<tr>");
    text.Append("<td align=\"center\" colspan=\"2\" bgcolor=\"#C8C8C8\">");
    text.Append("<font face=\"arial\" size=\"4\" >");
    text.Append("<br />");
    text.Append("<br />");
    text.Append("<b>Loading. Please wait...</b>");
    text.Append("<br />");
    text.Append("</font>");
    text.Append("</td>");
    text.Append("</tr>");
  }

  // Close table
  text.Append("</table>");

  return text.ToStdString();
}

std::string msvWxHtmlWindow::BuildViews() {
  if (m_Button == NULL) {
    return "";
  }

  wxString text;
  text.Append("<Body bgcolor=\"#E6E6FA\">");

  text.Append(BuildHeader());

  text.Append(BuildMenu());

  text.Append(BuildViewsMainContent());

  text.Append("</Body>");
  return text.ToStdString();
}

std::string msvWxHtmlWindow::BuildViewsMainContent() {
  blTagMap::Pointer views = m_Dialog->GetViewManager()->GetViews();
  if (views.IsNull()) {
    return "";
  }

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  wxString text;

  // Get children of current Data
  text.Append("<table border=\"1\" width=\"100%\" cellspacing=\"0\">");
  blTagMap::Iterator itView;
  for (itView = views->GetIteratorBegin(); itView != views->GetIteratorEnd(); itView++) {
    text.Append("<tr>");

    blTagMap::Pointer view = itView->second->GetValueCasted< blTagMap::Pointer >();

    if (view->GetTag("Visible").IsNull() ||
        !view->GetTag("Visible")->GetValueCasted< bool >())
      continue;

    // Get image, name
    wxImage image = m_Dialog->GetViewManager()->GetImage(view);
    std::string name = m_Dialog->GetViewManager()->GetName(view);
    std::string tempFilname = m_Dialog->GetViewManager()->SaveTempPreview(view);
    bool isSelected = view == m_Dialog->GetViewManager()->GetSelectedView();

    // Snapshot
    text.Append("<td width=\"80\">");
    text.Append("<a href=\"view:" + name + "\">");
    text.Append("<img  height=\"80\" width=\"80\" src=\"" + tempFilname + "\"/>");
    text.Append("</a>");
    text.Append("</td>");

    // Name
    text.Append("<td>");
    text.Append("<font face=\"arial\" size=\"5\">");
    if (isSelected)
      text.Append("<b>");
    text.Append(name);
    if (isSelected)
      text.Append("</b>");
    text.Append("</font>");
    text.Append("</td>");

    text.Append("</tr>");
  }
  text.Append("</table>");

  return text.ToStdString();
}
