/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "WebServicesPreferencesPanelWidget.h"
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreLogFileViewer.h"
#include "itksys/SystemTools.hxx"
#include "coreDirectory.h"
#include "blTextUtils.h"

BEGIN_EVENT_TABLE(WebServicesPreferencesPanelWidget, WebServicesPreferencesPanelWidgetUI)
EVT_COMMAND(wxID_ANY, wxEVT_WEB_SERVER_STATUS,
            WebServicesPreferencesPanelWidget::OnServerStatus)
END_EVENT_TABLE();

WebServicesPreferencesPanelWidget::WebServicesPreferencesPanelWidget(
    WebServerProcessor::Pointer processor, wxWindow *parent, int id /*= wxID_ANY*/,
    const wxPoint &pos /*= wxDefaultPosition*/, const wxSize &size /*= wxDefaultSize*/,
    long style /* = 0*/)
    : WebServicesPreferencesPanelWidgetUI(parent, id, pos, size, style) {
  m_Processor = processor;

  // Update the already log info
  if (m_Processor->IsStarted()) {
    std::list< std::string > words;
    blTextUtils::ParseLine(m_Processor->GetLogStream(), '\n', words);

    std::list< std::string >::iterator it;
    for (it = words.begin(); it != words.end(); it++) {
      m_lblStatus->SetLabel(*it);
      m_txtLog->AppendText(*it + "\n");
    }
  }

  m_Processor->SetStatusObserver(this);

  m_LogViewer = NULL;
  m_Configuration = NULL;
}

WebServicesPreferencesPanelWidget::~WebServicesPreferencesPanelWidget() {
  // We don't need to destroy anything because all the child windows
  // of this wxWindow are destroyed automatically
  m_Processor->SetStatusObserver(NULL);
}

void WebServicesPreferencesPanelWidget::OnInit() {}

void WebServicesPreferencesPanelWidget::UpdateWidget() {
  m_btnStart->SetValue(m_Processor->IsStarted());
}

void WebServicesPreferencesPanelWidget::UpdateData() { m_Processor->ReadSettings(); }

void WebServicesPreferencesPanelWidget::OnStart(wxCommandEvent &event) {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  UpdateData();

  if (event.IsChecked()) {
    m_Processor->Start();
    settings->SetPluginProperty("WebServicesPlugin", "status", "enabled");
  } else {
    m_Processor->Stop();
    settings->SetPluginProperty("WebServicesPlugin", "status", "disabled");
  }
}

void WebServicesPreferencesPanelWidget::OnServerStatus(wxCommandEvent &event) {
  m_lblStatus->SetLabel(event.GetString());
  m_txtLog->AppendText(event.GetString() + "\n");
}

void WebServicesPreferencesPanelWidget::OnShowLog(wxCommandEvent &event) {
  if (!m_LogViewer) {
    m_LogViewer = new Core::Widgets::LogFileViewer(this);

    Core::Runtime::Settings::Pointer settings;
    settings = Core::Runtime::Kernel::GetApplicationSettings();
    m_LogViewer->SetFileName(settings->GetProjectHomePath() + "/" + "axis2.log");
    m_LogViewer->SetSize(600, 600);
    m_LogViewer->CenterOnParent();
  }

  m_LogViewer->Show();
}

void WebServicesPreferencesPanelWidget::OnConfiguration(wxCommandEvent &event) {
  if (!m_Configuration) {
    m_Configuration =
        new WebServerConfigurationPanelWidget(this, wxID_ANY, "Server configuration");
  }

  m_Configuration->UpdateWidget();
  m_Configuration->Show();
}
