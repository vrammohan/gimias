/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "WebServerThread.h"

#include <axis2_http_server.h>
#include <axis2_http_transport.h>
#include <platforms/axutil_platform_auto_sense.h>
#include <stdio.h>
#include <stdlib.h>
#include <axutil_error_default.h>
#include <axutil_log_default.h>
#include <axutil_thread_pool.h>
#include <signal.h>
#include <axutil_types.h>
#include <ctype.h>
#include <axiom_xml_reader.h>
#include <axutil_version.h>
#include <axutil_file_handler.h>

#include "axis2_options.h"
#include "axis2_http_header.h"

#include "coreSettings.h"

extern "C" {
AXIS2_IMPORT extern axis2_char_t *axis2_request_url_prefix;
AXIS2_IMPORT extern int axis2_http_socket_read_timeout;
AXIS2_IMPORT axis2_char_t *AXIS2_LIB_FOLDER;
}

axutil_env_t *WebServerThread::m_Env = NULL;
axis2_transport_receiver_t *WebServerThread::m_Server = NULL;

DEFINE_EVENT_TYPE(wxEVT_WEB_SERVER_STATUS);

WebServerThread::WebServerThread() : wxThread(wxTHREAD_JOINABLE) {
  m_Allocator = NULL;
  m_Observer = NULL;
}

WebServerThread::~WebServerThread() {
  axutil_allocator_t *allocator = NULL;
  if (m_Server) {
    axis2_transport_receiver_free(m_Server, m_Env);
    m_Server = NULL;
  }
  if (m_Env) {
    allocator = m_Env->allocator;
    axutil_env_free(m_Env);
    m_Env = NULL;
  }
  /*axutil_allocator_free(allocator); */
  axiom_xml_reader_cleanup();

  UpdateStatus("Stopped");
}

void WebServerThread::InitSystemEnv() {
  axutil_error_t *error = axutil_error_create(m_Allocator);
  axutil_log_t *log = axutil_log_create(m_Allocator, NULL, (char *)m_LogFile.c_str());
  /* if (!log) */

  /* 		  log = axutil_log_create_default (allocator); */
  axutil_thread_pool_t *thread_pool = axutil_thread_pool_init(m_Allocator);
  /* We need to init the parser in main thread before spawning child
  * threads
  */
  axiom_xml_reader_init();
  m_Env =
      axutil_env_create_with_error_log_thread_pool(m_Allocator, error, log, thread_pool);
}

/**
 * Signal handler
 */
void WebServerThread::sig_handler(int signal) {

  if (!m_Env) {
    AXIS2_LOG_ERROR(m_Env->log, AXIS2_LOG_SI,
                    "Received signal %d, unable to proceed system_env is NULL ,\
                         system exit with -1",
                    signal);
    _exit(-1);
  }

  switch (signal) {
  case SIGINT: {
    /* Use of SIGINT in Windows is valid, since we have a console application
     * Thus, eventhough this may a single threaded application, it does work.
     */
    AXIS2_LOG_INFO(m_Env->log, "Received signal SIGINT. Server "
                               "shutting down");
    if (m_Server) {
      axis2_http_server_stop(m_Server, m_Env);
    }

    AXIS2_LOG_INFO(m_Env->log, "Shutdown complete ...");
  } break;
#ifndef WIN32
  case SIGPIPE: {
    AXIS2_LOG_INFO(m_Env->log, "Received signal SIGPIPE.  Client "
                               "request serve aborted");
    return;
  } break;
#endif
  case SIGSEGV: {
    fprintf(stderr, "Received deadly signal SIGSEGV. Terminating\n");
    _exit(-1);
  } break;
  }
}

void *WebServerThread::Entry() {
  int log_file_size = AXUTIL_LOG_FILE_SIZE;
  unsigned int file_flag = 0;
  axutil_log_levels_t log_level = AXIS2_LOG_LEVEL_DEBUG;
  int port = atoi(m_Port.c_str());
  axis2_status_t status;
  unsigned int len;

  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  m_LogFile = settings->GetProjectHomePath() + "/" + "axis2.log";
  m_RepoPath = settings->GetProjectHomePath() + "/wsrepo";
  m_LibFolder = settings->GetApplicationPath();

  axis2_char_t *repo_path = (char *)m_RepoPath.c_str();
  AXIS2_LIB_FOLDER = (char *)m_LibFolder.c_str();

  /* Set the service URL prefix to be used. This could default to services if not
   set with AXIS2_REQUEST_URL_PREFIX macro at compile time */
  axis2_request_url_prefix = AXIS2_REQUEST_URL_PREFIX;

  m_Allocator = axutil_allocator_init(NULL);
  if (!m_Allocator) {
    UpdateStatus("Cannot create allocator");
    return NULL;
  }

  InitSystemEnv();
  m_Env->log->level = log_level;
  m_Env->log->size = log_file_size;

  axutil_error_init();

  signal(SIGINT, sig_handler);
#ifndef WIN32
  signal(SIGPIPE, sig_handler);
#endif

  AXIS2_LOG_INFO(m_Env->log, "Starting Axis2 HTTP server....");
  UpdateStatus("Starting Axis2 HTTP server....");

  std::stringstream stream;
  stream << "Apache Axis2/C version in use : " << axis2_version_string();
  AXIS2_LOG_INFO(m_Env->log, stream.str().c_str());
  UpdateStatus(stream.str());

  std::stringstream stream1;
  stream1 << "Server port : " << port;
  AXIS2_LOG_INFO(m_Env->log, stream1.str().c_str());
  UpdateStatus(stream1.str());

  std::stringstream stream2;
  stream2 << "Repo location : " << repo_path;
  AXIS2_LOG_INFO(m_Env->log, stream2.str().c_str());
  UpdateStatus(stream2.str());

  std::stringstream stream3;
  stream3 << "Read Timeout : " << axis2_http_socket_read_timeout << " ms";
  AXIS2_LOG_INFO(m_Env->log, stream3.str().c_str());
  UpdateStatus(stream3.str());

  status = axutil_file_handler_access(repo_path, AXIS2_R_OK);
  if (status == AXIS2_SUCCESS) {
    len = (unsigned int)strlen(repo_path);
    /* We are sure that the difference lies within the unsigned int range */
    if ((len >= 9) && !strcmp((repo_path + (len - 9)), "axis2.xml")) {
      file_flag = 1;
    }
  } else {
    std::stringstream stream4;
    stream4 << "provided repo path " << repo_path
            << " does "
               "not exist or no permissions to read, set "
               "repo_path to DEFAULT_REPO_PATH";

    AXIS2_LOG_WARNING(m_Env->log, AXIS2_LOG_SI, stream4.str().c_str());
    UpdateStatus(stream4.str());
  }

  if (!file_flag)
    m_Server = axis2_http_server_create(m_Env, repo_path, port);
  else
    m_Server = axis2_http_server_create_with_file(m_Env, repo_path, port);

  if (!m_Server) {
    AXIS2_LOG_ERROR(m_Env->log, AXIS2_LOG_SI, "Server creation failed: Error code:"
                                              " %d :: %s",
                    m_Env->error->error_number, AXIS2_ERROR_GET_MESSAGE(m_Env->error));
    std::stringstream stream;
    stream << "Server creation failed: Error code: " << m_Env->error->error_number
           << " :: " << AXIS2_ERROR_GET_MESSAGE(m_Env->error);
    UpdateStatus(stream.str());
    return NULL;
  }
  UpdateStatus("Started HTTP Server ...");

  // Configure custom HTTP headers

  /* Create EPR with given address */
  axis2_endpoint_ref_t *endpoint_ref = NULL;
  endpoint_ref = axis2_endpoint_ref_create(
      m_Env, "http://localhost:9090/axis2/services/wsGimias?wsdl");

  // Get the options for the client stub
  axis2_options_t *options = axis2_options_create(m_Env);
  axis2_options_set_to(options, m_Env, endpoint_ref);

  // Create a list of headers to be sent along with the HTTP request and add them to the
  // stub
  axutil_array_list_t *headers = axutil_array_list_create(m_Env, 1);
  axis2_http_header_t *http_header =
      axis2_http_header_create(m_Env, "Access-Control-Allow-Origin", "*");
  axutil_array_list_add(headers, m_Env, http_header);
  axis2_options_set_http_headers(options, m_Env, headers);

  if (axis2_transport_receiver_start(m_Server, m_Env) == AXIS2_FAILURE) {
    AXIS2_LOG_ERROR(m_Env->log, AXIS2_LOG_SI, "Server start failed: Error code:"
                                              " %d :: %s",
                    m_Env->error->error_number, AXIS2_ERROR_GET_MESSAGE(m_Env->error));
    std::stringstream stream;
    stream << "Server creation failed: Error code: " << m_Env->error->error_number
           << " :: " << AXIS2_ERROR_GET_MESSAGE(m_Env->error);
    UpdateStatus(stream.str());
    return NULL;
  }

  return NULL;
}

void WebServerThread::UpdateStatus(const std::string &status) {
  if (m_Observer) {
    wxCommandEvent evt(wxEVT_WEB_SERVER_STATUS, wxID_ANY);
    evt.SetEventObject(m_Observer);
    evt.SetString(status);
    // m_Observer->AddPendingEvent( evt );
    wxQueueEvent(m_Observer, evt.Clone());
  } else {
    m_StreamLog.append(status + "\n");
  }
}

wxEvtHandler *WebServerThread::GetObserver() const { return m_Observer; }

void WebServerThread::SetObserver(wxEvtHandler *val) { m_Observer = val; }

std::string WebServerThread::GetLogFile() const { return m_LogFile; }

void WebServerThread::SetLogFile(std::string val) { m_LogFile = val; }

std::string WebServerThread::GetPort() const { return m_Port; }

void WebServerThread::SetPort(std::string val) { m_Port = val; }

std::string WebServerThread::GetLogStream() { return m_StreamLog; }
