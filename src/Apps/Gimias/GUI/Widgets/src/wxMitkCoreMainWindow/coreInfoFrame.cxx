/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreInfoFrame.h"

coreInfoFrame::coreInfoFrame(wxWindow *parent, const wxString &message)
    : wxFrame(parent, wxID_ANY, wxT("Busy"), wxDefaultPosition, wxDefaultSize,

#if defined(__WXX11__)
              wxRESIZE_BORDER
#else
              wxSIMPLE_BORDER
#endif
                  | wxFRAME_TOOL_WINDOW) {
  m_panel = new wxPanel(this);
  m_text = new wxStaticText(m_panel, wxID_ANY, message);

  m_panel->SetCursor(*wxHOURGLASS_CURSOR);
  m_text->SetCursor(*wxHOURGLASS_CURSOR);

  this->SetMessage(message);
}

void coreInfoFrame::SetMessage(wxString message) {
  m_text->SetLabel(message);

  // make the frame of at least the standard size (400*80) but big enough
  // for the text we show
  wxSize sizeText = m_text->GetBestSize();
  SetClientSize(wxMax(sizeText.x, 340) + 60, wxMax(sizeText.y, 40) + 40);

  // need to size the panel correctly first so that text->Centre() works
  m_panel->SetSize(GetClientSize());
  m_text->Update();

  m_text->Centre(wxBOTH);
  Centre(wxBOTH);

  this->Update();
}