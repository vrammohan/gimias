/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _WebServerThread_H
#define _WebServerThread_H

#include "corePluginMacros.h"

#include <sstream>

#include "axutil_env.h"

struct axutil_allocator;
typedef struct axutil_allocator axutil_allocator_t;

struct axutil_env;
typedef struct axutil_env axutil_env_t;

struct axis2_transport_receiver;
typedef struct axis2_transport_receiver axis2_transport_receiver_t;

BEGIN_DECLARE_EVENT_TYPES()
DECLARE_EXPORTED_EVENT_TYPE(PLUGIN_EXPORT, wxEVT_WEB_SERVER_STATUS, -1)
END_DECLARE_EVENT_TYPES()

/**
Starts/stop the Web services server.

This server is implemented using AXIS2C library.

The server expects the following folder structure:
wsrepo/modules/
        addressing/
                axis2_mod_addr.dll
                module.xml
        logging/
                axis2_mod_log.dll
                module.xml
wsrepo/services
        wsGimias/
                services.xml
                wsGimias.dll
                wsGimias.wsdl
wsrepo/axis2.xml

\ingroup WebServicesPlugin
\author Xavi Planes
\date Sept 2011
*/
class PLUGIN_EXPORT WebServerThread : public wxThread {
public:
  //!
  WebServerThread();

  //!
  ~WebServerThread();

  //!
  virtual void *Entry();

  //!
  wxEvtHandler *GetObserver() const;
  void SetObserver(wxEvtHandler *val);

  //!
  std::string GetLogFile() const;
  void SetLogFile(std::string val);

  //!
  std::string GetPort() const;
  void SetPort(std::string val);

  //! Get log when observer is not configured
  std::string GetLogStream();

private:
  //!
  static void sig_handler(int signal);

  //!
  void InitSystemEnv();

  //! Notify observers
  void UpdateStatus(const std::string &status);

private:
  //!
  static axutil_env_t *m_Env;

  //!
  axutil_allocator_t *m_Allocator;

  //!
  static axis2_transport_receiver_t *m_Server;

  //! Main Repository path with modules, services and lib
  std::string m_RepoPath;

  //! Folder where to find the main libraries
  std::string m_LibFolder;

  //! Log file path
  std::string m_LogFile;

  //! Receive the status events
  wxEvtHandler *m_Observer;

  //!
  std::string m_Port;

  //! In case Observer is not configured, save it in a stream
  std::string m_StreamLog;
};

#endif //_WebServerThread_H
