/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportWidgetTool.h"

#include "capture_screen.xpm"
#include "report_icon.xpm"
#include "new_comment_icon.xpm"

ClinicalReportWidgetTool::ClinicalReportWidgetTool(wxWindow *parent, int id,
                                                   const wxPoint &pos, const wxSize &size,
                                                   long style)
    : ClinicalReportWidgetToolUI(parent, id, pos, size, style) {
  m_bmpBtnCaptureScreen->SetBitmapLabel(wxBitmap(capture_screen_xpm));
  m_bmpBtnNewComment->SetBitmapLabel(wxBitmap(new_comment_icon_xpm));
  m_bmpBtnClinicalReportDesigner->SetBitmapLabel(wxBitmap(report_icon_xpm));

  m_Processor = ClinicalReportProcessor::New();
}

void ClinicalReportWidgetTool::OnInit() { GetReportToolbar()->SetProcessor(m_Processor); }

void ClinicalReportWidgetTool::OnNewSnapshot(wxCommandEvent &event) {
  wxCommandEvent cmd(wxEVT_COMMAND_TOOL_CLICKED);
  cmd.SetId(wxID_ADD_SCREENSHOT_TO_CLINICAL_REPORT);
  GetReportToolbar()->GetEventHandler()->ProcessEvent(cmd);
}

void ClinicalReportWidgetTool::OnNewComment(wxCommandEvent &event) {
  wxCommandEvent cmd(wxEVT_COMMAND_TOOL_CLICKED);
  cmd.SetId(wxID_ADD_COMMENT_TO_CLINICAL_REPORT);
  GetReportToolbar()->GetEventHandler()->ProcessEvent(cmd);
}

void ClinicalReportWidgetTool::OnClinicalReportDesginer(wxCommandEvent &event) {
  wxCommandEvent cmd(wxEVT_COMMAND_TOOL_CLICKED);
  cmd.SetId(wxID_OPEN_CLINICAL_REPORT_DESIGNER);
  GetReportToolbar()->GetEventHandler()->ProcessEvent(cmd);
}

Core::BaseProcessor::Pointer ClinicalReportWidgetTool::GetProcessor() {
  return m_Processor.GetPointer();
}

Core::Widgets::ClinicalReportToolbar *ClinicalReportWidgetTool::GetReportToolbar() {
  if (GetPluginTab() == NULL) {
    throw Core::Exceptions::Exception("ClinicalReportWidgetTool::GetReportToolbar",
                                      "Cannot find Plugin Tab");
  }

  wxWindow *win = GetPluginTab()->GetWindow("Clinical Report Toolbar");
  Core::Widgets::ClinicalReportToolbar *toolbar;
  toolbar = dynamic_cast< Core::Widgets::ClinicalReportToolbar * >(win);
  if (toolbar == NULL) {
    throw Core::Exceptions::Exception("ClinicalReportWidgetTool::OnNewSnapshot",
                                      "Cannot find Clinical Report Toolbar");
  }

  return toolbar;
}