// Copyright 2009 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona,
// Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.

#include "vmtkVesselCenterlinePanelWidget.h"
#include "mitkProperties.h"
#include "blMitkUnicode.h"
#include "coreDataContainer.h"
#include "mitkMaterialProperty.h"
#include "coreKernel.h"
#include "corePointInteractorPointSet.h"
#include "coreDataTreeMITKHelper.h"
#include "coreProcessorInputWidget.h"
#include "mitkBaseRenderer.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityTypes.h"

VesselCenterlinePanelWidget::VesselCenterlinePanelWidget(wxWindow *parent, int id,
                                                         const wxPoint &pos,
                                                         const wxSize &size, long style)
    : wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL) {

  m_seedsStaticBox = new wxStaticBox(this, -1, wxT("Seeds selection"));
  toggleBtnSources =
      new wxToggleButton(this, wxID_btnSources, wxT("Select source points"));
  toggleBtnTargets =
      new wxToggleButton(this, wxID_btnTargets, wxT("Select target points"));
  btnCenterline =
      new wxButton(this, wxID_btnCenterline, wxT("Compute Vessel Centerline"));
  m_chkAppendEndPoints = new wxCheckBox(this, wxID_ANY, wxT("Append EndPoints"));
  /*btnOpenProfiles = new wxButton(this, wxID_btnOpenProfiles, wxT("Compute inlets and
  outlets"));
  btnCenterlineOP = new wxButton(this, wxID_btnCenterlineOP, wxT("Compute Vessel
  Centerline"));

  m_openProfilesStaticBox = new wxStaticBox(this, -1, wxT("Open profiles selection"));
  m_staticTextInlet = new wxStaticText(this, wxID_ANY, wxT("Inlet:"));
  m_textCtrlInlet = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
  m_staticTextOutlet = new wxStaticText(this, wxID_ANY, wxT("Outlet:"));
  m_textCtrlOutlet = new wxTextCtrl(this, wxID_ANY, wxEmptyString);

  btnOpenProfiles->Enable();
  */
  btnCenterline->Disable();
  toggleBtnTargets->Disable();
  toggleBtnSources->Enable();

  m_processor = VesselCenterlineProcessor::New();

  m_processor->GetOutputDataEntityHolder(
                 VesselCenterlineProcessor::OUTPUT_CAPSCENTER_POINTS)
      ->AddObserver(this, &VesselCenterlinePanelWidget::OnModifiedOutputCapCenters);

  m_sourceEnabled = false;
  m_targetEnabled = false;

  do_layout();
  UpdateWidget();
}
VesselCenterlinePanelWidget::~VesselCenterlinePanelWidget() {}

BEGIN_EVENT_TABLE(VesselCenterlinePanelWidget, wxPanel)
// begin wxGlade: ptRingCutPanelWidget::event_table
EVT_TOGGLEBUTTON(wxID_btnSources, VesselCenterlinePanelWidget::OnButtonSources)
EVT_TOGGLEBUTTON(wxID_btnTargets, VesselCenterlinePanelWidget::OnButtonTargets)
EVT_BUTTON(wxID_btnCenterline, VesselCenterlinePanelWidget::OnButtonCenterline)
// EVT_BUTTON(wxID_btnOpenProfiles, VesselCenterlinePanelWidget::OnButtonOpenProfiles)
// EVT_BUTTON(wxID_btnCenterlineOP,
// VesselCenterlinePanelWidget::OnButtonCenterlineWithOpenProfiles)

// end wxGlade
END_EVENT_TABLE();

void VesselCenterlinePanelWidget::OnButtonSources(wxCommandEvent &event) {

  try {

    // the input mesh
    Core::vtkPolyDataPtr inputMesh;
    Core::DataEntityHelper::GetProcessingData(
        m_processor->GetInputDataEntityHolder(VesselCenterlineProcessor::INPUT_SURFACE),
        inputMesh);
    if (inputMesh == NULL)
      throw Core::Exceptions::Exception("vmtkVesselCenterlineProcessor::Update",
                                        "Input surface is not set");

    m_processor->SetNumSource(0);
    if (m_sourceEnabled) {
      DoCancel(SOURCE_BUTTON);
      return;
    }

    // reset opacity of input surface to 1
    mitk::DataTreeNode::Pointer node;
    boost::any anyData = GetRenderingTree()->GetNode(
        m_processor->GetInputDataEntity(VesselCenterlineProcessor::INPUT_SURFACE));
    Core::CastAnyProcessingData(anyData, node);

    if (node.IsNotNull()) {
      node->SetOpacity(1.0);
    }
    mitk::RenderingManager::GetInstance()->RequestUpdateAll();

    // enable interaction
    ConnectInteractor();
    m_sourceEnabled = true;
    UpdateWidget();
  } catch (Core::Exceptions::Exception e) {
    throw e;
  } catch (...) {
  }
}

void VesselCenterlinePanelWidget::OnButtonTargets(wxCommandEvent &event) {
  if (m_targetEnabled) {
    DoCancel(TARGET_BUTTON);
    return;
  }

  if (m_processor->GetInputDataEntity(VesselCenterlineProcessor::INPUT_SEED_POINTS)
          .IsNull())
    return;

  Core::vtkPolyDataPtr points;
  Core::DataEntityHelper::GetProcessingData(
      m_processor->GetInputDataEntityHolder(VesselCenterlineProcessor::INPUT_SEED_POINTS),
      points);
  int numSources = points->GetNumberOfPoints();
  m_processor->SetNumSource(numSources);

  m_sourceEnabled = false;
  toggleBtnSources->Disable();
  m_targetEnabled = true;
  UpdateWidget();
}

void VesselCenterlinePanelWidget::OnButtonCenterline(wxCommandEvent &event) {
  m_processor->SetSeedSelectorType(VesselCenterlineProcessor::SEED_POINTLIST);
  m_processor->SetAppendEndPoints(m_chkAppendEndPoints->GetValue());
  UpdateProcessor();
  btnCenterline->Disable();
  toggleBtnSources->Enable();
  toggleBtnTargets->Disable();
  m_sourceEnabled = false;
  m_targetEnabled = false;
  DisconnectInteractor();
  UpdateWidget();
  m_processor->SetNumSource(0);
  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
  list->Remove(
      m_processor->GetInputDataEntity(VesselCenterlineProcessor::INPUT_SEED_POINTS));
  mitk::DataTreeNode::Pointer nodeSurface;
  boost::any anyDataSurface = GetRenderingTree()->GetNode(
      m_processor->GetInputDataEntity(VesselCenterlineProcessor::INPUT_SURFACE));
  Core::CastAnyProcessingData(anyDataSurface, nodeSurface);

  if (nodeSurface.IsNotNull()) {
    nodeSurface->SetOpacity(0.4);
  }
  mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void VesselCenterlinePanelWidget::do_layout() {
  // begin wxGlade: ptRingCutPanelWidget::do_layout

  wxBoxSizer *GlobalSizer = new wxBoxSizer(wxVERTICAL);

  wxBoxSizer *sizer_0 = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *seedStaticBox = new wxStaticBoxSizer(m_seedsStaticBox, wxVERTICAL);
  wxBoxSizer *sizer_1 = new wxBoxSizer(wxHORIZONTAL);
  sizer_1->Add(toggleBtnSources, 1, wxALIGN_LEFT, 0);
  sizer_1->Add(toggleBtnTargets, 0, wxALL | wxALIGN_LEFT, 0);
  sizer_1->Add(btnCenterline, 0, wxALL | wxALIGN_LEFT, 0);
  seedStaticBox->Add(sizer_1, 0, wxALL | wxALIGN_RIGHT, 5);

  /*wxStaticBoxSizer* openProfilesStaticBox = new
  wxStaticBoxSizer(m_openProfilesStaticBox, wxVERTICAL);
  wxBoxSizer* sizer_2 = new wxBoxSizer(wxVERTICAL);
  sizer_2->Add(btnOpenProfiles,  0, wxALL|wxEXPAND, 5);

  wxBoxSizer* sizer_4 = new wxBoxSizer(wxHORIZONTAL);
  sizer_4->Add(20, 20, 0, wxEXPAND, 0);
  sizer_4->Add(m_staticTextInlet, 0, wxALL|wxALIGN_LEFT, 0);
  sizer_4->Add(20, 20, 0, wxEXPAND, 0);
  sizer_4->Add(m_textCtrlInlet, 0, wxALL|wxALIGN_LEFT, 0);
  sizer_4->Add(20, 20, 0, wxEXPAND, 0);
  sizer_4->Add(m_staticTextOutlet, 0, wxALL|wxALIGN_LEFT, 0);
  sizer_4->Add(20, 20, 0, wxEXPAND, 0);
  sizer_4->Add(m_textCtrlOutlet, 0, wxALL|wxALIGN_LEFT, 0);
  sizer_2->Add(sizer_4);

  sizer_2->Add(btnCenterlineOP, 0, wxALL|wxEXPAND, 5);

  openProfilesStaticBox->Add(sizer_2, 0, wxALL|wxALIGN_RIGHT, 5);*/

  sizer_0->Add(seedStaticBox);
  sizer_0->Add(m_chkAppendEndPoints, 0, wxALL | wxEXPAND, 5);
  // sizer_0->Add(openProfilesStaticBox, 1, wxALL|wxEXPAND, 5);
  GlobalSizer->Add(sizer_0);
  SetSizer(GlobalSizer);
  GlobalSizer->Fit(this);
  // end wxGlade
}

void VesselCenterlinePanelWidget::OnModifiedSelectedPoint() {
  // Add a point to the INPUT_POINT_SET data entity
  if (m_processor->GetInputDataEntity(VesselCenterlineProcessor::INPUT_SEED_POINTS)
          .IsNull())
    return;

  Core::vtkPolyDataPtr point;
  Core::DataEntityHelper::GetProcessingData(
      m_processor->GetInputDataEntityHolder(VesselCenterlineProcessor::INPUT_SEED_POINTS),
      point);
  int n = point->GetNumberOfPoints();
  if (n == 0)
    return;
  toggleBtnTargets->Enable();

  Core::vtkPolyDataPtr points;
  Core::DataEntityHelper::GetProcessingData(
      m_processor->GetInputDataEntityHolder(VesselCenterlineProcessor::INPUT_SEED_POINTS),
      points);
  int numPoints = points->GetNumberOfPoints();

  if ((numPoints > (m_processor->GetNumSource())) && ((m_processor->GetNumSource()) > 0))
    btnCenterline->Enable();
}

Core::BaseProcessor::Pointer VesselCenterlinePanelWidget::GetProcessor() {
  return m_processor.GetPointer();
}

void VesselCenterlinePanelWidget::ConnectInteractor() {
  Core::Widgets::LandmarkSelectorWidget *widget;
  widget = GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget >(
      "Landmark selector");

  // Connect interactor
  widget->SetAllowedInputDataTypes(Core::SurfaceMeshTypeId);

  widget->SetInteractorType(Core::PointInteractor::POINT_SET);
  widget->SetLandmarksName("Seed points");
  widget->SetAllowedInputDataTypes(Core::SurfaceMeshTypeId);
  widget->SetInputDataEntity(
      m_processor->GetInputDataEntity(VesselCenterlineProcessor::INPUT_SURFACE));
  widget->StartInteractor();

  Core::PointInteractorPointSet *pointSetInteractor;
  pointSetInteractor = static_cast< Core::PointInteractorPointSet * >(
      widget->GetPointInteractor().GetPointer());

  m_processor->SetInputDataEntity(VesselCenterlineProcessor::INPUT_SEED_POINTS,
                                  pointSetInteractor->GetSelectedPointsDataEntity());

  Core::DataTreeMITKHelper::ChangeShowLabelsProperty(
      pointSetInteractor->GetSelectedPointsDataEntity(), GetRenderingTree(), false);

  // Disable
  blMITKUtils::SetScalarMode(pointSetInteractor->GetSelectedDataEntityNode(), "", 2);
}

void VesselCenterlinePanelWidget::DisconnectInteractor() {
  Core::Widgets::LandmarkSelectorWidget *widget;
  widget = GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget >(
      "Landmark selector");
  if (widget == NULL) {
    return;
  }
  widget->StopInteraction();

  widget->SetDefaultAllowedInputDataTypes();
}

void VesselCenterlinePanelWidget::OnInit() {
  GetProcessorOutputObserver(VesselCenterlineProcessor::OUTPUT_CENTERLINE)
      ->SetHideInput(false);

  GetInputWidget(VesselCenterlineProcessor::INPUT_SURFACE)->SetAutomaticSelection(false);
  GetInputWidget(VesselCenterlineProcessor::INPUT_SURFACE)
      ->SetDefaultDataEntityFlag(false);
  GetInputWidget(VesselCenterlineProcessor::INPUT_SEED_POINTS)
      ->SetAutomaticSelection(false);
  GetInputWidget(VesselCenterlineProcessor::INPUT_SEED_POINTS)
      ->SetDefaultDataEntityFlag(false);

  m_processor->GetInputDataEntityHolder(VesselCenterlineProcessor::INPUT_SEED_POINTS)
      ->AddObserver(this, &VesselCenterlinePanelWidget::OnModifiedSelectedPoint);

  UpdateWidget();
}

void VesselCenterlinePanelWidget::DoCancel(int seedButton) {
  if (seedButton == SOURCE_BUTTON) {
    if (!m_sourceEnabled)
      return;
  } else if (seedButton == TARGET_BUTTON) {
    if (!m_targetEnabled)
      return;
    toggleBtnSources->Enable();
  }
  toggleBtnTargets->Disable();
  btnCenterline->Disable();
  m_sourceEnabled = false;
  m_targetEnabled = false;
  btnCenterline->Disable();
  DisconnectInteractor();
  UpdateWidget();
  m_processor->SetNumSource(0);
  // remove seeds from datalist
  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
  list->Remove(
      m_processor->GetInputDataEntity(VesselCenterlineProcessor::INPUT_SEED_POINTS));
}

void VesselCenterlinePanelWidget::UpdateWidget() {
  toggleBtnSources->SetValue(m_sourceEnabled);
  std::string label = m_sourceEnabled ? "Cancel " : "Source Points ";
  toggleBtnSources->SetLabel(_U(label));

  toggleBtnTargets->SetValue(m_targetEnabled);
  label = m_targetEnabled ? "Cancel " : "Target Points ";
  toggleBtnTargets->SetLabel(_U(label));
}

bool VesselCenterlinePanelWidget::Enable(bool enable /*= true */) {
  bool bReturn = wxPanel::Enable(enable);

  try {
    const std::string helpStr = " ";
    SetInfoUserHelperWidget(helpStr);
  }
  coreCatchExceptionsReportAndNoThrowMacro("VesselCenterlinePanelWidget::Enable");

  return bReturn;
}
//
// void VesselCenterlinePanelWidget::OnButtonOpenProfiles(wxCommandEvent &event)
//{
//	m_processor->GetCapCenterIDs();
//	m_processor->ShowCapCenters();
//}
void VesselCenterlinePanelWidget::OnModifiedOutputCapCenters() {
  if ((m_processor->GetOutputDataEntity(
           VesselCenterlineProcessor::OUTPUT_CAPSCENTER_POINTS))
          .IsNull())
    return;
  Core::DataTreeMITKHelper::ChangeShowLabelsProperty(
      m_processor->GetOutputDataEntity(
          VesselCenterlineProcessor::OUTPUT_CAPSCENTER_POINTS),
      GetRenderingTree(), true);

  mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}
//
// void VesselCenterlinePanelWidget::OnButtonCenterlineWithOpenProfiles(wxCommandEvent
// &event)
//{
//	//Set inlet and outled IDs to processor
//	m_processor->SetSeedSelectorType(VesselCenterlineProcessor::SEED_OPENPROFILES);
//	int inlet = -1;
//	int outlet = -1;
//	wxString inletString = m_textCtrlInlet->GetValue();
//	wxString outletString = m_textCtrlOutlet->GetValue();
//	inlet = wxAtoi(inletString);
//	outlet = wxAtoi(outletString);
//
//	if ( ( m_processor->GetNumberOfIds() < inlet )||( m_processor->GetNumberOfIds() )<
//outlet )
//	{
//		throw Core::Exceptions::Exception(
//			"vmtkVesselCenterlinePanelWidget::OnButtonCenterlineWithOpenProfiles",
//			"Inlet and/or outlet out of range");
//	}
//
//	m_processor->SetInletOutletIDs(inlet, outlet);
//
//	//Update processor
//	UpdateProcessor( );
//
//	//change opacity of capped surface
//	mitk::DataTreeNode::Pointer node;
//	boost::any anyData =
//GetRenderingTree()->GetNode(m_processor->GetOutputDataEntity(VesselCenterlineProcessor::OUTPUT_CAPPEDSURFACE));
//	Core::CastAnyProcessingData( anyData, node );
//
//	if(node.IsNotNull())
//	{
//		node->SetOpacity(0.4);
//	}
//	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
//
//
//	//remove cap centers from datalist
//	Core::DataContainer::Pointer dataContainer =
//Core::Runtime::Kernel::GetDataContainer();
//	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
//	list->Remove( m_processor->GetOutputDataEntity(
//VesselCenterlineProcessor::OUTPUT_CAPSCENTER_POINTS ) );
//
//
//}
