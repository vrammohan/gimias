/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SecurityPluginWidgetCollective.h"
#include "SecurityPluginUserManager.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTab.h"
#include "PluginSecurityWidget.h"

SecurityPlugin::WidgetCollective::WidgetCollective() {
  SecurityPluginCryptoProcessor::Pointer ptrSecPlugin =
      SecurityPluginCryptoProcessor::New();

  ptrSecPlugin->Init();

  Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
      PluginSecurityWidget::Factory::NewBase(),
      Core::WindowConfig().Preferences().Caption("Plugins security"));

  // Panel widgets
  Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
      SecurityPluginUserManager::Factory::New(ptrSecPlugin),
      Core::WindowConfig().Preferences().Caption("Users Node Manager"));
}
