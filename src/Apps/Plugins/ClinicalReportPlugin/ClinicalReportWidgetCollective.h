/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ClinicalReportWidgetCollective_H
#define _ClinicalReportWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

#include "ClinicalReportProcessorCollective.h"

/**

\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 19 Oct 2010
*/

class ClinicalReportWidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(ClinicalReportWidgetCollective,
                                    Core::WidgetCollective);

  //!
  void Init();

private:
  ClinicalReportWidgetCollective();

private:
};

#endif //_ClinicalReportWidgetCollective_H
