/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ClinicalReportPlugin_H
#define _ClinicalReportPlugin_H

#include "ClinicalReportProcessorCollective.h"
#include "ClinicalReportWidgetCollective.h"

// core
#include "coreFrontEndPlugin.h"

/**
\brief Clinical Report Plugin
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 19 Oct 2010
*/

class PLUGIN_EXPORT ClinicalReportPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
public:
  coreDeclareSmartPointerClassMacro(ClinicalReportPlugin,
                                    Core::FrontEndPlugin::FrontEndPlugin);

protected:
  //!
  ClinicalReportPlugin(void);

  //!
  virtual ~ClinicalReportPlugin(void);

private:
  //! Purposely not implemented
  ClinicalReportPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  ClinicalReportProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  ClinicalReportWidgetCollective::Pointer m_Widgets;
};

#endif // _ClinicalReportPlugin_H
