/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _AhePluginProcessorCollective_H
#define _AhePluginProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

namespace ahePlugin {

/**
This class instantiates all processors used in the plugin and registers them.

\ingroup AhePlugin
*/

class ProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(ahePlugin::ProcessorCollective,
                                    Core::SmartPointerObject);

private:
  //! The constructor instantiates all the processors and connects them.
  ProcessorCollective();

}; // class ProcessorCollective

} // namespace ahePlugin{

#endif //_AhePluginProcessorCollective_H
