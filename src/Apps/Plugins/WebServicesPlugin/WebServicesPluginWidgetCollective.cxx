/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "WebServicesPluginWidgetCollective.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTab.h"
#include "coreSettings.h"

#include "WebServicesPreferencesPanelWidget.h"

WebServicesPlugin::WidgetCollective::WidgetCollective() {
  // Processor should be created at the beginning to start the web server without GUI
  m_Processor = WebServerProcessor::New();

  Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
      WebServicesPreferencesPanelWidget::Factory::New(m_Processor),
      Core::WindowConfig().Preferences().Caption("Web Services"));
}