/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "XNATWidgetCollective.h"
#include "XNATWorkingArea.h"
#include "XNATPluginUtilities.h"
#include "XNATToolbar.h"
#include "XNATPluginConnectionPanelWidget.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTabFactory.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSimpleProcessingWidget.h"
#include "coreWindowConfig.h"
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreDirectory.h"

XNATWidgetCollective::XNATWidgetCollective() {}

void XNATWidgetCollective::Init() {
  // Environment initialization
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string xnatFolder = settings->GetProjectHomePath() + Core::IO::SlashChar + "XNAT";

  Core::IO::Directory::Pointer xnatDir = Core::IO::Directory::New();
  xnatDir->SetDirNameFullPath(xnatFolder);

  if (!xnatDir->Exists()) {
    xnatDir->Create();
    XNATPluginUtilities::CreateConfigFile(xnatFolder);
  } else {
    XNATPluginUtilities::SetConfigFilePath(xnatFolder);
  }
  XNATPluginManageConnections::Pointer ptrXNATPluginManageConn =
      XNATPluginManageConnections::New();

  ptrXNATPluginManageConn->InitConnections(XNATPluginUtilities::ReadConfigFile());

  Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();

  gIface->RegisterFactory(
      Core::Widgets::XNATToolbar::Factory::NewBase(),
      Core::WindowConfig().Toolbar().Top().Show().Caption("XNAT Toolbar"));

  gIface->RegisterFactory(
      XNATWorkingArea::Factory::New(ptrXNATPluginManageConn),
      Core::WindowConfig().WorkingArea().Caption("XNAT Working Area"));

  gIface->RegisterFactory(
      XNATPluginConnectionPanelWidget::Factory::New(ptrXNATPluginManageConn),
      Core::WindowConfig().Preferences().Caption("XNAT configuration"));
}
