// -*- C++ -*- generated by wxGlade 0.6.3 on Fri Sep 30 15:19:22 2011

#include "XNATPluginEditConfigurationDialogUI.h"

// begin wxGlade: ::extracode

// end wxGlade

XNATPluginEditConfigurationDialogUI::XNATPluginEditConfigurationDialogUI(
    wxWindow *parent, int id, const wxString &title, const wxPoint &pos,
    const wxSize &size, long style)
    : wxDialog(parent, id, title, pos, size, wxDEFAULT_DIALOG_STYLE) {
  // begin wxGlade:
  // XNATPluginEditConfigurationDialogUI::XNATPluginEditConfigurationDialogUI
  label_Name = new wxStaticText(this, wxID_ANY, wxT("Name"));
  m_txtName = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
  label_1 = new wxStaticText(this, wxID_ANY, wxT("URL"));
  m_txtURL = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
  label_1_copy = new wxStaticText(this, wxID_ANY, wxT("User"));
  m_txtUser = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
  label_1_copy_copy = new wxStaticText(this, wxID_ANY, wxT("Password"));
  m_txtPassword = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition,
                                 wxDefaultSize, wxTE_PASSWORD);
  m_btnTest = new wxButton(this, wxID_BTN_TEST, wxT("Test URL"));
  m_btnOK = new wxButton(this, wxID_BTN_OK, wxT("OK"));
  m_btnCancel = new wxButton(this, wxID_BTN_CANCEL, wxT("Cancel"));

  set_properties();
  do_layout();
  // end wxGlade
}

BEGIN_EVENT_TABLE(XNATPluginEditConfigurationDialogUI, wxDialog)
// begin wxGlade: XNATPluginEditConfigurationDialogUI::event_table
EVT_BUTTON(wxID_BTN_TEST, XNATPluginEditConfigurationDialogUI::OnBtnTest)
EVT_BUTTON(wxID_BTN_OK, XNATPluginEditConfigurationDialogUI::OnOK)
EVT_BUTTON(wxID_BTN_CANCEL, XNATPluginEditConfigurationDialogUI::OnCancel)
// end wxGlade
END_EVENT_TABLE();

void XNATPluginEditConfigurationDialogUI::OnBtnTest(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT(
      "Event handler (XNATPluginEditConfigurationDialogUI::OnBtnTest) not implemented "
      "yet")); // notify the user that he hasn't implemented the event handler yet
}

void XNATPluginEditConfigurationDialogUI::OnOK(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(
      wxT("Event handler (XNATPluginEditConfigurationDialogUI::OnOK) not implemented "
          "yet")); // notify the user that he hasn't implemented the event handler yet
}

void XNATPluginEditConfigurationDialogUI::OnCancel(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(
      wxT("Event handler (XNATPluginEditConfigurationDialogUI::OnCancel) not implemented "
          "yet")); // notify the user that he hasn't implemented the event handler yet
}

// wxGlade: add XNATPluginEditConfigurationDialogUI event handlers

void XNATPluginEditConfigurationDialogUI::set_properties() {
  // begin wxGlade: XNATPluginEditConfigurationDialogUI::set_properties
  SetToolTip(wxT("Connection Dialog"));
  m_btnTest->SetToolTip(wxT("Test Connection"));
  m_btnOK->SetToolTip(wxT("OK"));
  m_btnOK->SetDefault();
  m_btnCancel->SetToolTip(wxT("Cancel"));
  // end wxGlade
}

void XNATPluginEditConfigurationDialogUI::do_layout() {
  // begin wxGlade: XNATPluginEditConfigurationDialogUI::do_layout
  wxBoxSizer *GlobalSizer = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_2_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_1_copy_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_1_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_1 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_1_copy_1 = new wxBoxSizer(wxHORIZONTAL);
  sizer_1_copy_1->Add(label_Name, 1, wxALIGN_CENTER_VERTICAL, 0);
  sizer_1_copy_1->Add(m_txtName, 1, 0, 0);
  GlobalSizer->Add(sizer_1_copy_1, 0, wxALL | wxEXPAND, 5);
  sizer_1->Add(label_1, 1, wxALIGN_CENTER_VERTICAL, 0);
  sizer_1->Add(m_txtURL, 1, 0, 0);
  GlobalSizer->Add(sizer_1, 0, wxALL | wxEXPAND, 5);
  sizer_1_copy->Add(label_1_copy, 1, wxALIGN_CENTER_VERTICAL, 0);
  sizer_1_copy->Add(m_txtUser, 1, 0, 0);
  GlobalSizer->Add(sizer_1_copy, 0, wxALL | wxEXPAND, 5);
  sizer_1_copy_copy->Add(label_1_copy_copy, 1, wxALIGN_CENTER_VERTICAL, 0);
  sizer_1_copy_copy->Add(m_txtPassword, 1, 0, 0);
  GlobalSizer->Add(sizer_1_copy_copy, 0, wxALL | wxEXPAND, 5);
  sizer_2_copy->Add(m_btnTest, 0, wxALL, 5);
  sizer_2_copy->Add(m_btnOK, 0, wxALL, 5);
  sizer_2_copy->Add(m_btnCancel, 0, wxALL, 5);
  GlobalSizer->Add(sizer_2_copy, 0, wxALIGN_RIGHT, 0);
  SetSizer(GlobalSizer);
  GlobalSizer->Fit(this);
  Layout();
  // end wxGlade
}
