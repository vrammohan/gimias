/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef SecurityPluginUserMannager_H
#define SecurityPluginUserManager_H

#include "SecurityPluginUserManagerUI.h"
#ifndef SecurityPluginCryptoProcessor_H
#include "SecurityPluginCryptoProcessor.h"
#endif

#include "corePreferencesPage.h"
#include "blLightObject.h"
#include "CILabExceptionMacros.h"

#include <map>

/**
Configure all Panel Connections

\ingroup SecurityPlugin
\author H�ctor Fernandez
\date Jan 2012
*/
class PLUGIN_EXPORT SecurityPluginUserManager : public SecurityPluginUserManagerUI,
                                                public Core::Widgets::PreferencesPage {

  // OPERATIONS
public:
  //!
  // coreDefineBaseWindowFactory(SecurityPluginUserManager);
  coreDefineBaseWindowFactory1param(SecurityPluginUserManager,
                                    SecurityPluginCryptoProcessor::Pointer)

      //!
      SecurityPluginUserManager(SecurityPluginCryptoProcessor::Pointer userManager,
                                wxWindow *parent, int id,
                                const wxPoint &pos = wxDefaultPosition,
                                const wxSize &size = wxDefaultSize, long style = 0);

  //!
  ~SecurityPluginUserManager();

  //! When Ok is pressed
  virtual void UpdateData();

  //! When the dialog is shown
  virtual void UpdateWidget();

  SecurityPluginCryptoProcessor::Pointer GetMPManager();

protected:
  // working data
  SecurityPluginCryptoProcessor::Pointer m_MPManager;

private:
  //! add new node
  void OnBtnAdd(wxCommandEvent &event);

  //! removes node
  void OnBtnRemove(wxCommandEvent &event);

  //! open master password dialog
  void OnCheckBoxOK(wxCommandEvent &event);

  //! opens master password dialog
  void OnBtnChangeMasterPassword(wxCommandEvent &event);

  //! search
  void OnBtntxtEnter(wxGridEvent &event);

  //! check list
  void OnBtnLeftClick(wxGridEvent &event);
};

#endif // SecurityPluginUserManager_H
