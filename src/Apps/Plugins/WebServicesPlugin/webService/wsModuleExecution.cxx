/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "wsModuleExecution.h"

#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"
#include "coreDynProcessor.h"
#include "coreProcessorManager.h"
#include "corePluginProviderManager.h"
#include "coreModuleDescriptionSearch.h"
#include "coreDynDataTransferCLP.h"
#include "coreDataEntityWriter.h"
#include "coreSettings.h"

#include "dynModuleHelper.h"

#include <axiom_element.h>

#include "blTextUtils.h"

#include "itksys/SystemTools.hxx"

// This should be static to pass it to the AXIS2C calling code
static axis2_char_t *error_message;

wsModuleExecution::wsModuleExecution(const axutil_env_t *env) { m_Env = env; }

void wsModuleExecution::GetModuleDescription(axiom_node_t *node) {
  axiom_node_t *complex_node = NULL;
  axiom_node_t *seq_node = NULL;

  if (!node) {
    AXIS2_ERROR_SET(m_Env->error, AXIS2_ERROR_SVC_SKEL_INVALID_XML_FORMAT_IN_REQUEST,
                    AXIS2_FAILURE);
    throw Core::Exceptions::Exception("SetParameters", "ERROR: invalid XML in request");
  }

  axiom_types_t nodeType = axiom_node_get_node_type(node, m_Env);
  if (nodeType != AXIOM_ELEMENT) {
    AXIS2_ERROR_SET(m_Env->error, AXIS2_ERROR_SVC_SKEL_INVALID_XML_FORMAT_IN_REQUEST,
                    AXIS2_FAILURE);
    throw Core::Exceptions::Exception("SetParameters", "ERROR: invalid XML in request");
  }

  axiom_element_t *element = NULL;
  element = (axiom_element_t *)axiom_node_get_data_element(node, m_Env);
  if (!element) {
    AXIS2_ERROR_SET(m_Env->error, AXIS2_ERROR_SVC_SKEL_INVALID_XML_FORMAT_IN_REQUEST,
                    AXIS2_FAILURE);
    throw Core::Exceptions::Exception("SetParameters", "ERROR: invalid XML in request");
  }

  axis2_char_t *op_name = axiom_element_get_localname(element, m_Env);
  std::string moduleTitle = op_name;
  blTextUtils::StrSub(moduleTitle, "_", " ");

  // Get ModuleDescription from factory of CLP
  ModuleDescriptionSearch::Pointer search = ModuleDescriptionSearch::New();
  search->ScanOnlyEnabled(true);
  search->SetModuleTitle(moduleTitle);
  search->CreateWorkflowModules();
  search->Update();
  if (search->GetFoundModuleList().empty()) {
    std::stringstream stream;
    stream << "Cannot find factory: " << moduleTitle;
    throw Core::Exceptions::Exception("GetModuleDescription", stream.str().c_str());
  }

  m_Module = *search->GetFoundModuleList().begin();
  m_DefaultPluginProvider = search->FindProvider(moduleTitle);
}

void wsModuleExecution::CreateProcessor() {
  // Create processor with this Module
  m_Processor = Core::DynProcessor::New();
  m_Processor->SetModule(&m_Module);
  m_Processor->SetMultithreading(false);
  m_Processor->UpdateProcessorDirectories(m_DefaultPluginProvider);

  // IO timeout
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();
  std::string timeoutStr = "0";
  settings->GetPluginProperty("WebServicesPlugin", "IOTimeout", timeoutStr);
  size_t timeout = atoi(timeoutStr.c_str());
  m_Processor->SetIOTimeout(timeout);

  // Use CommandLineModule to avoid reading data from disk into memory
  if (m_Module.GetType() == "SharedObjectModule") {
    m_Processor->GetModuleExecution()->SetForceExecutionMode("CommandLineModule");
  }
}

void wsModuleExecution::Execute() {
  // When the outputs are empty, create an output based on the input filename
  // This will allow to execute a WS using remote data without specifying
  // the output filename
  ModuleParameter *inParam = NULL;
  if (dynModuleHelper::GetNumberOfInputs(&m_Module) != 0) {
    inParam = dynModuleHelper::GetInput(&m_Module, 0);
  }

  for (int i = 0; i < dynModuleHelper::GetNumberOfOutputs(&m_Module); i++) {
    ModuleParameter *param = dynModuleHelper::GetOutput(&m_Module, i);
    std::string outBaseName = itksys::SystemTools::GetFilenameName(param->GetDefault());

    // Not setting default values for optional parameters
    bool isOptional = param->GetIndex() == "";

    if (inParam && param->GetDefault().empty() && !isOptional) {
      // Don't use itksys::SystemTools::GetFilenamePath( ) because it
      // will convert http:// -> http:/
      std::string inBaseName =
          itksys::SystemTools::GetFilenameName(inParam->GetDefault());
      size_t npos = inParam->GetDefault().rfind(inBaseName);
      std::string path = inParam->GetDefault().substr(0, npos);
      std::string fileName = m_Module.GetTitle() + "_" + param->GetName();
      std::string extension = Core::DynDataTransferCLP::GetExtension(param);
      std::string fullPath = path + fileName + extension;
      param->SetDefault(fullPath);
    }
    // Missing base name: Reuse output path
    else if (outBaseName.empty() && !param->GetDefault().empty()) {
      std::string path = param->GetDefault();
      std::string fileName = m_Module.GetTitle() + "_" + param->GetName();
      std::string extension = Core::DynDataTransferCLP::GetExtension(param);
      std::string fullPath = path + fileName + extension;
      param->SetDefault(fullPath);
    } else if (param->GetDefault().empty() && isOptional) {
      // Disable this output
      param->SetDefault("false");
    }
  }

  // Configure active inputs for optional parameters that are set to a value
  for (int i = 0; i < dynModuleHelper::GetNumberOfInputs(&m_Module); i++) {
    ModuleParameter *param = dynModuleHelper::GetInput(&m_Module, i);
    if (!param->GetDefault().empty()) {
      m_Processor->GetInputPort(i)->SetActive(true);
    }
  }

  // Don't show processing message
  Core::ProcessorThread::Pointer processorThread;
  processorThread = Core::Runtime::Kernel::GetProcessorManager()->CreateProcessorThread(
      m_Processor.GetPointer());
  processorThread->SetShowProcessingMessage(false);

  // Execute
  Core::Runtime::Kernel::GetProcessorManager()->Execute(m_Processor.GetPointer());

  // Aborted?
  if (m_Processor->GetUpdateCallback()->GetAbortProcessing()) {
    throw Core::Exceptions::Exception("Execute", "Process aborted");
  }
  // Get output message
  else if (!m_Processor->GetUpdateCallback()->GetExceptionMessage().empty()) {
    throw Core::Exceptions::Exception(
        "Execute", m_Processor->GetUpdateCallback()->GetExceptionMessage().c_str());
  }
}

axiom_node_t *wsModuleExecution::GetResponse() {
  std::string fileName;
  if (m_Processor.IsNull()) {
    return NULL;
  }

  axiom_node_t *om_node = NULL;
  axiom_element_t *om_ele = NULL;
  axiom_node_t *node1 = NULL;
  axiom_node_t *node2 = NULL;

  std::string moduleTitle = m_Processor->GetModule()->GetTitle();
  blTextUtils::StrSub(moduleTitle, " ", "_");
  const axis2_char_t *title = moduleTitle.c_str();
  om_ele = axiom_element_create(m_Env, NULL, title, NULL, &om_node);

  // For each output, create a node in the XML
  for (size_t i = 0; i < m_Processor->GetNumberOfOutputs(); i++) {
    ModuleParameter *param = dynModuleHelper::GetOutput(m_Processor->GetModule(), i);
    if (!param->GetDefault().empty()) {
      const axis2_char_t *name = param->GetName().c_str();
      axiom_element_create(m_Env, om_node, name, NULL, &node1);
      const axis2_char_t *value = param->GetDefault().c_str();
      axiom_text_create(m_Env, node1, value, &node2);
    }
  }

  return om_node;
}

void wsModuleExecution::SetParameters(axiom_node_t *node) {
  axiom_node_t *param_node = NULL;
  axiom_node_t *complex_node = NULL;
  axiom_node_t *seq_node = NULL;

  if (!node) {
    AXIS2_ERROR_SET(m_Env->error, AXIS2_ERROR_SVC_SKEL_INVALID_XML_FORMAT_IN_REQUEST,
                    AXIS2_FAILURE);
    throw Core::Exceptions::Exception("SetParameters", "ERROR: invalid XML in request");
  }

  // Parameters
  complex_node = axiom_node_get_first_child(node, m_Env);
  while (complex_node) {
    if (axiom_node_get_node_type(complex_node, m_Env) != AXIOM_ELEMENT) {
      AXIS2_ERROR_SET(m_Env->error, AXIS2_ERROR_SVC_SKEL_INVALID_XML_FORMAT_IN_REQUEST,
                      AXIS2_FAILURE);
      throw Core::Exceptions::Exception("SetParameters", "ERROR: invalid XML in request");
    }

    axiom_element_t *element = NULL;
    element = (axiom_element_t *)axiom_node_get_data_element(complex_node, m_Env);
    axis2_char_t *param_name = axiom_element_get_localname(element, m_Env);

    param_node = axiom_node_get_first_child(complex_node, m_Env);

    if (param_node && axiom_node_get_node_type(param_node, m_Env) == AXIOM_TEXT) {
      axiom_text_t *param_value;
      param_value = (axiom_text_t *)axiom_node_get_data_element(param_node, m_Env);
      if (param_value) {
        const axis2_char_t *param_value_str = axiom_text_get_value(param_value, m_Env);
        if (param_value_str) {
          m_Module.SetParameterDefaultValue(param_name, param_value_str);
        }
      }
    }

    complex_node = axiom_node_get_next_sibling(complex_node, m_Env);
  }
}

void wsModuleExecution::SetError(Core::Exceptions::Exception &e) {
  AXIS2_LOG_ERROR(m_Env->log, AXIS2_LOG_SI, e.what());

  error_message = new axis2_char_t[strlen(e.what()) + 1];
  strcpy(error_message, e.what());
  axutil_error_set_error_message(m_Env->error, error_message);

  // User defined error
  axutil_error_codes_t errorCode = axutil_error_codes_t(AXUTIL_ERROR_MAX);
  AXIS2_ERROR_SET(m_Env->error, errorCode, AXIS2_FAILURE);
}
