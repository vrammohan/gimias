/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "WebServerProcessor.h"

#include "coreSettings.h"
#include "corePluginProviderManager.h"
#include "coreWorkflowManager.h"
#include "coreModuleDescriptionSearch.h"
#include "coreProcessorThread.h"
#include "coreProcessorManager.h"
#include "itksys/SystemTools.hxx"
#include "wsModuleExecution.h"

#include <signal.h>

#include "tinyxml.h"

#include "blTextUtils.h"

#include "dynModuleHelper.h"

#ifdef _WIN32
#include "WinSock.h"
#else
#include <stdio.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#endif

WebServerProcessor::WebServerProcessor() {
  SetName("WebServerProcessor");
  m_Thread = NULL;
  m_Observer = NULL;

  ReadSettings();

  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

  Core::Runtime::PluginProviderManager::Pointer pluginProviderManager;
  pluginProviderManager = graphicalIface->GetPluginProviderManager();

  pluginProviderManager->GetStateHolder()->AddObserver1(
      this, &WebServerProcessor::OnPluginManagerModified);
}

WebServerProcessor::~WebServerProcessor() { Stop(); }

void WebServerProcessor::Start() {
  if (m_Thread != NULL) {
    return;
  }

  ModuleDescriptionSearch::Pointer search = ModuleDescriptionSearch::New();
  // Search only enabled modules
  search->ScanOnlyEnabled(true);
  search->AddType("CommandLineModule");
  search->AddType("ProcessorModule");
  search->AddType("WorkflowModule");
  search->AddType("ExternalApp");
  search->ScanOnlyEnabled(true);
  search->CreateWorkflowModules();
  search->Update();
  std::list< ModuleDescription > moduleList = search->GetFoundModuleList();

  // Copy wsrepo folder to User application folder because
  // in Windows 7 is not possible to modify "Program files" files
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();
  std::string repoSrc = settings->GetApplicationPath() + "/wsrepo";
  std::string repoDst = settings->GetProjectHomePath() + "/wsrepo";
  itksys::SystemTools::CopyADirectory(repoSrc.c_str(), repoDst.c_str(), false);

  // Create wsrepo/services/wsGimias
  std::string wsGimiasPath = settings->GetProjectHomePath() + "/wsrepo/services/wsGimias";
  itksys::SystemTools::MakeDirectory(wsGimiasPath.c_str());

// Copy wsGimias.dll to wsrepo/services/wsGimias
#ifdef _WIN32
  repoSrc = settings->GetApplicationPath() + "/wsGimias.dll";
  repoDst = wsGimiasPath + "/wsGimias.dll";
#else
  repoSrc = settings->GetApplicationPath() + "/libwsGimias.so";
  repoDst = wsGimiasPath + "/libwsGimias.so";
#endif
  itksys::SystemTools::CopyAFile(repoSrc.c_str(), repoDst.c_str(), false);

  // Create services.xml that define the available operations
  CreateServicesXML(moduleList);

  // Create wsGimias.wsdl that define the interface to the operations
  CreateWSDL(moduleList);

  // Start the web server
  if (m_Thread == NULL) {
    m_Thread = new WebServerThread();
    if (m_Thread->Create() != wxTHREAD_NO_ERROR) {
      throw Core::Exceptions::Exception("Start", "Can't create thread!");
    }
    m_Thread->SetPort(m_ServerPort);
    m_Thread->SetObserver(m_Observer);
    m_Thread->Run();
  }
}

void WebServerProcessor::Stop() {
  if (m_Thread != NULL) {
    // Stop the thread
    raise(SIGINT);

    // Check if there's a processor executing
    // Then we cannot stop the web server because the processor
    // could be called by the web server -> Kill the thread
    Core::ProcessorThread::Pointer processorThread;
    processorThread = Core::Runtime::Kernel::GetProcessorManager()->GetProcessorThread(
        -1, Core::ProcessorThread::STATE_ACTIVE);
    if (processorThread.IsNotNull()) {
      // Kill it. Very dangerous!
      std::cout << "WebServerProcessor::Stop: Killing thread" << std::endl;
      m_Thread->Kill();
    } else {
      // Waits for a joinable thread to terminate and returns the value the thread
      // returned
      m_Thread->Wait();
    }

    delete m_Thread;
    m_Thread = NULL;
  }
}

void WebServerProcessor::SetStatusObserver(wxEvtHandler *handler) {
  m_Observer = handler;
  if (m_Thread != NULL) {
    m_Thread->SetObserver(m_Observer);
  }
}

void WebServerProcessor::CreateServicesXML(std::list< ModuleDescription > &moduleList) {

  // Create document
  TiXmlDocument doc;

  TiXmlElement *service = new TiXmlElement("service");
  service->SetAttribute("name", "wsGimias");
  doc.LinkEndChild(service);

  TiXmlElement *pluginParameter = new TiXmlElement("parameter");
  pluginParameter->SetAttribute("name", "ServiceClass");
  pluginParameter->SetAttribute("locked", "xsd:false");
  pluginParameter->LinkEndChild(new TiXmlText("wsGimias"));
  service->LinkEndChild(pluginParameter);

  TiXmlElement *pluginDescription = new TiXmlElement("description");
  pluginDescription->LinkEndChild(new TiXmlText(
      "GIMIAS web service for execution of biomedical processing algorithms"));
  service->LinkEndChild(pluginDescription);

  std::list< ModuleDescription >::iterator itModule;
  for (itModule = moduleList.begin(); itModule != moduleList.end(); itModule++) {
    std::string moduleTitle = itModule->GetTitle();
    blTextUtils::StrSub(moduleTitle, " ", "_");

    // Operation
    TiXmlElement *pluginOperation = new TiXmlElement("operation");
    pluginOperation->SetAttribute("name", moduleTitle);
    service->LinkEndChild(pluginOperation);

    // REST service method
    TiXmlElement *parameter1 = new TiXmlElement("parameter");
    parameter1->SetAttribute("name", "RESTMethod");
    parameter1->LinkEndChild(new TiXmlText("POST"));
    pluginOperation->LinkEndChild(parameter1);

    // REST service location
    TiXmlElement *parameter2 = new TiXmlElement("parameter");
    parameter2->SetAttribute("name", "RESTLocation");
    parameter2->LinkEndChild(new TiXmlText(moduleTitle));
    pluginOperation->LinkEndChild(parameter2);
  }

  // Save to the output file
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();
  std::string servicesFileName = settings->GetProjectHomePath();
  servicesFileName += "/wsrepo/services/wsGimias/services.xml";
  doc.SaveFile(servicesFileName);
}

void WebServerProcessor::CreateWSDL(std::list< ModuleDescription > &moduleList) {
  // Create document
  TiXmlDocument doc;
  TiXmlDeclaration *decl = new TiXmlDeclaration("1.0", "UTF-8", "");
  doc.LinkEndChild(decl);

  TiXmlElement *definitions = new TiXmlElement("wsdl:definitions");
  definitions->SetAttribute("xmlns", "http://schemas.xmlsoap.org/wsdl/");
  definitions->SetAttribute("xmlns:apachesoap", "http://xml.apache.org/xml-soap");
  definitions->SetAttribute("xmlns:impl", "urn:GIMIAS");
  definitions->SetAttribute("xmlns:intf", "urn:GIMIAS");
  definitions->SetAttribute("xmlns:soap", "http://schemas.xmlsoap.org/wsdl/soap/");
  definitions->SetAttribute("xmlns:soapenc", "http://schemas.xmlsoap.org/soap/encoding/");
  definitions->SetAttribute("xmlns:wsdl", "http://schemas.xmlsoap.org/wsdl/");
  definitions->SetAttribute("xmlns:wsdlsoap", "http://schemas.xmlsoap.org/wsdl/soap/");
  definitions->SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
  definitions->SetAttribute("targetNamespace", "urn:GIMIAS");
  doc.LinkEndChild(definitions);

  // Port
  TiXmlElement *portType = new TiXmlElement("wsdl:portType");
  portType->SetAttribute("name", "GIMIASPort");
  definitions->LinkEndChild(portType);

  // Binding
  TiXmlElement *binding = new TiXmlElement("wsdl:binding");
  binding->SetAttribute("name", "GIMIASBinding");
  binding->SetAttribute("type", "impl:GIMIASPort");
  definitions->LinkEndChild(binding);

  TiXmlElement *soapbinding = new TiXmlElement("wsdlsoap:binding");
  soapbinding->SetAttribute("style", "rpc");
  soapbinding->SetAttribute("transport", "http://schemas.xmlsoap.org/soap/http");
  binding->LinkEndChild(soapbinding);

  // Fault type
  TiXmlElement *wsdlTypes = new TiXmlElement("wsdl:types");
  definitions->LinkEndChild(wsdlTypes);

  TiXmlElement *schema = new TiXmlElement("schema");
  schema->SetAttribute("targetNamespace", "urn:GIMIAS");
  schema->SetAttribute("xmlns", "http://www.w3.org/2001/XMLSchema");
  wsdlTypes->LinkEndChild(schema);

  TiXmlElement *import = new TiXmlElement("import");
  import->SetAttribute("namespace", "http://schemas.xmlsoap.org/soap/encoding/");
  schema->LinkEndChild(import);

  TiXmlElement *complexType = new TiXmlElement("complexType");
  complexType->SetAttribute("name", "GIMIASException");
  schema->LinkEndChild(complexType);

  TiXmlElement *sequence = new TiXmlElement("sequence");
  complexType->LinkEndChild(sequence);

  TiXmlElement *element = new TiXmlElement("element");
  element->SetAttribute("name", "message");
  element->SetAttribute("type", "xsd:string");
  element->SetAttribute("nillable", "true");
  sequence->LinkEndChild(element);

  // Fault message
  TiXmlElement *faultMessageDef = new TiXmlElement("wsdl:message");
  faultMessageDef->SetAttribute("name", "GIMIASException");
  definitions->LinkEndChild(faultMessageDef);

  TiXmlElement *faultPart = new TiXmlElement("wsdl:part");
  faultPart->SetAttribute("name", "GIMIASException");
  faultPart->SetAttribute("type", "impl:GIMIASException");
  faultMessageDef->LinkEndChild(faultPart);

  // Operations
  std::list< ModuleDescription >::iterator itModule;
  for (itModule = moduleList.begin(); itModule != moduleList.end(); itModule++) {
    std::string moduleTitle = itModule->GetTitle();
    blTextUtils::StrSub(moduleTitle, " ", "_");

    // messages
    TiXmlElement *requestMessage = new TiXmlElement("wsdl:message");
    requestMessage->SetAttribute("name", moduleTitle + "Request");
    definitions->LinkEndChild(requestMessage);

    // messages
    TiXmlElement *responseMessage = new TiXmlElement("wsdl:message");
    responseMessage->SetAttribute("name", moduleTitle + "Response");
    definitions->LinkEndChild(responseMessage);

    // All parameters
    std::vector< ModuleParameterGroup > parameterGroups = itModule->GetParameterGroups();
    std::vector< ModuleParameterGroup >::iterator itGroup;
    itGroup = itModule->GetParameterGroups().begin();
    while (itGroup != itModule->GetParameterGroups().end()) {
      std::vector< ModuleParameter >::iterator itParam;
      itParam = itGroup->GetParameters().begin();
      while (itParam != itGroup->GetParameters().end()) {
        TiXmlElement *part = new TiXmlElement("wsdl:part");
        part->SetAttribute("name", itParam->GetName());

        if (itParam->GetTag() == "double") {
          part->SetAttribute("type", "xsd:double");
        } else if (itParam->GetTag() == "integer") {
          part->SetAttribute("type", "xsd:int");
        } else if (itParam->GetTag() == "uinteger") {
          part->SetAttribute("type", "xsd:unsignedInt");
        } else if (itParam->GetTag() == "float") {
          part->SetAttribute("type", "xsd:float");
        } else if (itParam->GetTag() == "string") {
          part->SetAttribute("type", "xsd:string");
        } else if (itParam->GetTag() == "boolean") {
          part->SetAttribute("type", "xsd:boolean");
        } else if (itParam->GetChannel() == "input") {
          part->SetAttribute("type", "xsd:string");
        } else if (itParam->GetChannel() == "output") {
          part->SetAttribute("type", "xsd:string");
        } else {
          part->SetAttribute("type", "xsd:string");
        }

        requestMessage->LinkEndChild(part);

        if (itParam->GetChannel() == "output") {
          TiXmlElement *part = new TiXmlElement("wsdl:part");
          part->SetAttribute("name", itParam->GetName());
          part->SetAttribute("type", "xsd:string");
          responseMessage->LinkEndChild(part);
        }

        ++itParam;
      }

      ++itGroup;
    }

    // Operation
    TiXmlElement *operation = new TiXmlElement("wsdl:operation");
    operation->SetAttribute("name", moduleTitle);
    portType->LinkEndChild(operation);

    TiXmlElement *inputMessage = new TiXmlElement("wsdl:input");
    inputMessage->SetAttribute("message",
                               "impl:" + std::string(requestMessage->Attribute("name")));
    operation->LinkEndChild(inputMessage);

    TiXmlElement *outputMessage = new TiXmlElement("wsdl:output");
    outputMessage->SetAttribute(
        "message", "impl:" + std::string(responseMessage->Attribute("name")));
    operation->LinkEndChild(outputMessage);

    TiXmlElement *faultMessage = new TiXmlElement("wsdl:fault");
    faultMessage->SetAttribute("message",
                               "impl:" + std::string(faultMessageDef->Attribute("name")));
    faultMessage->SetAttribute("name", std::string(faultMessageDef->Attribute("name")));
    operation->LinkEndChild(faultMessage);

    // Binding
    operation = new TiXmlElement("wsdl:operation");
    operation->SetAttribute("name", moduleTitle);
    binding->LinkEndChild(operation);

    TiXmlElement *soapoperation = new TiXmlElement("wsdlsoap:operation");
    soapoperation->SetAttribute("soapAction", "urn:#" + moduleTitle);
    operation->LinkEndChild(soapoperation);

    TiXmlElement *input = new TiXmlElement("wsdl:input");
    operation->LinkEndChild(input);

    TiXmlElement *soapbody = new TiXmlElement("wsdlsoap:body");
    soapbody->SetAttribute("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
    soapbody->SetAttribute("namespace", "urn:GIMIAS");
    soapbody->SetAttribute("use", "encoded");
    input->LinkEndChild(soapbody);

    TiXmlElement *output = new TiXmlElement("wsdl:output");
    operation->LinkEndChild(output);

    soapbody = new TiXmlElement("wsdlsoap:body");
    soapbody->SetAttribute("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
    soapbody->SetAttribute("namespace", "urn:GIMIAS");
    soapbody->SetAttribute("use", "encoded");
    output->LinkEndChild(soapbody);

    TiXmlElement *fault = new TiXmlElement("wsdl:fault");
    fault->SetAttribute("name", std::string(faultMessageDef->Attribute("name")));
    operation->LinkEndChild(fault);

    soapbody = new TiXmlElement("wsdlsoap:fault");
    soapbody->SetAttribute("name", std::string(faultMessageDef->Attribute("name")));
    soapbody->SetAttribute("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
    soapbody->SetAttribute("namespace", "urn:GIMIAS");
    soapbody->SetAttribute("use", "encoded");
    fault->LinkEndChild(soapbody);
  }

  // Service
  TiXmlElement *service = new TiXmlElement("wsdl:service");
  service->SetAttribute("name", "GIMIASService");
  definitions->LinkEndChild(service);

  TiXmlElement *documentation = new TiXmlElement("documentation");
  documentation->LinkEndChild(new TiXmlText("WSDL File for GIMIAS"));
  service->LinkEndChild(documentation);

  TiXmlElement *port = new TiXmlElement("wsdl:port");
  port->SetAttribute("binding", "impl:GIMIASBinding");
  port->SetAttribute("name", "GIMIAS");
  service->LinkEndChild(port);

  // Location
  TiXmlElement *soapaddress = new TiXmlElement("wsdlsoap:address");
  soapaddress->SetAttribute("location", "http://" + m_ServiceProvideripAddress + ":" +
                                            m_ServiceProviderPort +
                                            "/axis2/services/wsGimias");
  port->LinkEndChild(soapaddress);

  // Save to the output file
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();
  std::string servicesFileName = settings->GetProjectHomePath();
  servicesFileName += "/wsrepo/services/wsGimias/wsGimias.wsdl";
  doc.SaveFile(servicesFileName);
}

std::list< std::string > WebServerProcessor::GetPublicIpAddressList() {
  std::list< std::string > IPlist;

#ifdef _WIN32

  // IP address
  WSAData wsaData;
  if (WSAStartup(MAKEWORD(1, 1), &wsaData) != 0) {
  }

  char ac[80];
  if (gethostname(ac, sizeof(ac)) == SOCKET_ERROR) {
    std::cerr << "Error " << WSAGetLastError() << " when getting local host name."
              << std::endl;
    return IPlist;
  }

  struct hostent *phe = gethostbyname(ac);
  if (phe == 0) {
    std::cerr << "Yow! Bad host lookup." << std::endl;
    return IPlist;
  }

  for (int i = 0; phe->h_addr_list[i] != 0; ++i) {
    struct in_addr addr;
    memcpy(&addr, phe->h_addr_list[i], sizeof(struct in_addr));
    IPlist.push_back(inet_ntoa(addr));
  }

  WSACleanup();
#else

  struct ifaddrs *ifAddrStruct = NULL;
  struct ifaddrs *ifa = NULL;
  void *tmpAddrPtr = NULL;

  getifaddrs(&ifAddrStruct);

  for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
    if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
      // is a valid IP4 Address
      tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
      char addressBuffer[INET_ADDRSTRLEN];
      inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
      //			printf("%s IP Address %s\n", ifa->ifa_name,
      //addressBuffer);
      IPlist.push_back(addressBuffer);
    } else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
      // is a valid IP6 Address
      tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
      char addressBuffer[INET6_ADDRSTRLEN];
      inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
      //			printf("%s IP Address %s\n", ifa->ifa_name,
      //addressBuffer);
      IPlist.push_back(addressBuffer);
    }
  }
  if (ifAddrStruct != NULL)
    freeifaddrs(ifAddrStruct);

#endif

  if (IPlist.empty()) {
    IPlist.push_back("127.0.0.1");
  }

  return IPlist;
}

std::string WebServerProcessor::GetServiceProviderPort() const {
  return m_ServiceProviderPort;
}

void WebServerProcessor::SetServiceProviderPort(std::string val) {
  m_ServiceProviderPort = val;
}

std::string WebServerProcessor::GetServerPort() const { return m_ServerPort; }

void WebServerProcessor::SetServerPort(std::string val) { m_ServerPort = val; }

std::string WebServerProcessor::GetServiceProviderIPAddress() const {
  return m_ServiceProvideripAddress;
}

void WebServerProcessor::SetServiceProviderIPAddress(std::string val) {
  m_ServiceProvideripAddress = val;
}

void WebServerProcessor::ReadSettings() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  // Update configuration
  std::list< std::string > addresses = WebServerProcessor::GetPublicIpAddressList();
  std::string ipAddress = *addresses.begin();
  settings->GetPluginProperty("WebServicesPlugin", "ServiceProviderIPAddress", ipAddress);
  SetServiceProviderIPAddress(ipAddress);

  // Port
  std::string portStr = "9090";
  settings->GetPluginProperty("WebServicesPlugin", "ServiceProviderPort", portStr);
  SetServiceProviderPort(portStr);

  // Server port
  std::string serverPortStr = "9090";
  settings->GetPluginProperty("WebServicesPlugin", "ServerPort", serverPortStr);
  SetServerPort(serverPortStr);
}

bool WebServerProcessor::IsStarted() { return m_Thread != NULL; }

std::string WebServerProcessor::GetLogStream() {
  if (m_Thread == NULL) {
    return "";
  }

  return m_Thread->GetLogStream();
}

void WebServerProcessor::OnPluginManagerModified(
    Core::Runtime::PluginProviderManager::STATE state) {

  // If server is enabled, start it
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  std::string status = "disabled";
  settings->GetPluginProperty("WebServicesPlugin", "status", status);
  if (status != "enabled") {
    return;
  }

  // Check if there's a processor executing
  // Then we cannot stop the web server because the processor
  // could be called by the web server
  Core::ProcessorThread::Pointer processorThread;
  processorThread = Core::Runtime::Kernel::GetProcessorManager()->GetProcessorThread(
      -1, Core::ProcessorThread::STATE_ACTIVE);
  if (processorThread.IsNotNull()) {
    return;
  }

  switch (state) {
  case Core::Runtime::PluginProviderManager::STATE_IDLE:
    Start();
    break;
  case Core::Runtime::PluginProviderManager::STATE_LOADING_PLUGINS:
    Stop();
    break;
  }
}