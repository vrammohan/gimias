# Used to configure SceneViewPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

sceneViewPlugin = GimiasPluginProject("SceneViewPlugin", api)
sceneViewPlugin.AddProjects([gmCore])
sceneViewPlugin.AddSources(["*.cxx", "*.h"])
sceneViewPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
sceneViewPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "ExecuteCommandWidget"
  ]
sceneViewPlugin.AddWidgetModules(widgetModules, _useQt = 0)

sceneViewPlugin.SetPrecompiledHeader("SceneViewPluginPCH.h")

