/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "ContouringWidgetPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreFactoryManager.h"
#include "coreProcessorManager.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"

#include "mitkProperties.h"
#include "ManualContouringTool.h"

namespace contouringPlugin {

ContouringWidgetPanelWidget::ContouringWidgetPanelWidget(
    wxWindow *parent, int id /*= wxID_ANY*/, const wxPoint &pos /*= wxDefaultPosition*/,
    const wxSize &size /*= wxDefaultSize*/, long style /* = 0*/)
    : ContouringWidgetPanelWidgetUI(parent, id, pos, size, style) {
  m_Processor = contouringPlugin::ContouringWidgetProcessor::New();
  m_InputDataHolder = Core::DataEntityHolder::New();
  m_ContourDataHolder = Core::DataEntityHolder::New();

  // Contour is closed by default
  m_chkClosedContour->SetValue(true);

  // Each time a text control is modified, this function will be called
  this->changeInWidgetObserver.SetSlotFunction(this,
                                               &ContouringWidgetPanelWidget::UpdateData);

  // Set the observer on the parameters that are accessible through the GUI
  this->changeInWidgetObserver.Observe(m_txtMinDist);
  this->changeInWidgetObserver.Observe(m_chkClosedContour);

  SetName("ContouringWidgetPanelWidget");
}

ContouringWidgetPanelWidget::~ContouringWidgetPanelWidget() {

  // We don't need to destroy anything because all the child windows
  // of this wxWindow are destroyed automatically
}

void ContouringWidgetPanelWidget::OnInit() {
  //------------------------------------------------------
  // Observers to data
  m_Processor->GetOutputDataEntityHolder(ContouringWidgetProcessor::CONTOUR)
      ->AddObserver(this, &ContouringWidgetPanelWidget::OnModifiedOutputDataEntity);

  m_Processor->GetInputDataEntityHolder(ContouringWidgetProcessor::REFERENCE_IMAGE)
      ->AddObserver(this, &ContouringWidgetPanelWidget::OnModifiedInputDataEntity);

  m_LevelWindowInteractorState = true;

  UpdateWidget();
}

void ContouringWidgetPanelWidget::UpdateWidget() {
  Core::DataEntity::Pointer dataEntity;
  dataEntity =
      m_Processor->GetInputDataEntity(ContouringWidgetProcessor::REFERENCE_IMAGE);

  if (dataEntity.IsNotNull()) {
    // m_btnAcceptContour->Enable(true); // to be controlled by the behaviour of the
    // buttons
    m_btnNewContour->Enable(true);
    m_labelMinDist->Enable(true);
    m_txtMinDist->Enable(true);
    m_chkClosedContour->Enable(true);
  } else {
    m_btnNewContour->Enable(false);
    m_btnAcceptContour->Enable(false);
    m_labelMinDist->Enable(false);
    m_txtMinDist->Enable(false);
    m_chkClosedContour->Enable(false);
  }

  UpdateHelperWidget();
}

void ContouringWidgetPanelWidget::UpdateData() {
  if (!this->Validate())
    return;

  if (m_ContouringInteractor) {
    // Get tool
    mitk::ManualContouringTool *manualContour =
        static_cast< mitk::ManualContouringTool * >(
            m_ContouringInteractor->GetContourTool());

    // So far there is only one parameter to change
    manualContour->SetDistBetweenPoints(gbl::GetNumber(m_txtMinDist));
    manualContour->SetContourClosed(m_chkClosedContour->GetValue());
  }
}

void ContouringWidgetPanelWidget::OnBtnNewContour(wxCommandEvent &event) {
  // Catch the exception from the processor and show the message box
  try {
    // check if reference image is available
    Core::DataEntity::Pointer inputDataEntity;
    inputDataEntity =
        m_Processor->GetInputDataEntity(ContouringWidgetProcessor::REFERENCE_IMAGE);
    if (inputDataEntity.IsNull()) {
      return;
    }

    // create the data entity for the contour
    Core::DataEntity::Pointer contourDataEntity;
    contourDataEntity = Core::DataEntity::New(
        Core::DataEntityType(Core::PointSetTypeId | Core::MeasurementTypeId));
    contourDataEntity->GetMetadata()->SetName("Contour");
    contourDataEntity->Resize(inputDataEntity->GetNumberOfTimeSteps(),
                              typeid(Core::vtkPolyDataPtr));
    contourDataEntity->SetFather(inputDataEntity);

    // set input data (image + contour) holders
    m_ContourDataHolder->SetSubject(contourDataEntity);
    m_InputDataHolder->SetSubject(inputDataEntity);

    // Add to data entity list
    Core::DataEntityHelper::AddDataEntityToList(
        m_Processor->GetOutputDataEntityHolder(ContouringWidgetProcessor::CONTOUR),
        false);

    // Start interaction
    StartInteraction();

    // Disable this until contour accepted
    m_btnNewContour->Enable(false);

    // ... and enable this
    m_btnAcceptContour->Enable(true);

    // Reset this and update tool
    m_chkClosedContour->SetValue(true);
    UpdateData();
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      "ContouringWidgetPanelWidget::OnBtnNewContour");
}

void ContouringWidgetPanelWidget::StartInteraction() {
  try {
    // Create interactor
    m_ContouringInteractor = Core::ContouringInteractor::New(
        GetRenderingTree(), m_ContourDataHolder, m_InputDataHolder,
        Core::ContouringInteractor::MANUALCONTOURTOOL);

    // activate interaction
    m_ContouringInteractor->ConnectToDataTreeNode();
    m_ContouringInteractor->GetInteractorStateHolder()->SetSubject(
        Core::ContouringInteractor::INTERACTOR_ENABLED);

    if (!GetMultiRenderWindow()) {
      return;
    }

    // Disable some default behavior of multirenderwindow
    GetMultiRenderWindow()->GetMetadata()->AddTag("AxisLocked", true);
    /*m_LevelWindowInteractorState =
    GetMultiRenderWindow()->GetMetadata()->GetTagValue<bool>( "LevelWindowInteractor" );
    GetMultiRenderWindow()->GetMetadata()->AddTag( "LevelWindowInteractor", false );
    GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();*/
    GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      "ContouringWidgetPanelWidget::StartInteraction");
}

void ContouringWidgetPanelWidget::OnBtnAcceptContour(wxCommandEvent &event) {
  // Catch the exception from the processor and show the message box
  try {
    Core::DataEntity::Pointer inputDataEntity;
    inputDataEntity =
        m_Processor->GetInputDataEntity(ContouringWidgetProcessor::REFERENCE_IMAGE);
    if (inputDataEntity.IsNotNull()) {
      // Get tool
      mitk::ManualContouringTool *manualContour =
          static_cast< mitk::ManualContouringTool * >(
              m_ContouringInteractor->GetContourTool());

      // get temporary contour as polydata
      vtkSmartPointer< vtkPolyData > polydata = manualContour->GetContourAsPolyData();

      // set polydata into contour data holder
      Core::DataEntity::Pointer contourDataEntity = m_ContourDataHolder->GetSubject();
      contourDataEntity->SetTimeStep(polydata, 0);

      // customize rendering of the node
      mitk::DataTreeNode::Pointer node;
      boost::any anyData = GetRenderingTree()->GetNode(contourDataEntity);
      Core::CastAnyProcessingData(anyData, node);
      node->SetProperty("pointsize", mitk::FloatProperty::New(0.5f));
      node->SetProperty("contoursize", mitk::FloatProperty::New(0.25f));
      node->SetProperty("show distances", mitk::BoolProperty::New(false));
      // node->SetProperty("show points",mitk::BoolProperty::New(false));

      // Stop Interaction
      StopInteraction();

      // Re-enable this button
      m_btnNewContour->Enable(true);
      // ... and disable this
      m_btnAcceptContour->Enable(false);
    }

    UpdateData();

    UpdateWidget();
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      "ContouringWidgetPanelWidget::OnBtnAcceptContour");
}

void ContouringWidgetPanelWidget::StopInteraction() {
  try {
    if (m_ContouringInteractor) {
      mitk::ManualContouringTool *manualContour =
          static_cast< mitk::ManualContouringTool * >(
              m_ContouringInteractor->GetContourTool());

      // hide temporary contour
      if (manualContour)
        manualContour->SetContourVisible(false);

      // disable interactor
      m_ContouringInteractor->DisconnectFromDataTreeNode();
      m_ContouringInteractor->GetInteractorStateHolder()->SetSubject(
          Core::ContouringInteractor::INTERACTOR_DISABLED);
      m_ContouringInteractor = NULL;
      m_ContourDataHolder->SetSubject(NULL);
      m_InputDataHolder->SetSubject(NULL);
    }

    // reactivate previous multirenderwindow behavior
    GetMultiRenderWindow()->GetMetadata()->AddTag("AxisLocked", false);
    /*GetMultiRenderWindow()->GetMetadata()->AddTag( "LevelWindowInteractor",
     * m_LevelWindowInteractorState );*/
    GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      "ContouringWidgetPanelWidget::StopInteraction");
}

void ContouringWidgetPanelWidget::OnModifiedOutputDataEntity() { UpdateWidget(); }

void ContouringWidgetPanelWidget::UpdateHelperWidget() {
  if (GetHelperWidget() == NULL) {
    return;
  }
  GetHelperWidget()->SetInfo(Core::Widgets::HELPER_INFO_LEFT_BUTTON,
                             " info that is useful in order to use the processor");
}

bool ContouringWidgetPanelWidget::Enable(bool enable /*= true */) {
  bool bReturn = ContouringWidgetPanelWidgetUI::Enable(enable);

  // If this panel widget is selected -> Update the widget
  if (enable) {
    UpdateWidget();
  }

  return bReturn;
}

void ContouringWidgetPanelWidget::OnModifiedInputDataEntity() {
  try {
    StopInteraction();
    UpdateWidget();
  }
  coreCatchExceptionsLogAndNoThrowMacro(
      "ContouringWidgetPanelWidget::OnModifiedInputDataEntity");
}

Core::BaseProcessor::Pointer ContouringWidgetPanelWidget::GetProcessor() {
  return m_Processor.GetPointer();
}

} // namespace contouringPlugin
