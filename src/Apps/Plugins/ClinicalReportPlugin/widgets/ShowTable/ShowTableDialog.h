/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ShowTableDialog_H
#define _ShowTableDialog_H

#include "ShowTableDialogUI.h"
#include "coreBaseWindow.h"

// stl
#include <string>
#include <vector>

namespace Core {
namespace Widgets {

/**
\brief Used to display a table in a widget.
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 09 Nov 2010
*/

#define wxID_ShowTableDialog wxID("wxID_ShowTableDialog")

  class ShowTableDialog : public ShowTableDialogUI, public Core::BaseWindow {
    // OPERATIONS
  public:
    //!
    ShowTableDialog();

    //!
    ~ShowTableDialog();

    //!
    virtual Core::BaseProcessor::Pointer GetProcessor();

    //!
    void SetTable(std::vector< std::string > header,
                  std::vector< std::vector< std::string > > lines);

    // ATTRIBUTES
  public:
  private:
    DECLARE_EVENT_TABLE()

    // ATTRIBUTES
  private:
  };

} // namespace Widgets
} // namespace Core

#endif //_ShowTableDialog_H
