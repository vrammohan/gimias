/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef SecurityPluginCryptoProcessor_H
#define SecurityPluginCryptoProcessors_H

// core

#include "coreException.h"
#include "coreBaseProcessor.h"

// poco
#include "Poco/Crypto/CipherFactory.h"
#include "Poco/Crypto/Cipher.h"
#include "Poco/Crypto/CipherKey.h"
#include "Poco/Crypto/X509Certificate.h"

/**
\brief ncrypts and decrypts texts using a master password

\ingroup Security Plugin
\author H�ctor Fernandez
\date Jan 2012
*/

class PLUGIN_EXPORT SecurityPluginCryptoProcessor : public Core::SmartPointerObject {
public:
  coreDeclareSmartPointerClassMacro(SecurityPluginCryptoProcessor,
                                    Core::SmartPointerObject);

  //! Initializates the Crypto processor
  void Init();

  //! Sets master key value
  void SetMasterKey(std::string str);

  //! changes masterkey to str and updates regitered password related with it
  void UpdateMasterKey(std::string str);

  //! remove master key and sets encripted to zero
  void ResetMasterKey();

  /**returns de encrypted password & register it in the m_RegisterPasswords
          list depending on value exists (if exists register is replaced)
          \param[in] keyword when keyword is empty Encript function uses the variable in
     m_MasterkeHolder,
          if it is defined, encript is made using keyword as a password
  */
  std::string Encrypt(std::string name, std::string password, std::string keyword = "",
                      bool exists = false);

  //! returns de decrypted password of the registered name in m_RegisterPass
  std::string DecryptFromList(std::string name);

  //! returns de decrypted value of name
  std::string Decrypt(std::string name, std::string keyword = "");

  //! register a new pair of name-password in the register password depending on value
  //! exists (if exists register is replaced)
  void RegisterPassword(std::string name, std::string password, bool exists = false);

  //! checks if Masterkey is the same as the variable
  bool CheckMasterKey(std::string str);

  // returns true if master key is define
  bool GetBoolEncrypt();

  // returns store password from register reg
  std::string GetPassword(std::string reg);

  //!
  Core::DataHolder< std::string >::Pointer GetMasterKeyHolder();

protected:
  //! Encryted;
  bool m_Encripted;

  //! master Password
  Core::DataHolder< std::string >::Pointer m_MasterKeyHolder;

private:
  //! finds if some node is save throw master key strKey
  bool Find(std::string strKey);

  //! master Password
  // Core::DataHolder<std::string>::Pointer m_MasterKeyHolder;

  //! use! to permute master key
  std::string m_salt;

  //! registered passwords
  std::map< std::string, std::string > m_RegisterPasswords;
};
#endif // SecurityPluginCryptoProcessor_H
