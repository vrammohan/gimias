# Used to configure coreIO
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmCommonObjects = api.CreateCompiledProject("gmCommonObjects", "dll")
gmCommonObjects.AddProjects([ 
  itk,
  boost,
  baseLib
])

gmCommonObjects.SetPrecompiledHeader("gmCommonObjectsPCH.h")

baseFolders = [
  "src"
]

for baseFolder in baseFolders:
        gmCommonObjects.AddSources(["%s/*.cxx" % baseFolder], checkExists = 0)
        gmCommonObjects.AddSources(["%s/*.txx" % baseFolder], checkExists = 0)
        gmCommonObjects.AddSources(["%s/*.h" % baseFolder], checkExists = 0)
        gmCommonObjects.AddIncludeFolders([baseFolder])

