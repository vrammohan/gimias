/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SshPluginWidgetCollective.h"
#include "SshPluginConfigurationPanelWidget.h"
#include "wxID.h"
#include "coreWindowConfig.h"
#include "coreFactoryManager.h"
#include "corePluginProviderManager.h"
#include "coreFactoryManager.h"

#include "SSHPluginProvider.h"
#include "SSHPluginProviderConfigurationDialog.h"
#include "SSHPluginDataTransfer.h"
#include "SSHPluginModuleExecution.h"

SshPlugin::WidgetCollective::WidgetCollective() {
  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

  SshPluginNodeManager::Pointer nodeManager = SshPluginNodeManager::New();

  // Panel widgets
  graphicalIface->RegisterFactory(
      sshPluginConfigurationPanelWidget::Factory::New(nodeManager),
      Core::WindowConfig().Preferences().Caption(
          wxString(wxT("SSH configuration")).ToStdString()));

  graphicalIface->RegisterFactory(
      SSHPluginProviderConfigurationDialog::Factory::New(nodeManager),
      Core::WindowConfig().Free().Caption(SSHPluginProvider::GetNameClass()));

  graphicalIface->RegisterFactory(Core::Runtime::PluginProvider::GetNameClass(),
                                  SSHPluginProvider::Factory::New(nodeManager));

  graphicalIface->RegisterImpl("SSHCommandLineModule",
                               SSHPluginModuleExecution::Factory::New(nodeManager));

  graphicalIface->RegisterFactory(Core::DynDataTransferBase::GetNameClass(),
                                  SSHPluginDataTransfer::Factory::New(nodeManager));

  // After register, we need to load configuration again to add the plugin
  // provider
  graphicalIface->GetPluginProviderManager()->LoadConfiguration(false);
}

SshPlugin::WidgetCollective::~WidgetCollective() {}
