/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _XNATProcessorCollective_H
#define _XNATProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

/**
\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class XNATProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(XNATProcessorCollective, Core::SmartPointerObject);

private:
  XNATProcessorCollective();

  //!
  ~XNATProcessorCollective();

private:
};

#endif //_XNATProcessorCollective_H
