/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _WebServicesPlugin_H
#define _WebServicesPlugin_H

#include "WebServicesPluginWidgetCollective.h"

// CoreLib
#include "coreFrontEndPlugin.h"

namespace WebServicesPlugin {

/**
\brief Web Services Plugin

\ingroup WebServicesPlugin
\author Xavi Planes
\date Sept 2011
*/
class PLUGIN_EXPORT WebServicesPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
  // TYPE DEFINITIONS
public:
  coreDeclareSmartPointerClassMacro(WebServicesPlugin,
                                    Core::FrontEndPlugin::FrontEndPlugin);

  // OPERATIONS
protected:
  //!
  WebServicesPlugin(void);

  //!
  virtual ~WebServicesPlugin(void);

private:
  //! Purposely not implemented
  WebServicesPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the widgets for the plugin
  WidgetCollective::Pointer m_Widgets;
};

} // namespace WebServicesPlugin

#endif // WebServicesPlugin_H
