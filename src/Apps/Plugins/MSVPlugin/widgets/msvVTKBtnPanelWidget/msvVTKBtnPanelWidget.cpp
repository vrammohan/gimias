/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvVTKBtnPanelWidget.h"

// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreFactoryManager.h"
#include "coreProcessorManager.h"
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"
#include "coreRenderWindowContainer.h"
#include "coreRenderWindowBase.h"
#include "coreWorkingAreaFactory.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreFile.h"
#include "coreDirectory.h"
#include "coreDataHolderBase.h"

// wxMitk
#include "wxMitkSelectableGLWidget.h"
#include "wxMitkMultiRenderWindowLayout.h"
#include "wx/busyinfo.h"

// wxWidgets
#include <wx/utils.h>

// VTK
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkTransform.h"
#include "vtkTransformPolyDataFilter.h"

// xml
#include "tinyxml.h"

// streams
// read a file into memory
#include <iostream>
#include <fstream>

namespace msvPlugin {

VTKBtnPanelWidget::VTKBtnPanelWidget(wxWindow *parent, int id /*= wxID_ANY*/,
                                     const wxPoint &pos /*= wxDefaultPosition*/,
                                     const wxSize &size /*= wxDefaultSize*/,
                                     long style /* = 0*/)
    : msvVTKBtnPanelWidgetUI(parent, id, pos, size, style) {
  UpdateWidget();
}

VTKBtnPanelWidget::~VTKBtnPanelWidget() {
  // We don't need to destroy anything because all the child windows
  // of this wxWindow are destroyed automatically
}

void VTKBtnPanelWidget::UpdateWidget() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  std::string showStr;
  settings->GetPluginProperty("MSV", "Show", showStr);
  bool show = showStr == "false" ? false : true;
  m_checkShow->SetValue(show);

  std::string showLabelsStr;
  settings->GetPluginProperty("MSV", "ShowLabels", showLabelsStr);
  bool showLabels = showLabelsStr == "true" ? true : false;
  m_checkShowLabels->SetValue(showLabels);

  std::string flyToStr;
  settings->GetPluginProperty("MSV", "FlyTo", flyToStr);
  bool flyTo = flyToStr == "false" ? false : true;
  m_checkFlyTo->SetValue(flyTo);

  std::string centerStr;
  settings->GetPluginProperty("MSV", "Center", centerStr);
  bool center = centerStr == "false" ? false : true;
  m_checkCenter->SetValue(center);

  std::string flyToNStepsStr = "1000";
  settings->GetPluginProperty("MSV", "FlyToNSteps", flyToNStepsStr);
  int flyToNSteps = atoi(flyToNStepsStr.c_str());
  m_sldFlyToNSteps->SetValue(flyToNSteps);
}

void VTKBtnPanelWidget::UpdateData() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  std::string showStr = m_checkShow->GetValue() ? "true" : "false";
  settings->SetPluginProperty("MSV", "Show", showStr);

  std::string showLabelsStr = m_checkShowLabels->GetValue() ? "true" : "false";
  settings->SetPluginProperty("MSV", "ShowLabels", showLabelsStr);

  std::string flyToStr = m_checkFlyTo->GetValue() ? "true" : "false";
  settings->SetPluginProperty("MSV", "FlyTo", flyToStr);

  std::string centerStr = m_checkCenter->GetValue() ? "true" : "false";
  settings->SetPluginProperty("MSV", "Center", centerStr);

  std::string flyToNStepsStr =
      wxString::Format("%d", m_sldFlyToNSteps->GetValue()).ToStdString();
  settings->SetPluginProperty("MSV", "FlyToNSteps", flyToNStepsStr);

  // Refresh render window
  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
  Core::Widgets::PluginTab *pluginTab = dynamic_cast< Core::Widgets::PluginTab * >(
      graphicalIface->GetMainWindow()->GetCurrentPluginTab());
  if (!pluginTab)
    return;

  Core::BaseWindow *workingArea = pluginTab->GetCurrentWorkingArea();
  if (!workingArea)
    return;

  Core::Widgets::RenderWindowContainer *container =
      dynamic_cast< Core::Widgets::RenderWindowContainer * >(workingArea);
  if (!container || container->GetNumberOfWindows() == 0)
    return;

  container->GetWindow(0)->GetMetadataHolder()->NotifyObservers();
}
}
