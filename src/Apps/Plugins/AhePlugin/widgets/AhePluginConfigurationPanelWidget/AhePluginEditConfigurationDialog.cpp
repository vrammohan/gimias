/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "AhePluginEditConfigurationDialog.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreFactoryManager.h"
#include "coreProcessorManager.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"

namespace ahePlugin {

/**
 *  Class constructor.
 *  Creates the AHE node and initializes the text fields.
 */
AhePluginEditConfigurationDialog::AhePluginEditConfigurationDialog(
    wxWindow *parent, int id, const wxString &title,
    const wxPoint &pos /*=wxDefaultPosition*/, const wxSize &size /*=wxDefaultSize*/,
    long style /*=wxDEFAULT_DIALOG_STYLE*/)
    : AhePluginEditConfigurationDialogUI(parent, id, title, pos, size, style) {
  m_AppNode = aheAppInst::New();
  UpdateWidget();
}

/**
 *  Class destructor.
 */
AhePluginEditConfigurationDialog::~AhePluginEditConfigurationDialog() {
  // We don't need to destroy anything because all the child windows
  // of this wxWindow are destroyed automatically
}

/**
 * Loads the dialog's initial content and configuration
 */
void AhePluginEditConfigurationDialog::OnInit() { UpdateWidget(); }

/**
 * Updates GUI from working data
 */
void AhePluginEditConfigurationDialog::UpdateWidget() {
  if (m_AppNode->GetHost()) {
    m_txtHostname->SetValue(m_AppNode->GetHost()->GetHostname());
    m_txtUsername->SetValue(m_AppNode->GetHost()->GetUsername());
    m_txtPort->SetValue(wxString::Format("%d", m_AppNode->GetHost()->GetPort()));
    m_txtResourceName->SetValue(m_AppNode->GetApp()->GetResourceName());
    m_txtAppName->SetValue(m_AppNode->GetApp()->GetName());
    // m_txtPassword is ignored
  }
}

/**
 * Updates working data from GUI
 */
void AhePluginEditConfigurationDialog::UpdateData() {
  // Set parameters to processor. Pending
  aheHost::Pointer node = aheHost::New();
  aheApp::Pointer app = aheApp::New();
  node->SetHostname(m_txtHostname->GetValue().ToStdString());
  node->SetUsername(m_txtUsername->GetValue().ToStdString());
  long port;
  m_txtPort->GetValue().ToLong(&port);
  node->SetPort(port);
  app->SetResourceName(m_txtResourceName->GetValue().ToStdString());
  app->SetName(m_txtAppName->GetValue().ToStdString());
  m_AppNode->SetApp(app);
  m_AppNode->SetHost(node);
  // m_txtPassword is ignored
}

/**
 * Gets the configured node
 */
aheAppInst::Pointer AhePluginEditConfigurationDialog::GetNode() const {
  return m_AppNode;
}

/**
 * Initializes the configured node. Used for editing the node.
 */
void AhePluginEditConfigurationDialog::SetNodeData(aheAppInst::Pointer val) {
  m_AppNode->SetApp(val->GetApp());
  m_AppNode->SetHost(val->GetHost());
  UpdateWidget();
}

/**
 * OK button event handler.
 */
void AhePluginEditConfigurationDialog::OnOK(wxCommandEvent &event) {
  UpdateData();
  EndModal(wxID_OK);
}

/**
 * Test button event handler.
 */
void AhePluginEditConfigurationDialog::OnTest(wxCommandEvent &event) {
  UpdateData();
  if (m_AppNode->GetHost()->GetHostname().empty() ||
      m_AppNode->GetHost()->GetPort() <= 0 ||
      m_AppNode->GetHost()->GetUsername().empty() ||
      m_txtPassword->GetValue()
          .empty() || // Only used for the Test. Not really stored anywhere.
      m_AppNode->GetApp()->GetName().empty() ||
      m_AppNode->GetApp()->GetResourceName().empty()) {
    throw Core::Exceptions::Exception("AhePluginEditConfigurationDialog::OnTest",
                                      "Invalid or incomplete data. Cannot perform test");
  } else {
    m_AppNode->GetHost()->SetPassword(m_txtPassword->GetValue().ToStdString());
    std::pair< bool, std::string > result = m_AppNode->SimpleInstCheck();
    m_AppNode->GetHost()->SetPassword("");
    if (result.first) {
      wxMessageBox(wxT("Test passed"));
    } else {
      result.second = "Test failed. " + result.second;
      throw Core::Exceptions::Exception("AhePluginEditConfigurationDialog::OnTest",
                                        result.second.c_str());
    }
  }
}

/**
 * Cancel button event handler.
 */
void AhePluginEditConfigurationDialog::OnCancel(wxCommandEvent &event) {
  EndModal(wxID_CANCEL);
}

} // namespace ahePlugin
