from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

# plugin project definition
msvPlugin = GimiasPluginProject("msvPlugin", api)
# plugin dependencies
projects = [
    gmCore
]
msvPlugin.AddProjects(projects)
# plugin sources
msvPlugin.AddSources(["*.cxx", "*.h"])
msvPlugin.SetPrecompiledHeader("msvPluginPCH.h")

# plugin widgets
widgetModules = [
  "msvVTKBtnPanelWidget",
  "msvRenderingTree"
]
msvPlugin.AddWidgetModules(widgetModules, _useQt = 0)
# msvPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
msvPlugin.AddIncludeFolders(["processors",])
msvPlugin.AddFilesToInstall(msvPlugin.Glob("resource"), "resource")
