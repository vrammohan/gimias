/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ClinicalReportProcessorCollective_H
#define _ClinicalReportProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 19 Oct 2010
*/

class ClinicalReportProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(ClinicalReportProcessorCollective,
                                    Core::SmartPointerObject);

private:
  ClinicalReportProcessorCollective();

private:
};

#endif //_ClinicalReportProcessorCollective_H
