/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATPlugin.h"

// core
#include "coreReportExceptionMacros.h"
#include "corePluginMacros.h"
#include "coreProfile.h"

coreBeginDefinePluginMacro(XNATPlugin) coreEndDefinePluginMacro()

    XNATPlugin::XNATPlugin(void)
    : FrontEndPlugin() {
  try {
    m_Processors = XNATProcessorCollective::New();
    m_Widgets = XNATWidgetCollective::New();

    m_Widgets->Init();
  }
  coreCatchExceptionsReportAndNoThrowMacro(XNATPlugin::XNATPlugin)
}

XNATPlugin::~XNATPlugin(void) {}
