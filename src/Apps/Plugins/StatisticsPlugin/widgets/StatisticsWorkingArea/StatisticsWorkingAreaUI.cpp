// -*- C++ -*- generated by wxGlade 0.6.3 on Tue Oct 18 17:50:05 2011

#include "StatisticsWorkingAreaUI.h"

// begin wxGlade: ::extracode

// end wxGlade

StatisticsWorkingAreaUI::StatisticsWorkingAreaUI(wxWindow *parent, int id,
                                                 const wxPoint &pos, const wxSize &size,
                                                 long style)
    : wxScrolledWindow(parent, id, pos, size, style) {
  // begin wxGlade: StatisticsWorkingAreaUI::StatisticsWorkingAreaUI
  window_1 = new Core::Widgets::TableView(this, wxID_ANY);

  set_properties();
  do_layout();
  // end wxGlade
}

void StatisticsWorkingAreaUI::set_properties() {
  // begin wxGlade: StatisticsWorkingAreaUI::set_properties
  SetScrollRate(10, 10);
  // end wxGlade
}

void StatisticsWorkingAreaUI::do_layout() {
  // begin wxGlade: StatisticsWorkingAreaUI::do_layout
  wxBoxSizer *sizer_7 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_4 = new wxBoxSizer(wxHORIZONTAL);
  sizer_7->Add(20, 20, 0, 0, 0);
  sizer_7->Add(window_1, 1, wxEXPAND, 0);
  sizer_7->Add(sizer_4, 0, wxEXPAND, 0);
  sizer_7->Add(20, 20, 0, 0, 0);
  SetSizer(sizer_7);
  sizer_7->Fit(this);
  // end wxGlade
}
