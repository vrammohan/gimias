# Used to configure lcore
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

import csnGIMIAS
import csnGIMIASCommon

import Core.CommonObjects.csnGMCommonObjectsTests
import Core.Kernel.csnGMKernelTests
import Apps.Plugins.MITKPlugin.csnMITKPluginTests

api = GetAPI("2.5.0-beta")

gimiasLib = api.RewrapProject(csnGIMIAS.gimiasLib)
gimiasLib.AddTests(["Tests/*.*"], cxxTest)

# GIMIAS
gimias = api.RewrapProject(csnGIMIASCommon.CreateGIMIAS( ))

# Add dependencies to gimias executable project
gimias.AddProjects([ gimiasLib] )
gimias.AddProjects([mitkPlugin] )
