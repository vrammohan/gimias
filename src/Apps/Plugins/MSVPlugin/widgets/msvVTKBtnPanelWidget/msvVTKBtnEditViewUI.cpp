// -*- C++ -*- generated by wxGlade 0.6.5 (standalone edition) on Mon Nov 26 22:34:53 2012

#include "msvVTKBtnEditViewUI.h"

// begin wxGlade: ::extracode

// end wxGlade

msvVTKBtnEditViewUI::msvVTKBtnEditViewUI(wxWindow *parent, int id, const wxPoint &pos,
                                         const wxSize &size, long style)
    : wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL) {
  // begin wxGlade: msvVTKBtnEditViewUI::msvVTKBtnEditViewUI
  m_txtName = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
  m_bmpClose = new wxBitmapButton(this, wxID_CANCEL, wxNullBitmap);
  m_bmpSnapshot = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap, wxDefaultPosition,
                                     wxDefaultSize, wxDOUBLE_BORDER);
  m_btnTakeSnapshot = new wxBitmapButton(this, wxID_CAPTURE_VIEW, wxNullBitmap);
  m_btnCleanSnapshot = new wxBitmapButton(this, wxID_REMOVE_VIEW, wxNullBitmap);
  m_btnLoadSnapshot = new wxBitmapButton(this, wxID_LOAD_SNAPSHOT, wxNullBitmap);
  m_chkVisualProps = new wxCheckBox(this, wxID_ANY, wxT("Visual props"));
  m_chkCamera = new wxCheckBox(this, wxID_ANY, wxT("Camera"));

  set_properties();
  do_layout();
  // end wxGlade
}

BEGIN_EVENT_TABLE(msvVTKBtnEditViewUI, wxPanel)
// begin wxGlade: msvVTKBtnEditViewUI::event_table
EVT_BUTTON(wxID_CANCEL, msvVTKBtnEditViewUI::OnCancel)
EVT_BUTTON(wxID_CAPTURE_VIEW, msvVTKBtnEditViewUI::OnCaptureView)
EVT_BUTTON(wxID_REMOVE_VIEW, msvVTKBtnEditViewUI::OnRemoveView)
EVT_BUTTON(wxID_LOAD_SNAPSHOT, msvVTKBtnEditViewUI::OnLoadSnapshot)
// end wxGlade
END_EVENT_TABLE();

void msvVTKBtnEditViewUI::OnCancel(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT(
      "Event handler (msvVTKBtnEditViewUI::OnCancel) not implemented yet")); // notify the
                                                                             // user that
                                                                             // he hasn't
                                                                             // implemented
                                                                             // the event
                                                                             // handler
                                                                             // yet
}

void msvVTKBtnEditViewUI::OnCaptureView(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT("Event handler (msvVTKBtnEditViewUI::OnCaptureView) not implemented "
                 "yet")); // notify the user that he hasn't implemented the event handler
                          // yet
}

void msvVTKBtnEditViewUI::OnRemoveView(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT("Event handler (msvVTKBtnEditViewUI::OnRemoveView) not implemented "
                 "yet")); // notify the user that he hasn't implemented the event handler
                          // yet
}

void msvVTKBtnEditViewUI::OnLoadSnapshot(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT("Event handler (msvVTKBtnEditViewUI::OnLoadSnapshot) not implemented "
                 "yet")); // notify the user that he hasn't implemented the event handler
                          // yet
}

// wxGlade: add msvVTKBtnEditViewUI event handlers

void msvVTKBtnEditViewUI::set_properties() {
  // begin wxGlade: msvVTKBtnEditViewUI::set_properties
  m_txtName->SetFont(wxFont(14, wxDEFAULT, wxNORMAL, wxBOLD, 0, wxT("")));
  m_bmpClose->SetMinSize(wxSize(32, 32));
  m_bmpSnapshot->SetMinSize(wxSize(80, 80));
  m_btnTakeSnapshot->SetMinSize(wxSize(24, 24));
  m_btnTakeSnapshot->SetToolTip(
      wxT("Take snapshot and camera parameters of current working area"));
  m_btnCleanSnapshot->SetMinSize(wxSize(24, 24));
  m_btnCleanSnapshot->SetToolTip(wxT("Remove view"));
  m_btnLoadSnapshot->SetMinSize(wxSize(24, 24));
  m_btnLoadSnapshot->SetToolTip(wxT("Load snapshot from disk"));
  m_chkVisualProps->SetToolTip(wxT("Save visual properties"));
  m_chkVisualProps->SetValue(1);
  m_chkCamera->SetToolTip(wxT("Save camera parameters"));
  m_chkCamera->SetValue(1);
  // end wxGlade
}

void msvVTKBtnEditViewUI::do_layout() {
  // begin wxGlade: msvVTKBtnEditViewUI::do_layout
  wxBoxSizer *GlobalSizer = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_1_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_3 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_2 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_4 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_1 = new wxBoxSizer(wxHORIZONTAL);
  sizer_1->Add(m_txtName, 3, 0, 0);
  sizer_1->Add(m_bmpClose, 0, wxLEFT, 5);
  GlobalSizer->Add(sizer_1, 0, wxALL | wxEXPAND, 5);
  sizer_2->Add(m_bmpSnapshot, 0, wxALIGN_CENTER_HORIZONTAL, 0);
  sizer_4->Add(m_btnTakeSnapshot, 0, wxTOP, 5);
  sizer_4->Add(m_btnCleanSnapshot, 0, wxLEFT | wxTOP, 5);
  sizer_4->Add(m_btnLoadSnapshot, 0, wxLEFT | wxTOP, 5);
  sizer_2->Add(sizer_4, 0, wxALIGN_CENTER_HORIZONTAL, 0);
  sizer_1_copy->Add(sizer_2, 1, wxEXPAND, 0);
  sizer_3->Add(m_chkVisualProps, 0, wxALL, 5);
  sizer_3->Add(m_chkCamera, 0, wxALL, 5);
  sizer_1_copy->Add(sizer_3, 1, wxEXPAND, 0);
  GlobalSizer->Add(sizer_1_copy, 1, wxALL | wxEXPAND, 5);
  SetSizer(GlobalSizer);
  GlobalSizer->Fit(this);
  // end wxGlade
}
