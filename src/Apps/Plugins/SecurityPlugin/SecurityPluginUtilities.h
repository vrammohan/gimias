/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef SECURITYPLUGINUTILITIES_H
#define SECURITYPLUGINUTILITIES_H

// security

#include "SecurityPluginUserManager.h"

/**
\ingroup Security Plugin
\author H�ctor Fernandez
\date Jan 2012
*/

class PLUGIN_EXPORT SecurityPluginUtilities {
public:
  //! returns Master Password manager from preferences window
  static SecurityPluginCryptoProcessor::Pointer GetMPManager();
};

#endif // SECURITYPLUGINUTILITIES_H
