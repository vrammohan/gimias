/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SSHPluginDataTransfer.h"

#include "coreWxMitkGraphicalInterface.h"
#include "corePluginProviderManager.h"
#include "coreProcessorThread.h"
#include "coreProcessorManager.h"

#include "blTextUtils.h"

#include "sshDirectory.h"
#include "sshFile.h"

#include "ModuleDescriptionUtilities.h"
#include "dynModuleHelper.h"

#include "itksys/SystemTools.hxx"
#include "itksys/Directory.hxx"

SSHPluginDataTransfer::SSHPluginDataTransfer(SshPluginNodeManager::Pointer nodeManager) {
  m_nodeManager = nodeManager;
}

SSHPluginDataTransfer::~SSHPluginDataTransfer() {}

void SSHPluginDataTransfer::UpdateProcessorOutput(ModuleParameter *param, int num) {
  // Call parent class to load output
  DynDataTransferCLP::UpdateProcessorOutput(param, num);
}

void SSHPluginDataTransfer::PostProcessData() {
  time_t saveLocalInputs = ComputeLocalUseCaseTime();

  CreateConnection();

  // Download use case directory
  sshDirectory remoteUseCaseDir(m_Connection);
  remoteUseCaseDir.SetPath(m_RemoteUseCasePath);

  // Compute remote time
  time_t remoteTime = ComputeRemoteUseCaseTime(remoteUseCaseDir);
  time_t updateOutputsTime = itksys::SystemTools::GetTime();

  // Download
  remoteUseCaseDir.Download(m_LocalUseCasePath, true);

  // Set use case to local
  SetTemporaryDirectory(m_LocalPath);
  BuildUseCaseDirectory();

  // Change remote folder to local folder of all parameters
  for (size_t i = 0; i < m_DynProcessor->GetNumberOfInputs(); i++) {
    ModuleParameter *param = dynModuleHelper::GetInput(m_DynProcessor->GetModule(), i);
    bool isOptional = m_DynProcessor->GetInputPort(i)->GetOptional();
    bool isActive = m_DynProcessor->GetInputPort(i)->GetActive();
    if (isOptional && !isActive) {
      // do nothing
    } else {
      std::string value = param->GetDefault();
      blTextUtils::StrSub(value, m_RemoteUseCasePath, m_LocalUseCasePath);
      param->SetDefault(value);
    }
  }

  for (size_t i = 0; i < m_DynProcessor->GetNumberOfOutputs(); i++) {
    ModuleParameter *param = dynModuleHelper::GetOutput(m_DynProcessor->GetModule(), i);
    bool isOptional = m_DynProcessor->GetOutputPort(i)->GetOptional();
    if (isOptional && param->GetDefault() == "false") {
      // Will be ignored
    } else {
      std::string value = param->GetDefault();
      blTextUtils::StrSub(value, m_RemoteUseCasePath, m_LocalUseCasePath);
      param->SetDefault(value);
    }
  }

  // Update all outputs
  DynDataTransferCLP::PostProcessData();
  updateOutputsTime = itksys::SystemTools::GetTime() - updateOutputsTime;

  // Update finished time
  Core::ProcessorThread::Pointer processorThread;
  processorThread = Core::Runtime::Kernel::GetProcessorManager()->GetProcessorThread(
      -1, -1, m_DynProcessor.GetPointer());
  processorThread->SetFinishedTime(saveLocalInputs + remoteTime + updateOutputsTime +
                                   processorThread->GetStartedTime());
}

void SSHPluginDataTransfer::CleanTemporaryFiles() {
  // Clean local use case directory
  SetTemporaryDirectory(m_LocalPath);
  BuildUseCaseDirectory();
  DynDataTransferCLP::CleanTemporaryFiles();

  if (m_Connection.IsNotNull()) {
    // Clean remote use case directory
    sshDirectory dir(m_Connection);
    dir.SetPath(m_RemoteUseCasePath);
    dir.Delete();
  }
}

void SSHPluginDataTransfer::PreProcessData() {
  if (!m_DynProcessor->GetAttachedProcess()) {
    // Set default value as local path file and
    // Save all inputs to local temporary directory
    DynDataTransferBase::PreProcessData();
  } else {
    // Only set use case directory
    BuildUseCaseDirectory();
  }

  // By default is the local
  m_LocalPath = GetTemporaryDirectory();
  m_LocalUseCasePath = GetUseCaseDirectory();

  // Change to remote and get the folders
  m_RemotePath = GetDynProcessor()->GetModuleExecution()->GetWorkingDirectory();
  SetTemporaryDirectory(m_RemotePath);
  BuildUseCaseDirectory();
  m_RemoteUseCasePath = GetUseCaseDirectory();

  // Update start time with min local file time
  Core::ProcessorThread::Pointer processorThread;
  processorThread = Core::Runtime::Kernel::GetProcessorManager()->GetProcessorThread(
      -1, -1, m_DynProcessor.GetPointer());
  time_t minLocalFileTime = 0;
  ComputeLocalUseCaseTime(&minLocalFileTime);
  processorThread->SetStartedTime(minLocalFileTime);

  // Transfer local folder to remote computer using SSH
  if (!m_DynProcessor->GetAttachedProcess()) {
    CreateConnection();

    // Create use case directory
    sshDirectory dir(m_Connection);
    dir.SetPath(m_RemoteUseCasePath);
    dir.Create();

    sshFile emptyFile(m_Connection);
    emptyFile.SetRemoteFilename(m_RemoteUseCasePath + "/emptyRemote.txt");
    emptyFile.UploadByContent("Empty");

    // Upload use case directory
    dir.Upload(m_LocalUseCasePath);

    // Change local folder to remote folder of all parameters
    for (size_t i = 0; i < m_DynProcessor->GetNumberOfInputs(); i++) {
      ModuleParameter *param = dynModuleHelper::GetInput(m_DynProcessor->GetModule(), i);
      bool isOptional = m_DynProcessor->GetInputPort(i)->GetOptional();
      bool isActive = m_DynProcessor->GetInputPort(i)->GetActive();
      if (isOptional && !isActive) {
        // do nothing
      } else {
        std::string value = param->GetDefault();
        blTextUtils::StrSub(value, m_LocalUseCasePath, m_RemoteUseCasePath);
        param->SetDefault(value);
      }
    }

    for (size_t i = 0; i < m_DynProcessor->GetNumberOfOutputs(); i++) {
      ModuleParameter *param = dynModuleHelper::GetOutput(m_DynProcessor->GetModule(), i);
      bool isOptional = m_DynProcessor->GetOutputPort(i)->GetOptional();
      if (isOptional && param->GetDefault() == "false") {
        // Will be ignored
      } else {
        std::string value = param->GetDefault();
        blTextUtils::StrSub(value, m_LocalUseCasePath, m_RemoteUseCasePath);
        param->SetDefault(value);
      }
    }
  }
}

void SSHPluginDataTransfer::CreateConnection() {
  if (m_Connection.IsNotNull()) {
    return;
  }

  std::string sshNodeName = GetDynProcessor()->GetModule()->GetTarget();

  sshNode::Pointer node = m_nodeManager->Get(sshNodeName);
  if (node.IsNull()) {
    throw Core::Exceptions::Exception("SSHPluginDataTransfer::CreateConnection",
                                      "Cannot find the SSH node");
  }

  // Connect to remote machine
  m_Connection = sshConnection::New();
  m_Connection->SetNode(node);
  m_Connection->Connect();
}

time_t SSHPluginDataTransfer::ComputeLocalUseCaseTime(time_t *minTime, time_t *maxTime) {
  // Update finished time searching for all local files
  itksys::Directory localDir;
  localDir.Load(m_LocalUseCasePath.c_str());

  time_t minLocalFileTime = 0;
  time_t maxLocalFileTime = 0;
  for (unsigned int ii = 0; ii < localDir.GetNumberOfFiles(); ++ii) {
    std::string filename = localDir.GetFile(ii);
    if (filename == "." || filename == "..") {
      continue;
    }

    filename = m_LocalUseCasePath + "/" + filename;
    time_t fileTime = itksys::SystemTools::ModifiedTime(filename.c_str());
    maxLocalFileTime = std::max(maxLocalFileTime, fileTime);
    if (minLocalFileTime == 0) {
      minLocalFileTime = fileTime;
    } else {
      minLocalFileTime = std::min(minLocalFileTime, fileTime);
    }
  }

  if (minTime) {
    *minTime = minLocalFileTime;
  }
  if (maxTime) {
    *maxTime = maxLocalFileTime;
  }

  // Started time is the creation time of use case folder
  return maxLocalFileTime - minLocalFileTime;
}

time_t SSHPluginDataTransfer::ComputeRemoteUseCaseTime(sshDirectory &remoteDir) {
  remoteDir.Load();
  std::map< std::string, long int > modifiedTimeForAllFiles;
  modifiedTimeForAllFiles = remoteDir.ModifiedTimeForAllFiles();

  // Search for the minimum modification time ok all remote files
  time_t minRemoteFileTime = 0;
  time_t maxRemoteFileTime = 0;
  if (!modifiedTimeForAllFiles.empty()) {
    minRemoteFileTime = modifiedTimeForAllFiles.begin()->second;
  }
  std::map< std::string, long int >::iterator it;
  for (it = modifiedTimeForAllFiles.begin(); it != modifiedTimeForAllFiles.end(); it++) {
    if (it->first == "." || it->first == "..") {
      continue;
    }
    time_t fileTime = it->second;
    minRemoteFileTime = std::min(minRemoteFileTime, fileTime);
    maxRemoteFileTime = std::max(maxRemoteFileTime, fileTime);
  }

  return maxRemoteFileTime - minRemoteFileTime;
}
