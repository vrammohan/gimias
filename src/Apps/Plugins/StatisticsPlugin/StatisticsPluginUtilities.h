/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef StatisticsPLUGINUTILITIES_H
#define StatisticsPLUGINUTILITIES_H

// std
#include <string>
#include <vector>
#include <map>

// core
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreDirectory.h"
#include "coreReportExceptionMacros.h"
#include "coreBaseExceptions.h"

// boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

// xml
#include "tinyxml.h"

/**
\ingroup StatisticsPlugin
\author Albert Sanchez
\date 18 Oct 2011
*/

class StatisticsPluginUtilities {
public:
protected:
  ~StatisticsPluginUtilities();
};

#endif // StatisticsPLUGINUTILITIES_H
