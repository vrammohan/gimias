# Used to configure lcore
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmDataHandling = api.CreateCompiledProject("gmDataHandling", "dll")
gmDataHandling.AddProjects([
  cilabMacros,
  baseLibNumericData,
  boost,
  gmCommonObjects
])

gmDataHandling.SetPrecompiledHeader("gmDataHandlingPCH.h")

gmDataHandling.AddSources(["src/*.cxx" ], checkExists = 0)
gmDataHandling.AddSources(["src/*.txx" ], checkExists = 0)
gmDataHandling.AddSources(["src/*.h" ], checkExists = 0)
gmDataHandling.AddIncludeFolders(["src"])

if api.GetCompiler().TargetIsWindows():
    gmDataHandling.AddDefinitions(["/bigobj"], private = 1)

