/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATDownloadProcessor.h"
#include "coreDataEntityReader.h"

XNATDownloadProcessor::XNATDownloadProcessor() {
  SetName("XNATDownloadProcessor");

  BaseProcessor::SetNumberOfInputs(NUMBEROFINPUTS);
  BaseProcessor::SetNumberOfOutputs(NUMBEROFOUTPUTS);

  for (int index = 0; index < NUMBEROFOUTPUTS; index++) {
    GetOutputPort(index)->SetDataEntityType(Core::ImageTypeId);
    GetOutputPort(index)->SetReuseOutput(false);
  }
}

XNATDownloadProcessor::~XNATDownloadProcessor() {}

void XNATDownloadProcessor::SetParameters(XNATDownloadParameters params) {
  m_Params = params;
}

void XNATDownloadProcessor::Update() {

  try {
    std::string url;

    XNAT::Utils client(m_Params.usr, m_Params.pwd, m_Params.siteUrl);
    // get it
    if (m_Params.downloadScans) {
      url = client.retrieveAllImageScanFiles(m_Params.project, m_Params.label,
                                             m_Params.experiment.label,
                                             m_Params.downloadFileName);
      Core::IO::DataEntityReader::Pointer reader;
      reader = Core::IO::DataEntityReader::New();
      reader->SetFileName(m_Params.downloadFileName);
      reader->Update();

      // sanity check
      if (m_Params.experimentScans.size() != reader->GetNumberOfOutputs()) {
      }

      int numOutputImages = 0;
      if (reader->GetNumberOfOutputs() >= NUMBEROFOUTPUTS) {
        numOutputImages = NUMBEROFOUTPUTS;
      } else {
        numOutputImages = (int)reader->GetNumberOfOutputs();
      }

      for (int scanIdx = 0; scanIdx < numOutputImages; scanIdx++) {
        // std::ostringstream convert;
        // convert << scanIdx;
        // reader->GetOutputDataEntity(scanIdx)->GetMetadata()->SetName("Scan_"+convert.str());
        Core::DataEntityMetadata::Pointer scanMeta =
            reader->GetOutputDataEntity(scanIdx)->GetMetadata();

        std::string seriesNo = "";
        if (scanMeta->FindTagByName("SeriesId")) // TODO// AMB: remove the literal!
        {
          seriesNo = scanMeta->FindTagByName("SeriesId")->GetValueAsString();
        }

        int experIdx = -1;
        for (int experFindIdx = 0; experFindIdx < m_Params.experimentScans.size();
             experFindIdx++) {
          if (seriesNo == (m_Params.experimentScans[experFindIdx]).ID) {
            experIdx = experFindIdx;
          }
        }

        if (experIdx != -1) {
          scanMeta->SetName("ScanID: " + (m_Params.experimentScans[experIdx]).ID +
                            " Type: " + (m_Params.experimentScans[experIdx]).type);
        } else {
          scanMeta->SetName("ScanID: " + seriesNo);
        }
        UpdateOutput(scanIdx, reader->GetOutputDataEntity(scanIdx));
      }

      // End of downloading and pre-processing
    }

    if (m_Params.downloadSubjectResources) {
      url = client.retrieveSubjectResourceFile(m_Params.subjectResourceFile,
                                               m_Params.downloadFileName);
      Core::IO::DataEntityReader::Pointer reader;
      reader = Core::IO::DataEntityReader::New();
      reader->SetFileName(m_Params.downloadFileName);
      reader->Update();

      int numOutputFiles = 0;
      if (reader->GetNumberOfOutputs() >= NUMBEROFOUTPUTS) {
        numOutputFiles = NUMBEROFOUTPUTS;
      } else {
        numOutputFiles = (int)reader->GetNumberOfOutputs();
      }

      for (int index = 0; index < numOutputFiles; index++) {
        reader->GetOutputDataEntity(index)->GetMetadata()->SetName(
            m_Params.subjectResourceFile.collection + ": " +
            m_Params.subjectResourceFile.label);
        UpdateOutput(index, reader->GetOutputDataEntity(index));
      }
    }

  } catch (Poco::Exception &ex) {
    std::cerr << ex.displayText() << std::endl;
    throw ex;
  }
}
