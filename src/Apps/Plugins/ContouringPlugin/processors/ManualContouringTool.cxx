/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "ManualContouringTool.h"

#include "mitkToolManager.h"
#include "mitkRenderingManager.h"
#include "mitkBaseRenderer.h"
#include "mitkProperties.h"

#include "resource/mitkManualContouringTool.xpm"

namespace mitk {
MITK_TOOL_MACRO(PLUGIN_EXPORT, ManualContouringTool, "Manual Contouring tool");
}

mitk::ManualContouringTool::ManualContouringTool(int paintingPixelValue)
    : FeedbackContourTool("PressMoveReleaseWithCTRLInversionWithDELETE"),
      m_LastPositionEvent(0, 0, 0, 0, 0, Point2D(), Point3D()) {
  m_MinDistBetweenPoints = 1.0;
  CONNECT_ACTION(33, OnDeletePressed); // Look for PressMoveReleaseWithCTRLInversion in
                                       // StateMachine.xml for this magic number

  // create the set of control points
  m_PointSet = mitk::PointSet::New();
  m_PointSetNode = DataTreeNode::New();
  m_PointSetNode->SetData(m_PointSet);
  m_PointSetNode->SetProperty("name", StringProperty::New("One of PointSet nodes"));
  m_PointSetNode->SetProperty("visible", BoolProperty::New(true));
  m_PointSetNode->SetProperty("helper object", BoolProperty::New(true));
  m_PointSetNode->SetProperty("layer", IntProperty::New(1000));
  m_PointSetNode->SetProperty("project", BoolProperty::New(true));
  m_Drawing = false;
}

mitk::ManualContouringTool::~ManualContouringTool() {}

const char **mitk::ManualContouringTool::GetXPM() const {
  return mitkManualContouringTool_xpm;
}

const char *mitk::ManualContouringTool::GetName() const { return "ManualContouring"; }

void mitk::ManualContouringTool::Activated() {
  Superclass::Activated();
  DataStorage *storage = m_ToolManager->GetDataStorage();
  storage->Add(m_PointSetNode);
}

void mitk::ManualContouringTool::Deactivated() {
  Superclass::Deactivated();
  DataStorage *storage = m_ToolManager->GetDataStorage();
  storage->Remove(m_PointSetNode);
}

void mitk::ManualContouringTool::SetContourVisible(bool b) {
  FeedbackContourTool::SetFeedbackContourVisible(b);
}

mitk::Contour::Pointer mitk::ManualContouringTool::GetContour() {
  return FeedbackContourTool::GetFeedbackContour();
}

void mitk::ManualContouringTool::SetDistBetweenPoints(double dist) {
  m_MinDistBetweenPoints = std::max(1.0, dist);
}

/**
 Just show the contour, insert the first point.
*/
bool mitk::ManualContouringTool::OnMousePressed(Action *action,
                                                const StateEvent *stateEvent) {
  if (!FeedbackContourTool::OnMousePressed(action, stateEvent))
    return false;

  // FeedbackContourTool::GetFeedbackContour()->SetClosed(true);  // It is closed by
  // default. Use SetContourClosed() to change this.

  if (!stateEvent->GetEvent()->GetButtonState())
    return false; // only process if CTRL is pressed

  const PositionEvent *positionEvent =
      dynamic_cast< const PositionEvent * >(stateEvent->GetEvent());
  if (!positionEvent)
    return false;

  m_LastPositionEvent = mitk::PositionEvent(*positionEvent);

  FeedbackContourTool::SetFeedbackContourVisible(true);

  Contour *contour = FeedbackContourTool::GetFeedbackContour();
  if (!contour->IsInitialized()) {
    contour->Initialize();
  }

  // add the point to the contour
  if (contour->GetNumberOfPoints() == 0) {
    contour->AddVertex(positionEvent->GetWorldPosition());
    mitk::RenderingManager::GetInstance()->RequestUpdateAll(); // only for the first point
  } else {
    //  contour->AddVertex() should be enough, but the Reserve() is crashing for some
    //  reason with this branch
    mitk::Contour::BoundingBoxType::PointType p;
    p.CastFrom(positionEvent->GetWorldPosition());
    mitk::Contour::BoundingBoxType::PointsContainer::Iterator itOld =
        contour->GetPoints()->Begin();
    mitk::Contour::PointsContainerPointer newPoints =
        mitk::Contour::BoundingBoxType::PointsContainer::New();
    newPoints->Reserve(contour->GetNumberOfPoints() + 1);
    mitk::Contour::BoundingBoxType::PointsContainer::Iterator itNew = newPoints->Begin();
    while (itOld != contour->GetPoints()->End()) {
      itNew->Value() = itOld->Value();
      itNew++;
      itOld++;
    }
    itNew->Value() = p;
    contour->SetPoints(newPoints);
  }

  // add the point to the set of control points
  m_PointSet->InsertPoint(contour->GetNumberOfPoints(),
                          positionEvent->GetWorldPosition());

  // update all views is done when the mouse is released or moved
  // mitk::RenderingManager::GetInstance()->RequestUpdateAll();

  m_Drawing = true;

  return true;
}

/**
 Insert the point to the feedback contour.
*/
bool mitk::ManualContouringTool::OnMouseMoved(Action *action,
                                              const StateEvent *stateEvent) {
  if (!FeedbackContourTool::OnMouseMoved(action, stateEvent))
    return false;

  if (!m_Drawing)
    return true; // if not drawing, do nothing

  const PositionEvent *positionEvent =
      dynamic_cast< const PositionEvent * >(stateEvent->GetEvent());
  if (!positionEvent)
    return false;

  m_LastPositionEvent = mitk::PositionEvent(*positionEvent);

  Contour *contour = FeedbackContourTool::GetFeedbackContour();
  if (contour->GetNumberOfPoints() > 0) {
    mitk::Contour::PointsContainerPointer points = contour->GetPoints();
    mitk::Contour::PointsContainer::Iterator it = points->End();
    it--;
    itk::Point< float, 3 > lastPoint = it->Value();
    mitk::Point3D currentPoint = positionEvent->GetWorldPosition();
    itk::Point< float, 3 > itkCurrentPoint(currentPoint.GetDataPointer());
    double dist = lastPoint.EuclideanDistanceTo(itkCurrentPoint);

    if (dist > m_MinDistBetweenPoints) {
      // add the point to the contour
      contour->AddVertex(positionEvent->GetWorldPosition());

      // add the point to the set of control points
      m_PointSet->InsertPoint(contour->GetNumberOfPoints(),
                              positionEvent->GetWorldPosition());

      // update all views
      mitk::RenderingManager::GetInstance()->RequestUpdateAll();
    } else {
      assert(positionEvent->GetSender()->GetRenderWindow());
      mitk::RenderingManager::GetInstance()->RequestUpdate(
          positionEvent->GetSender()->GetRenderWindow());
    }
  }

  return true;
}

/**
  Stop the interaction and draw final contour
*/
bool mitk::ManualContouringTool::OnMouseReleased(Action *action,
                                                 const StateEvent *stateEvent) {
  if (!FeedbackContourTool::OnMouseReleased(action, stateEvent))
    return false;

  if (!m_Drawing)
    return true; // if not drawing, do nothing

  const PositionEvent *positionEvent =
      dynamic_cast< const PositionEvent * >(stateEvent->GetEvent());
  if (!positionEvent)
    return false;

  m_LastPositionEvent = mitk::PositionEvent(*positionEvent);

  /// Make sure the result is drawn again, in all views
  mitk::RenderingManager::GetInstance()->RequestUpdateAll();

  m_Drawing = false; // drawing stopped

  return true;
}

void mitk::ManualContouringTool::SetContourClosed(bool value) {
  FeedbackContourTool::GetFeedbackContour()->SetClosed(value);
  mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

bool mitk::ManualContouringTool::OnDeletePressed(Action *action,
                                                 const StateEvent *stateEvent) {
  const KeyEvent *positionEvent =
      dynamic_cast< const KeyEvent * >(stateEvent->GetEvent());
  if (!positionEvent)
    return false;

  Contour *contour = FeedbackContourTool::GetFeedbackContour();
  mitk::Contour::PointsContainerPointer points = contour->GetPoints();
  if (points->size() > 0) {
    StateEvent tmpEvent(0, &m_LastPositionEvent);

    // Do the delete through the mouse events or the 3D view is not updated. //

    // This adds one more control point, which has to be deleted later
    OnMousePressed(NULL, &tmpEvent);

    // Delete the control points
    mitk::PointSet::DataType *pointSet = m_PointSet->GetPointSet(GetTimeStep());
    mitk::PointSet::PointsContainer *controlPoints = pointSet->GetPoints();
    controlPoints->DeleteIndex(contour->GetNumberOfPoints());
    controlPoints->DeleteIndex(contour->GetNumberOfPoints() - 1);

    // Delete the points from the contour
    points->pop_back();
    points->pop_back();

    // This closes the interaction cycle and updates the views
    OnMouseReleased(NULL, &tmpEvent);
  }

  return true;
}

/**
  Return contour as vtkPolyData.
  If the contour is closed, the last point of the vtkPolyData is the same as the first.
*/
vtkSmartPointer< vtkPolyData > mitk::ManualContouringTool::GetContourAsPolyData() {
  mitk::Contour *contour = GetContour();
  int numPoints = contour->GetNumberOfPoints();
  mitk::Contour::PointsContainerPointer points = contour->GetPoints();
  mitk::Contour::PointsContainer::Iterator it, end;
  vtkSmartPointer< vtkPolyData > polydata = vtkSmartPointer< vtkPolyData >::New();

  if (numPoints > 0) {
    polydata->Allocate(numPoints, numPoints);

    // insert points
    vtkSmartPointer< vtkPoints > newPoints = vtkSmartPointer< vtkPoints >::New();
    for (it = points->Begin(); it != points->End(); it++) {
      itk::Point< float, 3 > aPoint = it->Value();
      newPoints->InsertNextPoint(aPoint[0], aPoint[1], aPoint[2]);
    }
    if (contour->GetClosed()) {
      it = points->Begin();
      itk::Point< float, 3 > aPoint = it->Value();
      newPoints->InsertNextPoint(aPoint[0], aPoint[1], aPoint[2]);
      numPoints++;
    }

    polydata->SetPoints(newPoints);

    // insert connectivity information
    for (int index = 0; index < numPoints - 1; index++) {
      vtkIdType pts[2] = { index, index + 1 };
      polydata->InsertNextCell(VTK_LINE, 2, pts);
    }
  }

  return polydata;
}
