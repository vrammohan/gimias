/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _vmtkWidgetCollective_H
#define _vmtkWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

#include "vmtkProcessorCollective.h"

/**

\ingroup VMTKPlugin
\author Valeria Barbarito
\date 4 February 2011
*/

class vmtkWidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(vmtkWidgetCollective, Core::WidgetCollective);

  //!
  void Init();

private:
  vmtkWidgetCollective();

private:
};

#endif //_vmtkWidgetCollective_H
