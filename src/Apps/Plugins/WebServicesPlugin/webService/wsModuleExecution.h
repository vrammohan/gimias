/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _wsModuleExecution_H
#define _wsModuleExecution_H

#include "wsGimiasWin32Header.h"

#include "axutil_env.h"
#include "axiom_node.h"

#include "ModuleDescription.h"
#include "coreDynProcessor.h"

/**
Class to execute modules using Web service request
- Use the XML retrieved as input
- Execute the CLP
- Create an XML as output

The client must specify the output folder to store the output file
otherwise, the file will be written into the use-case folder and deleted
after execution.

\ingroup WebServicesPlugin
\author Xavi Planes
\date Oct 2011
*/
class WSGIMIAS_EXPORT wsModuleExecution {
public:
  wsModuleExecution(const axutil_env_t *env);

  //! Find loaded module description in service provider
  void GetModuleDescription(axiom_node_t *node);

  //!
  void SetParameters(axiom_node_t *node);

  //! Create m_Processor
  void CreateProcessor();

  //! Execute CLP
  void Execute();

  //! Get respone as AXIS2C XML instance
  axiom_node_t *GetResponse();

  //! Set error message using exception text into AXIS2C system
  void SetError(Core::Exceptions::Exception &e);

public:
  //!
  const axutil_env_t *m_Env;
  //!
  ModuleDescription m_Module;
  //!
  std::string m_DefaultPluginProvider;
  //!
  Core::DynProcessor::Pointer m_Processor;
};

#endif //_wsModuleExecution_H
