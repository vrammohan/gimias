/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "wxDataNavigationWidget.h"
#include "msvWxVTKButtons.h"
#include "wxUnicode.h"

#include "msvVTKBtnEdit.h"
#include "msvVTKBtnEditView.h"
#include "msvWxHtmlWindow.h"
#include "msvMultiRenderWindow.h"

#include "coreDataContainer.h"
#include "coreMainMenu.h"
#include "coreDataEntityInfoHelper.h"

#include <wx/wupdlock.h>

#include "blTextUtils.h"

#include "DataNavigation.xpm"

#define wxTOOLTIP_WIDTH 250
#define wxTOOLTIP_HEIGHT 100
#define wxNAVIGATION_WIDTH 400
#define wxNAVIGATION_HEIGHT 320
#define wxBRIEF_INFORMATION_WIDTH 400
#define wxBRIEF_INFORMATION_HEIGHT 210
#define wxINFORMATION_WIDTH 400
#define wxINFORMATION_HEIGHT 250
#define wxVIEWS_WIDTH 400
#define wxVIEWS_HEIGHT 320

#define MLSEC_HIDE_ARROWS 2000

#define wxID_EDIT_DIALOG wxID("wxID_EDIT_DIALOG")
#define wxID_EDIT_VIEW_DIALOG wxID("wxID_EDIT_VIEW_DIALOG")

DEFINE_EVENT_TYPE(wxID_REFRESH_CONTENTS)

BEGIN_EVENT_TABLE(wxDataNavigationWidget, wxPanel)
EVT_HTML_LINK_CLICKED(wxID_ANY, wxDataNavigationWidget::OnLinkClicked)
EVT_HTML_CELL_HOVER(wxID_HTML_WINDOW, wxDataNavigationWidget::OnCellHover)
EVT_BUTTON(wxID_CANCEL, wxDataNavigationWidget::OnCancelEdit)
EVT_MENU(wxID_EDIT_DETAILS, wxDataNavigationWidget::OnEditDetails)
EVT_MENU(wxID_REMOVE_CURRENT, wxDataNavigationWidget::OnRemoveCurrent)
EVT_MENU(wxID_LOAD_CHILD_DATA, wxDataNavigationWidget::OnLoadChildData)
EVT_MENU(wxID_SAVE_DATA, wxDataNavigationWidget::OnSaveData)
EVT_MENU(wxID_EDIT_VIEW, wxDataNavigationWidget::OnEditView)
EVT_MENU(wxID_ADD_CHILD_SESSION, wxDataNavigationWidget::OnAddChildSession)
EVT_MENU(wxID_ADD_VIEW, wxDataNavigationWidget::OnAddView)
EVT_MENU(wxID_REMOVE_VIEW, wxDataNavigationWidget::OnRemoveView)
EVT_MENU(wxID_LOAD_DATA, wxDataNavigationWidget::OnLoadData)
EVT_MENU(wxID_UNLOAD_DATA, wxDataNavigationWidget::OnUnLoadData)
EVT_BUTTON(wxID_REMOVE_VIEW, wxDataNavigationWidget::OnRemoveView)
EVT_COMMAND(wxID_ANY, wxID_REFRESH_CONTENTS, wxDataNavigationWidget::OnRefreshContents)
EVT_BUTTON(wxID_TAKE_SNAPSHOT, wxDataNavigationWidget::OnTakeSnapshot)
EVT_BUTTON(wxID_CAPTURE_VIEW, wxDataNavigationWidget::OnCaptureView)
END_EVENT_TABLE()

wxDataNavigationWidget::wxDataNavigationWidget(wxWindow *parent, wxWindowID id,
                                               const wxPoint &pos /*= wxDefaultPosition*/,
                                               const wxSize &size /*= wxDefaultSize*/,
                                               long style /*= wxDEFAULT_DIALOG_STYLE*/,
                                               const wxString &name /* = "dialogBox"*/)
    : // wxDialog(parent, id, title, pos, size, style | wxSTAY_ON_TOP, name)
      wxPanel(parent, id, pos, size, style, name) {
  SetBitmap(DataNavigation_xpm);

  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  m_DataEntityList = dataContainer->GetDataEntityList();

  // Update manager
  m_ViewManager = msvViewManager::New();
  m_ViewManager->SetDataEntityHolder(m_DataEntityList->GetSelectedDataEntityHolder());

  // draw the dialog with the html window
  wxBoxSizer *topsizer = new wxBoxSizer(wxVERTICAL);
  wxSize htmlSz((size.x * .9), (size.y * .9));
  m_htmlWin = new msvWxHtmlWindow(this, wxID_HTML_WINDOW, wxDefaultPosition, htmlSz,
                                  wxHW_SCROLLBAR_AUTO);
  m_htmlWin->SetBorders(0);
  m_htmlWin->SetPage("");
  topsizer->Add(m_htmlWin, 1, wxEXPAND | wxALL, 5);

  m_msvVTKBtnEdit = new msvVTKBtnEdit(this, wxID_EDIT_DIALOG);
  topsizer->Add(m_msvVTKBtnEdit, 1, wxEXPAND | wxALL, 5);
  m_msvVTKBtnEdit->Hide();
  m_msvVTKBtnEdit->SetViewManager(m_ViewManager);

  m_msvVTKBtnEditView = new msvVTKBtnEditView(this, wxID_EDIT_VIEW_DIALOG);
  topsizer->Add(m_msvVTKBtnEditView, 1, wxEXPAND | wxALL, 5);
  m_msvVTKBtnEditView->Hide();
  m_msvVTKBtnEditView->SetViewManager(m_ViewManager);

  this->SetSizer(topsizer);
  Layout();

  m_oldMode = msvWxHtmlWindow::MODE_NONE;
  m_Mode = msvWxHtmlWindow::MODE_DESCRIPTION;
  m_Button = NULL;
  m_ContextMenu = NULL;
}

wxDataNavigationWidget::~wxDataNavigationWidget() {
  if (m_ContextMenu) {
    delete m_ContextMenu;
  }

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string tempFilname = settings->GetProjectHomePath() + "/Temp/Temp.png";
  itksys::SystemTools::RemoveFile(tempFilname.c_str());
}

void wxDataNavigationWidget::SetRenderWindow(msvMultiRenderWindow *renderWindow) {
  m_RenderWindow = renderWindow;
  m_htmlWin->SetRenderWindow(m_RenderWindow);
}

void wxDataNavigationWidget::SetButtonsManager(
    msvWxVTKButtonsManager::Pointer buttonsManager) {
  m_ButtonsManager = buttonsManager;
}

void wxDataNavigationWidget::LoadPage(wxString hPage) { m_htmlWin->LoadPage(hPage); }

wxHtmlWindow *wxDataNavigationWidget::GetHtmlWin() { return m_htmlWin; }

void wxDataNavigationWidget::SetButton(msvWxVTKButtons *button) {
  if (GetDataEntity().IsNotNull()) {
    GetDataEntity()->RemoveObserverOnModified(
        this, &wxDataNavigationWidget::OnModifiedDataEntity);
  }

  m_Button = button;
  m_htmlWin->SetButton(m_Button);
  m_msvVTKBtnEdit->SetDataEntity(GetDataEntity());

  if (m_Button) {
    PushBack(m_Button->getId());
  }

  if (GetDataEntity().IsNotNull()) {
    GetDataEntity()->AddObserverOnModified(this,
                                           &wxDataNavigationWidget::OnModifiedDataEntity);
  }
}

msvWxVTKButtons *wxDataNavigationWidget::GetButton() { return m_Button; }

void wxDataNavigationWidget::SetMode(msvWxHtmlWindow::MODE_TYPE mode) {
  m_oldMode = m_Mode;
  m_Mode = mode;
}

bool wxDataNavigationWidget::Show(bool show /*=true*/) {
  wxAuiPaneInfo pane = GetPluginTab()->GetAuiPaneInfo(this);
  if (show == true && pane.IsOk() && !pane.IsShown()) {
    return false;
  }

  int scrollPos = m_htmlWin->GetScrollPos(wxVERTICAL);
  wxWindowUpdateLocker lock(this);

  // show/hide edit/editView/html window
  m_htmlWin->Show(m_Mode != msvWxHtmlWindow::MODE_EDIT_VIEW &&
                  m_Mode != msvWxHtmlWindow::MODE_EDIT);
  m_msvVTKBtnEditView->Show(m_Mode == msvWxHtmlWindow::MODE_EDIT_VIEW);
  m_msvVTKBtnEdit->Show(m_Mode == msvWxHtmlWindow::MODE_EDIT);
  Layout();

  // HTML contents
  m_htmlWin->BuildContents(GetMode());

  //// Set size
  // switch ( m_Mode )
  //{
  // case msvWxHtmlWindow::MODE_TOOLTIP:SetSize( wxSize(wxTOOLTIP_WIDTH,wxTOOLTIP_HEIGHT)
  // );break;
  // case msvWxHtmlWindow::MODE_NAVIGATION:SetSize(
  // wxSize(wxNAVIGATION_WIDTH,wxNAVIGATION_HEIGHT) );break;
  // case msvWxHtmlWindow::MODE_DESCRIPTION:SetSize(
  // wxSize(wxBRIEF_INFORMATION_WIDTH,wxBRIEF_INFORMATION_HEIGHT) );break;
  // case msvWxHtmlWindow::MODE_COMPLETE_INFORMATION:SetSize(
  // wxSize(wxINFORMATION_WIDTH,wxINFORMATION_HEIGHT) );break;
  // case msvWxHtmlWindow::MODE_EDIT:SetSize(
  // wxSize(wxINFORMATION_WIDTH,wxINFORMATION_HEIGHT) );break;
  // case msvWxHtmlWindow::MODE_BROWSE_VIEWS:SetSize( wxSize(wxVIEWS_WIDTH,wxVIEWS_HEIGHT)
  // );break;
  // case msvWxHtmlWindow::MODE_EDIT_VIEW:SetSize(
  // wxSize(wxBRIEF_INFORMATION_WIDTH,wxBRIEF_INFORMATION_HEIGHT) );break;
  // case msvWxHtmlWindow::MODE_NONE:break;
  //}

  if (m_oldMode == m_Mode) {
    m_htmlWin->Scroll(0, scrollPos);
  }

  if (GetButton())
    PushBack(GetButton()->getId());

  // Show this window
  return wxPanel::Show(show);
}

bool wxDataNavigationWidget::Hide() {
  SetMode(msvWxHtmlWindow::MODE_NONE);
  return Show(false);
}

msvWxHtmlWindow::MODE_TYPE wxDataNavigationWidget::GetMode() { return m_Mode; }

void wxDataNavigationWidget::OnLinkClicked(wxHtmlLinkEvent &event) {
  std::string href = event.GetLinkInfo().GetHref().ToStdString();
  if (href == "read more") {
    SetMode(msvWxHtmlWindow::MODE_COMPLETE_INFORMATION);
    Show();
  } else if (href == "descriptionView") {
    SetMode(msvWxHtmlWindow::MODE_DESCRIPTION);
    Show();
  } else if (href == "browseView") {
    SetMode(msvWxHtmlWindow::MODE_NAVIGATION);
    Show();
  } else if (href == "edit") {
    SetMode(msvWxHtmlWindow::MODE_EDIT);
    Show();
  } else if (href == "views") {
    SetMode(msvWxHtmlWindow::MODE_BROWSE_VIEWS);
    Show();
  } else if (href == "more") {
    ShowContextMenu(event.GetLinkInfo().GetEvent()->GetPosition());
  } else if (href == "back") {
    int id = PopBack();
    if (id != -1)
      m_RenderWindow->SelectDataEntity(id);
  } else if (href.find("nav:") == 0) {
    blTextUtils::StrSub(href, "nav:", "");
    int id = atoi(href.c_str());
    m_RenderWindow->SelectDataEntity(id);
  } else if (href.find("image:") == 0) {
    blTextUtils::StrSub(href, "image:", "");
    int id = atoi(href.c_str());

    // Select data
    m_RenderWindow->SelectDataEntity(id);
    Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetSelectedDataEntity();
    std::string name = dataEntity->GetMetadata()->GetName();

    // Select view
    blTagMap::Pointer view = GetViewManager()->GetView(name);
    GetViewManager()->SetSelectedView(view);

    // Update camera parameters to animate
    m_ButtonsManager->UpdateCameraParameters(GetDataEntity(), view);

    // Animate
    if (GetButton()) {
      GetButton()->Animate();
    }

    // Refresh after data is loaded
    Show();
  } else if (href.find("view:") == 0) {
    blTextUtils::StrSub(href, "view:", "");

    // Change selected view
    blTagMap::Pointer view = GetViewManager()->GetView(href);
    GetViewManager()->SetSelectedView(view);

    // Update camera parameters for current selected data and vtk Button before animating
    m_ButtonsManager->UpdateCameraParameters(GetDataEntity(), view);

    // Animate
    if (GetButton()) {
      GetButton()->Animate();
    }

    // Refresh after view is loaded
    Show();
  } else if (href.find("show:") == 0) {
    blTextUtils::StrSub(href, "show:", "");
    int id = atoi(href.c_str());
    Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(id);
    m_RenderWindow->LockCamera(true);
    if (!GetRenderingTree()->IsDataEntityRendered(dataEntity))
      GetRenderingTree()->Add(dataEntity, true);
    else
      GetRenderingTree()->Show(m_DataEntityList->GetDataEntity(id), true);
    m_RenderWindow->LockCamera(false);
    Show();
  } else if (href.find("hide:") == 0) {
    blTextUtils::StrSub(href, "hide:", "");
    int id = atoi(href.c_str());
    Core::DataEntity::Pointer dataEntity = m_DataEntityList->GetDataEntity(id);
    m_RenderWindow->LockCamera(true);
    if (!GetRenderingTree()->IsDataEntityRendered(dataEntity))
      GetRenderingTree()->Add(dataEntity, false);
    else
      GetRenderingTree()->Show(m_DataEntityList->GetDataEntity(id), false);
    m_RenderWindow->LockCamera(false);
    Show();
  } else {
    wxLaunchDefaultBrowser(href);
  }
}

void wxDataNavigationWidget::ShowContextMenu(const wxPoint &position) {
  if (GetDataEntity().IsNull())
    return;

  if (m_ContextMenu)
    delete m_ContextMenu;
  m_ContextMenu = new wxMenu();

  bool isSession = GetDataEntity()->GetType() == Core::SessionTypeId;
  bool isLoaded = m_RenderWindow->IsLoaded(GetDataEntity());

  if (isLoaded) {
    switch (m_Mode) {
    case msvWxHtmlWindow::MODE_BROWSE_VIEWS:
      m_ContextMenu->Append(wxID_ADD_VIEW, wxT("Add view"));
      m_ContextMenu->Append(wxID_EDIT_VIEW, wxT("Edit view"));
      m_ContextMenu->Append(wxID_REMOVE_VIEW, wxT("Remove view"));
      m_ContextMenu->Append(wxID_SAVE_DATA, wxT("Save data"));
      break;
    default:
      m_ContextMenu->Append(wxID_EDIT_DETAILS, wxT("Edit data"));
      m_ContextMenu->Append(wxID_SAVE_DATA, wxT("Save data"));
      m_ContextMenu->Append(wxID_LOAD_CHILD_DATA, wxT("Load child data"));
      m_ContextMenu->Append(wxID_ADD_CHILD_SESSION, wxT("Add child session"));
      m_ContextMenu->Append(wxID_REMOVE_CURRENT, wxT("Remove current data"));
      m_ContextMenu->Append(wxID_UNLOAD_DATA, wxT("UnLoad current data"));
      break;
    }
  } else {
    m_ContextMenu->Append(wxID_REMOVE_CURRENT, wxT("Remove current data"));
    m_ContextMenu->Append(wxID_LOAD_DATA, wxT("Load current data"));
  }

  PopupMenu(m_ContextMenu, position);
}

void wxDataNavigationWidget::OnCellHover(wxHtmlCellEvent &event) { event.Skip(); }

Core::DataEntity::Pointer wxDataNavigationWidget::GetDataEntity() {
  if (!GetButton()) {
    return NULL;
  }

  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
  if (!list->IsInTheList(GetButton()->getId())) {
    return NULL;
  }

  // Add session node
  Core::DataEntity::Pointer dataEntity = list->GetDataEntity(GetButton()->getId());
  if (dataEntity.IsNull()) {
    return NULL;
  }

  return dataEntity;
}

void wxDataNavigationWidget::OnEditDetails(wxCommandEvent &event) {
  SetMode(msvWxHtmlWindow::MODE_EDIT);
  Show();
}

void wxDataNavigationWidget::OnModifiedDataEntity() {
  // Sometimes this function is called in a secondary thread
  wxCommandEvent ce;
  ce.SetEventType(wxID_REFRESH_CONTENTS);
  AddPendingEvent(ce);
}

void wxDataNavigationWidget::OnCancelEdit(wxCommandEvent &event) {
  switch (m_Mode) {
  case msvWxHtmlWindow::MODE_EDIT:
    SetMode(msvWxHtmlWindow::MODE_DESCRIPTION);
    break;
  case msvWxHtmlWindow::MODE_EDIT_VIEW:
    SetMode(msvWxHtmlWindow::MODE_BROWSE_VIEWS);
    break;
  }

  Show();
}

void wxDataNavigationWidget::OnRemoveCurrent(wxCommandEvent &event) {
  // Add session node
  Core::DataEntity::Pointer dataEntity = GetDataEntity();
  if (dataEntity.IsNull()) {
    return;
  }

  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
  list->Remove(dataEntity);

  if (dataEntity->GetFather().IsNotNull()) {
    m_ModifiedList.push_back(dataEntity->GetFather()->GetId());
  }
}

void wxDataNavigationWidget::OnLoadChildData(wxCommandEvent &event) {
  Core::DataEntity::Pointer dataEntity = GetDataEntity();
  if (dataEntity.IsNull()) {
    return;
  }

  Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();
  wxFrame *win = dynamic_cast< wxFrame * >(gIface->GetMainWindow());
  if (!win) {
    return;
  }

  wxMenuBar *menuBar = win->GetMenuBar();
  Core::Widgets::MainMenu *mainMenu = dynamic_cast< Core::Widgets::MainMenu * >(menuBar);

  wxCommandEvent menuEvt(wxEVT_COMMAND_MENU_SELECTED);
  menuEvt.SetId(wxID_OpenDataMenuItem);
  menuEvt.SetInt(dataEntity->GetId());
  mainMenu->GetEventHandler()->ProcessEvent(menuEvt);

  m_ModifiedList.push_back(dataEntity->GetId());

  // Refresh window
  Show();
}

void wxDataNavigationWidget::OnSaveData(wxCommandEvent &event) {
  Core::DataEntity::Pointer dataEntity = GetDataEntity();
  if (dataEntity.IsNull()) {
    return;
  }

  Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();
  wxFrame *win = dynamic_cast< wxFrame * >(gIface->GetMainWindow());
  if (!win) {
    return;
  }

  // Refresh last save path
  if (dataEntity->GetMetadata()->GetTag("FilePath").IsNotNull()) {
    std::string filepath =
        dataEntity->GetMetadata()->GetTag("FilePath")->GetValueAsString();
    Core::Runtime::Settings::Pointer settings =
        Core::Runtime::Kernel::GetApplicationSettings();
    settings->SetLastSavePath(itksys::SystemTools::GetFilenamePath(filepath));
  }

  // Process event by main window
  wxMenuBar *menuBar = win->GetMenuBar();
  Core::Widgets::MainMenu *mainMenu = dynamic_cast< Core::Widgets::MainMenu * >(menuBar);
  wxCommandEvent menuEvt(wxEVT_COMMAND_MENU_SELECTED);
  menuEvt.SetId(wxID_SaveDataMenuItem);
  mainMenu->GetEventHandler()->ProcessEvent(menuEvt);

  // Remove from modified list
  Core::DataEntity::ChildrenListType list;
  list = m_RenderWindow->GetListOfDataToLoad(dataEntity);
  Core::DataEntity::ChildrenListType::iterator it;
  for (it = list.begin(); it != list.end(); it++) {
    m_ModifiedList.remove((*it)->GetId());
  }

  // Refresh "*"
  Show();
}

void wxDataNavigationWidget::OnEditView(wxCommandEvent &event) {
  if (m_ViewManager->GetSelectedView().IsNull()) {
    throw Core::Exceptions::Exception("OnEditView", "Please select a view to edit");
  }

  Core::DataEntity::Pointer dataEntity = GetDataEntity();
  if (dataEntity.IsNull())
    return;

  Core::DataEntity::Pointer session;
  session = m_DataEntityList->FindRelative(dataEntity, Core::SessionTypeId);
  if (session.IsNull())
    return;

  // Add to modified list
  m_ModifiedList.push_back(session->GetId());

  SetMode(msvWxHtmlWindow::MODE_EDIT_VIEW);
  Show();
}

int wxDataNavigationWidget::PopBack() {
  // If it's empty or just the current one, don't do anything
  if (m_History.empty() || m_History.size() == 1)
    return -1;

  // Remove current one
  m_History.pop_back();

  // Return the last one
  return m_History.back();
}

void wxDataNavigationWidget::PushBack(int id) {
  // Check current mode
  if (GetMode() == msvWxHtmlWindow::MODE_TOOLTIP || GetButton() == NULL)
    return;

  // Check last element
  if (!m_History.empty() && m_Button->getId() == m_History.back())
    return;

  // add new element
  m_History.push_back(m_Button->getId());
}

void wxDataNavigationWidget::AddModified(int id) { m_ModifiedList.push_back(id); }

bool wxDataNavigationWidget::CheckModified(int id) {
  std::list< int >::iterator itFind =
      std::find(m_ModifiedList.begin(), m_ModifiedList.end(), id);
  return itFind != m_ModifiedList.end();
}

void wxDataNavigationWidget::OnAddChildSession(wxCommandEvent &event) {
  // Add session node
  Core::DataEntity::Pointer dataEntity = GetDataEntity();
  if (dataEntity.IsNull())
    return;

  Core::DataEntity::Pointer session = Core::DataEntity::New(Core::SessionTypeId);
  session->AddTimeStep(blTagMap::New());
  session->SetFather(dataEntity);

  Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
  Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
  list->Add(session);

  m_RenderWindow->GetPrivateRenderingTree()->Add(session);

  // Set selected
  list->GetSelectedDataEntityHolder()->SetSubject(session);

  // Mark parent session as modified
  Core::DataEntity::Pointer parentSession;
  parentSession = m_DataEntityList->FindRelative(dataEntity, Core::SessionTypeId);
  if (parentSession.IsNotNull()) {
    AddModified(parentSession->GetId());
  }

  // Edit new data
  SetMode(msvWxHtmlWindow::MODE_EDIT);
  Show();
}

void wxDataNavigationWidget::OnAddView(wxCommandEvent &event) {
  blTagMap::Pointer view = m_ViewManager->AddView();
  m_ViewManager->SetSelectedView(view);

  // Edit created view
  SetMode(msvWxHtmlWindow::MODE_EDIT_VIEW);
  Show();

  // Add to modified list
  Core::DataEntity::Pointer parentSession;
  parentSession = m_DataEntityList->FindRelative(GetDataEntity(), Core::SessionTypeId);
  if (parentSession.IsNotNull()) {
    AddModified(parentSession->GetId());
  }
}

msvVTKBtnEditView *wxDataNavigationWidget::GetEditView() { return m_msvVTKBtnEditView; }

void wxDataNavigationWidget::OnRemoveView(wxCommandEvent &event) {
  m_ViewManager->RemoveView(m_ViewManager->GetSelectedView());

  // Refresh dialog
  SetMode(msvWxHtmlWindow::MODE_BROWSE_VIEWS);
  Show();

  // Add to modified list
  Core::DataEntity::Pointer session;
  session = m_DataEntityList->FindRelative(GetDataEntity(), Core::SessionTypeId);
  if (session.IsNotNull())
    m_ModifiedList.push_back(session->GetId());
}

msvViewManager::Pointer wxDataNavigationWidget::GetViewManager() { return m_ViewManager; }

void wxDataNavigationWidget::OnLoadData(wxCommandEvent &event) {
  Core::DataEntity::Pointer dataEntity = GetDataEntity();
  if (dataEntity.IsNull()) {
    return;
  }

  m_RenderWindow->LoadDataEntity(dataEntity->GetId());

  // Refresh window
  Show();
}

void wxDataNavigationWidget::OnUnLoadData(wxCommandEvent &event) {
  Core::DataEntity::Pointer dataEntity = GetDataEntity();
  if (dataEntity.IsNull()) {
    return;
  }

  m_RenderWindow->UnloadDataEntity(dataEntity->GetId());

  // Refresh window
  Show();
}

void wxDataNavigationWidget::OnRefreshContents(wxCommandEvent &WXUNUSED(event)) {
  Core::DataEntity::Pointer dataEntity = GetDataEntity();
  if (dataEntity.IsNull() || GetButton() == NULL || m_ButtonsManager.IsNull()) {
    return;
  }

  // Get data
  Core::DataEntityMetadata::Pointer metadata = dataEntity->GetMetadata();
  std::string desc;
  if (metadata->GetTag("Description").IsNotNull()) {
    desc = metadata->GetTag("Description")->GetValueAsString();
  }
  std::string preview = dataEntity->GetPreview()->GetFileName();
  std::string name = metadata->GetName();

  // Refresh button information
  GetButton()->SetName(name);
  GetButton()->setLabel(name);
  GetButton()->setDescription(desc);

  // Update bounds when applying transform box
  m_ButtonsManager->UpdateDataObject(dataEntity);

  // Refresh window
  Show();

  // Refresh editing after taking snapshot
  if (m_msvVTKBtnEdit->GetDataEntity() == dataEntity) {
    m_msvVTKBtnEdit->SetDataEntity(dataEntity);
  }
}

void wxDataNavigationWidget::OnTakeSnapshot(wxCommandEvent &event) {
  m_RenderWindow->CaptureSnapshot();
}

void wxDataNavigationWidget::OnCaptureView(wxCommandEvent &event) {
  m_RenderWindow->CaptureView();
}
