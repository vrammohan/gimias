/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _StatisticsProcessorCollective_H
#define _StatisticsProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

/**
\ingroup StatisticsPlugin
\author Albert Sanchez
\date 18 Oct 2011
*/

class StatisticsProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(StatisticsProcessorCollective,
                                    Core::SmartPointerObject);

private:
  StatisticsProcessorCollective();

private:
};

#endif //_StatisticsProcessorCollective_H
