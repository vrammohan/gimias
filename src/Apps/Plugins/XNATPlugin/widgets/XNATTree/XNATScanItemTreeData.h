/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef XNATScanItemTreeData_H
#define XNATScanItemTreeData_H

#include <wx/treectrl.h>

#include "XNATPluginUtilities.h"

/**
A class that holds some information about Scan in the tree item

\ingroup XNATPlugin
\author Albert Sanchez
\date 10 Oct 2011
*/

class ScanItemTreeData : public wxTreeItemData {
public:
  //!
  ScanItemTreeData(int id, int subjectId, int experimentId, std::string &experimentLabel,
                   std::string &subjectLabel, XNAT::Utils::Scan &scan)
      : m_id(id), m_subjectId(subjectId), m_experimentId(experimentId),
        m_experimentLabel(experimentLabel), m_subjectLabel(subjectLabel), m_scan(scan) {}

  //!
  ~ScanItemTreeData() {}

  //!
  int GetId() const { return m_id; }
  //!
  int GetSubjectId() const { return m_subjectId; }
  //!
  int GetExperimentId() const { return m_experimentId; }
  //!
  std::string GetExperimentLabel() const { return m_experimentLabel; }
  //!
  std::string GetSubjectlabel() const { return m_subjectLabel; }
  //!
  XNAT::Utils::Scan GetScan() const { return m_scan; }

private:
  //!
  int m_id;
  //!
  int m_subjectId;
  //!
  int m_experimentId;
  //!
  std::string m_experimentLabel;
  //!
  std::string m_subjectLabel;
  //!
  XNAT::Utils::Scan m_scan;
};

#endif // XNATScanItemTreeData_H
