/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVersion_H
#define coreVersion_H

#define GM_MAJOR_VERSION 1
#define GM_MINOR_VERSION 6
#define GM_PATCH_VERSION 1
#define GM_VERSION "1.6.r1"

#endif // coreVersion_H
