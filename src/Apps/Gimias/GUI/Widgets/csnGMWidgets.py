# Used to configure coreIO
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmWidgets = api.CreateCompiledProject("gmWidgets", "dll")
gmWidgets.AddProjects([
    gmProcessors,
    gmWxEvents,
    guiBridgeLibWxWidgets,
    dynWxAGUILib,
    wxMitk
])

# GRID
#if (sys.platform == "win32"):
# gmWidgets.AddDefinitions(["-DUSE_GRID_API"] ) 
# gmWidgets.AddProjects([gridAPI])

gmWidgets.SetPrecompiledHeader("gmWidgetsPCH.h")

widgetFolders = gmWidgets.Glob("src/*")
for widgetFolder in widgetFolders:
    if os.path.isdir(widgetFolder):
        gmWidgets.AddSources(["%s/*.cpp" % widgetFolder], checkExists=0)
        gmWidgets.AddSources(["%s/*.cxx" % widgetFolder], checkExists=0)
        gmWidgets.AddSources(["%s/*.txx" % widgetFolder], checkExists=0)
        gmWidgets.AddSources(["%s/*.h" % widgetFolder], checkExists=0)
        gmWidgets.AddIncludeFolders([widgetFolder])
        resourceFiles = gmWidgets.Glob("%s/resource" % widgetFolder)
        gmWidgets.AddFilesToInstall(resourceFiles, "resource")

if api.GetCompiler().TargetIsMac():
    gmWidgets.AddLibraries(["-framework ApplicationServices"])
