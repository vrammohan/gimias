/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "WebServerConfigurationPanelWidget.h"
#include "WebServerProcessor.h"

#include "coreSettings.h"

#include "wx/wupdlock.h"

#include "web_services.xpm"

BEGIN_EVENT_TABLE(WebServerConfigurationPanelWidget, WebServerConfigurationPanelWidgetUI)
EVT_BUTTON(wxID_OK, WebServerConfigurationPanelWidget::OnOK)
EVT_BUTTON(wxID_CANCEL, WebServerConfigurationPanelWidget::OnCancel)
END_EVENT_TABLE();

WebServerConfigurationPanelWidget::WebServerConfigurationPanelWidget(
    wxWindow *parent, int id, const wxString &title,
    const wxPoint &pos /*=wxDefaultPosition*/, const wxSize &size /*=wxDefaultSize*/,
    long style /*=wxDEFAULT_DIALOG_STYLE*/)
    : WebServerConfigurationPanelWidgetUI(parent, id, title, pos, size, style) {
  m_bmpDiagram->SetBitmap(wxBitmap(web_services_xpm));

  // Replace old txt control by hyperlink control
  n_EndPointHyperLink = new wxHyperlinkCtrl(this, wxID_ANY, wxT("End point"), "");
  GetSizer()->Replace(m_txtEndPoint, n_EndPointHyperLink, true);
  m_txtEndPoint->Destroy();
}

WebServerConfigurationPanelWidget::~WebServerConfigurationPanelWidget() {}

void WebServerConfigurationPanelWidget::UpdateData() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  std::string serverPortStr = m_txtServerPort->GetValue().ToStdString();
  settings->SetPluginProperty("WebServicesPlugin", "ServerPort", serverPortStr);

  std::string portStr = m_txtPort->GetValue().ToStdString();
  settings->SetPluginProperty("WebServicesPlugin", "ServiceProviderPort", portStr);

  std::string ipStr = m_cmbIPAddress->GetValue().ToStdString();
  settings->SetPluginProperty("WebServicesPlugin", "ServiceProviderIPAddress", ipStr);

  std::string timeoutStr = m_txtTimeout->GetValue().ToStdString();
  settings->SetPluginProperty("WebServicesPlugin", "IOTimeout", timeoutStr);
}

void WebServerConfigurationPanelWidget::UpdateWidget() {
  wxWindowUpdateLocker lock(this);

  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  std::string serverPortStr;
  if (settings->GetPluginProperty("WebServicesPlugin", "ServerPort", serverPortStr)) {
    m_txtServerPort->SetValue(serverPortStr);
  } else {
    m_txtServerPort->SetValue("9090");
  }

  std::string portStr;
  if (settings->GetPluginProperty("WebServicesPlugin", "ServiceProviderPort", portStr)) {
    m_txtPort->SetValue(portStr);
  } else {
    m_txtPort->SetValue("9090");
  }

  m_cmbIPAddress->Clear();
  std::list< std::string > addresses = WebServerProcessor::GetPublicIpAddressList();
  std::list< std::string >::iterator it;
  for (it = addresses.begin(); it != addresses.end(); it++) {
    m_cmbIPAddress->Append(*it);
  }

  std::string ipStr;
  if (settings->GetPluginProperty("WebServicesPlugin", "ServiceProviderIPAddress",
                                  ipStr)) {
    m_cmbIPAddress->SetValue(ipStr);
  } else {
    m_cmbIPAddress->SetValue(*addresses.begin());
  }

  std::string timeoutStr;
  if (settings->GetPluginProperty("WebServicesPlugin", "IOTimeout", timeoutStr)) {
    m_txtTimeout->SetValue(timeoutStr);
  } else {
    m_txtTimeout->SetValue("0");
  }

  n_EndPointHyperLink->SetURL("http://localhost:" + m_txtServerPort->GetValue() +
                              "/axis2/services/wsGimias?wsdl");
  n_EndPointHyperLink->SetLabel("http://localhost:" + m_txtServerPort->GetValue() +
                                "/axis2/services/wsGimias?wsdl");
}

void WebServerConfigurationPanelWidget::OnCancel(wxCommandEvent &event) { event.Skip(); }

void WebServerConfigurationPanelWidget::OnOK(wxCommandEvent &event) {
  UpdateData();
  event.Skip();
}

void WebServerConfigurationPanelWidget::OnApply(wxCommandEvent &event) {
  UpdateData();
  UpdateWidget();
}
