/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ClinicalReportWidgetTool_H
#define _ClinicalReportWidgetTool_H

#include "coreProcessingWidget.h"
#include "ClinicalReportWidgetToolUI.h"
#include "ClinicalReportToolbar.h"

class ClinicalReportWidgetTool : public ClinicalReportWidgetToolUI,
                                 public Core::Widgets::ProcessingWidget {
public:
  //!
  coreDefineBaseWindowFactory(ClinicalReportWidgetTool);

  //!
  ClinicalReportWidgetTool(wxWindow *parent, int id,
                           const wxPoint &pos = wxDefaultPosition,
                           const wxSize &size = wxDefaultSize, long style = 0);

  //!
  Core::BaseProcessor::Pointer GetProcessor();

  //!
  void OnInit();

private:
  void OnNewSnapshot(wxCommandEvent &event);
  void OnNewComment(wxCommandEvent &event);
  void OnClinicalReportDesginer(wxCommandEvent &event);

  //!
  Core::Widgets::ClinicalReportToolbar *GetReportToolbar();

protected:
  //! processor
  ClinicalReportProcessor::Pointer m_Processor;
};

#endif // _ClinicalReportWidgetTool_H
