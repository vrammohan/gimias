/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef VesselBifurcationsPanelWidget_H
#define VesselBifurcationsPanelWidget_H

#include <wx/wx.h>
#include <wx/image.h>

#include "corePluginMacros.h"
#include "coreProcessingWidget.h"
#include "vmtkVesselBifurcationsProcessor.h"

#define wxID_btnExtract wxID_HIGHEST + 1
#define wxID_btnSplit wxID_HIGHEST + 2
#define wxID_btnVectors wxID_HIGHEST + 3

/**
\brief Vessel Bifurcations
\ingroup VMTKPlugin
\author Valeria Barbarito
\date 08 Mar 2011
*/
class PLUGIN_EXPORT VesselBifurcationsPanelWidget
    : public wxPanel,
      public Core::Widgets::ProcessingWidget {
public:
  coreDefineBaseWindowFactory(VesselBifurcationsPanelWidget)

      VesselBifurcationsPanelWidget(wxWindow *parent, int id = wxID_ANY,
                                    const wxPoint &pos = wxDefaultPosition,
                                    const wxSize &size = wxDefaultSize, long style = 0);

  ~VesselBifurcationsPanelWidget();

protected:
  //!
  void OnInit();

  //!
  Core::BaseProcessor::Pointer GetProcessor();

private:
  //!
  void do_layout();

  //!
  virtual void OnButtonExtract(wxCommandEvent &event);

  //!
  virtual void OnButtonSplit(wxCommandEvent &event);

  //!
  virtual void OnButtonVectors(wxCommandEvent &event);

protected:
  //!
  wxButton *btnExtract;
  //!
  wxButton *btnSplit;
  //!
  wxButton *btnVectors;

  DECLARE_EVENT_TABLE();

private:
  //!
  VesselBifurcationsProcessor::Pointer m_processor;
  //!
  wxStaticBox *m_extractBranchesStaticBox;

}; // wxGlade: end class

#endif // VesselBifurcationsPanelWidget_H