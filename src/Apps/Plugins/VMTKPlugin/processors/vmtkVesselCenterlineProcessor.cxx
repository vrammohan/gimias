/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#include "vmtkVesselCenterlineProcessor.h"

#include "coreBaseWindow.h"
#include "coreDataEntityHelper.h"

#include "vtkPolyDataWriter.h"
#include "vtkPolyDataReader.h"
#include "vtkCleanPolyData.h"
#include "vtkSelection.h"
#include "vtkSelectionNode.h"
#include "vtkInformation.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkExtractSelection.h"
#include "vtkvmtkPolyDataCenterlines.h"
#include "vtkIdList.h"
#include "vtkvmtkCenterlineSmoothing.h"
#include "vtkvmtkCapPolyData.h"
#include "vtkvmtkNeighborhoods.h"
#include "vtkvmtkNeighborhood.h"
#include "vtkTriangleFilter.h"
#include "vtkStringArray.h"
#include "vtkPointSet.h"
#include "vtkPoints.h"
#include "vtkCleanPolyData.h"
#include "coreDataEntityBuilder.h"
#include "coreLandmarkSelectorWidget.h"
#include "coreMultiRenderWindowEventHandler.h"
#include "coreDataTreeMITKHelper.h"
#include "wxMitkRenderWindow.h"
#include "mitkRenderingManager.h"

VesselCenterlineProcessor::VesselCenterlineProcessor() {
  SetNumberOfInputs(NUMBER_OF_INPUTS);
  GetInputPort(INPUT_SURFACE)->SetName("Vessel geometry");
  GetInputPort(INPUT_SURFACE)->SetDataEntityType(Core::SurfaceMeshTypeId);
  GetInputPort(INPUT_SEED_POINTS)->SetName("Input Seed Points");
  GetInputPort(INPUT_SEED_POINTS)->SetDataEntityType(Core::PointSetTypeId);

  SetNumberOfOutputs(NUMBER_OF_OUTPUTS);
  GetOutputPort(OUTPUT_CENTERLINE)
      ->SetDataEntityType(Core::SkeletonTypeId | Core::SurfaceMeshTypeId);

  GetOutputPort(OUTPUT_CAPPEDSURFACE)->SetDataEntityType(Core::SurfaceMeshTypeId);
  GetOutputPort(OUTPUT_CAPSCENTER_POINTS)->SetDataEntityType(Core::PointSetTypeId);

  m_radiusArrayName = "MaximumInscribedSphereRadius";
  m_costFunction = "1/R";
  m_flipNormals = 0;

  // Desmond Ryan needs this option to 1
  m_appendEndPoints = 1;
  m_simplifyVoronoi = 0;
  m_resampling = 0;
  m_resamplingStepLength = 1.0;

  m_numSource = 0;
  m_numTarget = 0;

  m_inletID = -1;
  m_outletID = -1;

  m_sourcePointsList = vtkSmartPointer< vtkIdList >::New();
  m_targetPointsList = vtkSmartPointer< vtkIdList >::New();
  m_capCenterIDs = vtkSmartPointer< vtkIdList >::New();
  m_cappedSurfaceInput = Core::vtkPolyDataPtr::New();
}

VesselCenterlineProcessor::~VesselCenterlineProcessor() {}

void VesselCenterlineProcessor::Update() {
  bool warning = vtkObject::GetGlobalWarningDisplay();
  vtkObject::SetGlobalWarningDisplay(false);

  try {
    if (m_seedSelectorType == SEED_POINTLIST) {

      Core::vtkPolyDataPtr seedLandmarks;
      Core::DataEntityHelper::GetProcessingData(
          GetInputDataEntityHolder(INPUT_SEED_POINTS), seedLandmarks);
      if (GetNumSource() < 0)
        throw Core::Exceptions::Exception("vmtkVesselCenterlineProcessor::Update",
                                          "You must select at least 1 SOURCE point");
      if (seedLandmarks->GetNumberOfPoints() == GetNumSource())
        throw Core::Exceptions::Exception("vmtkVesselCenterlineProcessor::Update",
                                          "You must select at least 1 TARGET point");

      ComputePointIdLists();
      ComputeCenterline(SEED_POINTLIST);
    }

    if (m_seedSelectorType == SEED_OPENPROFILES) {
      ComputeCenterline(SEED_OPENPROFILES);
    }
  } catch (...) {
    vtkObject::SetGlobalWarningDisplay(warning);
    throw;
  }

  vtkObject::SetGlobalWarningDisplay(warning);
}

void VesselCenterlineProcessor::ComputePointIdLists() {

  m_sourcePointsList->Reset();
  m_targetPointsList->Reset();
  vtkIdType pointId;

  Core::vtkPolyDataPtr pointsProcessingData;
  Core::DataEntityHelper::GetProcessingData(GetInputDataEntityHolder(INPUT_SEED_POINTS),
                                            pointsProcessingData);
  Core::vtkPolyDataPtr surface;
  Core::DataEntityHelper::GetProcessingData(GetInputDataEntityHolder(INPUT_SURFACE),
                                            surface);

  vtkPoints *points = pointsProcessingData->GetPoints();
  if (points == NULL)
    return;

  if (!(points->GetNumberOfPoints() > 0 &&
        GetInputDataEntity(INPUT_SEED_POINTS)->GetFather() ==
            GetInputDataEntity(INPUT_SURFACE))) {
    throw Core::Exceptions::Exception(
        "VesselCenterlineProcessor::ComputeCenterlinPointId", "Point is not correct");
  }

  for (int i = 0; i < points->GetNumberOfPoints(); i++) {
    pointId = surface->FindPoint(points->GetPoint(i)[0], points->GetPoint(i)[1],
                                 points->GetPoint(i)[2]);

    if (i < m_numSource)
      m_sourcePointsList->InsertNextId(pointId);
    else
      m_targetPointsList->InsertNextId(pointId);
  }
}

void VesselCenterlineProcessor::ComputeCenterline(int seedSelectorType) {

  vtkSmartPointer< vtkvmtkPolyDataCenterlines > centerlineFilter =
      vtkvmtkPolyDataCenterlines::New();
  Core::vtkPolyDataPtr surfaceInput;
  Core::CastAnyProcessingData(GetInputDataEntity(INPUT_SURFACE)->GetProcessingData(),
                              surfaceInput);

  vtkSmartPointer< vtkIdList > inletSeedIds = vtkIdList::New();
  vtkSmartPointer< vtkIdList > outletSeedIds = vtkIdList::New();

  if (seedSelectorType == SEED_OPENPROFILES) {

    centerlineFilter->SetInput(m_cappedSurfaceInput);
    inletSeedIds->InsertNextId(m_capCenterIDs->GetId(m_inletID));
    outletSeedIds->InsertNextId(m_capCenterIDs->GetId(m_outletID));
  }
  if (seedSelectorType == SEED_POINTLIST) {
    centerlineFilter->SetInput(surfaceInput);
    inletSeedIds->DeepCopy(m_sourcePointsList);
    outletSeedIds->DeepCopy(m_targetPointsList);
  }
  centerlineFilter->SetSourceSeedIds(inletSeedIds);
  centerlineFilter->SetTargetSeedIds(outletSeedIds);
  centerlineFilter->SetRadiusArrayName(m_radiusArrayName.c_str());
  centerlineFilter->SetCostFunction(m_costFunction.c_str());
  centerlineFilter->SetFlipNormals(m_flipNormals);
  centerlineFilter->SetAppendEndPointsToCenterlines(m_appendEndPoints);
  centerlineFilter->SetSimplifyVoronoi(m_simplifyVoronoi);
  centerlineFilter->SetCenterlineResampling(m_resampling);
  centerlineFilter->SetResamplingStepLength(m_resamplingStepLength);
  centerlineFilter->Update();

  // clean centerline
  vtkSmartPointer< vtkCleanPolyData > cleanFilter = vtkCleanPolyData::New();
  cleanFilter->SetInput(centerlineFilter->GetOutput());
  cleanFilter->SetTolerance(0.0);
  cleanFilter->SetAbsoluteTolerance(1.0);
  cleanFilter->SetConvertLinesToPoints(false);
  cleanFilter->SetConvertPolysToLines(false);
  cleanFilter->SetConvertStripsToPolys(false);
  cleanFilter->Update();

  // Extract selection

  // std::cout << "There are " << vtkOutput->GetNumberOfPoints()
  //	<< " input points." << std::endl;
  // std::cout << "There are " << vtkOutput->GetNumberOfCells()
  //	<< " input cells." << std::endl;

  // vtkSmartPointer<vtkIdTypeArray> ids =
  //	vtkSmartPointer<vtkIdTypeArray>::New();
  // ids->SetNumberOfComponents(1);

  // for(vtkIdType i = 0; i < vtkOutput->GetNumberOfPoints(); i++)
  //{
  //	ids->InsertNextValue(i);
  //}

  // vtkSelection *selection = vtkSelection::New();
  // vtkSelectionNode *selectionNode = vtkSelectionNode::New();
  // selectionNode->SetFieldType(vtkSelectionNode::POINT);
  // selectionNode->SetContentType(vtkSelectionNode::INDICES);
  // selectionNode->SetSelectionList(ids);
  // selectionNode->GetProperties()->Set(vtkSelectionNode::CONTAINING_CELLS(), 1);
  // selection->AddNode(selectionNode);
  //

  // vtkExtractSelection *extractSelection  = vtkExtractSelection::New();
  // extractSelection->SetInput(0, vtkOutput);
  // extractSelection->SetInput(1, selection);
  // extractSelection->Update();
  // extractSelection->PreserveTopologyOff();
  //
  ////in selection
  // vtkSmartPointer<vtkUnstructuredGrid> selected =
  //	vtkSmartPointer<vtkUnstructuredGrid>::New();
  // selected->DeepCopy(extractSelection->GetOutput());

  // std::cout << "There are " << selected->GetNumberOfPoints()
  //	<< " points in the selection." << std::endl;
  // std::cout << "There are " << selected->GetNumberOfCells()
  //	<< " cells in the selection." << std::endl;

  //
  ////extract polydata

  // vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter =
  //	vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
  // surfaceFilter->SetInput(selected);
  // surfaceFilter->Update();

  // vtkPolyData* selectedPolyData =  vtkPolyData::New();
  // selectedPolyData->DeepCopy(surfaceFilter->GetOutput());
  //
  ////clean centerline
  // cleanFilter->SetInput(selectedPolyData);
  // cleanFilter->Update();
  // Core::vtkPolyDataPtr cleanSelected = vtkPolyData::New();
  // cleanSelected->DeepCopy(cleanFilter->GetOutput());

  // std::cout << "Output has " << cleanSelected->GetNumberOfPoints() << " points." <<
  // std::endl;

  UpdateOutput(OUTPUT_CENTERLINE, cleanFilter->GetOutput(), "centerline", false, 1,
               GetInputDataEntity(INPUT_SURFACE));

  //-------------------------------------------------------------------------
}

void VesselCenterlineProcessor::Smoothing(Core::vtkPolyDataPtr centerline) {
  // not used
  vtkvmtkCenterlineSmoothing *centerlineSmoothingFilter =
      vtkvmtkCenterlineSmoothing::New();

  centerlineSmoothingFilter->SetInput(centerline);
  centerlineSmoothingFilter->SetNumberOfSmoothingIterations(4);
  centerlineSmoothingFilter->SetSmoothingFactor(0.1);
  centerlineSmoothingFilter->Update();
  Core::vtkPolyDataPtr vtkOutputSmooth = vtkPolyData::New();
  vtkOutputSmooth->DeepCopy(centerlineSmoothingFilter->GetOutput());
  UpdateOutput(0, vtkOutputSmooth, "SmoothCenterline", false, 1,
               GetInputDataEntity(INPUT_SURFACE));
}

void VesselCenterlineProcessor::GetCapCenterIDs() {
  m_cappedSurfaceInput->Reset();
  m_capCenterIDs->Reset();
  vtkvmtkCapPolyData *surfaceCapper = vtkvmtkCapPolyData::New();

  Core::vtkPolyDataPtr surfaceInput;
  Core::CastAnyProcessingData(GetInputDataEntity(INPUT_SURFACE)->GetProcessingData(),
                              surfaceInput);

  // NonManifold check
  NonManifoldSurfaceChecker(surfaceInput);

  // CleaningSurface
  vtkCleanPolyData *surfaceCleaner = vtkCleanPolyData::New();
  surfaceCleaner->SetInput(surfaceInput);
  surfaceCleaner->Update();

  // Triangulating surface
  vtkTriangleFilter *surfaceTriangulator = vtkTriangleFilter::New();
  surfaceTriangulator->SetInput(surfaceCleaner->GetOutput());
  surfaceTriangulator->PassLinesOff();
  surfaceTriangulator->PassVertsOff();
  surfaceTriangulator->Update();
  surfaceTriangulator->GetOutput();

  surfaceCapper->SetInput(surfaceInput);
  surfaceCapper->SetDisplacement(0.0);
  surfaceCapper->SetInPlaneDisplacement(0.0);
  surfaceCapper->Update();

  m_cappedSurfaceInput->DeepCopy(surfaceCapper->GetOutput());
  m_capCenterIDs->DeepCopy(surfaceCapper->GetCapCenterIds());
}
int VesselCenterlineProcessor::GetNumberOfIds() {
  return m_capCenterIDs->GetNumberOfIds();
}

void VesselCenterlineProcessor::NonManifoldSurfaceChecker(Core::vtkPolyDataPtr surface) {

  Core::vtkPolyDataPtr inputSurface;

  int numberOfNonManifoldEdges = 0;
  std::string report;
  vtkSmartPointer< vtkIdList > NonManifoldEdgePointIds = vtkIdList::New();

  if (surface == NULL) {
    cout << "NonManifoldSurfaceChecker error: Surface not set" << endl;
    return;
  }

  int nonManifoldEdgesFound = 0;
  report = "";

  vtkvmtkNeighborhoods *neighborhoods = vtkvmtkNeighborhoods::New();
  neighborhoods->SetNeighborhoodTypeToPolyDataManifoldNeighborhood();
  neighborhoods->SetDataSet(surface);
  neighborhoods->Build();

  vtkSmartPointer< vtkIdList > neighborCellIds;
  vtkSmartPointer< vtkIdList > cellPointIds = vtkIdList::New();

  surface->BuildCells();
  surface->BuildLinks();
  surface->Update();

  vtkvmtkNeighborhood *neighborhood;
  int neighborId = 0;

  for (int i = 0; i < neighborhoods->GetNumberOfNeighborhoods(); i++) {

    neighborhood = neighborhoods->GetNeighborhood(i);
    for (int j = 0; j < neighborhood->GetNumberOfPoints(); j++) {
      neighborId = neighborhood->GetPointId(j);
      if (i < neighborId) {
        neighborCellIds = vtkIdList::New();
        surface->GetCellEdgeNeighbors(-1, i, neighborId, neighborCellIds);
        if (neighborCellIds->GetNumberOfIds() > 2) {
          numberOfNonManifoldEdges = numberOfNonManifoldEdges + 1;
          cout << "Non-manifold edge found" << i << " " << neighborId << endl;
          NonManifoldEdgePointIds->InsertNextId(i);
          NonManifoldEdgePointIds->InsertNextId(neighborId);
        }
      }
    }
  }
}

void VesselCenterlineProcessor::ShowCapCenters() {

  vtkPolyData *pointSetData = vtkPolyData::New();
  vtkPoints *points = vtkPoints::New();
  double coord[3];
  vtkSmartPointer< vtkStringArray > stringData;
  stringData = vtkSmartPointer< vtkStringArray >::New();
  stringData->SetName("LandmarksName");
  for (int k = 0; k < m_capCenterIDs->GetNumberOfIds(); k++) {
    m_cappedSurfaceInput->GetPoint(m_capCenterIDs->GetId(k), coord);
    points->InsertNextPoint(coord);
    char capLabel[5] = "";
    sprintf(capLabel, "%d", k);
    stringData->InsertNextValue(capLabel);
  }
  pointSetData->SetPoints(points);
  pointSetData->GetPointData()->AddArray(stringData);
  if ((m_capCenterIDs->GetNumberOfIds()) > 0)
    pointSetData->Modified();

  UpdateOutput(OUTPUT_CAPPEDSURFACE, m_cappedSurfaceInput, "CappedSurface", false, 1,
               GetInputDataEntity(INPUT_SURFACE));

  UpdateOutput(OUTPUT_CAPSCENTER_POINTS, pointSetData, "CapCenters", false, 1,
               GetInputDataEntity(INPUT_SURFACE));
}

void VesselCenterlineProcessor::SetInletOutletIDs(int inletID, int outletID) {
  if ((inletID < 0) || (outletID < 0) || (inletID == outletID)) {
    throw Core::Exceptions::Exception(
        "Core::VesselCenterlineProcessor::SetInletOutletIDs",
        "inlet and/or outlet ID is not correct");
  }
  m_inletID = inletID;
  m_outletID = outletID;
}

void VesselCenterlineProcessor::SetAppendEndPoints(bool val) { m_appendEndPoints = val; }