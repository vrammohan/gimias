# CSNake configuration of the AhePlugin

# CSNake imports
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")
# Other dependencies
from csnGIMIASDef import *
from csnToolkitOpen import * 
from csnAheCLP import * 

# Definition of the template plugin
ahePlugin = GimiasPluginProject("AhePlugin", api)
# plugin dependencies
projects = [
    gmCore, 
    aheclpCLP, # This is not a real dependency. It is here just to automatically include the CLP version too in the GIMIAS project.
    aheAPI
]
ahePlugin.AddProjects(projects)
# plugin sources
ahePlugin.AddSources(["*.cxx", "*.h"])
ahePlugin.SetPrecompiledHeader("AhePluginPCH.h")
# plugin tests
ahePlugin.AddTests(["tests/*.*"], cxxTest)

# plugin widgets
widgetModules = [
  "AhePluginConfigurationPanelWidget",
]
ahePlugin.AddWidgetModules(widgetModules, _useQt = 0)
ahePlugin.AddSources(["processors/*.cxx", "processors/*.h"])
ahePlugin.AddIncludeFolders(["processors"])
