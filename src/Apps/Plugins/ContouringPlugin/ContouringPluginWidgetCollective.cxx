/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "ContouringPluginWidgetCollective.h"
#include "ContouringWidgetPanelWidget.h"
const long wxID_ContouringWidgetPanelWidget = wxNewId();

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTab.h"

namespace contouringPlugin {

WidgetCollective::WidgetCollective() {
  Core::WindowConfig windowConfig = Core::WindowConfig().ProcessingTool();
  windowConfig.Category("Contouring");

  Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();
  gIface->RegisterFactory(ContouringWidgetPanelWidget::Factory::NewBase(),
                          windowConfig.Caption("Manual Contouring"));
}

} // namespace contouringPlugin
