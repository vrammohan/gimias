/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportPatientInformation.h"

ClinicalReportPatientInformation::ClinicalReportPatientInformation() {}

//! Getters
wxString ClinicalReportPatientInformation::GetId() { return m_Id; }

wxString ClinicalReportPatientInformation::GetName() { return m_Name; }

int ClinicalReportPatientInformation::GetAge() { return m_Age; }

bool ClinicalReportPatientInformation::GetSex() { return m_Sex; }

wxDateTime ClinicalReportPatientInformation::GetAdquisitionDate() {
  return m_AdquisitionDate;
}

wxDateTime ClinicalReportPatientInformation::GetBirthDate() { return m_BirthDate; }

//! Setters
void ClinicalReportPatientInformation::SetId(wxString id) { m_Id = id; }

void ClinicalReportPatientInformation::SetName(wxString name) { m_Name = name; }

void ClinicalReportPatientInformation::SetAge(int age) { m_Age = age; }

void ClinicalReportPatientInformation::SetSex(bool sex) { m_Sex = sex; }

void ClinicalReportPatientInformation::SetAdquisitionDate(wxDateTime adquisitionDate) {
  m_AdquisitionDate = adquisitionDate;
}

void ClinicalReportPatientInformation::SetBirthDate(wxDateTime birthDate) {
  m_BirthDate = birthDate;
}
