/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef TableView_H
#define TableView_H

#include "TableViewUI.h"
#include "StatisticsPluginUtilities.h"

#include "CILabNamespaceMacros.h"
#include "coreProcessingWidget.h"
#include "coreDICOMFileReader.h"

namespace Core {
namespace Widgets {

  /**
  \ingroup StatisticsPlugin
  \author Albert Sanchez
  \date 18 Oct 2011
  */

  class TableView : public TableViewUI {

  public:
    //! constructor. initializes members.
    TableView(wxWindow *parent, int id, const wxPoint &pos = wxDefaultPosition,
              const wxSize &size = wxDefaultSize, long style = 0);

  public:
    //!
    void SetTagMap(blTagMap::Pointer tagMag);

  private:
    //! Updates GUI
    void UpdateWidget();

    //!
    void FillGrid();

  private:
    //!
    blTagMap::Pointer m_TagMap;
  };

} // Widgets
} // Core

#endif // TableView_H
