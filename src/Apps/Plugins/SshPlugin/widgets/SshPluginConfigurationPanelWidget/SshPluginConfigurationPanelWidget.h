/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SshPluginConfigurationPanelWidget_H
#define _SshPluginConfigurationPanelWidget_H

#include "SshPluginConfigurationPanelWidgetUI.h"
#include "corePreferencesPage.h"
#include "SshPluginNodeManager.h"

/**
Configure all SSH nodes

\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/
class sshPluginConfigurationPanelWidget : public SshPluginConfigurationPanelWidgetUI,
                                          public Core::Widgets::PreferencesPage {

  // OPERATIONS
public:
  //!
  coreDefineBaseWindowFactory1param(sshPluginConfigurationPanelWidget,
                                    SshPluginNodeManager::Pointer);

  //!
  sshPluginConfigurationPanelWidget(SshPluginNodeManager::Pointer nodeManager,
                                    wxWindow *parent, int id = wxID_ANY,
                                    const wxPoint &pos = wxDefaultPosition,
                                    const wxSize &size = wxDefaultSize, long style = 0);

  //!
  ~sshPluginConfigurationPanelWidget();

private:
  //! Update GUI from working data
  void UpdateWidget();

  //! Update working data from GUI
  void UpdateData();

  void OnAdd(wxCommandEvent &event);
  void OnRemove(wxCommandEvent &event);
  void OnEdit(wxCommandEvent &event);
  void OnListDClick(wxCommandEvent &event);

  // ATTRIBUTES
private:
  //!
  SshPluginNodeManager::Pointer m_NodeManager;
};

#endif //_SshPluginConfigurationPanelWidget_H
