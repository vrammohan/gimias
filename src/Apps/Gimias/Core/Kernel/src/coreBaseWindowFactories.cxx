/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseWindowFactories.h"

namespace Core {
BaseWindowFactories::BaseWindowFactories() {
  m_CreatorsMapHolder = Core::BaseWindowFactories::FactoriesMapHolderType::New();
}

BaseWindow *BaseWindowFactories::CreateBaseWindow(const std::string &factoryName,
                                                  wxWindow *parent) {
  BaseWindowFactory::Pointer factory = FindFactory(factoryName);
  if (factory.IsNull()) {
    std::ostringstream strError;
    strError << factoryName << " is not registered" << std::endl;
    throw Core::Exceptions::Exception("BaseWindowFactories::CreateBaseWindow",
                                      strError.str().c_str());
  }

  Core::WindowConfig config = GetFactoriesMap()[factoryName].m_WindowConfig;

  factory->SetParent(parent);
  factory->SetWindowId(config.GetId());
  factory->SetWindowName(config.GetCaption());
  factory->SetBitmapFilename(config.GetBitmapFilename());

  return factory->CreateBaseWindow();
}

void BaseWindowFactories::CleanRegisteredWidgets() { GetFactoriesMap().clear(); }

bool BaseWindowFactories::GetWindowConfig(const std::string &factoryName,
                                          WindowConfig &config) {
  FactoriesMapType::iterator it = GetFactoriesMap().find(factoryName);
  if (it != GetFactoriesMap().end()) {
    config = it->second.m_WindowConfig;
    return true;
  }
  return false;
}

BaseWindowFactories::FactoriesMapHolderType::Pointer
BaseWindowFactories::GetFactoriesHolder() {
  return m_CreatorsMapHolder;
}

BaseWindowFactories::FactoriesMapType &BaseWindowFactories::GetFactoriesMap() {
  return m_CreatorsMapHolder->GetSubject();
}

void BaseWindowFactories::RegisterFactory(BaseWindowFactory::Pointer factory,
                                          WindowConfig widgetConfig) {

  std::string name = factory->GetNameOfClass();
  if (FindFactory(name)) {
    std::ostringstream strError;
    strError << "A factory with the class name: " << name << " is already registered."
             << std::endl;
    throw Core::Exceptions::Exception("BaseWindowFactories::RegisterFactory",
                                      strError.str().c_str());
  }

  FactoryData data;
  data.m_Factory = factory;
  data.m_WindowConfig = widgetConfig;

  m_CreatorsList.push_back(name);
  GetFactoriesMap()[name] = data;
  GetFactoriesHolder()->NotifyObservers();
}

void BaseWindowFactories::UnRegisterFactory(const std::string &factoryName) {
  if (FindFactory(factoryName)) {
    m_CreatorsList.remove(factoryName);
    GetFactoriesMap().erase(factoryName);
    GetFactoriesHolder()->NotifyObservers();
  }
}

BaseWindowFactory::Pointer
BaseWindowFactories::FindFactory(const std::string &factoryName) {
  FactoriesMapType::iterator it = GetFactoriesMap().find(factoryName);
  if (it == GetFactoriesMap().end()) {
    return NULL;
  }

  return it->second.m_Factory;
}

BaseWindowFactories::FactoriesListType BaseWindowFactories::GetFactoryNames() const {
  return m_CreatorsList;
}
}
