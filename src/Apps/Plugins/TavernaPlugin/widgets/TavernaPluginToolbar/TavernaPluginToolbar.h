/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _TavernaPluginToolbar_H
#define _TavernaPluginToolbar_H

#include "coreToolbarBase.h"

namespace TavernaPlugin {

/**
\brief Toolbar for Cmgui Plugin
\ingroup TavernaPlugin
\author Xavi Planes
\date Nov 2010
*/
class Toolbar : public Core::Widgets::ToolbarBase {
public:
  //!
  coreDefineBaseWindowFactory(TavernaPlugin::Toolbar);

  //!
  Toolbar(wxWindow *parent, wxWindowID id = wxID_ANY,
          const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize,
          long style = wxAUI_TB_DEFAULT_STYLE, const wxString &name = wxPanelNameStr);

protected:
  //!
  void OnRefresh(wxCommandEvent &event);

  DECLARE_EVENT_TABLE();

private:
  //!
  std::string m_PluginToolbarPath;
};

} // namespace TavernaPlugin

#endif // _TavernaPluginToolbar_H
