/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _VesselBifurcationsProcessor_H
#define _VesselBifurcationsProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseFilter.h"
#include "coreBaseProcessor.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include "coreVTKPolyDataHolder.h"

//#include "coreDirectionalPlane.h"

/*
compute vessel bifurcations using VMTK

\ingroup VMTKPlugin
\author Valeria Barbarito
\date 08 Mar 2011
*/

class PLUGIN_EXPORT VesselBifurcationsProcessor : public Core::BaseProcessor {
public:
  coreDeclareSmartPointerClassMacro(VesselBifurcationsProcessor, Core::BaseProcessor);

  typedef enum { INPUT_CENTERLINE, NUMBER_OF_INPUTS } INPUT_TYPE;

  typedef enum {
    OUTPUT_CENTERLINE_ATTRIBUTES,
    OUTPUT_CENTERLINE_BRANCHES,
    OUTPUT_BIFURCATION_VECTORS,
    OUTPUT_SPLIT_CENTERLINE_1,
    OUTPUT_SPLIT_CENTERLINE_2,
    OUTPUT_SPLIT_CENTERLINE_3,
    OUTPUT_SPLIT_CENTERLINE_4,
    OUTPUT_SPLIT_CENTERLINE_5,
    NUMBER_OF_OUTPUTS
  } OUTPUT_TYPE;

  typedef enum {
    EXTRACT_BRANCHES,
    SPLIT,
    BIFURCATION_VECTORS,
  } OP_TYPE;

  //!
  void Update();
  void ExtractBranches();
  void SplitCenterline();
  void ComputeBifurcationVectors();
  int GetOpType() const { return m_op; }
  void SetOpType(int val) { m_op = val; }

private:
  /**
  */
  VesselBifurcationsProcessor();
  //!
  ~VesselBifurcationsProcessor();
  //!
  bool m_branchesExtracted;
  int m_op;
};

#endif //_coreVesselBifurcationsProcessor_H
