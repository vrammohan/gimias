/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _NewClinicalReportTableDialog_H
#define _NewClinicalReportTableDialog_H

#include "NewClinicalReportTableDialogUI.h"
#include "ClinicalReportProcessor.h"
#include "coreBaseWindow.h"
#include "coreWorkingAreaFactory.h"
#include <string>

namespace Core {
namespace Widgets {

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 25 Oct 2010
*/

#define wxID_NewClinicalReportTableDialog wxID("wxID_NewClinicalReportTableDialog")

  class NewClinicalReportTableDialog : public NewClinicalReportTableDialogUI,
                                       public Core::BaseWindow {
    // OPERATIONS
  public:
    coreDefineBaseWindowFactory(NewClinicalReportTableDialog)

        ////!
        // class Factory : public Core::BaseWindowFactory
        //{
        // public:
        //	coreDeclareSmartPointerTypesMacro(Factory,BaseWindowFactory)
        //	coreFactorylessNewMacro(Factory) \
	//	coreClassNameMacro(Factory)\
	//	static BaseWindowFactory::Pointer NewBase(void) \
	//	{ \
	//	    Pointer p(New()); \
	//		return p.GetPointer(); \
	//	} \
	//	virtual Core::BaseWindow* CreateWindow( ) \
	//	{ \
	//		NewClinicalReportTableDialog* window = new NewClinicalReportTableDialog( GetParent(), GetWindowId( ), GetPosition( ), GetSize( ) );\
	//		window->SetNewClinicalReportTableDialog( typeid( NewClinicalReportTableDialog ).name( ) );\
	//		window->SetTable( gridAneurysmDescriptors );
        //		if ( !GetWindowName( ).empty( ) ){ \
	//			wxWindowBase* windowBase = window; \
	//			windowBase->SetName( GetWindowName( ) ); \
	//		} \
	//		return window; \
	//	} \
	//	std::string GetWindowClassname() const \
	//	{ \
	//		return typeid( NewClinicalReportTableDialog ).name( ); \
	//	} \

        //	GridAneurysmDescriptors gridAneurysmDescriptors;
        //}; \

        //!
        NewClinicalReportTableDialog(wxWindow *parent, wxWindowID id = wxID_ANY,
                                     const wxPoint &pos = wxDefaultPosition,
                                     const wxSize &size = wxDefaultSize,
                                     long style = wxAUI_TB_DEFAULT_STYLE,
                                     const wxString &name = wxPanelNameStr);

    //!
    virtual Core::BaseProcessor::Pointer GetProcessor();

    //!
    void SetTable(wxGrid *grid);

    //!
    void SetTable(std::vector< std::vector< std::string > > table);

    //!
    std::string GetTitle();

    //!
    void LoadReports();

    //!
    void AddToReport();

    //!
    void OnShow(wxShowEvent &event);

    //!
    void SetProcessor(ClinicalReportProcessor::Pointer processor);

    // ATTRIBUTES
  public:
    std::string m_ReportName;
    std::string m_ReportAuthors;
    std::string m_ReportPath;

  private:
    DECLARE_EVENT_TABLE()

    std::vector< std::vector< std::string > > ReadFromFile(std::string filename);

    //!
    void OnTextTitle(wxCommandEvent &event);

    //!
    void OnChangeReport(wxCommandEvent &event);

    //!
    void LoadReportInfo(std::string reportName);

    //!
    void OnAddReport(wxCommandEvent &event); // wxGlade: <event_handler>

    //!
    void OnEditReport(wxCommandEvent &event); // wxGlade: <event_handler>

    // ATTRIBUTES
  private:
    std::string m_Title;

    std::string m_BaseDir;

    //! processor
    ClinicalReportProcessor::Pointer m_Processor;
  };

} // namespace Widgets
} // namespace Core

#endif //_NewClinicalReportTableDialog_H
