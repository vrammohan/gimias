/*
 *  msvWxVTKButtons.cxx porting on wxWidgets of msvQtVTKButtons.cxx
 *
 *
 *  Created by Luigi Carotenuto on 9/27/12.
 *  Copyright 2012 Universitat Pompeu Farbar. All rights reserved.
 *
 */

/*
 *  msvQVTKButtons.cpp
 *  VTKButtons
 *
 *  Created by Roberto Mucci on 13/01/12.
 *  Copyright 2012 B3C. All rights reserved.
 *
 *  See License at: http://tiny.cc/QXJ4D
 *
 */

#include "msvWxVTKButtons.h"
#include <wx/image.h>
#include <wx/dir.h>

#include "msvWxVTKAnimate.h"

#include <vtkSmartPointer.h>
#include <vtkAlgorithmOutput.h>
#include <vtkPNGReader.h>
#include <vtkPNGWriter.h>

#include <vtkTextProperty.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>

#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRendererCollection.h>
#include <vtkButtonWidget.h>
#include <vtkTexturedButtonRepresentation.h>
#include <vtkTexturedButtonRepresentation2D.h>
#include <vtkBalloonRepresentation.h>
#include <vtkCommand.h>

#include <vtkEllipticalButtonSource.h>
#include <vtkTexturedButtonRepresentation.h>

#include <vtkRenderWindow.h>
#include <vtkWindowToImageFilter.h>
#include <vtkDataSetMapper.h>
#include <vtkDataSet.h>
#include <vtkCamera.h>
#include <vtkPointData.h>

#include <vtkImageHistogramStatistics.h>
#include <vtkImageSliceMapper.h>
#include <vtkImageSlice.h>
#include <vtkImageProperty.h>
#include <vtkImageResliceMapper.h>

#include "itksys/SystemTools.hxx"

/*
//Create event types to simulate signal/slot mechanism
BEGIN_DECLARE_EVENT_TYPES()
 DECLARE_EVENT_TYPE(wxEVT_SHOW_TOOLTIP, -1)
 DECLARE_EVENT_TYPE(wxEVT_END_ANIMATION, -1);
END_DECLARE_EVENT_TYPES()

DEFINE_EVENT_TYPE(wxEVT_SHOW_TOOLTIP)
DEFINE_EVENT_TYPE(wxEVT_END_ANIMATION)
*/

#define VTK_CREATE(type, name)                                                           \
  vtkSmartPointer< type > name = vtkSmartPointer< type >::New()

#define WX_UNUSED(arg) (void) arg; // just a copy and paste from Q_UNUSED

// Callback responding to vtkCommand::StateChangedEvent
class vtkButtonCallback : public vtkCommand {
public:
  static vtkButtonCallback *New() { return new vtkButtonCallback; }

  virtual void Execute(vtkObject *caller, unsigned long, void *) {

    toolButton->showInformation(true);
  }

  void setFlyTo(bool fly) { flyTo = fly; }

  vtkButtonCallback() : toolButton(NULL), renderer(0), flyTo(true) {}
  msvWxVTKButtons *toolButton;
  vtkRenderer *renderer;
  bool flyTo;
};

// Callback responding to vtkCommand::HighlightEvent
class vtkButtonHighLightCallback : public vtkCommand {
public:
  static vtkButtonHighLightCallback *New() { return new vtkButtonHighLightCallback; }

  virtual void Execute(vtkObject *caller, unsigned long, void *) {
    vtkTexturedButtonRepresentation2D *rep =
        reinterpret_cast< vtkTexturedButtonRepresentation2D * >(caller);
    int highlightState = rep->GetHighlightState();

    if (highlightState == vtkButtonRepresentation::HighlightHovering &&
        previousHighlightState == vtkButtonRepresentation::HighlightNormal) {
      // show tooltip (not if previous state was selecting
      toolButton->showTooltip(true);
    } else if (highlightState == vtkButtonRepresentation::HighlightNormal) {
      // hide tooltip
      toolButton->showTooltip(false);
    }
    previousHighlightState = highlightState;
  }

  vtkButtonHighLightCallback() : toolButton(NULL), previousHighlightState(0) {}

  msvWxVTKButtons *toolButton;
  int previousHighlightState;
};

msvWxVTKButtons::msvWxVTKButtons(wxCallerWidget *parent, unsigned int id)
    : m_ShowButton(false), m_ShowLabel(true), m_FlyTo(true), m_OnCenter(false),
      m_ButtonWidget(NULL) {
  VTK_CREATE(vtkTexturedButtonRepresentation2D, rep);
  rep->SetNumberOfStates(1);

  m_caller = parent;
  m_id = id;
  m_prevFName = "";
  m_buttonCallback = vtkButtonCallback::New();
  m_buttonCallback->toolButton = this;
  m_IconSize = 16;

  m_highlightCallback = vtkButtonHighLightCallback::New();
  m_highlightCallback->toolButton = this;

  m_ButtonWidget = NULL;
  button()->SetRepresentation(rep);
  button()->AddObserver(vtkCommand::StateChangedEvent, m_buttonCallback);
  rep->AddObserver(vtkCommand::HighlightEvent, m_highlightCallback);

  m_AnimateCamera = new msvWxVTKAnimate();
}

msvWxVTKButtons::~msvWxVTKButtons() {
  button()->Delete();
  delete m_AnimateCamera;
  m_AnimateCamera = NULL;
  m_buttonCallback->Delete();
  m_highlightCallback->Delete();
}

void msvWxVTKButtons::setShowButton(bool show) { m_ShowButton = show; }

bool msvWxVTKButtons::showButton() const { return m_ShowButton; }

void msvWxVTKButtons::setShowLabel(bool show) { m_ShowLabel = show; }

bool msvWxVTKButtons::showLabel() const { return m_ShowLabel; }

bool msvWxVTKButtons::flyTo() const { return m_FlyTo; }

void msvWxVTKButtons::setOnCenter(bool onCenter) { m_OnCenter = onCenter; }

bool msvWxVTKButtons::onCenter() const { return m_OnCenter; }

void msvWxVTKButtons::setLabel(wxString text) { m_Label = text; }

vtkButtonWidget *msvWxVTKButtons::button() {
  if (m_ButtonWidget == NULL) {
    m_ButtonWidget = vtkButtonWidget::New();
  }
  return m_ButtonWidget;
}

void msvWxVTKButtons::setCurrentRenderer(vtkRenderer *renderer) {
  if (renderer) {
    button()->SetInteractor(renderer->GetRenderWindow()->GetInteractor());
    button()->SetCurrentRenderer(renderer); // to check
    m_buttonCallback->renderer = renderer;
    button()->EnabledOn();
    m_AnimateCamera->setCurrentRenderer(renderer);
  } else {
    button()->SetInteractor(NULL);
    button()->SetCurrentRenderer(NULL); // to check
    m_buttonCallback->renderer = NULL;
    button()->EnabledOff();
    m_AnimateCamera->setCurrentRenderer(NULL);
  }
}

vtkRenderer *msvWxVTKButtons::getCurrentRenderer() {
  return button()->GetCurrentRenderer();
}

void msvWxVTKButtons::setIconFileName(wxString iconFileName) {
  m_IconFileName = iconFileName;
  VTK_CREATE(vtkPNGReader, pngReader);
  pngReader->SetFileName(iconFileName.mb_str());
  pngReader->Update();
  vtkTexturedButtonRepresentation2D *rep =
      static_cast< vtkTexturedButtonRepresentation2D * >(button()->GetRepresentation());
  rep->SetButtonTexture(0, pngReader->GetOutput());
  int size[2];
  size[0] = m_IconSize;
  size[1] = m_IconSize;
  rep->GetBalloon()->SetImageSize(size);

  update();
}

void msvWxVTKButtons::setIconSize(int newsize) {
  m_IconSize = newsize;
  vtkTexturedButtonRepresentation2D *rep =
      static_cast< vtkTexturedButtonRepresentation2D * >(button()->GetRepresentation());
  int size[2];
  size[0] = m_IconSize;
  size[1] = m_IconSize;
  rep->GetBalloon()->SetImageSize(size);
}

void msvWxVTKButtons::setBounds(double b[6]) {
  m_AnimateCamera->setBounds(b);
  update();
}

void msvWxVTKButtons::calculatePosition() {
  // modify position of the vtkButton
  double bds[3];
  if (m_OnCenter) {
    bds[0] = (m_AnimateCamera->getBounds()[1] + m_AnimateCamera->getBounds()[0]) * .5;
    bds[1] = (m_AnimateCamera->getBounds()[3] + m_AnimateCamera->getBounds()[2]) * .5;
    bds[2] = (m_AnimateCamera->getBounds()[5] + m_AnimateCamera->getBounds()[4]) * .5;
  } else {
    // on the corner of the bounding box of the VME.
    bds[0] = m_AnimateCamera->getBounds()[0];
    bds[1] = m_AnimateCamera->getBounds()[2];
    bds[2] = m_AnimateCamera->getBounds()[4];
  }
  int size[2];
  size[0] = m_IconSize;
  size[1] = m_IconSize;
  vtkTexturedButtonRepresentation2D *rep =
      static_cast< vtkTexturedButtonRepresentation2D * >(button()->GetRepresentation());

  rep->PlaceWidget(bds, size);
  rep->Modified();
  button()->SetRepresentation(rep);
}

void msvWxVTKButtons::update() {
  calculatePosition();
  vtkTexturedButtonRepresentation2D *rep =
      reinterpret_cast< vtkTexturedButtonRepresentation2D * >(
          button()->GetRepresentation());

  if (m_ShowLabel) {
    // Add a label to the button and change its text property
    rep->GetBalloon()->SetBalloonText(m_Label.ToAscii());
    vtkTextProperty *textProp = rep->GetBalloon()->GetTextProperty();
    rep->GetBalloon()->SetPadding(2);
    textProp->SetFontSize(13);
    textProp->BoldOff();
    // textProp->SetColor(0.9,0.9,0.9);

    // Set label position
    rep->GetBalloon()->SetBalloonLayoutToImageLeft();

    // This method allows to set the label's background opacity
    rep->GetBalloon()->GetFrameProperty()->SetOpacity(0.65);
  } else {
    rep->GetBalloon()->SetBalloonText("");
  }

  if (m_ShowButton) {
    button()->GetRepresentation()->SetVisibility(true);
    button()->EnabledOn();
  } else {
    button()->GetRepresentation()->SetVisibility(false);
    button()->EnabledOff();
  }

  if (m_buttonCallback) {
    m_buttonCallback->flyTo = m_FlyTo;
  }
}

void msvWxVTKButtons::setFlyTo(bool active) {
  m_FlyTo = active;
  update();
}

void msvWxVTKButtons::setDescription(wxString text) { m_Description = text; }

wxString msvWxVTKButtons::getDescription() const { return m_Description; }

bool msvWxVTKButtons::buildPreview(vtkSmartPointer< vtkDataSet > data, int width,
                                   int height) {
  bool result = false;

  if (itksys::SystemTools::FileExists(m_prevFName)) {
    return false;
  }

  if (data) {
    // offscreen rendering
    VTK_CREATE(vtkRenderWindow, window);
    VTK_CREATE(vtkRenderer, renderer);
    window->OffScreenRenderingOn();
    window->AddRenderer(renderer);
    // There's a bug with 80,80 with vtkPolyData
    window->SetSize(160, 160);

    vtkImageData *image = vtkImageData::SafeDownCast(data);
    if (image) {
      VTK_CREATE(vtkImageHistogramStatistics, statistics);
      statistics->SetInput(data);
      statistics->GenerateHistogramImageOff();
      statistics->Update();

      // Use the autorange feature to get a better image range
      double autorange[2];
      statistics->GetAutoRange(autorange);

      VTK_CREATE(vtkImageResliceMapper, imageMapper);
      imageMapper->SetInput(image);
      imageMapper->SliceAtFocalPointOn();
      imageMapper->SliceFacesCameraOn();

      VTK_CREATE(vtkImageSlice, imageSlice);
      imageSlice->SetMapper(imageMapper);
      imageSlice->GetProperty()->SetInterpolationTypeToCubic();
      renderer->AddViewProp(imageSlice);
      renderer->ResetCamera();

      vtkCamera *camera = renderer->GetActiveCamera();
      camera->Zoom(2);

      imageSlice->GetProperty()->SetColorWindow(autorange[1] - autorange[0]);
      imageSlice->GetProperty()->SetColorLevel(0.5 * (autorange[0] + autorange[1]));
    } else {
      VTK_CREATE(vtkDataSetMapper, mapper);
      VTK_CREATE(vtkActor, actor);
      mapper->SetInput(data);
      mapper->Update();
      actor->SetMapper(mapper);

      renderer->AddActor(actor);
    }

    window->Render();

    // Extract the image from the 'hidden' renderer
    VTK_CREATE(vtkWindowToImageFilter, previewer);
    previewer->SetInput(window);
    previewer->Modified();
    previewer->Update();

    VTK_CREATE(vtkPNGWriter, pngWriter);
    pngWriter->SetFileName(m_prevFName.c_str());
    pngWriter->SetInput(previewer->GetOutput());
    pngWriter->Write();
    result = itksys::SystemTools::FileExists(m_prevFName);

    if (result) {
      wxImage image;
      image.LoadFile(m_prevFName);
      image.Scale(width, height).SaveFile(m_prevFName);
    }
  }
  return result;
}

wxString msvWxVTKButtons::getPreviewFName() { return m_prevFName; }

void msvWxVTKButtons::setPreviewFName(const wxString &fname) { m_prevFName = fname; }

void msvWxVTKButtons::showTooltip(bool value) { m_caller->ShowTooltip(this, value); }

void msvWxVTKButtons::showInformation(bool value) {
  m_caller->ShowInformation(this, value);
}

bool msvWxVTKButtons::beginAnimation() {
  return m_caller->BeginAnimation(m_AnimateCamera->isFlight(), m_id);
}

void msvWxVTKButtons::endAnimation() {
  m_caller->EndAnimation(m_AnimateCamera->isFlight(), m_id);
}

void msvWxVTKButtons::Animate() { beginAnimation(); }

msvWxVTKAnimate *msvWxVTKButtons::getAnimateCamera() { return m_AnimateCamera; }
