/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreInfoFrame_H
#define coreInfoFrame_H

#include "wx/frame.h"
#include "wx/stattext.h"
#include "wx/panel.h"
#include "wx/utils.h"

/**
 * \brief New InfoFrame based on wxWIDGETS wxInfoFrame
 *
 * \author Alberto Biancardi
 * \date 21 Nov 2013
 * \ingroup gmWidgets
 */
class coreInfoFrame : public wxFrame {

public:
  coreInfoFrame(wxWindow *parent, const wxString &message = _U("--..--"));

  void SetMessage(wxString message);

private:
  DECLARE_NO_COPY_CLASS(coreInfoFrame)

  wxPanel *m_panel;
  wxStaticText *m_text;
};

#endif // coreInfoFrame_H