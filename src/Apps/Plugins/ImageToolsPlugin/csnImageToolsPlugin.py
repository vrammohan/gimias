# Used to configure imageToolsPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

imageToolsPlugin = GimiasPluginProject("ImageToolsPlugin", api)

projects = [
    gmCore
]
imageToolsPlugin.AddProjects(projects)

imageToolsPlugin.AddSources(["*.cxx", "*.h"])
imageToolsPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
imageToolsPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "ProcessingToolsWidget",
  "SetSpacingWidget",
  "PointBasedImageAlignementWidget",
  "JoinImagesWidget",
  "ChangeDirectionWidget",
  "ImageContrastWidget"
  ]
imageToolsPlugin.AddWidgetModules(widgetModules, _useQt = 0)

imageToolsPlugin.SetPrecompiledHeader("ImageToolsPluginPCH.h")

imageToolsPlugin.AddFilesToInstall(imageToolsPlugin.Glob("resource"), "resource")
