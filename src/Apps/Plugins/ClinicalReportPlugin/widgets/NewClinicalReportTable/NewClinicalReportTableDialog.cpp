/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "NewClinicalReportTableDialog.h"
#include "ClinicalReportPluginUtilities.h"

// core
#include "coreSettings.h"
#include "coreDirectory.h"

// boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

// xml
#include "tinyxml.h"

#include <fstream>

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::NewClinicalReportTableDialog,
                  NewClinicalReportTableDialogUI)
END_EVENT_TABLE();

NewClinicalReportTableDialog::NewClinicalReportTableDialog(wxWindow *parent,
                                                           wxWindowID id,
                                                           const wxPoint &pos,
                                                           const wxSize &size, long style,
                                                           const wxString &name)
    : NewClinicalReportTableDialogUI(parent, id, "Clinical Report Table",
                                     wxPoint(280, 180)) {
  try {
    Connect(this->GetId(), wxEVT_SHOW,
            wxShowEventHandler(NewClinicalReportTableDialog::OnShow));
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      "NewClinicalReportTableDialog::NewClinicalReportTableDialog");
}

Core::BaseProcessor::Pointer Core::Widgets::NewClinicalReportTableDialog::GetProcessor() {
  return NULL;
}

void NewClinicalReportTableDialog::OnShow(wxShowEvent &event) {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string reportsFolder =
      settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  std::string filename = reportsFolder + Core::IO::SlashChar + "tableToFile.txt";

  if (event.GetShow()) {
    std::vector< std::vector< std::string > > table = ReadFromFile(filename);
    this->LoadReports();
    this->SetTable(table);

  } else {
    // Delete file
    remove(path(filename));

    // If the user clicked "Add"
    if (this->GetReturnCode() == wxID_OK) {
      // Add table to clinical report
      this->AddToReport();
    }
  }
}

std::vector< std::vector< std::string > >
NewClinicalReportTableDialog::ReadFromFile(std::string filename) {
  std::vector< std::vector< std::string > > table;

  std::ifstream file;
  file.open(filename.c_str());

  std::string line;
  while (getline(file, line)) {
    std::stringstream ss(line);
    char delim = ',';
    std::string token;
    std::vector< std::string > vLine;
    while (getline(ss, token, delim)) {
      vLine.push_back(token);
    }
    table.push_back(vLine);
  }

  return table;
}

void NewClinicalReportTableDialog::SetTable(wxGrid *grid) {
  wxGridTableBase *table = grid->GetTable();

  int nCols = grid->GetNumberCols();
  int nRows = grid->GetNumberRows();

  m_Grid->DeleteCols(0, m_Grid->GetNumberCols(), true);
  m_Grid->DeleteRows(0, m_Grid->GetNumberRows(), true);

  for (int j = 0; j < nCols; j++) {
    if (j >= m_Grid->GetNumberCols()) {
      m_Grid->AppendCols();
    }

    m_Grid->SetColLabelValue(j, grid->GetColLabelValue(j));
  }

  for (int i = 0; i < nRows; i++) {
    if (i >= m_Grid->GetNumberRows()) {
      m_Grid->AppendRows();
    }

    for (int j = 0; j < nCols; j++) {
      if (j >= m_Grid->GetNumberCols()) {
        m_Grid->AppendCols();
      }

      m_Grid->SetCellValue(i, j, grid->GetCellValue(i, j));
    }
  }

  // Cast a resize event
  wxSizeEvent resEventGlobal(this->GetSize(), this->GetId());
  resEventGlobal.SetEventObject(this);
  this->GetEventHandler()->ProcessEvent(resEventGlobal);
}

void NewClinicalReportTableDialog::SetTable(
    std::vector< std::vector< std::string > > table) {
  int nCols = 0;
  int nRows = 0;

  m_Grid->DeleteCols(0, m_Grid->GetNumberCols(), true);
  m_Grid->DeleteRows(0, m_Grid->GetNumberRows(), true);

  nRows = int(table.size());
  nCols = int(nRows > 0 ? table[0].size() : 0);

  for (int j = 0; j < nCols; j++) {
    if (j >= m_Grid->GetNumberCols()) {
      m_Grid->AppendCols();
    }

    m_Grid->SetColLabelValue(j, table[0][j]);
  }

  for (int i = 1; i < nRows; i++) {
    if (i - 1 >= m_Grid->GetNumberRows()) {
      m_Grid->AppendRows();
    }

    for (int j = 0; j < nCols; j++) {
      if (j >= m_Grid->GetNumberCols()) {
        m_Grid->AppendCols();
      }

      m_Grid->SetCellValue(i - 1, j, table[i][j]);
    }
  }

  // Cast a resize event
  wxSizeEvent resEventGlobal(this->GetSize(), this->GetId());
  resEventGlobal.SetEventObject(this);
  this->GetEventHandler()->ProcessEvent(resEventGlobal);
}

std::string NewClinicalReportTableDialog::GetTitle() { return m_Title; }

void NewClinicalReportTableDialog::OnTextTitle(wxCommandEvent &event) {
  m_Title = m_TitleTxt->GetValue();
}

void NewClinicalReportTableDialog::LoadReports() {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  m_BaseDir = settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  Core::IO::Directory::Pointer reportsDir = Core::IO::Directory::New();
  reportsDir->SetDirNameFullPath(m_BaseDir);

  // find subdirectories containing reports
  std::vector< std::string > subdirectories;

  try {
    subdirectories = ClinicalReportPluginUtilities::GetReports(m_BaseDir, reportsDir);
  } catch (...) {
    return;
  }

  m_ListReports->Clear();

  unsigned int lastSelectedPos = 0;
  std::string lastSelected = ClinicalReportPluginUtilities::GetLastSelectedFromConfig();

  for (int i = 0; i < subdirectories.size(); i++) {
    m_ListReports->Append(subdirectories[i]);

    if (lastSelected == subdirectories[i]) {
      lastSelectedPos = i;
    }
  }

  for (int i = int(subdirectories.size()) - 1; i >= 0; i--) {
    LoadReportInfo(subdirectories[i]);
  }

  if (lastSelectedPos != 0)
    LoadReportInfo(lastSelected);
  if (m_ListReports->GetCount() > lastSelectedPos)
    m_ListReports->Select(lastSelectedPos);
}

void NewClinicalReportTableDialog::LoadReportInfo(std::string reportName) {
  TiXmlElement *ClinicalReport;
  std::string file = m_BaseDir + Core::IO::SlashChar + reportName + Core::IO::SlashChar +
                     reportName + ".xml";

  try {
    ClinicalReport = ClinicalReportPluginUtilities::LoadReportXML(file);
  } catch (...) {
    return;
  }

  m_ReportName = ClinicalReport->Attribute("name");
  ;
  m_ReportAuthors = ClinicalReport->Attribute("authors");
  ;
  m_ReportPath = file;

  return;
}

void NewClinicalReportTableDialog::OnChangeReport(wxCommandEvent &event) {
  m_ReportName = std::string(m_ListReports->GetStringSelection().mb_str());
  ClinicalReportPluginUtilities::SetLastSelectedToConfig(m_ReportName);
  LoadReportInfo(m_ReportName);
}

void NewClinicalReportTableDialog::AddToReport() {
  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);

  TiXmlElement *ClinicalReport;

  doc = TiXmlDocument(m_ReportPath);
  if (!doc.LoadFile())
    return;

  ClinicalReport = hDoc.FirstChildElement().Element();
  if (!ClinicalReport)
    return;

  int numberOfElements = 0;

  for (TiXmlElement *elem = ClinicalReport->FirstChildElement(); elem;
       elem = elem->NextSiblingElement()) {
    numberOfElements++;
  }

  TiXmlElement *Element = new TiXmlElement("Element");

  Element->SetAttribute("position", numberOfElements + 1);
  Element->SetAttribute("selected", "1");
  Element->SetAttribute("type", "table");
  Element->SetAttribute("description", m_CommentsTxt->GetValue());
  Element->SetAttribute("title", m_TitleTxt->GetValue());

  std::string header = "";

  for (int j = 0; j < m_Grid->GetNumberCols(); j++) {
    header += m_Grid->GetColLabelValue(j);
    if (j != m_Grid->GetNumberCols() - 1)
      header += ";";
  }

  header += ":";

  TiXmlText *TextHeader = new TiXmlText(header);
  Element->LinkEndChild(TextHeader);

  for (int i = 0; i < m_Grid->GetNumberRows(); i++) {
    std::string line = "";
    for (int j = 0; j < m_Grid->GetNumberCols(); j++) {
      line += m_Grid->GetCellValue(i, j);
      if (j != m_Grid->GetNumberCols() - 1)
        line += ";";
    }

    line += ":";

    TiXmlText *TextLine = new TiXmlText(line);
    Element->LinkEndChild(TextLine);
  }
  ClinicalReport->LinkEndChild(Element);

  doc.SaveFile(m_ReportPath);
}

void NewClinicalReportTableDialog::OnAddReport(wxCommandEvent &event) {
  ClinicalReportPluginUtilities::OpenClinicalReportDesigner(m_Processor, true);
  LoadReports();
}

void NewClinicalReportTableDialog::OnEditReport(wxCommandEvent &event) {
  ClinicalReportPluginUtilities::OpenClinicalReportDesigner(m_Processor);
}

void NewClinicalReportTableDialog::SetProcessor(
    ClinicalReportProcessor::Pointer processor) {
  m_Processor = processor;
}
