// -*- C++ -*- generated by wxGlade 0.6.3 on Mon Dec 14 14:28:54 2009

#include "AhePluginConfigurationPanelWidgetUI.h"

/**
 *  Class constructor.
 *  Creates and configures GUI panel attributes.
 */
AhePluginConfigurationPanelWidgetUI::AhePluginConfigurationPanelWidgetUI(
    wxWindow *parent, int id, const wxPoint &pos, const wxSize &size, long style)
    : wxScrolledWindow(parent, id, pos, size, wxTAB_TRAVERSAL) {
  // begin wxGlade:
  // AhePluginConfigurationPanelWidgetUI::AhePluginConfigurationPanelWidgetUI
  sizer_1_staticbox = new wxStaticBox(this, -1, wxT("AHE nodes"));
  const wxString *m_listNodes_choices = NULL;
  m_listNodes = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0,
                              m_listNodes_choices, 0);
  m_btnAdd = new wxButton(this, wxID_BTN_ADD, wxT("Add"));
  m_btnRemove = new wxButton(this, wxID_BTN_REMOVE, wxT("Remove"));
  m_btnEdit = new wxButton(this, wxID_BTN_EDIT, wxT("Edit"));
  set_properties();
  do_layout();
  // end wxGlade
}

BEGIN_EVENT_TABLE(AhePluginConfigurationPanelWidgetUI, wxScrolledWindow)
// begin wxGlade: AhePluginConfigurationPanelWidgetUI::event_table
EVT_BUTTON(wxID_BTN_ADD, AhePluginConfigurationPanelWidgetUI::OnAdd)
EVT_BUTTON(wxID_BTN_REMOVE, AhePluginConfigurationPanelWidgetUI::OnRemove)
EVT_BUTTON(wxID_BTN_EDIT, AhePluginConfigurationPanelWidgetUI::OnEdit)
EVT_LISTBOX_DCLICK(wxID_ANY, AhePluginConfigurationPanelWidgetUI::OnListDClick)
// end wxGlade
END_EVENT_TABLE();

/**
 *  Add AHE node event handler. Virtual function.
 */
void AhePluginConfigurationPanelWidgetUI::OnAdd(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(
      wxT("Event handler (AhePluginConfigurationPanelWidgetUI::OnAdd) not implemented "
          "yet")); // notify the user that he hasn't implemented the event handler yet
}

/**
 *  Double-click AHE node list event handler. Virtual function.
 */
void AhePluginConfigurationPanelWidgetUI::OnListDClick(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT(
      "Event handler (AhePluginConfigurationPanelWidgetUI::OnListDClick) not implemented "
      "yet")); // notify the user that he hasn't implemented the event handler yet
}

/**
 *  Remove AHE node event handler. Virtual function.
 */
void AhePluginConfigurationPanelWidgetUI::OnRemove(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(
      wxT("Event handler (AhePluginConfigurationPanelWidgetUI::OnRemove) not implemented "
          "yet")); // notify the user that he hasn't implemented the event handler yet
}

/**
 *  Edit AHE node event handler. Virtual function.
 */
void AhePluginConfigurationPanelWidgetUI::OnEdit(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(
      wxT("Event handler (AhePluginConfigurationPanelWidgetUI::OnEdit) not implemented "
          "yet")); // notify the user that he hasn't implemented the event handler yet
}

/**
 *   Set class attributes properties.
 */
void AhePluginConfigurationPanelWidgetUI::set_properties() {
  // begin wxGlade: AhePluginConfigurationPanelWidgetUI::set_properties
  SetScrollRate(10, 10);
  // end wxGlade
}

/**
 *  Layout manager method.
 */
void AhePluginConfigurationPanelWidgetUI::do_layout() {
  // begin wxGlade: AhePluginConfigurationPanelWidgetUI::do_layout
  wxBoxSizer *GlobalSizer = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *sizer_1 = new wxStaticBoxSizer(sizer_1_staticbox, wxVERTICAL);
  wxBoxSizer *sizer_2 = new wxBoxSizer(wxHORIZONTAL);
  sizer_1->Add(m_listNodes, 1, wxEXPAND, 0);
  sizer_2->Add(m_btnAdd, 0, wxALL, 5);
  sizer_2->Add(m_btnRemove, 0, wxALL, 5);
  sizer_2->Add(m_btnEdit, 0, wxALL, 5);
  sizer_1->Add(sizer_2, 0, wxALIGN_RIGHT, 0);
  GlobalSizer->Add(sizer_1, 1, wxEXPAND, 0);
  SetSizer(GlobalSizer);
  GlobalSizer->Fit(this);
  // end wxGlade
}
