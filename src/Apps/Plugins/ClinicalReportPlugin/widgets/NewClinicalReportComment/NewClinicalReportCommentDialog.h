/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _NewClinicalReportCommentDialog_H
#define _NewClinicalReportCommentDialog_H

#include "NewClinicalReportCommentDialogUI.h"
#include "coreBaseWindow.h"
#include <string>

#include "ClinicalReportProcessor.h"

// xml
#include "tinyxml.h"

namespace Core {
namespace Widgets {

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 15 Nov 2010
*/

#define wxID_NewClinicalReportCommentDialog wxID("wxID_NewClinicalReportCommentDialog")

  class NewClinicalReportCommentDialog : public NewClinicalReportCommentDialogUI,
                                         public Core::BaseWindow {
    // OPERATIONS
  public:
    //!
    NewClinicalReportCommentDialog();

    //!
    ~NewClinicalReportCommentDialog();

    //!
    virtual Core::BaseProcessor::Pointer GetProcessor();

    //!
    std::string GetComments();

    //!
    void LoadReports();

    //!
    void OnChangeReport(wxCommandEvent &event);

    //!
    void SetProcessor(ClinicalReportProcessor::Pointer processor);

    // ATTRIBUTES
  public:
    std::string m_ReportName;
    std::string m_ReportAuthors;
    std::string m_ReportPath;

  private:
    DECLARE_EVENT_TABLE()

    //!
    void OnTextModified(wxCommandEvent &event);

    //!
    void LoadReportInfo(std::string reportName);

    //!
    void OnAddReport(wxCommandEvent &event);

    //!
    void OnEditReport(wxCommandEvent &event);

    // ATTRIBUTES
  private:
    std::string m_BaseDir;

    //! processor
    ClinicalReportProcessor::Pointer m_Processor;
  };

} // namespace Widgets
} // namespace Core

#endif //_NewClinicalReportCommentDialog_H
