/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef wxNavigationDialog_H
#define wxNavigationDialog_H

// wxWidgets
#include <wx/dialog.h>
#include <wx/wx.h>

/**
\brief Dialog to navigate

\ingroup msvKPlugin
\author Xavi Planes
\date Nov 2010
*/
class wxNavigationDialog : public wxDialog {

public:
  //!
  wxNavigationDialog(wxWindow *parent, wxWindowID id, wxBitmap &bitmap,
                     const wxPoint &pos = wxDefaultPosition,
                     const wxSize &size = wxDefaultSize, long style = wxBORDER_NONE,
                     const wxString &name = "dialogBox");

  ~wxNavigationDialog();

private:
  // DECLARE_EVENT_TABLE()

private:
  //!
  wxBitmapButton *m_bmpButton;
};

#endif // wxNavigationDialog_H