/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "vmtkProcessorCollective.h"

#include "vtkvmtkVesselnessMeasureImageFilter.h"
#include "vtkSmartPointer.h"

vmtkProcessorCollective::vmtkProcessorCollective() {
  // Just to load vtkvmtkSegmentation.dll. Otherwise it crashen when calling it
  vtkSmartPointer< vtkvmtkVesselnessMeasureImageFilter > filter;
  filter = vtkSmartPointer< vtkvmtkVesselnessMeasureImageFilter >::New();
}
