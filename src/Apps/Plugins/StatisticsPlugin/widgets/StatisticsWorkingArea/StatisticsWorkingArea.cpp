/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "StatisticsWorkingArea.h"

#include "blMitkUnicode.h"
#include "blTextUtils.h"

#include "coreWxMitkCoreMainWindow.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreDirectory.h"
#include "coreSettings.h"
#include "coreKernel.h"
#include "corePluginTab.h"
#include "coreMainMenu.h"
#include "coreToolbarIO.h"
#include "corePluginTab.h"
#include "corePluginTabFactory.h"
#include "coreProcessorManager.h"

#include <wx/event.h>
#include <wx/process.h>
#include <wx/dir.h>
#include <wx/zipstrm.h>
#include <wx/wfstream.h>

StatisticsWorkingArea::StatisticsWorkingArea(wxWindow *parent, int id, const wxPoint &pos,
                                             const wxSize &size, long style)
    : StatisticsWorkingAreaUI(parent, id, pos, size, style) {
  UpdateWidget();
}

void StatisticsWorkingArea::UpdateWidget() {}

Core::BaseProcessor::Pointer StatisticsWorkingArea::GetProcessor() { return NULL; }

void StatisticsWorkingArea::OnInit() {}
