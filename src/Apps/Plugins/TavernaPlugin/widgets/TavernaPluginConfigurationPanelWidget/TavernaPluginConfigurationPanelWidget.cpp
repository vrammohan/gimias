/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "TavernaPluginConfigurationPanelWidget.h"
#include "coreKernel.h"
#include "coreSettings.h"
#include "itksys/SystemTools.hxx"
#include "coreDirectory.h"
#include "blTextUtils.h"
#include "TavernaPluginWorkingArea.h"

TavernaPlugin::ConfigurationPanelWidget::ConfigurationPanelWidget(
    wxWindow *parent, int id /*= wxID_ANY*/, const wxPoint &pos /*= wxDefaultPosition*/,
    const wxSize &size /*= wxDefaultSize*/, long style /* = 0*/)
    : TavernaPluginConfigurationPanelWidgetUI(parent, id, pos, size, style) {}

TavernaPlugin::ConfigurationPanelWidget::~ConfigurationPanelWidget() {
  // We don't need to destroy anything because all the child windows
  // of this wxWindow are destroyed automatically
}

void TavernaPlugin::ConfigurationPanelWidget::OnInit() {}

void TavernaPlugin::ConfigurationPanelWidget::UpdateWidget() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();
  std::string executablePath;
  if (settings->GetPluginProperty("Taverna Plugin", "TavernaPath", executablePath)) {
    m_txtTavernaPath->SetValue(executablePath);
  }

  if (executablePath.empty() ||
      !itksys::SystemTools::FileExists(executablePath.c_str())) {
    executablePath =
        GetExecutablePath("C:\\Program Files (x86)", "Taverna Workbench", "taverna.exe");
    if (executablePath.empty()) {
      executablePath =
          GetExecutablePath("C:\\Program Files", "Taverna Workbench", "taverna.exe");
    }

    if (!executablePath.empty()) {
      m_txtTavernaPath->SetValue(executablePath);
      UpdateData();
    }
  }

  std::string caption;
  if (settings->GetPluginProperty("Taverna Plugin", "TavernaCaption", caption)) {
    m_txtTavernaCaption->SetValue(caption);
  }

  // Get sub folder name to set default caption
  if (caption.empty()) {
    blTextUtils::StrSub(executablePath, "\\", "/");
    std::list< std::string > words;
    blTextUtils::ParseLine(executablePath, '/', words);

    // Last word is taverna.exe, so we need to get the previous one
    std::string appCaption;
    std::list< std::string >::iterator it = words.begin();
    while (it != words.end() && *it != "taverna.exe") {
      appCaption = *it;
      it++;
    }

    if (it != words.end() && *it == "taverna.exe") {
      m_txtTavernaCaption->SetValue(appCaption);
      UpdateData();
    }
  }
}

void TavernaPlugin::ConfigurationPanelWidget::UpdateData() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();
  settings->SetPluginProperty("Taverna Plugin", "TavernaPath",
                              m_txtTavernaPath->GetValue().ToStdString());
  settings->SetPluginProperty("Taverna Plugin", "TavernaCaption",
                              m_txtTavernaCaption->GetValue().ToStdString());

  // Reinitialize working area
  Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();
  Core::Widgets::wxMitkCoreMainWindow *mainWindow;
  mainWindow =
      dynamic_cast< Core::Widgets::wxMitkCoreMainWindow * >(gIface->GetMainWindow());
  if (!mainWindow || !mainWindow->GetCurrentPluginTab()) {
    return;
  }

  Core::Widgets::PluginTab *pluginTab =
      dynamic_cast< Core::Widgets::PluginTab * >(mainWindow->GetCurrentPluginTab());
  Core::Widgets::WorkingAreaManager *manager = pluginTab->GetWorkingAreaManager();
  std::map< int, wxWindow * > workingAreas = manager->GetWorkingAreaMap();
  std::map< int, wxWindow * >::iterator it;
  for (it = workingAreas.begin(); it != workingAreas.end(); ++it) {
    TavernaPlugin::WorkingArea *workingArea =
        dynamic_cast< TavernaPlugin::WorkingArea * >(it->second);
    if (!workingArea) {
      continue;
    }

    workingArea->OnInit();
  }
}

void TavernaPlugin::ConfigurationPanelWidget::OnBrowse(wxCommandEvent &event) {
  std::string dataPath;
  wxFileDialog *openFileDialog =
      new wxFileDialog(this, wxT("Select Taverna executable"), wxT(""), wxT(""), wxT(""),
                       wxFD_OPEN | wxFD_FILE_MUST_EXIST);

  if (openFileDialog->ShowModal() == wxID_OK) {
    m_txtTavernaPath->SetValue(openFileDialog->GetPath());
  }
}

std::string TavernaPlugin::ConfigurationPanelWidget::GetExecutablePath(
    const std::string &rootFolder, const std::string &subFolderFilter,
    const std::string &executableName) {
  std::string executablePath;

#ifdef _WIN32
  // Get all sub folders of "Program Files"
  Core::IO::Directory::Pointer programFiles = Core::IO::Directory::New();
  programFiles->SetDirNameFullPath(rootFolder);
  Core::IO::DirEntryFilter::Pointer filter = Core::IO::DirEntryFilter::New();
  filter->SetMode(Core::IO::DirEntryFilter::SubdirsOnly);
  programFiles->SetFilter(filter);
  Core::IO::FileNameList subFolders = programFiles->GetContents();

  // Find sub folder
  std::string subFolder;
  Core::IO::FileNameList::iterator it = subFolders.begin();
  while (subFolder.empty() && it != subFolders.end()) {
    if (it->find(subFolderFilter) != std::string::npos) {
      subFolder = *it;
    }
    it++;
  }

  // Find executable
  std::string executable = subFolder + "/" + executableName;
  if (itksys::SystemTools::FileExists(executable.c_str())) {
    executablePath = executable;
  }
#endif

  return executablePath;
}
