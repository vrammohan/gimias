/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SSHPluginProviderConfigurationDialog_H
#define _SSHPluginProviderConfigurationDialog_H

#include "corePropertiesConfigurationDialog.h"
#include "SshPluginNodeManager.h"

/**
Configure SSH Plugin provider using a combo box for selecting the SSH node

- Working directory: This is the remote directory where the command line plugins will be
executed
- Temporary directory: Local directory where to store local data
- Scan folder: Folder where to find remote command line plugins
- SSH node: SSH node configuration
- Remote GIMIAS versions: If remote computer has GIMIAS installed and it
    has been executed, this control will allow you to select the desired GIMIAS
    version and the fields Scan folder and Working directory will be configured
automatically.

\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/
class SSHPluginProviderConfigurationDialog
    : public Core::Widgets::PropertiesConfigurationDialog {
public:
  coreDefineBaseWindowFactory1param(SSHPluginProviderConfigurationDialog,
                                    SshPluginNodeManager::Pointer)

      SSHPluginProviderConfigurationDialog(
          SshPluginNodeManager::Pointer nodeManager, wxWindow *parent, int id,
          const wxString &title = "SSH Plugin Provider configuration",
          const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize,
          long style = wxDEFAULT_DIALOG_STYLE);

private:
  void set_properties();

  void do_layout();

  void UpdateData();

  //!
  void UpdateWidget();

  //! Create combo box to select ssh nodes
  void CreateSSHNodesCombo();

  //! Create combo box to select remote GIMIAS versions
  void UpdateGIMIASComboBox();

  //!
  void OnCmbSSHNodesChanged(wxCommandEvent &event);

  //!
  void OnCmbRemoteGIMIASChanged(wxCommandEvent &event);

  DECLARE_EVENT_TABLE();

protected:
  //!
  SshPluginNodeManager::Pointer m_NodeManager;

  //!
  wxComboBox *m_cmbSshNode;

  //!
  wxBoxSizer *m_SizerRemoteGimias;

  //!
  wxStaticText *m_LabelRemoteGimias;

  //!
  wxComboBox *m_cmbRemoteGimias;
};

#endif // _SSHPluginProviderConfigurationDialog_H
