/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _WebServicesPluginWidgetCollective_H
#define _WebServicesPluginWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"
#include "WebServerProcessor.h"

namespace WebServicesPlugin {

/**

\ingroup WebServicesPlugin
\author Xavi Planes
\date Sept 2011
*/

class WidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(WidgetCollective, Core::WidgetCollective);

private:
  //! The constructor instantiates all the widgets and registers them.
  WidgetCollective();

private:
  //!
  WebServerProcessor::Pointer m_Processor;
};

} // namespace WebServicesPlugin

#endif //_WebServicesPluginWidgetCollective_H
