/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#ifndef StatisticsMeanProcessor_H
#define StatisticsMeanProcessor_H

#include "StatisticsPluginUtilities.h"

#include "gmProcessorsWin32Header.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreCommonDataTypes.h"
#include "corePluginMacros.h"
#include "coreBaseProcessor.h"
//#include "coreProcessorFactory.h"

#include <map>
#include <vector>

#include <wx/event.h>
#include <wx/process.h>
#include <wx/dir.h>
#include <wx/zipstrm.h>
#include <wx/wfstream.h>

/**
\ingroup StatisticsPlugin
\author Albert Sanchez
\date 18 Oct 2011
*/

class StatisticsMeanProcessor : public Core::BaseProcessor {
public:
  //!
  coreProcessor(StatisticsMeanProcessor, Core::BaseProcessor);

  //!
  StatisticsMeanProcessor();

  //!
  ~StatisticsMeanProcessor();

  //!
  void Update();

private:
  //! Purposely not implemented
  StatisticsMeanProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
};

#endif // StatisticsDownloadProcessor_H
