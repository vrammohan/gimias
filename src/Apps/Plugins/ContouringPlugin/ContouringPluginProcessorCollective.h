/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ContouringPluginProcessorCollective_H
#define _ContouringPluginProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

namespace contouringPlugin {

/**
This class instantiates all processors used in the plugin and registers them.

\ingroup ContouringPlugin
*/

class ProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(ProcessorCollective, Core::SmartPointerObject);

private:
  //! The constructor instantiates all the processors and connects them.
  ProcessorCollective();

}; // class ProcessorCollective

} // namespace contouringPlugin{

#endif //_ContouringPluginProcessorCollective_H
