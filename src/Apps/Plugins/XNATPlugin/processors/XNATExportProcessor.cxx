/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATExportProcessor.h"
#include "coreDataEntityReader.h"

#include "itksys/SystemTools.hxx"

XNATExportProcessor::XNATExportProcessor() {
  SetName("XNATExportProcessor");

  BaseProcessor::SetNumberOfInputs(NUMBEROFINPUTS);
  BaseProcessor::SetNumberOfOutputs(NUMBEROFOUTPUTS);

  GetOutputPort(OUTPUT_DATA_ENTITY)->SetDataEntityType(Core::ImageTypeId);
  GetOutputPort(OUTPUT_DATA_ENTITY)->SetReuseOutput(false);
}

XNATExportProcessor::~XNATExportProcessor() {}

void XNATExportProcessor::SetParameters(XNATExportParameters params) {
  m_Params = params;
}

void XNATExportProcessor::Update() {
  std::string path = itksys::SystemTools::GetFilenamePath(m_Params.filename);

  try {

    XNAT::Core client;
    client.New(m_Params.user, m_Params.password);
    client.retrieve(m_Params.url, m_Params.filename);

    Core::IO::DataEntityReader::Pointer reader;
    reader = Core::IO::DataEntityReader::New();
    reader->SetFileName(m_Params.filename);
    reader->Update();

    UpdateOutput(OUTPUT_DATA_ENTITY, reader->GetOutputDataEntity());

  } catch (...) {

    throw;
  }

  // Remove temporal folder
  itksys::SystemTools::RemoveADirectory(path.c_str());
}
