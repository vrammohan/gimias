/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#include "SshPluginNodeManager.h"
#include "coreKernel.h"
#include "coreSettings.h"

SshPluginNodeManager::SshPluginNodeManager() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  blTagMap::Pointer properties;
  properties = settings->GetPluginProperties("SshPlugin");
  if (properties.IsNotNull()) {
    blTagMap::Iterator it;
    for (it = properties->GetIteratorBegin(); it != properties->GetIteratorEnd(); it++) {
      blTagMap::Pointer tagNode;
      if (it->second->GetValue(tagNode)) {
        sshNode::Pointer node = sshNode::New();
        node->SetHostname(tagNode->FindTagByName("hostname")->GetValueAsString());
        node->SetUsername(tagNode->FindTagByName("username")->GetValueAsString());
        node->SetPort(tagNode->FindTagByName("port")->GetValueCasted< long >());
        node->SetAuthMethod(tagNode->FindTagByName("auth")->GetValueCasted< int >());
        Add(node);
      }
    }
  }
}

SshPluginNodeManager::~SshPluginNodeManager() { UpdateSettings(); }

void SshPluginNodeManager::UpdateSettings() {
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  blTagMap::Pointer properties = blTagMap::New();
  if (properties.IsNotNull()) {
    NodeMapType::iterator it;
    for (it = m_NodeMap.begin(); it != m_NodeMap.end(); it++) {
      blTagMap::Pointer tagNode = blTagMap::New();
      tagNode->AddTag("hostname", it->second->GetHostname());
      tagNode->AddTag("username", it->second->GetUsername());
      tagNode->AddTag("port", it->second->GetPort());
      tagNode->AddTag("auth", it->second->GetAuthMethod());
      properties->AddTag(it->second->GetHostname(), tagNode);
    }
  }

  settings->SetPluginProperties("SshPlugin", properties);
}
