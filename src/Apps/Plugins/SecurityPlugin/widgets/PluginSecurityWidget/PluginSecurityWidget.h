/*
* Copyright (c) 2012,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef PluginSecurityWidget_H
#define PluginSecurityWidget_H

#include "PluginSecurityWidgetUI.h"
#include "slTrial.h"
#include "corePreferencesPage.h"
#include "corePluginProvider.h"

/**
Configure plugin security

\ingroup SecurityPlugin
\author Xavi Planes
\date Oct 2012
*/
class PluginSecurityWidget : public PluginSecurityWidgetUI,
                             public Core::Widgets::PreferencesPage {
public:
  coreDefineBaseWindowFactory(PluginSecurityWidget)
      //!
      PluginSecurityWidget(wxWindow *parent, int id,
                           const wxPoint &pos = wxDefaultPosition,
                           const wxSize &size = wxDefaultSize, long style = 0);

  //! When Ok is pressed
  void UpdateData();

  //! When the dialog is shown
  void UpdateWidget();

private:
  //! Generate pair public/private keys
  void OnGenerateKey(wxCommandEvent &event);

  //! Load public key from file
  void OnLoadPublicKey(wxCommandEvent &event);

  //! Load private key from file
  void OnLoadPrivateKey(wxCommandEvent &event);

  //! Use item data to verify
  void OnBtnVerify(wxCommandEvent &event);

  //! Use item data to compute signature and update item data and widget control
  void OnBtnSign(wxCommandEvent &event);

  //! Update info from selected item data to widget controls
  void OnItemSelectedFromTree(wxTreeEvent &event);

  //! Update info from widget controls to selected item data
  void OnText(wxCommandEvent &event);

  //! Update tree children at specific item
  bool UpdateTree(wxTreeItemId parentItem, blTagMap::Pointer plugins,
                  Core::Runtime::PluginProvider::Pointer provider);

  //! Update provider
  void UpdateProvider(wxTreeItemId item);

protected:
  //!
  slTrial::Pointer m_Trial;
};

#endif // PluginSecurityWidget_H
