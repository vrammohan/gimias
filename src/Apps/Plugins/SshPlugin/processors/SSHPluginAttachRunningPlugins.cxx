/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SSHPluginAttachRunningPlugins.h"
#include "SSHPluginProvider.h"

#include "sshConnection.h"
#include "sshDirectory.h"
#include "sshFile.h"
#include "SshPluginAskPasswordWx.h"

#include "coreDynProcessor.h"
#include "coreProcessorManager.h"
#include "coreSettings.h"
#include "coreProcessorOutputsObserverBuilder.h"
#include "corePluginTab.h"

#include "dynModuleExecution.h"

#include "blTextUtils.h"

SSHPluginAttachRunningPlugins::SSHPluginAttachRunningPlugins() {}

/** Destructor for the class SSHPluginAttachRunningPlugins. */
SSHPluginAttachRunningPlugins::~SSHPluginAttachRunningPlugins(void) {}

void SSHPluginAttachRunningPlugins::SetSSHPluginProvider(SSHPluginProvider *val) {
  m_SSHPluginProvider = val;
}

void SSHPluginAttachRunningPlugins::SetNodeManager(SshPluginNodeManager::Pointer val) {
  m_nodeManager = val;
}

void SSHPluginAttachRunningPlugins::Update() {
  if (Core::Runtime::Kernel::GetGraphicalInterface().IsNull() ||
      Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow() == NULL)
    return;

  blTagMap::Pointer configuration;
  configuration = m_SSHPluginProvider->GetProperties()->GetTagValue< blTagMap::Pointer >(
      "Configuration");

  // Connect to remote machine
  std::string nodeName = configuration->GetTag("SSH node")->GetValueAsString();
  sshNode::Pointer node = m_nodeManager->Get(nodeName);
  if (node.IsNull()) {
    throw Core::Exceptions::Exception(
        "SSHPluginProvider::ScanForCommandLineModulesByExecuting",
        "Cannot find SSH node configuration");
  }

  sshConnection::Pointer connection = m_SSHPluginProvider->GetConnection();
  if (connection.IsNull())
    return;

  connection->Connect();

  // Retrieve configuration from provider
  std::string tempDir;
  tempDir = configuration->FindTagByName("Temporary directory")->GetValueAsString();
  std::string workingDir = m_SSHPluginProvider->GetWorkingDirectory();

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  settings->ReplaceGimiasPath(workingDir);
  settings->ReplaceGimiasPath(tempDir);

  // Scan use case folders
  sshDirectory dir(connection);
  dir.SetPath(workingDir + "/usecase");
  dir.Load();
  for (int i = 0; i < dir.GetNumberOfFiles(); i++) {
    std::string filename = dir.GetFile(i);

    // Read command
    std::string useCase = filename;
    std::string useCaseDir = workingDir + "/usecase/" + useCase;
    sshFile commandFile(connection);
    commandFile.SetRemoteFilename(useCaseDir + "/" + "command.txt");
    std::string commandFileContent;

    try {
      commandFile.DownloadByContent(commandFileContent);
    } catch (...) {
      // If file does not exist->continue
      continue;
    }

    // Find working dir
    if (commandFileContent.find(workingDir) == std::string::npos) {
      continue;
    }

    // Find the first space after the working dir
    size_t pos = commandFileContent.find(" ", workingDir.size());
    std::string command = commandFileContent.substr(0, pos);

    // Get ModuleDescription
    ModuleDescription module;
    module = m_SSHPluginProvider->GetModuleDescriptionByCommand(command);
    module.SetTarget(nodeName);

    // Create a new processor and execute it in multi threading
    Core::DynProcessor::Pointer dynProcessor = Core::DynProcessor::New();
    dynProcessor->SetModule(&module);

    // Add output observers
    Core::Widgets::ProcessorOutputsObserverBuilder::Pointer observerBuilder;
    observerBuilder = Core::Widgets::ProcessorOutputsObserverBuilder::New();

    // Get rendering tree
    Core::BasePluginTab *pluginTab;
    pluginTab = Core::Runtime::Kernel::GetGraphicalInterface()
                    ->GetMainWindow()
                    ->GetLastPluginTab();
    Core::RenderingTree::Pointer tree;
    if (pluginTab != NULL) {
      tree = pluginTab->GetRenderingTreeManager()->GetActiveTree();
    }

    observerBuilder->Init(dynProcessor.GetPointer(), tree);
    std::vector< Core::ProcessorOutputObserver::Pointer > observers;
    observers = observerBuilder->GetList();
    m_ProcessorOutputObserverVector.insert(m_ProcessorOutputObserverVector.end(),
                                           observers.begin(), observers.end());

    // Set directories
    dynProcessor->SetTemporaryDirectory(tempDir);
    dynProcessor->SetWorkingDirectory(workingDir);

    // This parameter will avoid transferring initial data and executing the process again
    dynProcessor->AttachProcess(useCase);

    dynProcessor->SetMultithreading(true);
    Core::Runtime::Kernel::GetProcessorManager()->Execute(dynProcessor.GetPointer());
  }
}
