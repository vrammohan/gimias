/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include <sstream>
#include "XNATPluginUtilities.h"
#include <sstream>
#include "XNATPluginUtilities.h"

// xml
#include "tinyxml.h"

// xnat
#include "xnatClient.h"
#include "Core.h"
#include "Utils.h"

// wxWidgets
#include <wx/treebook.h>

// Core
#include "coreWxMitkCoreMainWindow.h"
#include "corePreferencesDialog.h"

// security
#include "SecurityPluginUtilities.h"

std::string XNATPluginUtilities::m_Path;

std::string XNATPluginUtilities::CreateBaseFolder(std::string path) {
  Core::IO::Directory::Pointer dir = Core::IO::Directory::New();
  dir->SetDirNameFullPath(path);

  if (!dir->Exists()) {
    dir->Create();
  }

  return path;
}

void XNATPluginUtilities::CreateConfigFile(std::string xnatFolder) {
  std::ofstream connectionsFile;
  std::string path = xnatFolder + Core::IO::SlashChar + "connections.txt";

  connectionsFile.open(path.c_str());
  connectionsFile.close();

  m_Path = xnatFolder;
}

std::string XNATPluginUtilities::GetConfigFilePath() { return m_Path; }

void XNATPluginUtilities::SetConfigFilePath(std::string str) { m_Path = str; }

void XNATPluginUtilities::AddNodeConfigFile() {}

void XNATPluginUtilities::RemoveNodeConfigFile() {}

void XNATPluginUtilities::EditNodeConfigFile() {}

void XNATPluginUtilities::OverwriteConfigFile(
    std::map< std::string, XNATPluginDefineConetions > urlManager) {
  // get password manager to register nodes
  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();

  // check if Security Plugin is active
  bool encripted = false;

  // get from settings xml config file
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  blTagMap::Pointer properties = blTagMap::New();

  // creates a node XNATPlugin on xml config with connection info
  if (properties.IsNotNull()) {
    std::map< std::string, XNATPluginDefineConetions >::iterator it;
    for (it = urlManager.begin(); it != urlManager.end(); it++) {
      blTagMap::Pointer tagNode = blTagMap::New();
      tagNode->AddTag("encripted", it->second.GetEncripted());
      tagNode->AddTag("username", it->second.GetName());
      tagNode->AddTag("user", it->second.GetUser());
      std::string regPass =
          it->second.GetName() + it->second.GetUser() + it->second.GetURL();
      if (manager.IsNotNull() && it->second.GetEncripted()) {
        tagNode->AddTag("password", manager->GetPassword(regPass));
      } else {
        tagNode->AddTag("password", it->second.GetPassword());
      }
      tagNode->AddTag("url", it->second.GetURL());
      properties->AddTag(it->second.GetName(), tagNode);
    }
  }
  // add node to xml file
  settings->SetPluginProperties("XNATPlugin", properties);
}

std::map< std::string, XNATPluginDefineConetions > XNATPluginUtilities::ReadConfigFile() {
  // get password manager to register nodes
  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();
  std::map< std::string, XNATPluginDefineConetions > ret;

  // get from settings xml config file
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  blTagMap::Pointer properties;
  properties = settings->GetPluginProperties("XNATPlugin");
  if (properties.IsNotNull()) {
    blTagMap::Iterator it;
    for (it = properties->GetIteratorBegin(); it != properties->GetIteratorEnd(); it++) {
      blTagMap::Pointer tagNode;
      if (it->second->GetValue(tagNode)) {
        // define new connection
        XNATPluginDefineConetions connection;

        // appnd values to connection define encrypted value
        bool enc;
        blTag::Pointer ptrNode = tagNode->FindTagByName("encripted"); //->GetValue(enc);
        if (ptrNode.IsNotNull())
          ptrNode->GetValue(enc);
        else
          enc = false;
        connection.SetEncripted(enc);
        connection.SetName(tagNode->FindTagByName("username")->GetValueAsString());
        connection.SetURL(tagNode->FindTagByName("url")->GetValueAsString());
        connection.SetUser(tagNode->FindTagByName("user")->GetValueAsString());
        connection.SetPassword(tagNode->FindTagByName("password")->GetValueAsString());
        // includes connection on rep connection
        ret.insert(std::pair< std::string, XNATPluginDefineConetions >(
            tagNode->FindTagByName("username")->GetValueAsString(), connection));

        if (manager) {
          std::string registrationName =
              connection.GetName() + connection.GetUser() + connection.GetURL();
          manager->RegisterPassword(registrationName, connection.GetPassword(), true);
        }
      }
    }
  }
  return ret;
}

bool operator==(const XNATPluginDefineConetions &c1,
                const XNATPluginDefineConetions &c2) {
  return c1.GetName() == c2.GetName() && c1.GetURL() == c2.GetURL() &&
         c1.GetUser() == c2.GetUser() && c1.GetPassword() == c2.GetPassword() &&
         c1.GetEncripted() == c2.GetEncripted();
}
