# Used to configure GenericSegmentationPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

clinicalReportPlugin = GimiasPluginProject("ClinicalReportPlugin", api)

projects = [
    gmCoreLight,
	wxpdfdoc
]
clinicalReportPlugin.AddProjects(projects)

clinicalReportPlugin.AddSources(["*.cxx", "*.h"])
clinicalReportPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
clinicalReportPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "ClinicalReportToolbar",
  "ClinicalReportWidget",
  "NewClinicalReportComment",
  "NewClinicalReportImage",
  "NewClinicalReportTable",
  "ShowTable"
]

clinicalReportPlugin.AddWidgetModules(widgetModules, _useQt = 0)

clinicalReportPlugin.SetPrecompiledHeader("clinicalReportPluginPCH.h")



