/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "wxHtmlTooltipDialog.h"
#include "msvWxVTKButtons.h"
#include "wxUnicode.h"

#include "msvVTKBtnEdit.h"
#include "msvVTKBtnEditView.h"
#include "msvWxHtmlWindow.h"
#include "msvMultiRenderWindow.h"

#include "coreDataContainer.h"
#include "coreMainMenu.h"
#include "coreDataEntityInfoHelper.h"

#include <wx/wupdlock.h>

#define wxTOOLTIP_WIDTH 250
#define wxTOOLTIP_HEIGHT 100

BEGIN_EVENT_TABLE(wxHtmlTooltipDialog, wxDialog)
END_EVENT_TABLE()

wxHtmlTooltipDialog::wxHtmlTooltipDialog(wxWindow *parent, wxWindowID id,
                                         const wxPoint &pos /*= wxDefaultPosition*/,
                                         const wxSize &size /*= wxDefaultSize*/,
                                         long style /*= wxDEFAULT_DIALOG_STYLE*/,
                                         const wxString &name /* = "dialogBox"*/)
    : // wxDialog(parent, id, title, pos, size, style | wxSTAY_ON_TOP, name)
      wxDialog(parent, id, "", pos, size, style, name) {
  SetExtraStyle(GetExtraStyle() & ~wxWS_EX_BLOCK_EVENTS);

  // draw the dialog with the html window
  wxBoxSizer *topsizer = new wxBoxSizer(wxVERTICAL);
  wxSize htmlSz((size.x * .9), (size.y * .9));
  m_htmlWin =
      new msvWxHtmlWindow(this, wxID_ANY, wxDefaultPosition, htmlSz, wxHW_SCROLLBAR_AUTO);
  m_htmlWin->SetBorders(0);
  m_htmlWin->SetPage("<html><body>Hello, world!</body></html>");
  topsizer->Add(m_htmlWin, 1, wxEXPAND | wxALL, 5);

  this->SetSizer(topsizer);
  Layout();

  m_Button = NULL;
}

wxHtmlTooltipDialog::~wxHtmlTooltipDialog() {}

void wxHtmlTooltipDialog::SetRenderWindow(msvMultiRenderWindow *renderWindow) {
  m_RenderWindow = renderWindow;
  m_htmlWin->SetRenderWindow(m_RenderWindow);
}

void wxHtmlTooltipDialog::LoadPage(wxString hPage) { m_htmlWin->LoadPage(hPage); }

wxHtmlWindow *wxHtmlTooltipDialog::GetHtmlWin() { return m_htmlWin; }

void wxHtmlTooltipDialog::SetButton(msvWxVTKButtons *button) {
  m_Button = button;
  m_htmlWin->SetButton(m_Button);
}

void wxHtmlTooltipDialog::Show() {
  // show/hide edit/editView/html window
  m_htmlWin->Show(true);
  Layout();

  // HTML contents
  m_htmlWin->BuildContents(msvWxHtmlWindow::MODE_TOOLTIP);

  // Set size
  SetSize(wxSize(wxTOOLTIP_WIDTH, wxTOOLTIP_HEIGHT));

  wxDialog::Show();
}

bool wxHtmlTooltipDialog::Hide() { return wxDialog::Hide(); }
