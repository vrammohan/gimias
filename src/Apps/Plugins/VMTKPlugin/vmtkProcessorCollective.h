/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _vmtkProcessorCollective_H
#define _vmtkProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

/**

\ingroup VMTKPlugin
\author Valeria Barbarito
\date 4 February 2011
*/

class vmtkProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(vmtkProcessorCollective, Core::SmartPointerObject);

private:
  //! The constructor instantiates all the processors and connects them.
  vmtkProcessorCollective();

private:
};

#endif //_vmtkPluginProcessorCollective_H
