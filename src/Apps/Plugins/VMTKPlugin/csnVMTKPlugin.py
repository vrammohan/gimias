# Used to configure VMTKPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

vmtkPlugin = GimiasPluginProject("VMTKPlugin", api)

projects = [
    baseLibITK,
    tpExtLibMITK,
    gmCore,
    boost,
    cilabMacros,
    vmtk
]

vmtkPlugin.AddProjects(projects)

vmtkPlugin.AddSources(["*.cxx", "*.h"])
vmtkPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
vmtkPlugin.AddIncludeFolders(["processors"])

widgetModules = ["VesselCenterlineWidget", "VesselBifurcationsWidget"]
vmtkPlugin.AddWidgetModules(widgetModules, _useQt = 0)

vmtkPlugin.SetPrecompiledHeader("VMTKPluginPCH.h")

vmtkPlugin.AddFilesToInstall(vmtkPlugin.Glob("resource"), "resource")
