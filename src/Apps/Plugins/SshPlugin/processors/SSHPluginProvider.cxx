/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SSHPluginProvider.h"
#include "SshPluginAskPasswordWx.h"

#include "coreKernel.h"
#include "coreLogger.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSettings.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"
#include "coreDirectory.h"
#include "coreFile.h"
#include "coreAssert.h"
#include "coreBaseMainWindow.h"
#include "coreBaseWindowFactories.h"

#include "blTextUtils.h"

#include <deque>
#include <algorithm>
#include <cctype>

#include "itksys/SystemTools.hxx"
#include "itksys/DynamicLoader.hxx"
#include "itksys/Base64.h"

#include "sshConnection.h"
#include "sshDirectory.h"
#include "sshExecuteCommand.h"
#include "sshFile.h"

#include "ModuleDescriptionParser.h"

using namespace Core::Runtime;
using namespace Core::FrontEndPlugin;

class ModuleDescriptionMap : public std::map< std::string, ModuleDescription > {};
class ModuleFileMap : public std::set< std::string > {};

// cache entry for a module
struct ModuleCacheEntry {
  std::string Location;  // complete path to a file
  long int ModifiedTime; // file's modified time
  std::string Type; // SharedObjectModule, CommandLineModule, PythonModule, NotAModule
  //  std::string Title;           // name of the module
  std::string XMLDescription; // Module description
  int LogoWidth;
  int LogoHeight;
  int LogoPixelSize;
  unsigned long LogoLength;
  std::string Logo;
};

// map from a filename to cache entry
class ModuleCache : public std::map< std::string, ModuleCacheEntry > {};

SSHPluginProvider::SSHPluginProvider(SshPluginNodeManager::Pointer nodeManager) {
  m_nodeManager = nodeManager;
  blTagMap::Pointer configuration = blTagMap::New();
  configuration->AddTag("SSH node", std::string("ws083566.ca.upf.edu"));
  configuration->AddTag(
      "Scan folder",
      std::string("/home/cistib/software/gimias-1.3.0-release/commandLinePlugins/"));
  configuration->AddTag("Working directory",
                        std::string("/home/cistib/software/gimias-1.3.0-release/"));
  // On Windows 7, we should use APPDATA to write application data
  configuration->AddTag("Temporary directory", std::string("$(AppData)"));
  GetProperties()->AddTag("Configuration", configuration);
  GetProperties()->AddTag("Plugins", blTagMap::New());
  GetProperties()->AddTag("Priority", 5);

  m_AttachPlugins = SSHPluginAttachRunningPlugins::New();
  m_AttachPlugins->SetSSHPluginProvider(this);
  m_AttachPlugins->SetNodeManager(m_nodeManager);
}

/** Destructor for the class SSHPluginProvider. */
SSHPluginProvider::~SSHPluginProvider(void) {}

void SSHPluginProvider::LoadPlugins() {
  try {

    wxMitkGraphicalInterface::Pointer graphicalIface;
    graphicalIface = Kernel::GetGraphicalInterface();
    if (graphicalIface.IsNull())
      return;

    blTagMap::Pointer plugins;
    plugins = GetProperties()->GetTagValue< blTagMap::Pointer >("Plugins");

    // For each XML of ModuleFactory -> Register a factory
    std::vector< std::string > moduleNames;
    moduleNames = GetModuleNames();

    // Update number of outputs
    SetNumberOfOutputs(moduleNames.size());

    for (int i = 0; i < moduleNames.size(); i++) {
      ModuleDescription module;
      module = GetModuleDescription(moduleNames[i]);

      // Find module
      bool moduleIsLoaded = false;
      std::string factoryName =
          graphicalIface->GetMainWindow()->FindFactoryNameByModule(&module);
      if (!factoryName.empty()) {
        Core::BaseWindowFactory::Pointer factory;
        factory = graphicalIface->GetBaseWindowFactory()->FindFactory(factoryName);
        if (factory->FindModule(GetName())) {
          moduleIsLoaded = true;
        }
      }

      // Load / Unload
      blTag::Pointer tag = plugins->FindTagByName(module.GetTitle());
      // If category has the same name as a plugin -> search inside the category tag
      if (tag.IsNotNull() && tag->GetTypeName() != blTag::GetTypeName(typeid(bool))) {
        tag =
            tag->GetValueCasted< blTagMap::Pointer >()->FindTagByName(module.GetTitle());
      }

      if (tag.IsNotNull() && tag->GetValueCasted< bool >() == true && !moduleIsLoaded) {

        std::string message = "Loading plugin " + module.GetTitle() + "\n";
        GetUpdateCallback()->AddInformationMessage(message);
        GetUpdateCallback()->Modified();

        blTagMap::Pointer pluginInfo = blTagMap::New();
        pluginInfo->AddTag(module.GetTitle(), true);
        GetOutputPort(i)->SetWaitPortUpdate(false);
        UpdateOutput(i, pluginInfo, GetName());
      } else if (tag.IsNotNull() && tag->GetValueCasted< bool >() == false &&
                 moduleIsLoaded) {
        std::string message = "UnLoading plugin " + module.GetTitle() + "\n";
        GetUpdateCallback()->AddInformationMessage(message);
        GetUpdateCallback()->Modified();

        blTagMap::Pointer pluginInfo = blTagMap::New();
        pluginInfo->AddTag(module.GetTitle(), false);
        GetOutputPort(i)->SetWaitPortUpdate(false);
        UpdateOutput(i, pluginInfo, GetName());

        m_LoadedPlugins.remove(module.GetTitle());
      }
    }
  }
  coreCatchExceptionsAddTraceAndThrowMacro(SSHPluginProvider::LoadPlugins);

  SetNumberOfOutputs(0);
}

void SSHPluginProvider::LoadPlugin(const std::string &name) {
  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

  // Find module description
  ModuleDescription module;
  module = GetModuleDescription(name);

  // Patch to set the remote machine name
  // The function GetModuleFromCache( ) will call SetLocation( )
  // with the commandName
  module.SetTarget(m_Connection->GetNode()->GetHostname());

  // Register it
  graphicalIface->GetMainWindow()->RegisterModule(GetName(), &module);

  m_LoadedPlugins.push_back(name);
}

void SSHPluginProvider::UnLoadPlugin(const std::string &name) {
  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

  // Find module description
  ModuleDescription module;
  module = GetModuleDescription(name);

  graphicalIface->GetMainWindow()->UnRegisterModule(GetName(), &module);

  m_LoadedPlugins.remove(name);
}

void SSHPluginProvider::LoadXMLDescriptions(const std::string &path) {
  // Read the XML filters
  ModuleFactory moduleFactory;
  moduleFactory.SetSearchPaths(path.c_str());
  moduleFactory.Scan();

  wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Kernel::GetGraphicalInterface();

  // For each XML of ModuleFactory -> Register a factory
  std::vector< std::string > moduleNames;
  moduleNames = moduleFactory.GetModuleNames();
  for (int i = 0; i < moduleNames.size(); i++) {
    ModuleDescription module;
    module = moduleFactory.GetModuleDescription(moduleNames[i]);
    graphicalIface->GetMainWindow()->RegisterModule(GetName(), &module);
  }
}

void SSHPluginProvider::ScanPlugins() {

  try {
    // Clear the collected profile from the plugins and set it to the default
    blTagMap::Pointer plugins;
    plugins = GetProperties()->GetTagValue< blTagMap::Pointer >("Plugins");

    delete InternalCache;
    delete InternalMap;
    delete InternalFileMap;

    this->InternalCache = new ModuleCache;
    this->InternalMap = new ModuleDescriptionMap;
    this->InternalFileMap = new ModuleFileMap;

    Settings::Pointer settings = Kernel::GetApplicationSettings();

    blTagMap::Pointer configuration;
    configuration = GetProperties()->GetTagValue< blTagMap::Pointer >("Configuration");

    // Scan folders
    std::string scanPaths = configuration->GetTag("Scan folder")->GetValueAsString();
    settings->ReplaceGimiasPath(scanPaths);
    this->SetSearchPaths(scanPaths);

    // Working folder
    std::string folder = configuration->GetTag("Working directory")->GetValueAsString();
    settings->ReplaceGimiasPath(folder);
    this->SetWorkingDirectory(folder);
    this->SetCachePath(folder + "/CLPCache");

    // Scan
    this->Scan();

    std::vector< std::string > moduleNames = this->GetModuleNames();
    blTagMap::Pointer newPlugins = blTagMap::New();
    for (int i = 0; i < moduleNames.size(); i++) {
      bool selected = false;
      blTag::Pointer pluginTag = plugins->FindTagByName(moduleNames[i], true);
      if (pluginTag.IsNotNull()) {
        selected = pluginTag->GetValueCasted< bool >();
      }

      ModuleDescription module = this->GetModuleDescription(moduleNames[i]);

      // Add tags for categories
      blTag::Pointer categoryTag;
      blTagMap::Pointer categoryTagMap;
      std::string category = module.GetCategory();
      std::list< std::string > words;
      blTextUtils::ParseLine(category, '.', words);
      std::list< std::string >::iterator it;
      categoryTagMap = newPlugins;
      for (it = words.begin(); it != words.end(); it++) {
        categoryTag = categoryTagMap->GetTag(*it);
        if (categoryTag.IsNull()) {
          categoryTag = blTag::New(*it, blTagMap::New());
        }
        categoryTagMap->AddTag(categoryTag);
        categoryTagMap = categoryTag->GetValueCasted< blTagMap::Pointer >();
      }

      // Add tag for module
      categoryTagMap->AddTag(moduleNames[i], selected);
    }

    GetProperties()->AddTag("Plugins", newPlugins);
  }
  coreCatchExceptionsAddTraceAndThrowMacro(SSHPluginProvider::ScanPlugins);
}

std::string SSHPluginProvider::GetCurrentPluginName() const {
  return m_CurrentPluginName;
}

std::string SSHPluginProvider::GetName() const {
  blTagMap::Pointer configuration;
  configuration = m_Properties->GetTagValue< blTagMap::Pointer >("Configuration");

  std::string nodeName = configuration->GetTag("SSH node")->GetValueAsString();
  std::string folder = configuration->GetTag("Scan folder")->GetValueAsString();
  return "SSH Command Line at ssh:/" + nodeName + folder;
}

void SSHPluginProvider::Scan() {
  blTagMap::Pointer configuration;
  configuration = GetProperties()->GetTagValue< blTagMap::Pointer >("Configuration");

  std::string nodeName = configuration->GetTag("SSH node")->GetValueAsString();
  sshNode::Pointer node = m_nodeManager->Get(nodeName);
  if (node.IsNull()) {
    throw Core::Exceptions::Exception(
        "SSHPluginProvider::ScanForCommandLineModulesByExecuting",
        "Cannot find SSH node configuration");
  }

  m_Connection = sshConnection::New();
  m_Connection->SetNode(node);
  m_Connection->SetPasswordMethod(new sshPluginAskPasswordWx());
  m_Connection->Connect();

  // Load the module cache information
  this->LoadModuleCache();

  int numberOfExecutables, numberOfOtherFiles;
  numberOfExecutables = this->ScanForCommandLineModulesByExecuting();

  numberOfOtherFiles = this->ScanForNotAModuleFiles();

  // Store the module cache information
  this->SaveModuleCache();

  // clean up
  m_Connection->Disconnect();
}

long SSHPluginProvider::ScanForCommandLineModulesByExecuting() {
  // add any of the self-describing command-line modules available
  //
  // self-describing command-line modules live in a prescribed
  // path and respond to a command line argument "--xml"
  //
  if (this->GetSearchPaths() == "") {
    this->WarningMessage("Empty module search paths.");
    return 0;
  }

  std::list< std::string > modulePaths;
#ifdef _WIN32
  char delim(';');
#else
  char delim(':');
#endif
  std::string searchPaths = this->GetSearchPaths();
  blTextUtils::ParseLine(searchPaths, delim, modulePaths);

  std::list< std::string >::const_iterator pit;
  long numberTested = 0;
  long numberFound = 0;
  double t0, t1;

  blTagMap::Pointer configuration;
  configuration = GetProperties()->GetTagValue< blTagMap::Pointer >("Configuration");

  // Get working directory
  std::string workingDirectory;
  workingDirectory = configuration->GetTag("Working directory")->GetValueAsString();
  Settings::Pointer settings = Kernel::GetApplicationSettings();
  settings->ReplaceGimiasPath(workingDirectory);

  t0 = itksys::SystemTools::GetTime();
  for (pit = modulePaths.begin(); pit != modulePaths.end(); ++pit) {
    std::stringstream information;

    information << "Searching " << *pit
                << " for command line executable plugins by executing." << std::endl;

    sshDirectory directory(m_Connection);
    directory.SetPath((*pit).c_str());
    directory.Load();

    // Get Modified time for all files
    std::map< std::string, long int > modifiedTimeTable;
    modifiedTimeTable = directory.ModifiedTimeForAllFiles();

    for (unsigned int ii = 0; ii < directory.GetNumberOfFiles(); ++ii) {
      bool isAPlugin = true;
      const char *filename = directory.GetFile(ii);

      // skip any directories
      if (!itksys::SystemTools::FileIsDirectory(filename)) {

        // try to focus only on executables or those that have registered
        // programs to execute them

        // does the file have a known extension?
        std::string ext = itksys::SystemTools::GetFilenameExtension(filename);
        // On Linux, extension is empty
        if (ext == "" || ext == ".exe") {
          numberTested++;
          // std::cout << "Testing " << filename << " as a plugin:" <<std::endl;
          std::string commandName = std::string(directory.GetPath()) + "/" + filename;

          // early exit if we have already tested this file and succeeded
          ModuleFileMap::iterator fit = this->InternalFileMap->find(commandName);
          if (fit != this->InternalFileMap->end()) {
            // file was already discovered as a module
            information << "Module already discovered at " << commandName << std::endl;
            continue;
          }

          // determine the modified time of the module
          long int commandModifiedTime = modifiedTimeTable[filename];

          // early exit if we can find the module in the cache
          int cached = this->GetModuleFromCache(commandName, commandModifiedTime,
                                                "SSHCommandLineModule", information);
          if (cached != 0) {
            if (cached == 1) {
              numberFound++; // found in the cache and is a module
            }
            // whatever, it was in the cache, so we can safely skip it.
            continue;
          }

          // command, process and argument to probe the executable
          std::string command;
          std::string arg("--xml");

          // does the command have a known extension?
          ext = itksys::SystemTools::GetFilenameExtension(commandName);

          // build the command/parameter array.
          // command[1] = const_cast<char*>(arg.c_str());
          // command[2] = 0;
          // should be able to execute it on it's own
          command += commandName + " ";
          command += arg + " ";

          sshExecuteCommand::Pointer execute = sshExecuteCommand::New(m_Connection);
          execute->SetWorkingDirectory(workingDirectory);
          execute->SetLibraryPath(sshFile::GetFilenamePath(command));
          execute->Execute(command);

          // Wait for the command to finish
          std::string stdoutbuffer = execute->Getcout();
          std::string stderrbuffer = execute->Getcerr();

          // check the exit state / error state of the process
          int result = execute->GetState();
          if (result == sshExecuteCommand::State_Exited) {
            // executable exited cleanly and must of done
            // "something" when presented with a "--xml" argument
            // (note that it may have just printed out that it did
            // not understand --xml)
            if (execute->GetExitValue() == 0) {
              // executable exited without errors, check if it
              // generated a valid xml description
              if (stdoutbuffer.compare(0, 5, "<?xml") == 0) {
                // std::cout << "\t" << filename << " is a plugin." << std::endl;
                this->InternalFileMap->insert(commandName);

                // Construct and configure the module object
                ModuleDescription module;
                module.SetType("SSHCommandLineModule");

                // use location to point to the executable used to run the command in
                // commandName
                module.SetTarget(commandName);
                // This information will be lost after saving to cache
                // So when LoadingPlugins( ) is called, it is set again
                module.SetLocation(commandName);

                // Parse the xml to build the description of the module
                // and the parameters
                ModuleDescriptionParser parser;
                parser.Parse(stdoutbuffer, module);

                // Check to make sure the module is not already in the
                // list
                ModuleDescriptionMap::iterator mit =
                    this->InternalMap->find(module.GetTitle());

                std::string splash_msg("Discovered ");
                splash_msg += module.GetTitle();
                splash_msg += " Module (adding to cache)...";
                this->ModuleDiscoveryMessage(splash_msg.c_str());

                if (mit == this->InternalMap->end()) {
                  // See if the module has a logo, if so, store it in
                  // the module description
                  this->GetLogoForCommandLineModuleByExecuting(module);

                  // Store the module in the list
                  (*this->InternalMap)[module.GetTitle()] = module;
                  information << "ScanForCommandLineModulesByExecuting: A module named \""
                              << module.GetTitle() << "\" has been discovered at "
                              << module.GetLocation() << "(" << module.GetTarget() << ")"
                              << std::endl;

                  numberFound++;
                }

                // Put the module in the cache
                ModuleCacheEntry entry;
                entry.Location = commandName;
                entry.ModifiedTime = commandModifiedTime;
                entry.Type = "SSHCommandLineModule";
                entry.XMLDescription = stdoutbuffer;

                if (module.GetLogo().GetBufferLength() != 0) {
                  entry.LogoWidth = module.GetLogo().GetWidth();
                  entry.LogoHeight = module.GetLogo().GetHeight();
                  entry.LogoPixelSize = module.GetLogo().GetPixelSize();
                  entry.LogoLength = module.GetLogo().GetBufferLength();
                  // entry.Logo = std::string((const unsigned char
                  // *)module.GetLogo().GetLogo());
                  entry.Logo = std::string(module.GetLogo().GetLogo());
                } else {
                  entry.LogoWidth = 0;
                  entry.LogoHeight = 0;
                  entry.LogoPixelSize = 0;
                  entry.LogoLength = 0;
                  entry.Logo = "None";
                }

                (*this->InternalCache)[entry.Location] = entry;
                this->CacheModified = true;
              } else {
                isAPlugin = false;
                information << filename
                            << " is not a plugin (did not generate an XML description)."
                            << std::endl;
              }
            } else {
              std::cerr << stderrbuffer;
              isAPlugin = false;
              information << filename << " is not a plugin (exited with errors)."
                          << std::endl;
            }
          } else if (result == sshExecuteCommand::State_Expired) {
            isAPlugin = false;
            information << filename << " is not a plugin (timeout exceeded)."
                        << std::endl;
          } else {
            isAPlugin = false;
            information << filename
                        << " is not a plugin (did not exit cleanly), command[0] = "
                        << command[0] << ", [1] = " << command[1] << std::endl;
          }
        }
      }
    }
    this->InformationMessage(information.str().c_str());
  }
  t1 = itksys::SystemTools::GetTime();

  std::stringstream information;
  information << "Tested " << numberTested
              << " files as command line executable plugins by executing. Found "
              << numberFound << " new plugins in " << t1 - t0 << " seconds." << std::endl;
  this->InformationMessage(information.str().c_str());

  return numberFound;
}

void SSHPluginProvider::LoadModuleCache() {
  std::stringstream information;
  if (this->GetCachePath() == "") {
    information << "No module cache path set." << std::endl;

    // emit the message
    this->WarningMessage(information.str().c_str());
    return;
  } else {
    information << "Loading module cache." << std::endl;

    std::string buffer;
    sshFile file(m_Connection);
    file.SetRemoteFilename(this->GetCachePath() + "/ModuleCache.csv");

    // Catch exception if remote file doesn't exist
    try {
      file.DownloadByContent(buffer);
    } catch (...) {
    }

    // put code here to write the cache
    std::istringstream cache(buffer);

    if (cache) {
      ModuleCacheEntry entry;
      std::string line;
      char comma = ',';

      unsigned long lineNumber = 0;
      while (!cache.eof()) {
        std::list< std::string > wordsList;
        lineNumber++;
        std::getline(cache, line);
        blTextUtils::ParseLine(line, comma, wordsList);

        std::vector< std::string > words(wordsList.begin(), wordsList.end());

        if (words.size() == 9) {
          entry.Location = words[0];
          entry.ModifiedTime = atoi(words[1].c_str());

          // trim the Type of leading whitespace
          std::string::size_type pos = words[2].find_first_not_of(" \t\r\n");
          if (pos != std::string::npos) {
            words[2].erase(0, pos);
          }
          entry.Type = words[2];

          // trim the XMLDescription of leading whitespace
          pos = words[3].find_first_not_of(" \t\r\n");
          if (pos != std::string::npos) {
            words[3].erase(0, pos);
          }

          // convert XML Description from Base64
          if (words[3] != "None") {
            unsigned char *bin = new unsigned char[words[3].size()];
            unsigned int decodedLengthActual =
                itksysBase64_Decode((const unsigned char *)words[3].c_str(), 0,
                                    (unsigned char *)bin, words[3].size());

            entry.XMLDescription = std::string((char *)bin, decodedLengthActual);

            delete[] bin;
          } else {
            entry.XMLDescription = words[3];
          }

          entry.LogoWidth = atoi(words[4].c_str());
          entry.LogoHeight = atoi(words[5].c_str());
          entry.LogoPixelSize = atoi(words[6].c_str());
          entry.LogoLength = atoi(words[7].c_str());

          // trim the Logo of leading whitespace
          pos = words[8].find_first_not_of(" \t\r\n");
          if (pos != std::string::npos) {
            words[8].erase(0, pos);
          }

          entry.Logo = words[8];

          (*this->InternalCache)[entry.Location] = entry;

          information << "Found cache entry for " << entry.Location << std::endl;

        } else {
          if (words.size() > 0) {
            information << "Invalid cache entry for " << words[0] << std::endl;
          } else {
            information << "Invalid cache line at line " << lineNumber << std::endl;
          }
        }
      }

      // emit the message
      this->InformationMessage(information.str().c_str());

      return;
    } else {
      information << "Cannot read cache " << this->GetCachePath() + "/ModuleCache.csv"
                  << std::endl;

      // emit the message
      this->WarningMessage(information.str().c_str());
      return;
    }
  }
}

void SSHPluginProvider::SaveModuleCache() {
  if (this->CacheModified) {
    std::stringstream information;
    if (this->GetCachePath() == "") {
      information << "New modules discovered but no cache path set." << std::endl;

      // emit the message
      this->WarningMessage(information.str().c_str());
      return;
    } else {
      information << "New modules discovered, updating module cache in directory "
                  << this->GetCachePath() << std::endl;

      // put code here to write the cache
      std::ostringstream cache;

      if (cache) {
        ModuleCache::iterator cit;
        for (cit = this->InternalCache->begin(); cit != this->InternalCache->end();
             ++cit) {
          cache << (*cit).second.Location << ", " << (*cit).second.ModifiedTime << ", "
                << (*cit).second.Type << ", ";

          if ((*cit).second.XMLDescription != "None") {
            int encodedLengthEstimate = 2 * (*cit).second.XMLDescription.size();
            encodedLengthEstimate = ((encodedLengthEstimate / 4) + 1) * 4;

            char *bin = new char[encodedLengthEstimate];
            int encodedLengthActual = itksysBase64_Encode(
                (const unsigned char *)(*cit).second.XMLDescription.c_str(),
                (*cit).second.XMLDescription.size(), (unsigned char *)bin, 0);
            std::string encodedDescription(bin, encodedLengthActual);
            delete[] bin;

            cache << encodedDescription << ", ";
          } else {
            cache << "None, ";
          }

          if ((*cit).second.Logo != "None" && (*cit).second.Logo != "") {
            cache << (*cit).second.LogoWidth << ", " << (*cit).second.LogoHeight << ", "
                  << (*cit).second.LogoPixelSize << ", " << (*cit).second.LogoLength
                  << ", " << (*cit).second.Logo << std::endl;
          } else {
            // width, height, pixel size, logo length, logo
            cache << "0, 0, 0, 0, None" << std::endl;
          }
        }

        sshDirectory dir(m_Connection);
        dir.SetPath(this->GetCachePath().c_str());
        dir.Create();
        sshFile file(m_Connection);
        file.SetRemoteFilename(this->GetCachePath() + "/ModuleCache.csv");
        file.UploadByContent(cache.str());

        // emit the message
        this->InformationMessage(information.str().c_str());

        return;
      } else {
        information << "Cannot write to cache path "
                    << this->GetCachePath() + "/ModuleCache.csv" << std::endl;

        // emit the message
        this->WarningMessage(information.str().c_str());
        return;
      }
    }
  }
}

ModuleDescription
SSHPluginProvider::GetModuleDescriptionByCommand(const std::string &command) const {
  std::vector< std::string > moduleNames;
  moduleNames = GetModuleNames();
  for (int i = 0; i < moduleNames.size(); i++) {
    ModuleDescription module;
    module = GetModuleDescription(moduleNames[i]);
    if (module.GetLocation() == command) {
      return module;
    }
  }

  return ModuleDescription();
}

void SSHPluginProvider::AttachRunningPlugins() {
  // Attach detached plugins
  m_AttachPlugins->Update();
}

sshConnection::Pointer SSHPluginProvider::GetConnection() { return m_Connection; }
