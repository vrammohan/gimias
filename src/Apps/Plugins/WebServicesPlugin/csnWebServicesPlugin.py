# Used to configure WebServicesPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

#####################################################
# Web service DLL for GIMIAS
wsGimias = api.CreateCompiledProject("wsGimias", "dll")
wsGimias.AddSources(["webService/*.cxx"])
wsGimias.AddSources(["webService/*.h"])
wsGimias.AddIncludeFolders([u'webService'])
wsGimias.AddProjects([axis2C, gmCoreLight])
wsGimias.SetPrecompiledHeader("WebServicePCH.h")


#####################################################
# Plugin

webServicesPlugin = GimiasPluginProject("WebServicesPlugin", api)

projects = [
    gmCoreLight,
    axis2C,
    wsGimias
]
webServicesPlugin.AddProjects(projects)

webServicesPlugin.AddSources(["*.cxx", "*.h"])
webServicesPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
webServicesPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "WebServicesPreferencesPanelWidget"
  ]
webServicesPlugin.AddWidgetModules(widgetModules, _useQt = 0)

webServicesPlugin.SetPrecompiledHeader("WebServicesPluginPCH.h")

