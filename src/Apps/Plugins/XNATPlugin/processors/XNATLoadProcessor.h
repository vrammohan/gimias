/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#ifndef XNATLoadProcessor_H
#define XNATLoadProcessor_H

#include "gmProcessorsWin32Header.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreCommonDataTypes.h"
#include "corePluginMacros.h"
#include "coreBaseProcessor.h"

#include <map>
#include <vector>

/**
\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class XNATLoadProcessor : public Core::BaseProcessor {
public:
  //!
  coreProcessor(XNATLoadProcessor, Core::BaseProcessor);

  //!
  XNATLoadProcessor();
  //!
  ~XNATLoadProcessor();

  //!
  void Update();

private:
  //! Purposely not implemented
  XNATLoadProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
};

#endif // XNATLoadProcessor_H
