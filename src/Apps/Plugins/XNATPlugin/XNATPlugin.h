/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _XNATPlugin_H
#define _XNATPlugin_H

#include "XNATProcessorCollective.h"
#include "XNATWidgetCollective.h"

// core
#include "coreFrontEndPlugin.h"

/**
\brief Clinical Report Plugin
\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class PLUGIN_EXPORT XNATPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
public:
  coreDeclareSmartPointerClassMacro(XNATPlugin, Core::FrontEndPlugin::FrontEndPlugin);

protected:
  //!
  XNATPlugin(void);

  //!
  virtual ~XNATPlugin(void);

private:
  //! Purposely not implemented
  XNATPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  XNATProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  XNATWidgetCollective::Pointer m_Widgets;
};

#endif // _XNATPlugin_H
