/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ContouringWidgetProcessor_H
#define _ContouringWidgetProcessor_H

#include "coreBaseProcessor.h"

namespace contouringPlugin {

/**
Processor for ...

\ingroup ContouringPlugin
*/
class ContouringWidgetProcessor : public Core::BaseProcessor {
public:
  typedef itk::Image< float, 3 > ImageType;

  typedef enum {
    REFERENCE_IMAGE, // INPUT_0,
    // CONTOUR, //INPUT_1,
    NUMBER_OF_INPUTS
  } INPUT_TYPE;

  typedef enum {
    CONTOUR, /// OUTPUT_0,
    // OUTPUT_1,
    // OUTPUT_2,
    OUTPUTS_NUMBER
  } OUTPUT_TYPE;

public:
  //!
  coreProcessor(ContouringWidgetProcessor, Core::BaseProcessor);

  //! Call library to perform operation
  void Update();

private:
  //!
  ContouringWidgetProcessor();

  //!
  ~ContouringWidgetProcessor();

  //! Purposely not implemented
  ContouringWidgetProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);
}; // class ContouringWidgetProcessor

} // namespace contouringPlugin

#endif //_ContouringPluginContouringWidgetProcessor_H
