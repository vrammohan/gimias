/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportToolbar.h"
#include "coreKernel.h"
#include "corePluginTab.h"
#include "coreSettings.h"
#include "coreDirectory.h"

//#include ".xpm"
#include "capture_screen.xpm"
#include "report_icon.xpm"
#include "new_comment_icon.xpm"
#include "wxCaptureWindow.h"
#include "NewClinicalReportImageDialog.h"
#include "NewClinicalReportCommentDialog.h"
#include "ClinicalReportWidget.h"
#include "ClinicalReportPluginUtilities.h"

// xml
#include "tinyxml.h"

// Boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ClinicalReportToolbar, ToolbarBase)
EVT_TOOL(wxID_ADD_SCREENSHOT_TO_CLINICAL_REPORT,
         Core::Widgets::ClinicalReportToolbar::OnAddClinicalReportElement)
EVT_TOOL(wxID_ADD_COMMENT_TO_CLINICAL_REPORT,
         Core::Widgets::ClinicalReportToolbar::OnAddClinicalReportComment)
EVT_TOOL(wxID_OPEN_CLINICAL_REPORT_DESIGNER,
         Core::Widgets::ClinicalReportToolbar::OnOpenClinicalReportDesigner)
END_EVENT_TABLE()

Core::Widgets::ClinicalReportToolbar::ClinicalReportToolbar(wxWindow *parent, int id,
                                                            const wxPoint &pos,
                                                            const wxSize &size,
                                                            long style,
                                                            const wxString &name)
    : ToolbarBase(parent, id, pos, size, style, name) {
  wxBitmap bitmapCaptureScreen;
  wxBitmap bitmapNewComment;
  wxBitmap bitmapClinicalReportDesigner;

  m_Processor = ClinicalReportProcessor::New();

  bitmapCaptureScreen = wxBitmap(capture_screen_xpm);

  AddTool(wxID_ADD_SCREENSHOT_TO_CLINICAL_REPORT, _T("Add screenshot to clinical report"),
          bitmapCaptureScreen, _T("Add screenshot to clinical report"), wxITEM_NORMAL);

  bitmapNewComment = wxBitmap(new_comment_icon_xpm);

  AddTool(wxID_ADD_COMMENT_TO_CLINICAL_REPORT, _T("Add comment to clinical report"),
          bitmapNewComment, _T("Add comment to clinical report"), wxITEM_NORMAL);

  bitmapClinicalReportDesigner = wxBitmap(report_icon_xpm);
  AddTool(wxID_OPEN_CLINICAL_REPORT_DESIGNER, _T("Open clinical report designer"),
          bitmapClinicalReportDesigner, _T("Open clinical report designer"),
          wxITEM_NORMAL);

  AddSeparator();

  Realize();
}

void Core::Widgets::ClinicalReportToolbar::OnOpenClinicalReportDesigner(
    wxCommandEvent &event) {
  ClinicalReportPluginUtilities::OpenClinicalReportDesigner(m_Processor);
}

void Core::Widgets::ClinicalReportToolbar::OnAddClinicalReportComment(
    wxCommandEvent &event) {
  Core::Widgets::NewClinicalReportCommentDialog *commentDialog =
      new Core::Widgets::NewClinicalReportCommentDialog();
  commentDialog->SetProcessor(m_Processor);
  commentDialog->LoadReports();

  if (commentDialog->ShowModal() == wxID_OK) {
    std::string comments;
    comments = commentDialog->GetComments();

    AddTextElementToClinicalReport(commentDialog->m_ReportPath, comments);
  }
  if (commentDialog != NULL)
    delete commentDialog;
}

void Core::Widgets::ClinicalReportToolbar::OnAddClinicalReportElement(
    wxCommandEvent &event) {
  std::vector< std::string > tempFiles;

  RenderWindowContainer *window;
  window =
      dynamic_cast< RenderWindowContainer * >(GetPluginTab()->GetCurrentWorkingArea());

  std::vector< std::string > Screenshots;
  std::vector< std::string > ThumbnailsMedium;
  std::vector< std::string > ThumbnailsSmall;

  NewClinicalReportImageDialog *DialogNewClinicalReportImage =
      new NewClinicalReportImageDialog();
  DialogNewClinicalReportImage->SetProcessor(m_Processor);
  DialogNewClinicalReportImage->LoadReports();

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string tmpDirPath =
      settings->GetProjectHomePath() + Core::IO::SlashChar + "tmp" + Core::IO::SlashChar;

  Core::IO::Directory::Pointer tmpDir = Core::IO::Directory::New();
  tmpDir->SetDirNameFullPath(tmpDirPath);

  if (!tmpDir->Exists()) {
    tmpDir->Create();
  }

  std::vector< wxWindow * > windows;

  if (window) {
    wxCaptureWindow(window, tmpDirPath + "screenshot1.png");
    wxCaptureWindowThumbnail(window, tmpDirPath + "thumbnailmedium1.png", 128, 128, true);
    wxCaptureWindowThumbnail(window, tmpDirPath + "thumbnailsmall1.png", 64, 64, true);

    tempFiles.push_back(tmpDirPath + "screenshot1.png");
    tempFiles.push_back(tmpDirPath + "thumbnailmedium1.png");
    tempFiles.push_back(tmpDirPath + "thumbnailsmall1.png");

    Screenshots.push_back(tmpDirPath + "screenshot1.png");
    ThumbnailsMedium.push_back(tmpDirPath + "thumbnailmedium1.png");
    ThumbnailsSmall.push_back(tmpDirPath + "thumbnailsmall1.png");

    DialogNewClinicalReportImage->AddImage(tmpDirPath + "thumbnailmedium1.png");

    windows.push_back(window);
  }

  wxWindow *windowSignalViewer;
  windowSignalViewer =
      dynamic_cast< wxWindow * >(GetPluginTab()->GetWindow("Signal viewer"));

  if (windowSignalViewer && windowSignalViewer->IsShown()) {
    wxCaptureWindow(windowSignalViewer, tmpDirPath + "screenshotSV1.png");
    wxCaptureWindowThumbnail(windowSignalViewer, tmpDirPath + "thumbnailmediumSV1.png",
                             128, 128, true);
    wxCaptureWindowThumbnail(windowSignalViewer, tmpDirPath + "thumbnailsmallSV1.png", 64,
                             64, true);

    tempFiles.push_back(tmpDirPath + "screenshotSV1.png");
    tempFiles.push_back(tmpDirPath + "thumbnailmediumSV1.png");
    tempFiles.push_back(tmpDirPath + "thumbnailsmallSV1.png");

    Screenshots.push_back(tmpDirPath + "screenshotSV1.png");
    ThumbnailsMedium.push_back(tmpDirPath + "thumbnailmediumSV1.png");
    ThumbnailsSmall.push_back(tmpDirPath + "thumbnailsmallSV1.png");

    DialogNewClinicalReportImage->AddImage(tmpDirPath + "thumbnailmediumSV1.png");

    windows.push_back(windowSignalViewer);
  }

  wxWindow *windowShapeComparison;
  windowShapeComparison =
      dynamic_cast< wxWindow * >(GetPluginTab()->GetWindow("Compare shapes"));

  if (windowShapeComparison && windowShapeComparison->IsShown()) {
    wxCaptureWindow(windowShapeComparison, tmpDirPath + "screenshotSC1.png");
    wxCaptureWindowThumbnail(windowShapeComparison, tmpDirPath + "thumbnailmediumSC1.png",
                             128, 128, true);
    wxCaptureWindowThumbnail(windowShapeComparison, tmpDirPath + "thumbnailsmallSC1.png",
                             64, 64, true);

    tempFiles.push_back(tmpDirPath + "screenshotSC1.png");
    tempFiles.push_back(tmpDirPath + "thumbnailmediumSC1.png");
    tempFiles.push_back(tmpDirPath + "thumbnailsmallSC1.png");

    Screenshots.push_back(tmpDirPath + "screenshotSC1.png");
    ThumbnailsMedium.push_back(tmpDirPath + "thumbnailmediumSC1.png");
    ThumbnailsSmall.push_back(tmpDirPath + "thumbnailsmallSC1.png");

    DialogNewClinicalReportImage->AddImage(tmpDirPath + "thumbnailmediumSC1.png");

    windows.push_back(windowShapeComparison);
  }

  wxWindow *windowThumbnailWidget;
  windowThumbnailWidget =
      dynamic_cast< wxWindow * >(GetPluginTab()->GetWindow("Similar aneurysms"));

  if (windowThumbnailWidget && windowThumbnailWidget->IsShown()) {
    wxCaptureWindow(windowThumbnailWidget, tmpDirPath + "screenshotTW1.png");
    wxCaptureWindowThumbnail(windowThumbnailWidget, tmpDirPath + "thumbnailmediumTW1.png",
                             128, 128, true);
    wxCaptureWindowThumbnail(windowThumbnailWidget, tmpDirPath + "thumbnailsmallTW1.png",
                             64, 64, true);

    tempFiles.push_back(tmpDirPath + "screenshotTW1.png");
    tempFiles.push_back(tmpDirPath + "thumbnailmediumTW1.png");
    tempFiles.push_back(tmpDirPath + "thumbnailsmallTW1.png");

    Screenshots.push_back(tmpDirPath + "screenshotTW1.png");
    ThumbnailsMedium.push_back(tmpDirPath + "thumbnailmediumTW1.png");
    ThumbnailsSmall.push_back(tmpDirPath + "thumbnailsmallTW1.png");

    DialogNewClinicalReportImage->AddImage(tmpDirPath + "thumbnailmediumTW1.png");

    windows.push_back(windowThumbnailWidget);
  }

  std::vector< std::string > comments;
  std::vector< std::string > titles;
  std::vector< bool > selected;

  if (DialogNewClinicalReportImage->ShowModal() == wxID_OK) {
    comments = DialogNewClinicalReportImage->GetComments();
    titles = DialogNewClinicalReportImage->GetTitles();
    selected = DialogNewClinicalReportImage->GetSelected();

    if (DialogNewClinicalReportImage->GetMergeAllInOne()) {
      wxCaptureWindowsAndMerge(windows, tmpDirPath + "screenshotmerged1.png", false, 0, 0,
                               false);
      wxCaptureWindowsAndMerge(windows, tmpDirPath + "screenshotmergedmedium1.png", true,
                               128, 128, true);
      wxCaptureWindowsAndMerge(windows, tmpDirPath + "screenshotmergedsmall1.png", true,
                               64, 64, true);

      tempFiles.push_back(tmpDirPath + "screenshotmerged1.png");
      tempFiles.push_back(tmpDirPath + "screenshotmergedmedium1.png");
      tempFiles.push_back(tmpDirPath + "screenshotmergedsmall1.png");

      std::string AllComments = "";
      std::string AllTitles = "";

      for (int i = 0; i < comments.size(); i++) {
        AllTitles = titles[0];
        AllComments += comments[i] + "\n";
      }

      AddElementsToClinicalReport(
          DialogNewClinicalReportImage->m_ReportPath, std::vector< bool >(1, true),
          std::vector< std::string >(1, AllComments),
          std::vector< std::string >(1, AllTitles),
          std::vector< std::string >(1, tmpDirPath + "screenshotmergedsmall1.png"),
          std::vector< std::string >(1, tmpDirPath + "screenshotmergedmedium1.png"),
          std::vector< std::string >(1, tmpDirPath + "screenshotmerged1.png"), false,
          GetLastSelectedFromConfig(), DialogNewClinicalReportImage->m_ReportAuthors);
    } else {
      AddElementsToClinicalReport(DialogNewClinicalReportImage->m_ReportPath, selected,
                                  comments, titles, ThumbnailsSmall, ThumbnailsMedium,
                                  Screenshots, false, GetLastSelectedFromConfig(),
                                  DialogNewClinicalReportImage->m_ReportAuthors);
    }
  }

  // delete temporary files
  for (int i = 0; i < tempFiles.size(); i++) {
    std::string file = tempFiles[i];
    remove(path(file));
  }
  if (DialogNewClinicalReportImage != NULL)
    delete DialogNewClinicalReportImage;
}

std::string Core::Widgets::ClinicalReportToolbar::GetLastSelectedFromConfig() {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string reportsFolder =
      settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  std::string ConfigFilePath = reportsFolder + "/" + "config.xml";

  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);
  TiXmlElement *Config;

  doc = TiXmlDocument(ConfigFilePath);
  if (!doc.LoadFile())
    return "";

  Config = hDoc.FirstChildElement().Element();
  if (!Config)
    return "";

  return Config->Attribute("LastSelectedReport");
}

void Core::Widgets::ClinicalReportToolbar::SetLastSelectedToConfig(
    std::string newSelected) {

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string reportsFolder =
      settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  std::string ConfigFilePath = reportsFolder + "/" + "config.xml";

  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);
  TiXmlElement *Config;

  doc = TiXmlDocument(ConfigFilePath);
  if (!doc.LoadFile())
    return;

  Config = hDoc.FirstChildElement().Element();
  if (!Config)
    return;

  Config->SetAttribute("LastSelectedReport", newSelected);

  doc.SaveFile(ConfigFilePath);
}

void Core::Widgets::ClinicalReportToolbar::Init() {}

void Core::Widgets::ClinicalReportToolbar::UpdateState() {}

void Core::Widgets::ClinicalReportToolbar::AddTextElementToClinicalReport(
    std::string ReportPath, std::string comments) {
  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);
  TiXmlElement *ClinicalReport;

  doc = TiXmlDocument(ReportPath);
  if (!doc.LoadFile())
    return;

  ClinicalReport = hDoc.FirstChildElement().Element();
  if (!ClinicalReport)
    return;

  int numberOfElements = 0;

  for (TiXmlElement *elem = ClinicalReport->FirstChildElement(); elem;
       elem = elem->NextSiblingElement()) {
    numberOfElements++;
  }

  TiXmlElement *Element = new TiXmlElement("Element");

  ClinicalReport->LinkEndChild(Element);

  Element->SetAttribute("position", numberOfElements + 1);
  Element->SetAttribute("selected", "1");
  Element->SetAttribute("type", "text");
  Element->SetAttribute("description", comments);
  Element->SetAttribute("title", "");

  doc.SaveFile(ReportPath);
}

void Core::Widgets::ClinicalReportToolbar::AddElementsToClinicalReport(
    std::string ReportPath, std::vector< bool > _selectedV,
    std::vector< std::string > _commentsV, std::vector< std::string > _titlesV,
    std::vector< std::string > _thumbnailSmallV,
    std::vector< std::string > _thumbnailMediumV, std::vector< std::string > _screenshotV,
    bool overwrite, std::string name, std::string authors) {
  std::string ReportDir = ReportPath;
  bool slash_found = false;

  while (ReportDir.size() > 0) {
    size_t j = ReportDir.size() - 1;

    slash_found = (ReportDir[j] == Core::IO::SlashChar);
    if (slash_found)
      break;

    ReportDir = ReportDir.substr(0, j);
  }

  TiXmlDocument doc;
  TiXmlHandle hDoc(&doc);
  TiXmlElement *ClinicalReport;

  doc = TiXmlDocument(ReportPath);
  if (!doc.LoadFile())
    return;

  ClinicalReport = hDoc.FirstChildElement().Element();
  if (!ClinicalReport)
    return;

  int numberOfElements = 0;

  for (TiXmlElement *elem = ClinicalReport->FirstChildElement(); elem;
       elem = elem->NextSiblingElement()) {
    numberOfElements++;
  }

  for (int i = 0; i < _commentsV.size(); i++) {
    if (!_selectedV[i])
      continue;

    TiXmlElement *Element = new TiXmlElement("Element");
    TiXmlElement *Screenshot = new TiXmlElement("Image");
    TiXmlElement *ThumbnailSmall = new TiXmlElement("Image");
    TiXmlElement *ThumbnailMedium = new TiXmlElement("Image");

    std::string _comments = _commentsV[i];
    std::string _title = _titlesV[i];
    std::string _thumbnailSmall = _thumbnailSmallV[i];
    std::string _thumbnailMedium = _thumbnailMediumV[i];
    std::string _screenshot = _screenshotV[i];

    ClinicalReport->LinkEndChild(Element);

    Element->SetAttribute("position", numberOfElements + 1);
    Element->SetAttribute("selected", "1");
    Element->SetAttribute("type", "image");
    Element->SetAttribute("description", _comments);
    Element->SetAttribute("title", _title);

    Element->LinkEndChild(Screenshot);
    Element->LinkEndChild(ThumbnailSmall);
    Element->LinkEndChild(ThumbnailMedium);

    Screenshot->SetAttribute("imagetype", "screenshot");
    ThumbnailSmall->SetAttribute("imagetype", "thumbnailsmall");
    ThumbnailMedium->SetAttribute("imagetype", "thumbnailmedium");

    std::string screenshotname = GetNextAvailableName(ReportDir);
    copy_file(_screenshot, ReportDir + screenshotname);
    Screenshot->SetAttribute("path", ReportDir + screenshotname);

    std::string thumbmediumname = GetNextAvailableName(ReportDir);
    copy_file(_thumbnailMedium, ReportDir + thumbmediumname);
    ThumbnailMedium->SetAttribute("path", ReportDir + thumbmediumname);

    std::string thumbsmallname = GetNextAvailableName(ReportDir);
    copy_file(_thumbnailSmall, ReportDir + thumbsmallname);
    ThumbnailSmall->SetAttribute("path", ReportDir + thumbsmallname);

    numberOfElements++;
  }

  doc.SaveFile(ReportPath);
}

std::string
Core::Widgets::ClinicalReportToolbar::GetNextAvailableName(std::string basedir) {
  std::string ret;

  int i = 1;
  ret = GetString(i);

  bool finish = false;

  while (!finish) {
    ret = GetString(++i);

    // check if this filename already exists in basedir
    bool found = false;

    path dir_path(basedir);

    directory_iterator end_itr; // default construction yields past-the-end
    for (directory_iterator itr(dir_path); itr != end_itr; ++itr) {
      if (itr->path().filename() == ret) {
        found = true;
      }
    }

    finish = !found;
  }

  return ret;
}

std::string Core::Widgets::ClinicalReportToolbar::GetString(int i) {
  std::string StringInt = "";

  int n = i;

  while (n > 0) {
    StringInt = std::string(1, n % 10 + '0') + StringInt;
    n /= 10;
  }

  while (StringInt.size() < 4) {
    StringInt = "0" + StringInt;
  }

  return "CR" + StringInt + ".png";
}

Core::BaseProcessor::Pointer Core::Widgets::ClinicalReportToolbar::GetProcessor() {
  return m_Processor.GetPointer();
}

void Core::Widgets::ClinicalReportToolbar::SetProcessor(
    ClinicalReportProcessor::Pointer processor) {
  m_Processor = processor;
}
