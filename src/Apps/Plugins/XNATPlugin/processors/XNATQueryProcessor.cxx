/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATQueryProcessor.h"
#include <itksys/RegularExpression.hxx>

XNATQueryProcessor::XNATQueryProcessor() {
  SetName("XNATQueryProcessor");

  BaseProcessor::SetNumberOfInputs(NUMBEROFINPUTS);
  BaseProcessor::SetNumberOfOutputs(NUMBEROFOUTPUTS);

  GetOutputPort(OUTPUT_XNAT_QUERY_RESULTS)->SetDataEntityType(Core::NumericDataTypeId);
}

XNATQueryProcessor::~XNATQueryProcessor() {}

void XNATQueryProcessor::SetParameters(XNATQueryParameters params) { m_Params = params; }

std::vector< std::map< std::string, std::string > > XNATQueryProcessor::GetSubjects() {
  return m_Subjects;
}

std::vector< std::map< std::string, std::string > >
XNATQueryProcessor::GetFilteredSubjects() {
  return m_FilteredSubjects;
}

std::vector< std::map< std::string, std::string > > XNATQueryProcessor::FilterSubjects(
    std::map< std::string, std::string > restrictions,
    std::vector< std::map< std::string, std::string > > subjects) {
  std::vector< std::map< std::string, std::string > > ret;

  for (int i = 0; i < subjects.size(); i++) {
    std::map< std::string, std::string > subject;
    subject = subjects[i];

    std::map< std::string, std::string >::iterator it;

    bool acceptSubject = true;

    for (it = subject.begin(); it != subject.end(); it++) {
      std::map< std::string, std::string >::iterator itRes;
      itRes = restrictions.find(it->first);

      if (itRes != restrictions.end()) // found in restriction
      {
        std::string expression = itRes->second;
        if (expression == "")
          expression = ".*";

        itksys::RegularExpression regEx(expression.c_str());

        if (!regEx.find(it->second)) // no matching
        {
          acceptSubject = false;
          break;
        }
      }
    }
    if (acceptSubject)
      ret.push_back(subject);
  }

  return ret;
}

void XNATQueryProcessor::AddTagMap(
    Core::DataEntity::Pointer dataEntity,
    std::vector< std::map< std::string, std::string > > subjects) {
  blTagMap::Pointer tagMap = blTagMap::New();

  for (int i = 0; i < subjects.size(); i++) {
    std::map< std::string, std::string > subject = subjects[i];
    std::map< std::string, std::string >::iterator it;

    blTagMap::Pointer tagMapSubject = blTagMap::New();

    for (it = subject.begin(); it != subject.end(); it++) {
      tagMapSubject->AddTag(it->first, it->second);
    }

    tagMap->AddTag(subject["label"], tagMapSubject);
  }

  dataEntity->AddTimeStep(tagMap);
}

void XNATQueryProcessor::PrintSubjects(
    std::vector< std::map< std::string, std::string > > subjects) {
  for (int i = 0; i < subjects.size(); i++) {
    std::map< std::string, std::string > subject = subjects[i];
    std::map< std::string, std::string >::iterator it;

    for (it = subject.begin(); it != subject.end(); it++) {
      std::cout << it->first << " " << it->second << std::endl;
    }
  }
}

void XNATQueryProcessor::Update() {
  try {
    XNAT::Utils client(m_Params.usr, m_Params.pwd, m_Params.siteUrl);

    m_Subjects = client.getSubjectsColumns(m_Params.GetColumns());

    m_FilteredSubjects = FilterSubjects(m_Params.restrictions, m_Subjects);

    m_DataEntity = Core::DataEntity::New();
    m_DataEntity->SetType(Core::NumericDataTypeId);
    m_DataEntity->GetMetadata()->SetName("XNAT Subjects");

    AddTagMap(m_DataEntity, m_Subjects);

    m_FilteredDataEntity = Core::DataEntity::New();
    m_FilteredDataEntity->SetType(Core::NumericDataTypeId);
    m_FilteredDataEntity->GetMetadata()->SetName("XNAT Query Results");

    AddTagMap(m_FilteredDataEntity, m_FilteredSubjects);

    UpdateOutput(OUTPUT_XNAT_ALL_SUBJECTS, m_DataEntity);
    UpdateOutput(OUTPUT_XNAT_QUERY_RESULTS, m_FilteredDataEntity, m_DataEntity);
  } catch (...) {

    throw;
  }
}