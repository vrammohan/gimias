/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "AhePluginConfigurationProcessor.h"

#include <string>
#include <iostream>
#include <stdexcept>
#include "coreReportExceptionMacros.h"
#include "coreKernel.h"
#include "coreSettings.h"

namespace ahePlugin {

/**
 *  Class constructor.
 *  Automatically loads the AHE nodes for the configuration file into a map.
 */
ConfigurationProcessor::ConfigurationProcessor() {
  SetName("AhePluginProcessor");
  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();
  blTagMap::Pointer properties;
  properties = settings->GetPluginProperties("AhePlugin");
  if (properties.IsNotNull()) {
    blTagMap::Iterator it;
    for (it = properties->GetIteratorBegin(); it != properties->GetIteratorEnd(); it++) {
      blTagMap::Pointer tagNode;
      if (it->second->GetValue(tagNode)) {
        try {
          aheAppInst::Pointer appInst = aheAppInst::New();
          aheHost::Pointer node = aheHost::New();
          aheApp::Pointer app = aheApp::New();
          node->SetHostname(tagNode->FindTagByName("hostname")->GetValueAsString());
          node->SetUsername(tagNode->FindTagByName("username")->GetValueAsString());
          node->SetPort(tagNode->FindTagByName("port")->GetValueCasted< long >());
          app->SetResourceName(
              tagNode->FindTagByName("resourcename")->GetValueAsString());
          app->SetName(tagNode->FindTagByName("appname")->GetValueAsString());
          appInst->SetApp(app);
          appInst->SetHost(node);
          Add(appInst);
        }
        coreCatchExceptionsReportAndNoThrowMacro(
            ConfigurationProcessor::ConfigurationProcessor)
      }
    }
  }
}

/**
 *  Class destructor.
 *  Saves AHE nodes configuration.
 */
ConfigurationProcessor::~ConfigurationProcessor() { Update(); }

/**
 *  Saves the AHE nodes into the configuration.
 */
void ConfigurationProcessor::Update() {

  Core::Runtime::Settings::Pointer settings;
  settings = Core::Runtime::Kernel::GetApplicationSettings();

  blTagMap::Pointer properties = blTagMap::New();
  if (properties.IsNotNull()) {
    NodeMapType::iterator it;
    for (it = m_NodeMap.begin(); it != m_NodeMap.end(); it++) {
      blTagMap::Pointer tagNode = blTagMap::New();
      tagNode->AddTag("hostname", it->second->GetHost()->GetHostname());
      tagNode->AddTag("username", it->second->GetHost()->GetUsername());
      tagNode->AddTag("port", it->second->GetHost()->GetPort());
      tagNode->AddTag("resourcename", it->second->GetApp()->GetResourceName());
      tagNode->AddTag("appname", it->second->GetApp()->GetName());
      // The key is a string in the format: hostname|appname|resourcename. See the
      // ConfigurationProcessor::Add( aheAppInst::Pointer node ) method.
      std::string key = it->second->GetHost()->GetHostname() + "|" +
                        it->second->GetApp()->GetName() + "|" +
                        it->second->GetApp()->GetResourceName();
      properties->AddTag(key, tagNode);
    }
  }

  settings->SetPluginProperties("AhePlugin", properties);
}

/**
 *  Provides access to the AHE node map.
 */
ConfigurationProcessor::NodeMapType ConfigurationProcessor::GetNodeMap() const {
  return m_NodeMap;
}

/**
 *  Adds a new node to the AHE node map.
 */
void ConfigurationProcessor::Add(aheAppInst::Pointer node) {
  std::string key = node->GetHost()->GetHostname() + "|" + node->GetApp()->GetName() +
                    "|" + node->GetApp()->GetResourceName();
  m_NodeMap[key] = node;
  Update();
}

/**
 *  Returns a node in the AHE node map.
 *
 *  The key is a string in the format: hostname|appname|resourcename. See the
 * ConfigurationProcessor::Add( aheAppInst::Pointer node ) method.
 */
aheAppInst::Pointer ConfigurationProcessor::Get(const std::string &key) {
  NodeMapType::iterator it = m_NodeMap.find(key);
  if (it == m_NodeMap.end()) {
    return NULL;
  }

  return it->second;
}

/**
 *  Removes a node in the AHE node map.
 *
 *  The key is a string in the format: hostname|appname|resourcename. See the
 * ConfigurationProcessor::Add( aheAppInst::Pointer node ) method.
 */
void ConfigurationProcessor::Remove(const std::string &key) {
  NodeMapType::iterator it = m_NodeMap.find(key);
  if (it == m_NodeMap.end()) {
    std::ostringstream strError;
    strError << "Entry not found: " << key;
    throw std::runtime_error(strError.str());
  }

  m_NodeMap.erase(it);
  Update();
}

/**
 *  Removes all nodes in the AHE node map.
 */
void ConfigurationProcessor::RemoveAll() {
  m_NodeMap.clear();
  Update();
}

} // namespace ahePlugin
