/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "StatisticsWidgetCollective.h"
#include "StatisticsPluginUtilities.h"
#include "StatisticsToolbar.h"
#include "StatisticsWorkingArea.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTabFactory.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSimpleProcessingWidget.h"
#include "coreWindowConfig.h"
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreDirectory.h"

StatisticsWidgetCollective::StatisticsWidgetCollective() {}

void StatisticsWidgetCollective::Init() {

  Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();

  gIface->RegisterFactory(
      Core::Widgets::StatisticsToolbar::Factory::NewBase(),
      Core::WindowConfig().Toolbar().Top().Show().Caption("Statistics Toolbar"));

  gIface->RegisterFactory(
      StatisticsWorkingArea::Factory::NewBase(),
      Core::WindowConfig().WorkingArea().Caption("Statistics Working Area"));
}
