/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SecurityPluginUserManager.h"
//#include "SecurityPluginCryptoProcessor.h"
#include "MasterPasswordDefinition.h"

//--------------------------------------------------------------------------
SecurityPluginUserManager::SecurityPluginUserManager(
    SecurityPluginCryptoProcessor::Pointer userManager, wxWindow *parent, int id,
    const wxPoint &pos /*=wxDefaultPosition*/, const wxSize &size /*=wxDefaultSize*/,
    long style /*=0*/)
    : SecurityPluginUserManagerUI(parent, id) //, title), pos, size, style )
//--------------------------------------------------------------------------
{
  this->m_MPManager = userManager;
  this->m_btnChangeMasterPassword->Disable();

  // hide all elements refered to node managment
  this->m_txtCtrSearch->Hide();
  this->m_gridUserList->Hide();
  this->m_btnAdd->Hide();
  this->m_btnRemove->Hide();
  this->sizer_5_staticbox->Hide();
};

//--------------------------------------------------------------------------
void SecurityPluginUserManager::UpdateData()
//--------------------------------------------------------------------------
{}

//--------------------------------------------------------------------------
void SecurityPluginUserManager::UpdateWidget()
//--------------------------------------------------------------------------
{
  /*
  //map.insert(std::pair<int, std::string>(1, this->m_MPManager->m_MasterKey));
  //delete columns and rows on grid
  int numberRows = this->m_gridUserList->GetNumberRows();
  if(numberRows > 0)
          this->m_gridUserList->DeleteRows(0,numberRows);

  std::map<std::string,std::string>::iterator itUserMap =
  this->m_MPManager->m_RegisterPasswords.begin();

  while(itUserMap != this->m_MPManager->m_RegisterPasswords.end())
  {
          this->m_gridUserList->AppendRows();
          this->m_gridUserList->SetCellValue(
                  this->m_gridUserList->GetNumberRows()-1,
                  0,
                  itUserMap->first.c_str()
                  );
          if(this->GetMPManager()->Decrypt(itUserMap->second.c_str()).length())
                  this->m_gridUserList->SetCellValue(this->m_gridUserList->GetNumberRows()-1,1,this->GetMPManager()->Decrypt(itUserMap->second.c_str()));

          else
                  this->m_gridUserList->SetCellValue(this->m_gridUserList->GetNumberRows()-1,1,itUserMap->second.c_str());
          itUserMap++;
  }
  */
}

//--------------------------------------------------------------------------
void SecurityPluginUserManager::OnBtnAdd(wxCommandEvent &event)
//--------------------------------------------------------------------------
{
  /*
  std::string b;
  if(
          this->m_txtUser->GetValue().Length()
          && this->m_txtPassword->GetValue().Length()
          )
  {
          this->m_userMap.insert( std::pair<std::string,
  std::string>(m_txtUser->GetValue().c_str(),this->m_txtPassword->GetValue().c_str()) );
          this->UpdateWidget();


          try{
                  SecurityPluginCryptoProcessor::Pointer ptr =
  SecurityPluginCryptoProcessor::New();
                  ptr->SetMasterKey("TEST");

                  std::string a = ptr->Encrypt(this->m_txtPassword->GetValue().c_str());
                  ptr->SetMasterKey("TSET");
                  b = ptr->Decrypt(a);


                  this->m_userMap.insert( std::pair<std::string, std::string>(b,b ));
                  this->UpdateWidget();
          }catch(...)
          {
                  b = "";

          }
          this->m_userMap.insert( std::pair<std::string, std::string>(b,b ));
          this->UpdateWidget();
  }
  */
}

//--------------------------------------------------------------------------
void SecurityPluginUserManager::OnBtnRemove(wxCommandEvent &event)
//--------------------------------------------------------------------------
{
  /*
  const wxArrayInt& rows(this->m_gridUserList->GetSelectedRows());
  for (size_t i = 0; i < rows.size(); ++i)
  {
          std::map<std::string,std::string>::iterator it =
  this->m_userMap.find(this->m_gridUserList->GetCellValue(rows[i],0).c_str());
          if(it->first.c_str() == this->m_actPass.at(0))
                  this->m_actPass.at(0) = ""; this->m_actPass.at(1) = "";
          this->m_userMap.erase(it);
  }
  this->UpdateWidget();
  */
}

//--------------------------------------------------------------------------
void SecurityPluginUserManager::OnBtnLeftClick(wxGridEvent &event)
//--------------------------------------------------------------------------
{
  /*
  if(event.GetCol() == 1)
  {
          //user
          m_actPass.at(0) = this->m_gridUserList->GetCellValue(event.GetRow(),0).c_str();
          //finds password on map and insert it on actPass
          std::map<std::string,std::string>::iterator it =
  this->m_userMap.find(m_actPass.at(0).c_str());
          m_actPass.at(1) = it->second.c_str();
          this->UpdateWidget();
  }
  */
}

//--------------------------------------------------------------------------
void SecurityPluginUserManager::OnCheckBoxOK(wxCommandEvent &event)
//--------------------------------------------------------------------------
{
  try {
    if (this->m_checkBoxUseMasterPassword->IsChecked()) {
      // option change master Password Enable
      this->m_btnChangeMasterPassword->Enable();

      // create master Password definition window
      MasterPasswordDefinition dialog(this, wxID_ANY, "");

      // set window name
      dialog.SetTitle("Edit Master Password");

      // Hides non-used options
      dialog.HideRdBox();
      dialog.HideActualMP();
      // center window
      dialog.Center();

      // button add is ok
      if (dialog.ShowModal() == wxID_BTN_ADD) {
        if (!strcmp(dialog.GetTxtNewMasterPassword().c_str(),
                    dialog.GetTxtRepMasterPassword().c_str()) &&
            dialog.GetTxtRepMasterPassword().length())
          this->m_MPManager->SetMasterKey(dialog.GetTxtNewMasterPassword());
        else {
          this->m_MPManager->ResetMasterKey();
          // option change master Password Disable
          this->m_btnChangeMasterPassword->Disable();
          // check button off
          this->m_checkBoxUseMasterPassword->SetValue(false);

          throw std::runtime_error(
              std::string("Security Plugin"
                          " Bad Definition, try to define a new Master Password."));
        }
      } else {
        this->m_MPManager->ResetMasterKey();
        this->m_checkBoxUseMasterPassword->SetValue(false);
        // option change master Password Disable
        this->m_btnChangeMasterPassword->Disable();
      }
    } else {
      this->m_MPManager->ResetMasterKey();
      // option change master Password Disable
      this->m_btnChangeMasterPassword->Disable();
    }

    this->UpdateWidget();
  }
  coreCatchExceptionsReportAndNoThrowMacro(SecurityPluginUserManager::OnCheckBoxOK)
}

//--------------------------------------------------------------------------
void SecurityPluginUserManager::OnBtnChangeMasterPassword(wxCommandEvent &event)
//--------------------------------------------------------------------------
{
  try {
    // create master Password definition window
    MasterPasswordDefinition dialog(this, wxID_ANY, "");

    // set window name
    dialog.SetTitle("Edit Master Password");

    dialog.Center();

    // button add is ok
    if (dialog.ShowModal() == wxID_BTN_ADD) {
      // Hides non-used options
      if (dialog.GetRdBoxOption()) {
        if (this->m_MPManager->CheckMasterKey(dialog.GetTxtActiveMasterPassword()))
          if (!strcmp(dialog.GetTxtNewMasterPassword().c_str(),
                      dialog.GetTxtRepMasterPassword().c_str()))
            this->m_MPManager->UpdateMasterKey(dialog.GetTxtNewMasterPassword().c_str());
          else {
            throw std::runtime_error(std::string("Security Plugin"
                                                 " Bad Definition, keys doesn't match."));
          }
        else {
          throw std::runtime_error(std::string(
              "Security Plugin"
              " Wrong Active Master Key. Try to active a new one instead of changed it"));
        }
      } else {
        if (!strcmp(dialog.GetTxtNewMasterPassword().c_str(),
                    dialog.GetTxtRepMasterPassword().c_str()))
          this->m_MPManager->SetMasterKey(dialog.GetTxtNewMasterPassword());
        else {
          this->m_MPManager->ResetMasterKey();
          // option change master Password Disable
          this->m_btnChangeMasterPassword->Disable();

          throw std::runtime_error(std::string("Security Plugin"
                                               " Bad Definition, keys doesn't match."));
        }
      }
    }
    this->UpdateWidget();
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      SecurityPluginUserManager::OnBtnChangeMasterPassword)
}

//--------------------------------------------------------------------------
SecurityPluginCryptoProcessor::Pointer SecurityPluginUserManager::GetMPManager()
//--------------------------------------------------------------------------
{
  return this->m_MPManager;
}

//--------------------------------------------------------------------------
SecurityPluginUserManager::~SecurityPluginUserManager()
//--------------------------------------------------------------------------
{
  // We don't need to destroy anything because all the child windows
  // of this wxWindow are destroyed automatically
}