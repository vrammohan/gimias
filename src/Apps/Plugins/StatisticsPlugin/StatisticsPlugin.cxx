/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "StatisticsPlugin.h"

// core
#include "coreReportExceptionMacros.h"
#include "corePluginMacros.h"
#include "coreProfile.h"

coreBeginDefinePluginMacro(StatisticsPlugin) coreEndDefinePluginMacro()

    StatisticsPlugin::StatisticsPlugin(void)
    : FrontEndPlugin() {
  try {
    m_Processors = StatisticsProcessorCollective::New();
    m_Widgets = StatisticsWidgetCollective::New();

    m_Widgets->Init();
  }
  coreCatchExceptionsReportAndNoThrowMacro(StatisticsPlugin::StatisticsPlugin)
}

StatisticsPlugin::~StatisticsPlugin(void) {}
