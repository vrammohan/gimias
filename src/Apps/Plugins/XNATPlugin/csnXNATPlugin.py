# Used to configure XNATPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

XNATPlugin = GimiasPluginProject("XNATPlugin", api)

projects = [
	securityPlugin,
    gmCoreLight,
    xnat
]

XNATPlugin.AddProjects(projects)

XNATPlugin.AddSources(["*.cxx", "*.h"])
XNATPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
XNATPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "XNATToolbar",
  "XNATWorkingArea",
  "XNATTree",
  "XNATPluginConnectionPanelWidget"
]

XNATPlugin.AddWidgetModules(widgetModules)#, _useQt = 0)

XNATPlugin.SetPrecompiledHeader("XNATPluginPCH.h")

XNATPlugin.AddFilesToInstall(XNATPlugin.Glob("resource"), "resource")
