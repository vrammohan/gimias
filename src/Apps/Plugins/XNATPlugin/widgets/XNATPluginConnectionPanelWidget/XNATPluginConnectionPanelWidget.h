/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef XnatPluginConnectionPanelWidget_H
#define XnatPluginConnectionPanelWidget_H

#include "XNATListConnectionsDialogUI.h"
#include "XNATPluginUtilities.h"
#include "XNATPluginManageConnection.h"

#include "corePreferencesPage.h"
#include "blLightObject.h"
#include "CILabExceptionMacros.h"

#include <map>

#include "XNATPluginManageConnection.h"

/**
Configure all Panel Connections

\ingroup XNATPlugin
\author H�ctor Fern�ndez
\date Sept 2011
*/
class XNATPluginConnectionPanelWidget : public XNATListConnectionsDialogUI,
                                        public Core::Widgets::PreferencesPage {

  // OPERATIONS
public:
  //!
  // coreDefineBaseWindowFactory(XNATPluginConnectionPanelWidget);
  coreDefineBaseWindowFactory1param(XNATPluginConnectionPanelWidget,
                                    XNATPluginManageConnections::Pointer)

      //!
      XNATPluginConnectionPanelWidget(XNATPluginManageConnections::Pointer connManager,
                                      wxWindow *parent, int id,
                                      const wxPoint &pos = wxDefaultPosition,
                                      const wxSize &size = wxDefaultSize, long style = 0);

  //! When Ok is pressed
  virtual void UpdateData();

  //! When the dialog is shown
  virtual void UpdateWidget();

  //! Initializes params on GUI
  void OnInit();

  //!
  XNATPluginManageConnections::Pointer m_ConnectionsManager;

private:
  //!
  void OnAdd(wxCommandEvent &event);

  //!
  void OnRemove(wxCommandEvent &event);

  //!
  void OnEdit(wxCommandEvent &event);
};

#endif // XnatPluginConnectionPanelWidget_H
