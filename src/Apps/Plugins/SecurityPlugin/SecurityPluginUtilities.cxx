/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SecurityPluginUtilities.h"

// wxWidgets
#include <wx/treebook.h>

// Core
#include "coreWxMitkCoreMainWindow.h"
#include "corePreferencesDialog.h"

//-------------------------------------------------------------------------
SecurityPluginCryptoProcessor::Pointer SecurityPluginUtilities::GetMPManager()
//-------------------------------------------------------------------------
{
  // base Main Window
  Core::Widgets::BaseMainWindow *baseMainWindow;
  baseMainWindow = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow();
  // mainWindow
  Core::Widgets::wxMitkCoreMainWindow *mainWindow =
      dynamic_cast< Core::Widgets::wxMitkCoreMainWindow * >(baseMainWindow);

  if (mainWindow == NULL) {
    return NULL;
  }

  // preferences tree
  wxTreebook *trBkPreferences =
      mainWindow->GetPreferencesDialog()->GetPreferencesWindow();

  wxWindow *wdPreferences = NULL;

  if (trBkPreferences) {
    wdPreferences = trBkPreferences->FindWindow("Users Node Manager");
  }

  SecurityPluginCryptoProcessor::Pointer mpManager = NULL;

  if (wdPreferences) {
    SecurityPluginUserManager *secPlgWidget =
        dynamic_cast< SecurityPluginUserManager * >(wdPreferences);

    if (secPlgWidget) {
      mpManager = secPlgWidget->GetMPManager();
    }
  }

  return mpManager;
}
