/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATPluginEditConfigurationDialog.h"

// XNAT
#include "Core.h"
#include "Utils.h"

// UserManager
#include "XNATPluginUtilities.h"

// Poco
#include "Poco/HMACEngine.h"
#include "Poco/SHA1Engine.h"

using Poco::DigestEngine;
using Poco::HMACEngine;
using Poco::SHA1Engine;

/*test conection need a xnatClient class*/

XnatPluginEditConfigurationDialog::XnatPluginEditConfigurationDialog(
    wxWindow *parent, int id, const wxString &title,
    const wxPoint &pos /*=wxDefaultPosition*/, const wxSize &size /*=wxDefaultSize*/,
    long style /*=wxDEFAULT_DIALOG_STYLE*/)
    : XNATPluginEditConfigurationDialogUI(parent, id, title, pos, size, style) {}

void XnatPluginEditConfigurationDialog::OnBtnTest(wxCommandEvent &event) {
  try {
    XNAT::Core client;
    std::string URL = m_txtURL->GetValue().ToStdString();
    client.query(URL);
    wxMessageDialog dialog(NULL, "Connection tested successfully", "URL Test");
    dialog.ShowModal();
  }
  coreCatchExceptionsReportAndNoThrowMacro(XnatPluginEditConfigurationDialog::OnBtnTest)
}

void XnatPluginEditConfigurationDialog::OnInit() {
  m_Name, m_URL, m_User, m_Password = "";

  UpdateWidget();
}

void XnatPluginEditConfigurationDialog::UpdateWidget() {
  m_txtName->SetValue(m_Name.c_str());
  m_txtURL->SetValue(m_URL.c_str());
  m_txtUser->SetValue(m_User.c_str());
  m_txtPassword->SetValue(m_Password.c_str());
}

void XnatPluginEditConfigurationDialog::UpdateData() {
  m_Password = m_txtPassword->GetValue();
  m_Name = m_txtName->GetValue();
  m_URL = m_txtURL->GetValue();
  m_User = m_txtUser->GetValue();
}

void XnatPluginEditConfigurationDialog::OnOK(wxCommandEvent &event) {
  UpdateData();
  EndModal(wxID_BTN_OK);
}

void XnatPluginEditConfigurationDialog::OnCancel(wxCommandEvent &event) {
  EndModal(wxID_BTN_CANCEL);
}

std::string XnatPluginEditConfigurationDialog::GetConnectionName() const {
  return m_Name;
}

std::string XnatPluginEditConfigurationDialog::GetURL() const { return m_URL; }

std::string XnatPluginEditConfigurationDialog::GetUser() const { return m_User; }

std::string XnatPluginEditConfigurationDialog::GetPassword() const { return m_Password; }

void XnatPluginEditConfigurationDialog::SetConnectionName(std::string s) {
  m_Name = s;
  UpdateWidget();
}

void XnatPluginEditConfigurationDialog::SetURL(std::string s) {
  m_URL = s;
  UpdateWidget();
}

void XnatPluginEditConfigurationDialog::SetUser(std::string s) {
  m_User = s;
  UpdateWidget();
}

void XnatPluginEditConfigurationDialog::SetPassword(std::string s) {
  m_Password = s;
  UpdateWidget();
}
