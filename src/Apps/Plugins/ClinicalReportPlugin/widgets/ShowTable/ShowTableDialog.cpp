/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ShowTableDialog.h"

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ShowTableDialog, wxDialog)
END_EVENT_TABLE();

ShowTableDialog::ShowTableDialog() : ShowTableDialogUI(NULL, wxID_ANY, "") {
  try {
  }
  coreCatchExceptionsReportAndNoThrowMacro("ShowTableDialog::ShowTableDialog");
}

ShowTableDialog::~ShowTableDialog() {}

Core::BaseProcessor::Pointer ShowTableDialog::GetProcessor() { return NULL; }

void ShowTableDialog::SetTable(std::vector< std::string > header,
                               std::vector< std::vector< std::string > > lines) {
  for (int i = 0; i < header.size(); i++) {
    if (i == m_Grid->GetNumberCols()) {
      m_Grid->AppendCols(1);
    }

    m_Grid->SetColLabelValue(i, header[i]);
  }

  for (int i = 0; i < lines.size(); i++) {

    if (i == m_Grid->GetNumberRows()) {
      m_Grid->AppendRows(1);
    }

    for (int j = 0; j < lines[i].size(); j++) {
      m_Grid->SetCellValue(i, j, lines[i][j]);
    }
  }

  m_Grid->AutoSize();

  SetSize(GetBestSize());

  // Cast a resize event
  wxSizeEvent resEvent(m_Panel->GetSize(), m_Panel->GetId());
  resEvent.SetEventObject(m_Panel);
  m_Panel->GetEventHandler()->ProcessEvent(resEvent);

  FitInside();
}
