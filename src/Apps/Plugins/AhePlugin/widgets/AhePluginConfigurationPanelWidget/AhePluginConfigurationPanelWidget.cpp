/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "AhePluginConfigurationPanelWidget.h"
#include "AhePluginEditConfigurationDialog.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreFactoryManager.h"
#include "coreProcessorManager.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"

// Helper class
#include "wxAheNodeData.h"

namespace ahePlugin {

/**
 *  Class constructor.
 *  Creates the processor, which loads the AHE node configuration. Sets the panel's name.
 */
ConfigurationPanelWidget::ConfigurationPanelWidget(
    wxWindow *parent, int id /*= wxID_ANY*/, const wxPoint &pos /*= wxDefaultPosition*/,
    const wxSize &size /*= wxDefaultSize*/, long style /* = 0*/)
    : AhePluginConfigurationPanelWidgetUI(parent, id, pos, size, style) {
  m_Processor = ahePlugin::ConfigurationProcessor::New();

  SetName(AHE_CONF_PANEL_NAME);
}

/**
 *  Class destructor.
 */
ConfigurationPanelWidget::~ConfigurationPanelWidget() {
  // We don't need to destroy anything because all the child windows
  // of this wxWindow are destroyed automatically
}

/**
 *  Loads the panel's initial content and configuration.
 */
void ConfigurationPanelWidget::OnInit() { UpdateWidget(); }

/**
 *  Updates the list of AHE nodes
 */
void ConfigurationPanelWidget::UpdateWidget() {
  m_listNodes->Clear();
  ConfigurationProcessor::NodeMapType nodes = m_Processor->GetNodeMap();
  ConfigurationProcessor::NodeMapType::iterator it;
  for (it = nodes.begin(); it != nodes.end(); it++) {
    aheAppInst::Pointer node = it->second;
    m_listNodes->Append("* Host: " + node->GetHost()->GetHostname() + " - App: " +
                            node->GetApp()->GetName() + " - Rsrc: " +
                            node->GetApp()->GetResourceName(),
                        new wxAheNodeData(node));
  }
}

/**
 *  Updates the AHE nodes stored in the processor.
 */
void ConfigurationPanelWidget::UpdateData() {
  m_Processor->RemoveAll();
  for (int i = 0; i < m_listNodes->GetCount(); i++) {
    wxAheNodeData *aheNodeData;
    aheNodeData = static_cast< wxAheNodeData * >(m_listNodes->GetClientObject(i));
    m_Processor->Add(aheNodeData->m_Node);
  }
}

/**
 *  Returns the processor of this class
 */
Core::BaseProcessor::Pointer ConfigurationPanelWidget::GetProcessor() {
  return m_Processor.GetPointer();
}

/**
 *  Add AHE node event handler
 */
void ConfigurationPanelWidget::OnAdd(wxCommandEvent &event) {
  try {
    AhePluginEditConfigurationDialog dialog(this, wxID_ANY, "Add new AHE node");
    dialog.Center();
    if (dialog.ShowModal() == wxID_OK) {
      if (dialog.GetNode()->GetHost()->GetHostname().empty()) {
        throw Core::Exceptions::Exception("AhePluginEditConfigurationDialog::OnAdd",
                                          "Host name cannot be empty");
      }
      m_Processor->Add(dialog.GetNode());
      m_listNodes->Append("* Host: " + dialog.GetNode()->GetHost()->GetHostname() +
                              " - App: " + dialog.GetNode()->GetApp()->GetName() +
                              " - Rsrc: " + dialog.GetNode()->GetApp()->GetResourceName(),
                          new wxAheNodeData(dialog.GetNode()));
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(ConfigurationPanelWidget::OnAdd)
}

/**
 *  Remove AHE node event handler
 */
void ConfigurationPanelWidget::OnRemove(wxCommandEvent &event) {
  try {
    if (m_listNodes->GetSelection() != -1) {
      m_listNodes->Delete(m_listNodes->GetSelection());
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(ConfigurationPanelWidget::OnRemove)
}

/**
 *  Edit AHE node event handler
 */
void ConfigurationPanelWidget::OnEdit(wxCommandEvent &event) {
  try {
    if (m_listNodes->GetSelection() != -1) {
      wxAheNodeData *aheNodeData;
      aheNodeData = static_cast< wxAheNodeData * >(
          m_listNodes->GetClientObject(m_listNodes->GetSelection()));
      AhePluginEditConfigurationDialog dialog(this, wxID_ANY, "Edit AHE node");
      dialog.SetNodeData(aheNodeData->m_Node);
      dialog.Center();
      if (dialog.ShowModal() == wxID_OK) {
        if (dialog.GetNode()->GetHost()->GetHostname().empty()) {
          throw Core::Exceptions::Exception("AhePluginEditConfigurationDialog::OnEdit",
                                            "Host name cannot be empty");
        }
        aheNodeData->m_Node = dialog.GetNode();
        UpdateData();
        UpdateWidget();
      }
    }
  }
  coreCatchExceptionsReportAndNoThrowMacro(ConfigurationPanelWidget::OnEdit)
}

/**
 *  Double-click AHE node list event handler
 */
void ConfigurationPanelWidget::OnListDClick(wxCommandEvent &event) { OnEdit(event); }

} // namespace ahePlugin
