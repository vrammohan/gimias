;NSIS Modern User Interface
;Header Bitmap Example Script
;Written by Joost Verburg

;--------------------------------

  ;Include Modern UI
  !include "MUI2.nsh"
  ;Include architecture
  !include "x64.nsh"
  !include "FileFunc.nsh"
  !include "ZipDLL.nsh"

;--------------------------------
;General

  Name "Gimias-@PROJECT_NUMBER@"
  Caption "Gimias-@PROJECT_NUMBER@"
  !define ARCHITECTURE "@ARCHITECTURE@"
  !define GIMIAS_MAIN_VERSION "@MAIN_PROJECT_NUMBER@"
  !define GIMIAS_VERSION "@PROJECT_NUMBER@"
  !define GIMIAS_FOLDER "@GIMIAS_FOLDER@"
  !define TUTORIALS_FOLDER "@TUTORIALS_FOLDER@"
  !define GIMIAS_INSTALLER_RESOURCES "@GIMIAS_INSTALLER_RESOURCES@"
  !define VREDIST_FOLDER "@VREDIST_FOLDER@"
  !define APPNAME "Gimias-${GIMIAS_MAIN_VERSION}-${ARCHITECTURE}"

  OutFile "@OUT_FOLDER@\Gimias-${GIMIAS_VERSION}-${ARCHITECTURE}-setup.exe"
  Icon "${GIMIAS_INSTALLER_RESOURCES}\GIMIAS_Logo.ico"
  SetDateSave on
  SetDatablockOptimize on
  CRCCheck on
  SilentInstall normal
  XPStyle on
  AutoCloseWindow false
  ShowInstDetails show
  SetCompress auto

  ;Default installation folder
  InstallDir "$PROGRAMFILES\Gimias\${APPNAME}"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\${APPNAME}" "Install_Dir"

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin

;--------------------------------
;Variables

  Var StartMenuFolder

  
;--------------------------------
;Interface Configuration

  ;Display an image on the header of the page.
  !define MUI_HEADERIMAGE
  ;Bitmap image to display on the header of installers pages
  !define MUI_HEADERIMAGE_BITMAP "${GIMIAS_INSTALLER_RESOURCES}\GIMIAS_Installer.bmp"
  ;Bitmap image to display on the header of uninstaller pages
  !define MUI_HEADERIMAGE_UNBITMAP "${GIMIAS_INSTALLER_RESOURCES}\GIMIAS_Installer.bmp"
  !define MUI_ABORTWARNING
  ;The icon for the installer.
  !define MUI_ICON "${GIMIAS_INSTALLER_RESOURCES}\GIMIAS_Logo.ico"
  ;The icon for the uninstaller.
  !define MUI_UNICON "${GIMIAS_INSTALLER_RESOURCES}\GIMIAS_Logo.ico"
  ;Bitmap for the Welcome page and the Finish page (164x314 pixels)
  !define MUI_WELCOMEFINISHPAGE_BITMAP "${GIMIAS_INSTALLER_RESOURCES}\GIMIASVertical.bmp"
  ;Bitmap for the Welcome page and the Finish page (recommended size: 164x314 pixels).
  !define MUI_UNWELCOMEFINISHPAGE_BITMAP "${GIMIAS_INSTALLER_RESOURCES}\GIMIASVertical.bmp"

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "${GIMIAS_INSTALLER_RESOURCES}\License.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY

  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_DEFAULTFOLDER "Gimias\${APPNAME}"  
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\${APPNAME}" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
; Types of installation
!ifndef NOINSTTYPES ; only if not defined
  InstType "Default"
  InstType "Full"
  InstType "Light"
  ;InstType /NOCUSTOM
  ;InstType /COMPONENTSONLYONCUSTOM
!endif
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"


;--------------------------------
;Installer Sections

Section "" ; empty string makes it hidden, so would starting with -

  SetOutPath "$INSTDIR"
  
  ;Store installation folder
  WriteRegStr HKCU "Software\${APPNAME}" "Install_Dir" $INSTDIR
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "${APPNAME}" "${APPNAME}"
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "UninstallString" '"$INSTDIR\Uninstall.exe"'
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "DisplayName" '${APPNAME}'
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "DisplayIcon" '$INSTDIR\Gimias.exe,0'
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "Publisher" 'CISTIB-UPF'
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "HelpLink" 'http://www.gimias.org/support'
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "URLUpdateInfo" 'http://www.gimias.org'
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "URLInfoAbout" 'http://www.gimias.org'
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "DisplayVersion" '${GIMIAS_MAIN_VERSION}'
  WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "NoModify" 1
  WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "NoRepair" 1
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Gimias.lnk" "$INSTDIR\Gimias.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

Function .onInstSuccess
 ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
 IntFmt $0 "0x%08X" $0
 WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "EstimatedSize" "$0"
FunctionEnd 


Section "GIMIAS" GIMIAS
  SectionIn 1 2 3 RO
  SetOutPath $INSTDIR
  File "${GIMIAS_FOLDER}\*.*"
  SetOutPath "$INSTDIR\Copyrights"
  File /r "${GIMIAS_FOLDER}\Copyrights\*.*"
  SetOutPath "$INSTDIR\resource"
  File /r "${GIMIAS_FOLDER}\resource\*.*"
SectionEnd

SectionGroup Plugins

Section "Basic" PluginsBasic
  SectionIn 1 2 3
  SetOutPath "$INSTDIR\plugins\MITKPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\MITKPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\SceneViewPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\SceneViewPlugin\*.*"
SectionEnd

Section "Standard" PluginsStandard
  SectionIn 1 2
  SetOutPath "$INSTDIR\plugins\DicomPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\DicomPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\GenericSegmentationPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\GenericSegmentationPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\ImageToolsPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\ImageToolsPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\ManualSegmentationPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\ManualSegmentationPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\MeshEditorPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\MeshEditorPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\SandboxPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\SandboxPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\SignalViewerPlugin"
  File /r "${GIMIAS_FOLDER}\plugins\SignalViewerPlugin\*.*"
  SetOutPath "$INSTDIR\commandLinePlugins"
  File /r "${GIMIAS_FOLDER}\commandLinePlugins\*.*"
SectionEnd

Section "Extended" PluginsExtended
  SectionIn 1 2
  SetOutPath "$INSTDIR\plugins\ClinicalReportPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\ClinicalReportPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\CmguiPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\CmguiPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\msvPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\msvPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\RemoteDataPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\RemoteDataPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\SecurityPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\SecurityPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\SshPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\SshPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\TavernaPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\TavernaPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\VMTKPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\VMTKPlugin\*.*"
  SetOutPath "$INSTDIR\plugins\WebServicesPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\WebServicesPlugin\*.*"
  SetOutPath "$INSTDIR\wsrepo"
  File /nonfatal /r "${GIMIAS_FOLDER}\wsrepo\*.*"
  SetOutPath "$INSTDIR\plugins\XNATPlugin"
  File /nonfatal /r "${GIMIAS_FOLDER}\plugins\XNATPlugin\*.*"
SectionEnd

SectionGroupEnd
 
Section "Visual Studio runtime" VisualStudioRedistributable
  SectionIn 1 2 3 RO
  
  SetOutPath "$INSTDIR"
  File "${VREDIST_FOLDER}\vcredist_x64.exe"
  File "${VREDIST_FOLDER}\vcredist_x86.exe"
  
${If} ${RunningX64}
    # From http://blogs.msdn.com/astebner/archive/2007/02/07/update-regarding-silent-install-of-the-vc-8-0-runtime-vcredist-packages.aspx
    # "qb!" for progress with no cancel, "qb" for progress and cancel, "qn" for no interaction
    DetailPrint "Please wait for the next step to finish."
    ExecWait '$INSTDIR\vcredist_x64.exe /q:a /c:"VCREDI~2.EXE /q:a /c:""msiexec /i vcredist.msi /qn"" "' $0 # Only progress bar
    DetailPrint "vcredist_x64 SP1 Update returned $0"
${Else}
    DetailPrint "Please wait for the next step to finish."
    ExecWait '$INSTDIR\vcredist_x86.exe /q:a /c:"VCREDI~2.EXE /q:a /c:""msiexec /i vcredist.msi /qn"" "' $0 # Only progress bar
    DetailPrint "vcredist_x86 SP1 Update returned $0"
${EndIf}

SectionEnd

SectionGroup Tutorials

Section "Tutorials" Tutorials
  SectionIn 1 2
  
  SetOutPath "$INSTDIR"
  File "${TUTORIALS_FOLDER}\*.pdf"

  CreateDirectory "$SMPROGRAMS\$StartMenuFolder\Tutorials"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Tutorials\CardioImages.lnk" "$INSTDIR\GimiasTutorialCardioImages.pdf"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Tutorials\BrainImages.lnk" "$INSTDIR\GimiasTutorialBrainImages.pdf"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Tutorials\AngioImages.lnk" "$INSTDIR\GimiasTutorialAngioImages.pdf"
SectionEnd


Section "Sample Data" SampleData
  SectionIn 2
  NSISdl::download ftp://ftp.gimias.org/data/Tutorials/AngioImagesTutorialData.zip "$TEMP\AngioImagesTutorialData.zip"
  NSISdl::download ftp://ftp.gimias.org/data/Tutorials/CardioImagesTutorialData.zip "$TEMP\CardioImagesTutorialData.zip"
  NSISdl::download ftp://ftp.gimias.org/data/Tutorials/BrainImagesTutorialData.zip "$TEMP\BrainImagesTutorialData.zip"
  CreateDirectory $INSTDIR\Data
  ZipDLL::extractall "$TEMP\AngioImagesTutorialData.zip" "$INSTDIR\Data"
  ZipDLL::extractall "$TEMP\CardioImagesTutorialData.zip" "$INSTDIR\Data"
  ZipDLL::extractall "$TEMP\BrainImagesTutorialData.zip" "$INSTDIR\Data"
  Delete "$TEMP\AngioImagesTutorialData.zip"
  Delete "$TEMP\CardioImagesTutorialData.zip"
  Delete "$TEMP\BrainImagesTutorialData.zip"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Data.lnk" "$INSTDIR\Data"
 
SectionEnd

SectionGroupEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecGIMIAS ${LANG_ENGLISH} "GIMIAS Framework without plugins"
  LangString DESC_SecPluginsBasic ${LANG_ENGLISH} "Scene view and MITK plugins for data visualization"
  LangString DESC_SecPluginsStandard ${LANG_ENGLISH} "DICOM, Segmentation, Registration, Mesh Editing, Manual Segmentation, Signal viewer and command line plugins"
  LangString DESC_SecPluginsExtended ${LANG_ENGLISH} "Clinical Report, CMGUI, MSV, Remote Data, Security, SSH, Taverna Workbench, VMTK, Web services, XNAT"
  LangString DESC_SecVisualStudioRedistributable ${LANG_ENGLISH} "Visual Studio 2008 Redistributable package"
  LangString DESC_SecTutorials ${LANG_ENGLISH} "Example tutorials"
  LangString DESC_SecSampleData ${LANG_ENGLISH} "Sample Data for the tutorials will be downloaded and installed in the Data folder (this can take more tan 10 minutes)"
  
  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${GIMIAS} $(DESC_SecGIMIAS)
    !insertmacro MUI_DESCRIPTION_TEXT ${PluginsBasic} $(DESC_SecPluginsBasic)
    !insertmacro MUI_DESCRIPTION_TEXT ${PluginsStandard} $(DESC_SecPluginsStandard)
    !insertmacro MUI_DESCRIPTION_TEXT ${PluginsExtended} $(DESC_SecPluginsExtended)
    !insertmacro MUI_DESCRIPTION_TEXT ${VisualStudioRedistributable} $(DESC_SecVisualStudioRedistributable)
    !insertmacro MUI_DESCRIPTION_TEXT ${Tutorials} $(DESC_SecTutorials)
    !insertmacro MUI_DESCRIPTION_TEXT ${SampleData} $(DESC_SecSampleData)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  Delete "$INSTDIR\Uninstall.exe"

  RMDir /r /REBOOTOK "$INSTDIR"

  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
    
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\Gimias.lnk"
  RMDir /r "$SMPROGRAMS\$StartMenuFolder"
  
  DeleteRegKey HKCU "Software\${APPNAME}"
  
  DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}"

SectionEnd