/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef ClinicalReportElement_H
#define ClinicalReportElement_H

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 22 Oct 2010
*/

class ClinicalReportElement {
public:
  ClinicalReportElement();

  bool operator<(ClinicalReportElement &second);

  // getters and setters:
  std::string GetComments();
  void SetComments(std::string comments);
  std::string GetTitle();
  void SetTitle(std::string title);
  std::string GetThumbnailSmall(); // path
  void SetThumbnailSmall(std::string thumbnailPath);
  std::string GetThumbnailMedium(); // path
  void SetThumbnailMedium(std::string thumbnailPath);
  std::string GetScreenshot(); // path
  void SetScreenshot(std::string screenshotPath);
  std::string GetType();
  void SetType(std::string type);
  int GetPosition();
  void SetPosition(int position);
  bool GetSelected();
  void SetSelected(bool selected);
  std::string GetTable();
  void SetTable(std::string table);

private:
  std::string m_Comments;
  std::string m_Title;
  std::string m_ThumbnailSmall; // path
  std::string m_ThumbnailMedium; // path
  std::string m_Screenshot; // path
  std::string m_Type;
  std::string m_Table;

  int m_Position;
  bool m_Selected;
};

bool ClinicalReportElementSorter(ClinicalReportElement first,
                                 ClinicalReportElement second);

#endif // ClinicalReportElement_H
