/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SSHPluginAttachPlugins_H
#define _SSHPluginAttachPlugins_H

#include "coreObject.h"
#include "SshPluginNodeManager.h"

class SSHPluginProvider;

/**
\brief Attach remote command line plugins that have been detached
previously.

\ingroup SshPlugin
\author Xavi Planes
\date Mar 2011
*/
class SSHPluginAttachRunningPlugins : public Core::SmartPointerObject {
public:
  coreDeclareSmartPointerClassMacro(SSHPluginAttachRunningPlugins,
                                    Core::SmartPointerObject);

  //!
  void SetSSHPluginProvider(SSHPluginProvider *val);

  //!
  void SetNodeManager(SshPluginNodeManager::Pointer val);

  //!
  void Update();

protected:
  //!
  SSHPluginAttachRunningPlugins();
  //!
  virtual ~SSHPluginAttachRunningPlugins(void);

private:
  //!
  SSHPluginProvider *m_SSHPluginProvider;
  //!
  SshPluginNodeManager::Pointer m_nodeManager;
  //!
  std::vector< Core::ProcessorOutputObserver::Pointer > m_ProcessorOutputObserverVector;
};

#endif // _SSHPluginAttachPlugins_H
