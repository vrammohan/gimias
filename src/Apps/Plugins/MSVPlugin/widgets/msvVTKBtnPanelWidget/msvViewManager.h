/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef msvViewManager_H
#define msvViewManager_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "msvRenderingTree.h"
#include "coreDataEntityPreview.h"

typedef Core::DataHolder< blTagMap::Pointer > ViewHolderType;

/**
\brief Manage all views

\ingroup msvKPlugin
\author Xavi Planes
\date Nov 2010
*/
class msvViewManager : public Core::SmartPointerObject {

public:
  coreDeclareSmartPointerClassMacro(msvViewManager, Core::SmartPointerObject);
  //!
  void SetDataEntityHolder(Core::DataEntityHolder::Pointer dataEntity);

  //! Get views of current data entity session node. If does not exist, create one
  blTagMap::Pointer GetViews();

  //! Get view using name
  blTagMap::Pointer GetView(const std::string &viewName);

  //! Set selected view and notify observers if changed
  void SetSelectedView(blTagMap::Pointer view);

  //! Get selected view
  blTagMap::Pointer GetSelectedView();

  //! Get holder
  ViewHolderType::Pointer GetSelectedViewHolder();

  //! Remove a view
  void RemoveView(blTagMap::Pointer view);

  //! Add a new empty view
  blTagMap::Pointer AddView();

  //! Get image of a view
  wxImage GetImage(blTagMap::Pointer view);

  //! Get name of a view
  std::string GetName(blTagMap::Pointer view);

  //! Get working area of a view
  std::string GetWorkingAreaName(blTagMap::Pointer view);

  //! Save temp preview for a view
  std::string SaveTempPreview(blTagMap::Pointer view);

  //! Get props or create empty ones if not found
  blTagMap::Pointer GetProps(blTagMap::Pointer view,
                             const std::string &multiRenderWindowName,
                             const std::string &renderWindowName,
                             const std::string &name);

private:
  //!
  msvViewManager();

  ~msvViewManager();

private:
  //!
  Core::DataEntityHolder::Pointer m_DataEntityHolder;
  //!
  ViewHolderType::Pointer m_SelectedViewHolder;
  //! All files will be deleted at destructor
  std::map< std::string, Core::DataEntityPreview::Pointer > m_PreviewMap;
};

#endif // wxHtmlTooltipDialog_H