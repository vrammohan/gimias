/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATWorkingArea.h"

#include "blMitkUnicode.h"
#include "blTextUtils.h"

#include "coreWxMitkCoreMainWindow.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreDirectory.h"
#include "coreSettings.h"
#include "coreKernel.h"
#include "corePluginTab.h"
#include "coreMainMenu.h"
#include "coreToolbarIO.h"
#include "corePluginTab.h"
#include "corePluginTabFactory.h"
#include "coreProcessorManager.h"
#include "coreWxMitkCoreMainWindow.h"
#include "corePreferencesDialog.h"
#include "coreStringHelper.h"

// wxWidgets
#include <wx/event.h>
#include <wx/process.h>
#include <wx/dir.h>
#include <wx/zipstrm.h>
#include <wx/wfstream.h>
#include <wx/treebook.h>
#include <wx/busyinfo.h>

#include "xnat.xpm"

#include "SecurityPluginUtilities.h"

#include "itksys/SystemTools.hxx"

XNATWorkingArea::XNATWorkingArea(XNATPluginManageConnections::Pointer connManager,
                                 wxWindow *parent, int id, const wxPoint &pos,
                                 const wxSize &size, long style)
    : XNATWorkingAreaUI(parent, id, pos, size, style) {
  m_selectedSubjectIndex = -1;
  m_lastSelSubjectIndex = -1;
  m_selectedExperimentIndex = -1;
  m_selectedSubResourceIndex = -1;
  m_selectedSubResourceCollection = "";
  m_treeItemData = NULL;

  m_QueryProcessor = XNATQueryProcessor::New();

  m_LoadProcessor = XNATLoadProcessor::New();
  m_DownloadProcessor = XNATDownloadProcessor::New();
  m_ExportProcessor = XNATExportProcessor::New();

  m_QueryProcessor->SetMultithreading(true);
  m_LoadProcessor->SetMultithreading(true);

  m_ExportProcessor->SetMultithreading(true);

  // 131113 AMB: test if foreground downloading is better suited to the project
  m_DownloadProcessor->SetMultithreading(false);

  // Processor observers
  m_QueryProcessor->GetOutputDataEntityHolder(
                      XNATQueryProcessor::OUTPUT_XNAT_ALL_SUBJECTS)
      ->AddObserver(this, &XNATWorkingArea::OnOutputXNATAllSubjects);

  m_QueryProcessor->GetOutputDataEntityHolder(
                      XNATQueryProcessor::OUTPUT_XNAT_QUERY_RESULTS)
      ->AddObserver(this, &XNATWorkingArea::OnOutputXNATQueryResults);

  for (int index = 0; index < XNATDownloadProcessor::NUMBEROFOUTPUTS; index++) {
    m_DownloadProcessor->GetOutputDataEntityHolder(index)
        ->AddObserver(this, &XNATWorkingArea::OnDownloadDataEntity);
  }

  m_ExportProcessor->GetOutputDataEntityHolder(XNATDownloadProcessor::OUTPUT_DATA_ENTITY)
      ->AddObserver(this, &XNATWorkingArea::OnExportDataEntity);

  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();

  std::string dataPath = settings->GetProjectHomePath();

  m_BaseFolder = XNATPluginUtilities::CreateBaseFolder(dataPath + "\\XNAT_DB\\");

  bitmap_XNAT->SetBitmap(wxBitmap(xnat_xpm));

  m_SelectedConnection = "";

  m_Connections = connManager;

  if (this->m_Connections) {
    this->m_Connections->GetConnectionsHolder()->AddObserver(
        this, &XNATWorkingArea::FillConnections);
  }

  this->UpdateWidget();
}

XNATWorkingArea::~XNATWorkingArea() {}

void XNATWorkingArea::OnOutputXNATAllSubjects() {
  if (m_QueryProcessor->GetOutputDataEntity(XNATQueryProcessor::OUTPUT_XNAT_ALL_SUBJECTS)
          .IsNotNull()) {
    Core::DataTreeHelper::PublishOutput(m_QueryProcessor->GetOutputDataEntityHolder(
                                            XNATQueryProcessor::OUTPUT_XNAT_ALL_SUBJECTS),
                                        GetRenderingTree(), false, false);
  }
}

void XNATWorkingArea::OnOutputXNATQueryResults() {
  if (m_QueryProcessor->GetOutputDataEntity(XNATQueryProcessor::OUTPUT_XNAT_QUERY_RESULTS)
          .IsNotNull()) {
    Core::DataTreeHelper::PublishOutput(
        m_QueryProcessor->GetOutputDataEntityHolder(
            XNATQueryProcessor::OUTPUT_XNAT_QUERY_RESULTS),
        GetRenderingTree(), false, false);

    std::vector< std::map< std::string, std::string > > subjects;

    subjects = m_QueryProcessor->GetFilteredSubjects();

    UpdateSubjects(subjects);
    UpdateWidget();
  }
}

void XNATWorkingArea::OnDownloadDataEntity() {
  if (m_DownloadProcessor->GetOutputDataEntity(XNATDownloadProcessor::OUTPUT_DATA_ENTITY)
          .IsNotNull()) {
    GetPluginTab()->ShowWindow("OrthoSliceWorkingArea", true);

    for (int index = 0; index < m_DownloadProcessor->GetNumberOfOutputs(); index++) {
      Core::DataTreeHelper::PublishOutput(
          m_DownloadProcessor->GetOutputDataEntityHolder(index), GetRenderingTree(), true,
          false, false);
    }
  }

  // refresh screen
  wxTheApp->Yield();
}

void XNATWorkingArea::OnExportDataEntity() {
  if (m_ExportProcessor->GetOutputDataEntity(XNATExportProcessor::OUTPUT_DATA_ENTITY)
          .IsNotNull()) {
    GetPluginTab()->ShowWindow("OrthoSliceWorkingArea", true);

    Core::DataTreeHelper::PublishOutput(m_ExportProcessor->GetOutputDataEntityHolder(
                                            XNATExportProcessor::OUTPUT_DATA_ENTITY),
                                        GetRenderingTree(), true, true);
  }
}

void XNATWorkingArea::OnManageConnection(wxCommandEvent &event) {
  // Opens preferences dialog
  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
  graphicalIface->GetMainWindow()->ShowPreferencesdialog();

  // Updates working area
  this->UpdateWidget();
}

std::vector< std::string > XNATWorkingArea::GetAllProjects() {

  std::vector< std::string > ret;
  XNATPluginDefineConetions connection;

  if (m_Connections) {
    connection =
        m_Connections->GetConnectionsHolder()->GetSubject()[m_SelectedConnection];

    SecurityPluginCryptoProcessor::Pointer manager =
        SecurityPluginUtilities::GetMPManager();

    try {

      if (connection.GetURL() == "")
        return ret;
      // decrypt password from password manager
      if (manager) {
        connection.SetPassword(manager->DecryptFromList(
            connection.GetName() + connection.GetUser() + connection.GetURL()));
      }

      XNAT::Utils client(connection.GetUser(), connection.GetPassword(),
                         connection.GetURL());

      ret = client.getProjects();
    } catch (Poco::Exception &ex) {
      std::cerr << ex.displayText() << std::endl;
    }
  }

  return ret;
}

void XNATWorkingArea::FillProjects() {
  std::vector< std::string > projects;
  projects = GetAllProjects();

  combo_Projects->Clear();
  combo_Projects->Append("All");

  for (int i = 0; i < projects.size(); i++) {
    combo_Projects->Append(projects[i]);
  }

  if (combo_Projects->GetCount() > 0)
    combo_Projects->Select(0);

  m_projects = projects;
}

void XNATWorkingArea::OnTreeSelectionChanged(wxTreeEvent &event) {
  wxTreeItemId treeItem = event.GetItem();

  wxTreeItemData *treeItemData;
  if (treeItem.m_pItem == 0)
    return;

  treeItemData = m_XNATTree->GetItemData(treeItem);

  if (treeItemData) {
    XNATPluginDefineConetions connection;
    connection =
        m_Connections->GetConnectionsHolder()->GetSubject()[m_SelectedConnection];

    m_treeItemData = treeItemData;
    ResourceItemTreeData *resourceItemTreeData;
    resourceItemTreeData = dynamic_cast< ResourceItemTreeData * >(treeItemData);

    ExperimentItemTreeData *experimentItemTreeData;
    experimentItemTreeData = dynamic_cast< ExperimentItemTreeData * >(treeItemData);

    if (resourceItemTreeData) {
      m_btnDownload->Enable(true);
      m_selectedSubjectIndex = resourceItemTreeData->GetSubjectId();
      m_selectedSubResourceIndex = resourceItemTreeData->GetId();
      m_selectedSubResourceCollection = resourceItemTreeData->GetResource().collection;
      m_selectedExperimentIndex = -1;
    }

    else if (experimentItemTreeData) {
      m_btnDownload->Enable(true);
      m_selectedSubjectIndex = experimentItemTreeData->GetSubjectId();
      m_selectedExperimentIndex = experimentItemTreeData->GetId();
      m_selectedSubResourceIndex = -1;
      m_selectedSubResourceCollection = "";
    }

    else {
      m_btnDownload->Enable(false);
      SubjectItemTreeData *subjectItemTreeData;
      subjectItemTreeData = dynamic_cast< SubjectItemTreeData * >(treeItemData);

      if (subjectItemTreeData) {
        m_selectedSubjectIndex = subjectItemTreeData->GetId();
        m_selectedExperimentIndex = -1;
        m_selectedSubResourceIndex = -1;
        m_selectedSubResourceCollection = "";
      } else {
        ScanItemTreeData *scanItemTreeData;
        scanItemTreeData = dynamic_cast< ScanItemTreeData * >(treeItemData);
        if (scanItemTreeData) {

          m_selectedSubjectIndex = scanItemTreeData->GetSubjectId();
          m_selectedExperimentIndex = scanItemTreeData->GetExperimentId();
          m_selectedSubResourceIndex = -1;
          m_selectedSubResourceCollection = "";
        }
      }
    }
  } else {
    m_btnDownload->Enable(false);
    m_selectedSubjectIndex = -1;
    m_selectedExperimentIndex = -1;
    m_selectedSubResourceIndex = -1;
    m_selectedSubResourceCollection = "";
    m_treeItemData = NULL;
  }

  if (m_lastSelSubjectIndex != m_selectedSubjectIndex) {
    // std::cout << "Subject changed! curr: " << m_selectedSubjectIndex << " last: "<<
    // m_lastSelSubjectIndex << std::endl;
    m_lastSelSubjectIndex = m_selectedSubjectIndex;
    UpdateDetailedInformation();
  }
}

void XNATWorkingArea::UpdateDetailedInformation() {
  if (m_subjects.size() < 1 || m_selectedSubjectIndex < 0) {
    m_PanelDetailedInformation->GetSizer()->Clear(true);
    return;
  }

  Freeze();

  wxBoxSizer *globalSizer = new wxBoxSizer(wxVERTICAL);
  m_PanelDetailedInformation->GetSizer()->Clear(true);
  m_PanelDetailedInformation->SetSizer(globalSizer, true);

  XNATSubject subject = m_subjects[m_selectedSubjectIndex];

  bool bExperiment = false;
  XNAT::Utils::Experiment experiment;

  if (m_selectedExperimentIndex >= 0) {
    std::map< std::string, std::vector< XNAT::Utils::Experiment > >::iterator it;
    size_t countSoFar = 0;
    for (it = subject.experiments.begin(); it != subject.experiments.end(); it++) {
      size_t n = it->second.size();
      if (m_selectedExperimentIndex >= countSoFar &&
          m_selectedExperimentIndex < countSoFar + n) {
        bExperiment = true;
        experiment = it->second[m_selectedExperimentIndex - countSoFar];
        break;
      }
      countSoFar += n;
    }
  }

  // Get the Patient's weight and height from the Diagnosis information, if present for
  // the selected subject

  int listOfDiagnosisItems = 0;
  std::string weight, height;
  listOfDiagnosisItems = subject.diagnosisItems.size();

  if (listOfDiagnosisItems != 0) {
    // Extract the information
    for (XNAT::Utils::diagnosis::iterator it = subject.diagnosisItems.begin();
         it != subject.diagnosisItems.end(); it++) {
      if (it->first == "MSP:Diagnosis") {
        std::map< std::string, std::string > patientData = it->second;
        weight = patientData["MSP:Patient_weight"];
        height = patientData["MSP:Patient_height"];
      }
    }
  }

  std::map< std::string, std::string > subjectAttributes = subject.attributes;
  std::map< std::string, std::string >::iterator it;
  std::string attributes[] = { "Gender:", "Handedness:", "Height:", "Weight:", "YOB:" };
  // wxBoxSizer* sizerRow1 = new wxBoxSizer(wxHORIZONTAL);
  // wxBoxSizer* sizerRow2 = new wxBoxSizer(wxHORIZONTAL);
  int count = 0;

  for (it = subjectAttributes.begin(); it != subjectAttributes.end(); it++) {
    // std::string label = it->first;
    std::string text = it->second;

    if (it->first == "gender" || it->first == "handedness" || it->first == "height" ||
        it->first == "weight" || it->first == "yob") {
      wxStaticText *textLabel =
          new wxStaticText(m_PanelDetailedInformation, wxID_ANY, attributes[count]);
      wxStaticText *textCtrl;
      if (it->first == "height") {
        textCtrl = new wxStaticText(m_PanelDetailedInformation, wxID_ANY, height);
      } else if (it->first == "weight") {
        textCtrl = new wxStaticText(m_PanelDetailedInformation, wxID_ANY, weight);
      } else {
        textCtrl = new wxStaticText(m_PanelDetailedInformation, wxID_ANY, text);
      }
      wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);

      sizer->Add(20, 20, 0, 0, 0);
      sizer->Add(textLabel, 1, 0, 0);
      sizer->Add(20, 20, 0, 0, 0);

      sizer->Add(textCtrl, 4, wxEXPAND | wxALIGN_LEFT, 0);
      sizer->Add(20, 20, 0, 0, 0);
      globalSizer->Add(sizer, 0, wxEXPAND, 0);
      count++;
      // if(it->first == "gender")
      //{
      //	wxStaticText * textLabel = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(attributes[0]));
      //	wxStaticText * textCtrl = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(text));
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //	sizerRow1->Add(textLabel, 1, 0, 0);
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //	sizerRow1->Add(textCtrl, 1, wxEXPAND | wxALIGN_LEFT, 0);
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //}
      // else if(it->first == "handedness")
      //{
      //	wxStaticText * textLabel = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(attributes[1]));
      //	wxStaticText * textCtrl = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(text));
      //	sizerRow2->Add(20, 20, 0, 0, 0);
      //	sizerRow2->Add(textLabel, 1, 0, 0);
      //	sizerRow2->Add(20, 20, 0, 0, 0);
      //	sizerRow2->Add(textCtrl, 1, wxEXPAND | wxALIGN_LEFT, 0);
      //	sizerRow2->Add(20, 20, 0, 0, 0);
      //}
      // else if(it->first == "height")
      //{
      //	wxStaticText * textLabel = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(attributes[2]));
      //	wxStaticText * textCtrl = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(text));
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //	sizerRow1->Add(textLabel, 1, 0, 0);
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //	sizerRow1->Add(textCtrl, 1, wxEXPAND | wxALIGN_RIGHT, 0);
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //}
      // else if(it->first == "weight")
      //{
      //	wxStaticText * textLabel = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(attributes[3]));
      //	wxStaticText * textCtrl = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(text));
      //	sizerRow2->Add(20, 20, 0, 0, 0);
      //	sizerRow2->Add(textLabel, 1, 0, 0);
      //	sizerRow2->Add(20, 20, 0, 0, 0);
      //	sizerRow2->Add(textCtrl, 1, wxEXPAND | wxALIGN_RIGHT, 0);
      //	sizerRow2->Add(20, 20, 0, 0, 0);
      //}
      // else if(it->first == "yob")
      //{
      //	wxStaticText * textLabel = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(attributes[4]));
      //	wxStaticText * textCtrl = new wxStaticText(m_PanelDetailedInformation,
      //wxID_ANY, wxT(text));
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //	sizerRow1->Add(textLabel, 1, 0, 0);
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //	sizerRow1->Add(textCtrl, 1, wxEXPAND | wxALIGN_RIGHT, 0);
      //	sizerRow1->Add(20, 20, 0, 0, 0);
      //}
      // else
      //{
      //	//std::cout << "attribute: " << it->first << std::endl;
      //}
    }
  }
  // wxStaticText * blank = new wxStaticText(m_PanelDetailedInformation, wxID_ANY,
  // wxT(""));
  // sizerRow2->Add(20, 20, 0, 0, 0);
  // sizerRow2->Add(blank, 1, 0, 0);
  // sizerRow2->Add(20, 20, 0, 0, 0);
  // sizerRow2->Add(blank, 1, 0, 0);
  // sizerRow2->Add(20, 20, 0, 0, 0);
  // globalSizer->Add(sizerRow1, 0, wxEXPAND, 0);
  // globalSizer->Add(sizerRow2, 0, wxEXPAND, 0);

  // Display the Diagnosis information if present for the selected subject
  /*    if(listOfDiagnosisItems != 0)
      {
                  globalSizer->Add(20, 20, 0, 0, 0);

          // Extract the information
          for (XNAT::Utils::diagnosis::iterator it = subject.diagnosisItems.begin();
     it!=subject.diagnosisItems.end(); it++)
          {
                  // Display the other diagnosis data, if required in future.
          }
          }*/

  m_PanelDetailedInformation->SetSizer(globalSizer);

// Cast resize events to avoid sizer problems
#ifndef __WXMAC__
  wxSizeEvent resEventGlobal(this->GetSize(), this->GetId());
  resEventGlobal.SetEventObject(this);
  this->GetEventHandler()->ProcessEvent(resEventGlobal);

  wxSizeEvent resEvent(m_PanelDetailedInformation->GetSize(),
                       m_PanelDetailedInformation->GetId());

  resEvent.SetEventObject(m_PanelDetailedInformation);
  m_PanelDetailedInformation->GetEventHandler()->ProcessEvent(resEvent);

#else //__WXMAC__
  m_PanelDetailedInformation->GetSizer()->Fit(m_PanelDetailedInformation);
#endif //__WXMAC__

  if (IsFrozen()) {
    Thaw();
  }
}

void XNATWorkingArea::UpdateTree() { m_XNATTree->LoadSubjectsIntoTree(m_subjects); }

void XNATWorkingArea::UpdateWidget() {
  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();

  m_edSaveToDirectory->SetValue(m_BaseFolder);

  if (m_SelectedConnection.length() == 0) {
    FillConnections();
  }

  m_SelectedConnection = m_ComboConnections->GetValue();
  XNATPluginDefineConetions connection;
  connection = m_Connections->GetConnectionsHolder()->GetSubject()[m_SelectedConnection];
  // bitmap_XNAT->SetBitmap( wxBitmap( xnat_xpm ) ); // not needed right now

  FillProjects();
  UpdateTree();
  UpdateDetailedInformation();
}

Core::BaseProcessor::Pointer XNATWorkingArea::GetProcessor() {
  return m_QueryProcessor.GetPointer();
}

void XNATWorkingArea::FillConnections() {

  m_ComboConnections->Clear();

  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();

  if (m_Connections) {
    std::map< std::string, XNATPluginDefineConetions >::iterator it;
    for (it = m_Connections->GetConnectionsHolder()->GetSubject().begin();
         it != m_Connections->GetConnectionsHolder()->GetSubject().end(); it++) {
      // check if the node is encrypted only its shown on master password activation
      if (it->second.GetEncripted()) {
        std::string enc;
        try {
          if (manager) {
            enc = manager->Decrypt(it->second.GetPassword());
          } else {
            enc = "";
          }

        } catch (...) {
          enc = "";
        }

        if (enc.length())
          m_ComboConnections->Append(it->first);
      } else
        m_ComboConnections->Append(it->first);
    }
  }

  if (m_ComboConnections->GetCount() > 0) {
    m_ComboConnections->Select(0);
    m_SelectedConnection = m_ComboConnections->GetValue().c_str();
  }
}

void XNATWorkingArea::OpenDirectoryForSavingData(wxCommandEvent &event) {
  wxDirDialog openDirectoryDialog(NULL, wxT("Please select the XNAT root folder"),
                                  m_edSaveToDirectory->GetValue());

  if (openDirectoryDialog.ShowModal() == wxID_OK) {
    m_BaseFolder = std::string(openDirectoryDialog.GetPath().mb_str());
  }
  m_edSaveToDirectory->SetValue(m_BaseFolder);
}

void XNATWorkingArea::UpdateSubjects(
    std::vector< std::map< std::string, std::string > > subjects) {

  XNATPluginDefineConetions connection;
  connection = m_Connections->GetConnectionsHolder()->GetSubject()[m_SelectedConnection];
  m_subjects.clear();

  // decrypt password if master Pass is set
  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();

  if (manager) {
    connection.SetPassword(manager->DecryptFromList(
        connection.GetName() + connection.GetUser() + connection.GetURL()));
  }

  for (int i = 0; i < subjects.size(); i++) {

    XNATSubject subject;
    subject.id = i;
    subject.attributes = subjects[i];

    // fill experiments for this subject
    XNAT::Utils client(connection.GetUser(), connection.GetPassword(),
                       connection.GetURL());

    std::map< std::string, std::vector< XNAT::Utils::Experiment > > experiments;

    std::vector< XNAT::Utils::Experiment > projectExperiments;
    std::vector< XNAT::Utils::Experiment > projectImgExperiments;
    std::vector< XNAT::Utils::Experiment > projectNonImgExperiments;
    std::vector< XNAT::Utils::Scan > experimentScans;
    std::map< std::string, std::string > diagnosisData;

    try {
      if (subject.attributes["project"] == "" || subject.attributes["label"] == "")
        continue;

      // Get the list of resources at subject level
      std::vector< XNAT::Utils::Resource > res =
          client.getResources(subject.attributes["project"], subject.attributes["label"]);
      for (int iRes = 0; iRes < res.size(); iRes++) {
        std::map< std::string,
                  std::vector< XNAT::Utils::SubjectResourceFile > >::const_iterator it =
            subject.subjectResourceFiles.find(res[iRes].collection);
        if (it == subject.subjectResourceFiles.end()) {
          std::vector< XNAT::Utils::SubjectResourceFile > subjectResource =
              client.getSubResourceFiles(subject.attributes["project"],
                                         subject.attributes["label"],
                                         res[iRes].collection);
          subject.subjectResourceFiles[res[iRes].collection] = subjectResource;
        }
      }

      projectExperiments = client.getExperiments(subject.attributes["project"],
                                                 subject.attributes["label"]);
      experiments[subject.attributes["project"]] = projectExperiments;

      for (int j = 0; j < projectExperiments.size(); j++) {
        if (client.isImageExperiment(projectExperiments[j])) {
          experimentScans =
              client.getScans(subject.attributes["project"], subject.attributes["label"],
                              projectExperiments[j].label);
          subject.scans[projectExperiments[j].label] = experimentScans;
        } else if (client.isDiagnosis(projectExperiments[j])) {
          std::string diagnosisName = "MSP:SymptomaticSegment";
          std::string diagnosisComplete = "MSP:Diagnosis";

          subject.diagnosisItems[diagnosisName] = client.getDiagnosisData(
              subject.attributes["project"], subject.attributes["label"],
              projectExperiments[j].label, diagnosisName);
          subject.diagnosisItems[diagnosisComplete] = client.getDiagnosisData(
              subject.attributes["project"], subject.attributes["label"],
              projectExperiments[j].label, diagnosisComplete);
        }
      }
    } catch (Poco::Exception &ex) {
      std::cerr << ex.displayText() << std::endl;
    }

    subject.experiments = experiments;
    m_subjects.push_back(subject);
  }
}

void XNATWorkingArea::ClearSubjects() {
  m_selectedSubjectIndex = -1;
  m_selectedExperimentIndex = -1;
  m_lastSelSubjectIndex = -1;
  m_selectedSubResourceIndex = -1;
  m_selectedSubResourceCollection = "";
  m_treeItemData = NULL;
  m_subjects.clear();
  m_btnDownload->Enable(false);
}

void XNATWorkingArea::OnQuery(wxCommandEvent &event) {

  XNATPluginDefineConetions connection;
  m_SelectedConnection = m_ComboConnections->GetStringSelection().c_str();
  connection = m_Connections->GetConnectionsHolder()->GetSubject()[m_SelectedConnection];

  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();

  try {
    ClearSubjects();

    m_btnDownload->Enable(false);

    UpdateDetailedInformation();

    if (manager) {
      connection.SetPassword(manager->DecryptFromList(
          connection.GetName() + connection.GetUser() + connection.GetURL()));
    }

    XNATQueryParameters params;
    params.usr = connection.GetUser();
    params.pwd = connection.GetPassword();
    params.siteUrl = connection.GetURL();

    params.restrictions["label"] = m_edSubjectID->GetValue();
    params.restrictions["gender"] = combo_Gender->GetValue();
    params.restrictions["handedness"] = combo_Hand->GetValue();
    params.restrictions["yob"] = m_edYOB->GetValue();
    if (combo_Projects->GetValue() == "All")
      params.restrictions["project"] = "";
    else
      params.restrictions["project"] = combo_Projects->GetValue();
    params.restrictions["height"] = "";
    params.restrictions["weight"] = "";

    m_QueryProcessor->SetParameters(params);

    Core::Runtime::Kernel::GetProcessorManager()->Execute(
        (Core::BaseProcessor::Pointer)m_QueryProcessor);
  } catch (Poco::Exception &ex) {

    std::cerr << ex.displayText() << std::endl;
  }
}

void XNATWorkingArea::OnBrowseLocal(wxCommandEvent &event) {}

void XNATWorkingArea::OnConnectionItemSelect(wxCommandEvent &event) {
  ClearSubjects();
  UpdateWidget();
}

void XNATWorkingArea::OnDownload(wxCommandEvent &event) {
  if (m_selectedSubjectIndex < 0) {
    return;
  }

  Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();
  Core::Widgets::wxMitkCoreMainWindow *mainFrame =
      dynamic_cast< Core::Widgets::wxMitkCoreMainWindow * >(gIface->GetMainWindow());
  wxString msg = wxString("Processing of downloaded data ... Please wait");
  mainFrame->updateDialogMessage(msg);
  // wxWindowDisabler disableAll;
  // refresh screen
  wxTheApp->Yield();

  XNATPluginDefineConetions connection;
  connection = m_Connections->GetConnectionsHolder()->GetSubject()[m_SelectedConnection];

  XNATSubject subject = m_subjects[m_selectedSubjectIndex];

  SecurityPluginCryptoProcessor::Pointer manager =
      SecurityPluginUtilities::GetMPManager();

  try {

    XNATDownloadParameters params;
    XNATDownloadParameters params2;

    if (manager) {
      connection.SetPassword(manager->DecryptFromList(
          connection.GetName() + connection.GetUser() + connection.GetURL()));
    }

    params.usr = connection.GetUser();
    params.pwd = connection.GetPassword();
    params.siteUrl = connection.GetURL();
    if ((*params.siteUrl.rbegin()) == '/') {
      std::cerr << "Warning! End of XNAT server URL is \"/\":" << std::endl
                << "\tthe URL has been modified outside this program. Please, fix URL"
                << std::endl;
    }

    XNAT::Utils::Experiment selectedExperiment = { "", "", "", "", "" };
    XNAT::Utils::SubjectResourceFile selectedSubjectResourceFile = {
      "", "", "", 0, "", ""
    };

    if (m_selectedExperimentIndex != -1) {
      std::map< std::string, std::vector< XNAT::Utils::Experiment > >::iterator it;
      size_t countSoFar = 0;
      for (it = subject.experiments.begin(); it != subject.experiments.end(); it++) {
        size_t n = it->second.size();
        if (m_selectedExperimentIndex >= countSoFar &&
            m_selectedExperimentIndex < countSoFar + n) {
          selectedExperiment = it->second[m_selectedExperimentIndex - countSoFar];
          break;
        }
        countSoFar += n;
      }

      params.experiment = selectedExperiment;
      params.experimentScans = subject.scans[selectedExperiment.label];
      params.subjectResourceFile = selectedSubjectResourceFile;
      params.downloadFileName = m_BaseFolder + selectedExperiment.label + ".zip";
      params.project = subject.attributes["project"];
      params.label = subject.attributes["label"];
      params.baseFolder = m_BaseFolder;
      params.downloadScans = true;
      params.downloadSubjectResources = false;
    } else if (m_selectedSubResourceIndex != -1) {
      selectedSubjectResourceFile =
          (subject.subjectResourceFiles[m_selectedSubResourceCollection])
              [m_selectedSubResourceIndex];

      params.experiment = selectedExperiment;
      params.experimentScans = subject.scans[selectedExperiment.label];
      params.subjectResourceFile = selectedSubjectResourceFile;
      params.downloadFileName = m_BaseFolder + selectedSubjectResourceFile.label;
      params.project = subject.attributes["project"];
      params.label = subject.attributes["label"];
      params.baseFolder = m_BaseFolder;
      params.downloadScans = false;
      params.downloadSubjectResources = true;

      //@DBGC// std::cout << "\t res coll = " << m_selectedSubResourceCollection << ", res
      //idx = " << m_selectedSubResourceIndex << endl;
      //@DBGC// std::cout << "\t download fname = " << params.downloadFileName << endl;
      //@DBGC// std::cout << "\t project = " << params.project << ", label = " <<
      //params.label << endl;
    } else {
      return;
    }

    m_DownloadProcessor->SetParameters(params);

    Core::ProcessorThread::Pointer processorThread;
    processorThread = Core::Runtime::Kernel::GetProcessorManager()->CreateProcessorThread(
        m_DownloadProcessor.GetPointer());
    // processorThread->SetShowProcessingMessage( false );

    // Execute
    Core::Runtime::Kernel::GetProcessorManager()->Execute(
        m_DownloadProcessor.GetPointer());

    wxString msg = wxString("Done!");
    mainFrame->updateDialogMessage(msg);

  } catch (Poco::Exception &ex) {
    std::cerr << ex.displayText() << std::endl;
  }
}

std::string XNATWorkingArea::IntegerToString(int i) {
  if (i == 0)
    return "0";

  std::string ret = "";

  int temp = i;
  while (temp > 0) {

    ret = std::string(1, (char)('0' + (temp % 10))) + ret;
    temp /= 10;
  }

  return ret;
}

std::string XNATWorkingArea::FindAfter(std::string str, std::string pat) {

  std::string ret;

  size_t found;

  found = str.find(pat);

  if (found != std::string::npos) {

    int n = (int)found;
    n += pat.size();

    int count = 0;
    for (int i = n; i < str.size(); i++) {
      if (str[i] == '/')
        break;

      count++;
    }

    ret = str.substr(n, count);
  } else {

    ret = "";
  }

  return ret;
}
