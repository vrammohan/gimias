
set BIN_FOLDER=%1
set OUTPUT_FOLDER=%2
REM v1.5.r4
set VERSION=%3
REM win32, win64
set PLATFORM=%4

set ZIP="C:\Program Files\7-Zip\7z.exe"
set TEMP_FOLDER=%2\Temp

mkdir %OUTPUT_FOLDER%\%VERSION%
rmdir %OUTPUT_FOLDER%\%VERSION%\Packages
mkdir %OUTPUT_FOLDER%\%VERSION%\Packages /S/Q

REM Core
call BuildPackage Core %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
cd %BIN_FOLDER%
%ZIP% a %OUTPUT_FOLDER%\%VERSION%\Packages\Core-%VERSION%-%PLATFORM%.zip resource -r
%ZIP% a %OUTPUT_FOLDER%\%VERSION%\Packages\Core-%VERSION%-%PLATFORM%.zip Copyrights -r
copy tpExtLibWebUpdateAppApplications_webapp.exe _tpExtLibWebUpdateAppApplications_webapp.exe
%ZIP% a %OUTPUT_FOLDER%\%VERSION%\Packages\Core-%VERSION%-%PLATFORM%.zip _tpExtLibWebUpdateAppApplications_webapp.exe
del _tpExtLibWebUpdateAppApplications_webapp.exe
cd %~dp0

REM Plugins
call BuildPackage MITKPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage DicomPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage DicomExtensionPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage GenericSegmentationPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage ImageToolsPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage ManualSegmentationPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage MeshEditorPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage NeuroToolsPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage SandboxPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage SceneViewPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage SignalViewerPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage ClinicalReportPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage CmguiPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage RemoteDataPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage SecurityPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage SshPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage StatisticsPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage TavernaPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage VMTKPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
call BuildPackage WebServicesPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
cd %BIN_FOLDER%
%ZIP% a %OUTPUT_FOLDER%\%VERSION%\Packages\WebServicesPlugin-%VERSION%-%PLATFORM%.zip wsrepo -r
cd %~dp0
call BuildPackage XNATPlugin %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%

REM CommandLinePlugins
del %VERSION%\Packages\CommandLinePlugins-%VERSION%-%PLATFORM%.zip
cd %BIN_FOLDER%
%ZIP% a %OUTPUT_FOLDER%\%VERSION%\Packages\CommandLinePlugins-%VERSION%-%PLATFORM%.zip commandlineplugins -r
cd %~dp0

REM Modules
call BuildPackage Modules %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%

call BuildPackage ThirdParty %VERSION% %PLATFORM% %BIN_FOLDER% %OUTPUT_FOLDER%
