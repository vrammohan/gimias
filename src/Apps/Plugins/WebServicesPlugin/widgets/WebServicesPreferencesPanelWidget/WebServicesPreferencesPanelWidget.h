/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _WebServicesPreferencesPanelWidget_H
#define _WebServicesPreferencesPanelWidget_H

#include "WebServicesPreferencesPanelWidgetUI.h"
#include "WebServerProcessor.h"

#include "corePreferencesPage.h"
#include "WebServerConfigurationPanelWidget.h"

/**

\ingroup WebServicesPlugin
\author Xavi Planes
\date Sept 2011
*/
class WebServicesPreferencesPanelWidget : public WebServicesPreferencesPanelWidgetUI,
                                          public Core::Widgets::PreferencesPage {

  // OPERATIONS
public:
  coreDefineBaseWindowFactory1param(WebServicesPreferencesPanelWidget,
                                    WebServerProcessor::Pointer);

  //!
  WebServicesPreferencesPanelWidget(WebServerProcessor::Pointer processor,
                                    wxWindow *parent, int id = wxID_ANY,
                                    const wxPoint &pos = wxDefaultPosition,
                                    const wxSize &size = wxDefaultSize, long style = 0);

  //!
  ~WebServicesPreferencesPanelWidget();

  //! Add button events to the bridge and call UpdateWidget()
  void OnInit();

private:
  //! Update GUI from working data
  void UpdateWidget();

  //! Update working data from GUI
  void UpdateData();

  //! Start/ Stop web server
  virtual void OnStart(wxCommandEvent &event);

  //! Show log file
  virtual void OnShowLog(wxCommandEvent &event);

  //! Show server status
  void OnServerStatus(wxCommandEvent &event);

  //!
  void OnConfiguration(wxCommandEvent &event);

  DECLARE_EVENT_TABLE();

  // ATTRIBUTES
private:
  //!
  WebServerProcessor::Pointer m_Processor;

  //!
  Core::Widgets::LogFileViewer *m_LogViewer;

  //!
  WebServerConfigurationPanelWidget *m_Configuration;
};

#endif //_WebServicesPreferencesPanelWidget_H
