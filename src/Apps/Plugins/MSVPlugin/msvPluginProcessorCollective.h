/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _msvPluginProcessorCollective_H
#define _msvPluginProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

namespace msvPlugin {

/**
This class instantiates all processors used in the plugin and registers them.

\ingroup msvPlugin
*/

class ProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(ProcessorCollective, Core::SmartPointerObject);

private:
  //! The constructor instantiates all the processors and connects them.
  ProcessorCollective();

}; // class ProcessorCollective

} // namespace msvPlugin{

#endif //_msvPluginProcessorCollective_H
