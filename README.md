# Installation Process

This project is an on going project in **alpha** state. Some things are fixed in order to compile it successfully but certain things need improvement. Those things are:

- Update to a proper cmake usage
- Improve project folder structure
- 3party binaries when possible
- OSX and GNU/Linux first class citizens

## Vagrant GNU/Linux Debian Jessie VM

If you know what [Vagrant][va] is then you could find [here a vagrant recipe][recipe] for Linux. NOTICE: at this moment, vagrant doesn't work for this branch and repo but for open source only.

[va]: https://www.vagrantup.com/
[recipe]: https://bitbucket.org/CISTIB/cilab-gimiax-vagrant-linux64/overview

## Linux Installation Process

If you are using clang to compile the project you must to add this flag to `cmake` `-DCMAKE_COMPILER_IS_GNUCXX=TRUE`.

These are the steps you have to follow. You have to install these packages:

```bash
sudo apt-get install gcc g++ make autoconf libtool gdb \
    cmake git libosmesa6-dev libxt-dev libgtk2.0-dev \
    libgl1-mesa-dev pkg-config uuid-dev cmake-*-gui \
    libglu1-mesa-dev libtiff-dev wx3.0-headers wx3.0-i18n \
    libelfg0-dev build-essential vim libwxgtk-media3.0-dev \
    libboost1.55-dev
```

After packages have been installed you have to create a dir in which you can
build the code `mkdir build`.

There is another option that's using valgrant; [this repository][1] has all information about it.

[1]: https://bitbucket.org/CISTIB/cilab-gimiax-vagrant-linux64

## Windows Installation Process

###Prerequisites

1. Compile and install [ThirdParty][3party] **experimental** branch. For **Windows**, do a `git checkout 225bceb` on the cloned repository to get the commit `225bceb`. This is the compatible version for Windows.
2. Download [python][python_download] version 2.7.x. **x** could be any number of your choice but please do not use python 3.
3. Install [CSnake-v2.6.0][csnake_windows] from github.

[3party]: https://bitbucket.org/CISTIB/thirdparty
[python_download]: https://www.python.org/downloads/
[csnake_windows]: https://github.com/csnake-org/CSnake/releases/download/v2.6.0/CSnake-2.6.0.0-Setup.exe

**Steps to follow:**

* Checkout the `experimental` branch from `Gimias and Toolkit` repositories.
* Then on Gimias do a `git checkout c132ce3` to get the commit `c132ce3`.  
* Similarly on Toolkit do a `git checkout 521a780` to get the commit `521a780`. These are currently the stable versions.  
* Inside `gimias/src/Modules/` copy all the modules listed in the [README.md](https://bitbucket.org/CISTIB/gimias/src/8351fe7ecf736cc6da38e0472c1aa9725c3988fc/src/Modules/README.md?at=experimental&fileviewer=file-view-default), from the toolkit source.  
* Now start CSnake application and select all the project folders as shown below.

![CSnakeGUI_Context.PNG](https://bitbucket.org/repo/9x96bB/images/3918852025-CSnakeGUI_Context.PNG)

* Then under project configuration select the `CSnake File` from this repository's source folder and the corresponding instance `gimias`.
* Go to the `Options` tab at the top and select the compiler and the path settings to all the tools as shown below.

![CSnakeGUI_Options.PNG](https://bitbucket.org/repo/9x96bB/images/2069332810-CSnakeGUI_Options.PNG)

* Go to the `Select Projects` tab and deselect all the `Tests` and leave `everything else selected` as shown below.

![CSnakeGUI_SelectProjects.PNG](https://bitbucket.org/repo/9x96bB/images/3054988471-CSnakeGUI_SelectProjects.PNG)

* Get back to the `Context` tab and click on `Update` button.
* After successful update, click on the `Create CMake files and run CMake` button to generate project files and eventually the solution for `gimias`.
* Next click on the `Install files to Build Folder` button to install all the third party dependencies from the compiled third party folder before and other resource files required by the gimias.
* After installation of files click on the `Launch IDE`. This will open the Visual Studio with the `gimias solution`. Compile the solution.
* The binaries/libraries (.exe, .dll, .lib files) will be created under `bin\Debug (or) Release` folder in the build folder specified in the CSnake.
* The Debug/Release depends on the build configuration chosen while compiling the solution in Visual Studio.
* Finally copy all DLLs, debug/release from the C:\cistib\3party\wxWidgets\lib\vc_x64_dll directory to the respective build folder `bin\Debug (or) Release`. The debug version has a 'd' letter before the first underscore in the library name for instance wxbase30u**d**_net_vc100_x64.dll.
* Sometimes it happens that when performing `Install files to Build Folder` **not all** the third party DLLs are installed. If this is the case then do copy explicitly, all DLLs from the `<thirdparty build folder>\bin\Debug (or) Release` to the `<gimias build folder>\bin\Debug (or) Release` except for `version.dll` file.

## OSX Installation Process (WIP)

If you are using clang to compile the project you must add this flag to `cmake` `-DCMAKE_COMPILER_IS_GNUCXX=TRUE`.

You have to install these packages using `brew`:

```bash
# TODO brew recipes
Update vagrant deploy
brew install ...
```