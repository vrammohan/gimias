# Used to configure signalViewerPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

signalViewerPlugin = GimiasPluginProject("SignalViewerPlugin", api)

projects = [
    gmCore,
    wxMathPlot
]
signalViewerPlugin.AddProjects(projects, includeInSolution = False)

signalViewerPlugin.AddSources(["*.cxx", "*.h"])
signalViewerPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
signalViewerPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "PlotWidget",
  "SignalTimePropagationPanelWidget",
  "SignalReaderWidget"
  ]
signalViewerPlugin.AddWidgetModules(widgetModules, _useQt = 0)

signalViewerPlugin.SetPrecompiledHeader("SignalViewerPluginPCH.h")

