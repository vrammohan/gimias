/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef coreDownloadXMLReader_H
#define coreDownloadXMLReader_H

#include "coreBaseDataEntityReader.h"
#include "coreDataEntityWriter.h"
#include "coreSettings.h"
#include "coreKernel.h"
#include "coreDirectory.h"
#include "coreDataEntityReader.h"

#include <wx/zipstrm.h>
#include <wx/wfstream.h>

namespace Core {
namespace IO {
  /**
  A specialization of the DataEntityReader class for reading a specific purpose XML used
  to download data from XNAT to Unicore.

  \ingroup XNATPlugin
  \author Albert Sanchez
  \date 04 Jan 2012
  */

  class DownloadXMLReader : public BaseDataEntityReader {
  public:
    coreDeclareSmartPointerClassMacro(Core::IO::DownloadXMLReader, BaseDataEntityReader);

    struct DownloadElement {
      std::string URL;
      std::string usr;
      std::string pwd;
      std::string DestinationFolder;
      std::string Filename;
    };
    //!
    virtual void ReadData();

  protected:
    DownloadXMLReader(void);
    virtual ~DownloadXMLReader(void);

    //!
    virtual boost::any ReadSingleTimeStep(int iTimeStep, const std::string &filename);

  private:
    coreDeclareNoCopyConstructors(DownloadXMLReader);

    std::string m_TmpDir;

    std::string IntegerToString(int i);
  };
}
}

#endif // coreDownloadXMLReader_H
