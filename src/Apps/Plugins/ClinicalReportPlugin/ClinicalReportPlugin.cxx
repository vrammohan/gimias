/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportPlugin.h"

// core
#include "coreReportExceptionMacros.h"
#include "corePluginMacros.h"
#include "coreProfile.h"

coreBeginDefinePluginMacro(ClinicalReportPlugin)
    // coreDefinePluginAddProfileMacro("ClinicalReport")
    coreEndDefinePluginMacro()

        ClinicalReportPlugin::ClinicalReportPlugin(void)
    : FrontEndPlugin() {
  try {
    m_Processors = ClinicalReportProcessorCollective::New();
    m_Widgets = ClinicalReportWidgetCollective::New();

    m_Widgets->Init();
  }
  coreCatchExceptionsReportAndNoThrowMacro(ClinicalReportPlugin::ClinicalReportPlugin)
}

ClinicalReportPlugin::~ClinicalReportPlugin(void) {}
