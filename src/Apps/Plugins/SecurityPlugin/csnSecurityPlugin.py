# Used to configure SecurityPlugin

from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

securityPlugin = GimiasPluginProject("SecurityPlugin", api)

projects = [
    gmCoreLight, 
	openssl,
	poco,
	securityLib
]
securityPlugin.AddProjects(projects)

securityPlugin.AddSources(["*.cxx", "*.h"])
securityPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
securityPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "SecurityPluginUserManager",
  "PluginSecurityWidget"
  ]
securityPlugin.AddWidgetModules(widgetModules, _useQt = 0)

securityPlugin.SetPrecompiledHeader("SecurityPluginPCH.h")

