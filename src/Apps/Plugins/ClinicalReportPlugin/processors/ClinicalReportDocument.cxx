/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportDocument.h"
#include "ClinicalReportPluginUtilities.h"

#include "coreSettings.h"
#include "coreDirectory.h"

ClinicalReportDocument::ClinicalReportDocument() {
  m_HeaderFont = wxT("Arial");
  m_FooterFont = wxT("Arial");
  m_AuthorsFont = wxT("Arial");
  m_DateFont = wxT("Arial");
  m_TitleFont = wxT("Arial");
  m_RegularTextFont = wxT("Arial");

  m_HeaderFontSize = 20;
  m_FooterFontSize = 8;
  m_AuthorsFontSize = 12;
  m_DateFontSize = 9;
  m_TitleFontSize = 11;
  m_RegularTextFontSize = 10;
}

void ClinicalReportDocument::Header() {
  // Logos
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  m_ResourcePath = settings->GetResourcePath();

  // UPF logo
  wxString filename = m_ResourcePath + Core::IO::SlashChar + "upf.png";
  if (wxFileName(filename).FileExists()) {
    Image(filename, 10, 8, 18);
  }

  // Right image
  if (wxFileName(m_RightImage).FileExists()) {
    Image(m_RightImage, 172, 12, 28);
  }

  // Title
  SetFont(m_HeaderFont, wxT("B"), m_HeaderFontSize);
  Cell(0, 10, m_Title, 0, 0, wxPDF_ALIGN_CENTER);
  Ln(m_HeaderFontSize / 3);

  // Authors
  SetFont(m_AuthorsFont, wxT(""), m_AuthorsFontSize);
  Cell(0, 10, wxT("by " + m_Authors), 0, 0, wxPDF_ALIGN_CENTER);
  Ln(m_HeaderFontSize / 3);

  // Report date
  SetFont(m_AuthorsFont, wxT(""), m_AuthorsFontSize);
  Cell(0, 10, m_PatientInformation.GetAdquisitionDate().FormatDate(), 0, 0,
       wxPDF_ALIGN_CENTER);
  Ln(m_HeaderFontSize / 2);

  SetFillColour(wxColour(128, 128, 128));
  Cell(0, 1, wxT(""), 0, 1, wxPDF_ALIGN_CENTER, 1);

  Ln(m_HeaderFontSize / 2);
  SetFont(m_RegularTextFont, wxPDF_FONTSTYLE_BOLDITALIC, 8);
  wxString disclaimer =
      wxT("The content of this document is given as information to the Medical Doctor "
          "and should be considered as complementary information only and shall not "
          "under any circumstance be considered as a treatment indication.");

  MultiCell(0, 5, disclaimer, 0, wxPDF_ALIGN_JUSTIFY, 0);
  Ln(m_HeaderFontSize / 2);
}

void ClinicalReportDocument::Footer() {
  SetY(-15);
  SetFont(m_FooterFont, wxT("I"), m_FooterFontSize);
  // Page number
  Cell(0, 10, wxString::Format(wxT("Page %d/{nb}"), PageNo()), 0, 0, wxPDF_ALIGN_CENTER);
}

void ClinicalReportDocument::SetTitle(wxString title) {
  m_Title = title;
  wxPdfDocument::SetTitle(m_Title);
}

wxString ClinicalReportDocument::GetTitle() { return m_Title; }

void ClinicalReportDocument::SetAuthors(wxString authors) {
  m_Authors = authors;
  wxPdfDocument::SetAuthor(m_Authors);
}

void ClinicalReportDocument::SetImage(std::string image) { m_RightImage = image; }

wxString ClinicalReportDocument::GetAuthors() { return m_Authors; }

void ClinicalReportDocument::SetComments(wxString comments) { m_Comments = comments; }

wxString ClinicalReportDocument::GetComments() { return m_Comments; }

void ClinicalReportDocument::SetPatientInformation(
    ClinicalReportPatientInformation patientInfo) {
  m_PatientInformation = patientInfo;
}

void ClinicalReportDocument::SetReportElements(
    std::vector< ClinicalReportElement > ReportElements) {
  m_ReportElements = ReportElements;
}

void ClinicalReportDocument::WritePatientInformation() {
  SetFont(m_TitleFont, wxT("B"), m_TitleFontSize);
  WriteTitle("Patient information");
  SetFont(m_RegularTextFont, wxT(""), m_RegularTextFontSize);
  SetFillColour(wxColour(224, 224, 224));

  Ln(2);

  Cell(60, 8, wxT("Id"), wxPDF_BORDER_FRAME, 0, wxPDF_ALIGN_LEFT, 1);
  Cell(60, 8, m_PatientInformation.GetId(), wxPDF_BORDER_FRAME, 1, wxPDF_ALIGN_LEFT, 1);
  Cell(60, 8, wxT("Name"), wxPDF_BORDER_FRAME, 0, wxPDF_ALIGN_LEFT, 0);
  Cell(60, 8, m_PatientInformation.GetName(), wxPDF_BORDER_FRAME, 1, wxPDF_ALIGN_LEFT, 0);
  Cell(60, 8, wxT("Date of birth"), wxPDF_BORDER_FRAME, 0, wxPDF_ALIGN_LEFT, 1);
  Cell(60, 8, m_PatientInformation.GetBirthDate().FormatDate(), wxPDF_BORDER_FRAME, 1,
       wxPDF_ALIGN_LEFT, 1);
  Cell(60, 8, wxT("Age"), wxPDF_BORDER_FRAME, 0, wxPDF_ALIGN_LEFT, 0);
  Cell(60, 8, wxString::Format(wxT("%d"), m_PatientInformation.GetAge()),
       wxPDF_BORDER_FRAME, 1, wxPDF_ALIGN_LEFT, 0);
  Cell(60, 8, wxT("Sex"), wxPDF_BORDER_FRAME, 0, wxPDF_ALIGN_LEFT, 1);
  Cell(60, 8, (m_PatientInformation.GetSex() ? wxT("Female") : wxT("Male")),
       wxPDF_BORDER_FRAME, 1, wxPDF_ALIGN_LEFT, 1);
}

bool SortByElementOrder(ClinicalReportElement a, ClinicalReportElement b) {
  return (a.GetPosition() < b.GetPosition());
}

void ClinicalReportDocument::WriteReportElements() {
  SetFont(m_TitleFont, wxT("B"), m_TitleFontSize);
  WriteTitle("Detailed information");
  SetFont(m_RegularTextFont, wxT(""), m_RegularTextFontSize);

  Ln();

  size_t n = m_ReportElements.size();

  std::sort(m_ReportElements.begin(), m_ReportElements.end(), SortByElementOrder);

  for (unsigned int i = 0; i < n; i++) {
    if (m_ReportElements[i].GetSelected()) {
      ClinicalReportElement ReportElement = m_ReportElements[i];

      bool isImage = (ReportElement.GetType() == "image");
      bool isTable = !isImage && (ReportElement.GetType() == "table");

      if (isImage) {
        Ln(10);

        if (ReportElement.GetTitle().size() > 0) {
          MultiCell(0, 10, ReportElement.GetTitle(), wxPDF_BORDER_NONE, wxPDF_ALIGN_LEFT,
                    1);

          Ln(2);
        }

        AddAnImage(ReportElement.GetScreenshot(), 100,
                   GetPageWidth() - GetLeftMargin() - GetRightMargin(), true, true);

        if (ReportElement.GetComments().size() > 0) {
          MultiCell(0, 10, ReportElement.GetComments(), wxPDF_BORDER_NONE,
                    wxPDF_ALIGN_LEFT, 1);

          Ln(10);
        }

      } else if (isTable) {
        if (ReportElement.GetTitle().size() > 0) {
          MultiCell(0, 10, ReportElement.GetTitle(), wxPDF_BORDER_NONE, wxPDF_ALIGN_LEFT,
                    1);

          Ln(2);
        }

        std::vector< std::string > header;
        std::vector< std::vector< std::string > > lines;

        std::pair< std::vector< std::string >, std::vector< std::vector< std::string > > >
            res;

        res = ClinicalReportPluginUtilities::GetTableFormat(ReportElement.GetTable());

        header = res.first;
        lines = res.second;

        size_t N = header.size();
        int step = 4;

        for (unsigned int i = 0; i < N; i += step) {
          std::vector< std::vector< std::string > > _lines;

          std::vector< std::string > _header(step);
          std::vector< std::string >::iterator _it;

          if (i + step >= N)
            _it = header.end();
          else
            _it = header.begin() + i + step;

          std::copy(header.begin() + i, _it, _header.begin());

          for (int j = 0; j < lines.size(); j++) {
            std::vector< std::string > _line(step);
            std::vector< std::string >::iterator _it2;

            if (i + step >= N)
              _it2 = lines[j].end();
            else
              _it2 = lines[j].begin() + i + step;

            std::copy(lines[j].begin() + i, _it2, _line.begin());

            _lines.push_back(_line);
          }

          WriteTable(_header, _lines);
        }

        if (ReportElement.GetComments().size() > 0) {
          MultiCell(0, 10, ReportElement.GetComments(), wxPDF_BORDER_NONE,
                    wxPDF_ALIGN_LEFT, 1);

          Ln(2);
        }

      } else // is text!
      {
        if (ReportElement.GetComments().size() > 0) {
          MultiCell(0, 10, ReportElement.GetComments(), wxPDF_BORDER_NONE,
                    wxPDF_ALIGN_LEFT, 1);

          Ln(10);
        }
      }
    }
  }
}

void ClinicalReportDocument::AddAnImage(std::string imagePath, int MaxHeight,
                                        int MaxWidth, bool checkPageEnd,
                                        bool advanceAfter) {
  int originalWidth;
  int originalHeight;
  int finalWidth;
  int finalHeight;

  wxImage image(imagePath);

  originalWidth = image.GetWidth();
  originalHeight = image.GetHeight();

  if (originalWidth > MaxWidth || originalHeight > MaxHeight) // needs a resize
  {
    if (originalWidth > MaxWidth) {
      finalWidth = MaxWidth;
      double reduction = (double)MaxWidth / (double)originalWidth;
      finalHeight = (int)(reduction * (double)originalHeight);

      originalHeight =
          finalHeight; // just to simplify, originalHeight is not used after this
      if (originalHeight > MaxHeight) // we need to reduce more
      {
        finalHeight = MaxHeight;
        double reduction = (double)MaxHeight / (double)originalHeight;
        finalWidth = (int)(reduction * (double)finalWidth);
      }
    } else // originalHeight>MaxHeight
    {
      finalHeight = MaxHeight;
      double reduction = (double)MaxHeight / (double)originalHeight;
      finalWidth = (int)(reduction * (double)originalWidth);

      originalWidth = MaxWidth; // just to simplify, originalWidth is not used after this
      if (originalWidth > MaxWidth) // we need to reduce more
      {
        finalWidth = MaxWidth;
        double reduction = (double)MaxWidth / (double)originalWidth;
        finalHeight = (int)(reduction * (double)finalHeight);
      }
    }
  }

  if (checkPageEnd) {
    if (GetPageHeight() - 20 < GetY() + finalHeight) {
      AddPage();
    }
  }

  int pageCenter = GetPageWidth() / 2;
  int imagePosX = pageCenter - finalWidth / 2;

  Image(imagePath, imagePosX, GetY(), finalWidth, finalHeight);

  if (advanceAfter) {
    SetX(0);
    SetY(GetY() + finalHeight);
  }

  Ln(2);
}

void ClinicalReportDocument::WriteComments() {
  SetFont(m_TitleFont, wxT("B"), m_TitleFontSize);
  WriteTitle("General comments");
  SetFont(m_RegularTextFont, wxT(""), m_RegularTextFontSize);

  MultiCell(0, 10, m_Comments, wxPDF_BORDER_NONE, 0, wxPDF_ALIGN_LEFT, 0);
}

void ClinicalReportDocument::WriteTitle(wxString title) {
  SetFillColour(wxColour(240, 240, 240));
  Ln();
  Cell(0, 10, title, wxPDF_BORDER_FRAME, 0, wxPDF_ALIGN_LEFT, 1);
  Ln();
}

void ClinicalReportDocument::WriteTable(std::vector< std::string > header,
                                        std::vector< std::vector< std::string > > lines) {
  // Colors, line width and bold font
  SetFillColour(wxColour(64, 64, 64));
  SetTextColour(255);
  SetDrawColour(wxColour(0, 0, 0));
  SetLineWidth(.3);
  SetFont(wxT(""), wxT("B"));

  // Header
  size_t nRowsMax = 0;

  for (int i = 0; i < header.size(); i++) {
    Cell(45, 7, header[i], wxPDF_BORDER_FRAME, 0, wxPDF_ALIGN_LEFT, 1);
    if (header.size() > nRowsMax)
      nRowsMax = header.size();
  }
  Ln();

  // Color and font restoration
  SetFillColour(wxColour(224, 224, 224));
  SetTextColour(0);
  SetFont(wxT(""));

  int fill = 0;

  for (int i = 0; i < lines.size(); i++) {
    std::vector< std::string > row = lines[i];

    for (int j = 0; j < row.size(); j++) {
      Cell(45, 6, row[j], wxPDF_BORDER_LEFT | wxPDF_BORDER_RIGHT, 0, wxPDF_ALIGN_LEFT,
           fill);
    }

    for (size_t k = row.size(); k < header.size(); k++) {
      Cell(45, 6, "", wxPDF_BORDER_LEFT | wxPDF_BORDER_RIGHT, 0, wxPDF_ALIGN_LEFT, fill);
    }

    Ln();
    fill = 1 - fill;

    if (row.size() > nRowsMax)
      nRowsMax = row.size();
  }

  // Closure line
  Cell(nRowsMax * 45, 0, wxT(""), wxPDF_BORDER_TOP);

  Ln(10);
}
