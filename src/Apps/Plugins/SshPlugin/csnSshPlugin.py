# Used to configure SshPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

sshPlugin = GimiasPluginProject("SshPlugin", api)

projects = [
    gmCore, 
    sshAPI
]
sshPlugin.AddProjects(projects)

sshPlugin.AddSources(["*.cxx", "*.h"])
sshPlugin.AddSources(["processors/*.cxx", "processors/*.h"], checkExists = 0)
sshPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  'SshPluginConfigurationPanelWidget'
  ]
sshPlugin.AddWidgetModules(widgetModules, _useQt = 0)

sshPlugin.SetPrecompiledHeader("SshPluginPCH.h")

