/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#ifndef _sshAskPasswordWx_H
#define _sshAskPasswordWx_H

#include "sshAskPassword.h"

/**
\brief Get password for SSH Connection using wxWidgets
\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/
class sshPluginAskPasswordWx : public sshAskPassword {
public:
  //!
  sshPluginAskPasswordWx();

  virtual ~sshPluginAskPasswordWx();

  /** Redefined
  This function can be called by secondary thread
  */
  bool AskPassword();

  /** Redefined
  This function can be called by secondary thread
  */
  void AskUserConfirmation();

private:
  //!
  void AskPasswordInternal();

  //!
  void AskUserConfirmationInternal();

  //!
  bool m_AskPasswordReturnValue;
};

#endif //_sshAskPasswordWx_H
