
/*
 * Copyright 2004,2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <axis2_svc_skeleton.h>
#include <axutil_log_default.h>
#include <axutil_error_default.h>
#include <axutil_array_list.h>
#include <axiom_text.h>
#include <axiom_node.h>
#include <axiom_element.h>
#include <stdio.h>
#include "itkSimpleFastMutexLock.h"

#include "wsModuleExecution.h"

/** Mutex lock to protect use of Framework classes by multiple theads.
blLightObject smart pointer implementation does not support multithreading */
itk::SimpleFastMutexLock m_FrameworkLock;

axiom_node_t *axis2_gimias_execute(const axutil_env_t *env, axiom_node_t *node);

int AXIS2_CALL gimias_free(axis2_svc_skeleton_t *svc_skeleton, const axutil_env_t *env);

axiom_node_t *AXIS2_CALL gimias_invoke(axis2_svc_skeleton_t *svc_skeleton,
                                       const axutil_env_t *env, axiom_node_t *node,
                                       axis2_msg_ctx_t *msg_ctx);

int AXIS2_CALL gimias_init(axis2_svc_skeleton_t *svc_skeleton, const axutil_env_t *env);

axiom_node_t *AXIS2_CALL gimias_on_fault(axis2_svc_skeleton_t *svc_skeli,
                                         const axutil_env_t *env, axiom_node_t *node);

axiom_node_t *axis2_gimias_execute(const axutil_env_t *env, axiom_node_t *node) {
  axiom_node_t *return_node = NULL;

  AXIS2_ENV_CHECK(env, NULL);

  m_FrameworkLock.Lock();

  wsModuleExecution exec(env);

  try {
    exec.GetModuleDescription(node);

    // Update Module Parameter values
    exec.SetParameters(node);

    // Create DynProcessor
    exec.CreateProcessor();
  } catch (Core::Exceptions::Exception &e) {
    exec.SetError(e);

    m_FrameworkLock.Unlock();

    // When return node is NULL, send fault message
    return_node = NULL;
    return return_node;
  }

  m_FrameworkLock.Unlock();

  try {
    // Execute
    exec.Execute();

    // Update output values
    return_node = exec.GetResponse();
  } catch (Core::Exceptions::Exception &e) {
    exec.SetError(e);

    // When return node is NULL, send fault message
    return_node = NULL;
  }

  return return_node;
}

static const axis2_svc_skeleton_ops_t gimias_svc_skeleton_ops_var = {
  gimias_init, gimias_invoke, gimias_on_fault, gimias_free
};

axis2_svc_skeleton_t *axis2_gimias_create(const axutil_env_t *env) {
  axis2_svc_skeleton_t *svc_skeleton = NULL;
  svc_skeleton =
      (axis2_svc_skeleton_t *)AXIS2_MALLOC(env->allocator, sizeof(axis2_svc_skeleton_t));

  svc_skeleton->ops = &gimias_svc_skeleton_ops_var;

  svc_skeleton->func_array = NULL;

  return svc_skeleton;
}

int AXIS2_CALL gimias_init(axis2_svc_skeleton_t *svc_skeleton, const axutil_env_t *env) {
  svc_skeleton->func_array = axutil_array_list_create(env, 0);
  axutil_array_list_add(svc_skeleton->func_array, env, "helloString");
  return AXIS2_SUCCESS;
}

axiom_node_t *AXIS2_CALL gimias_invoke(axis2_svc_skeleton_t *svc_skeleton,
                                       const axutil_env_t *env, axiom_node_t *node,
                                       axis2_msg_ctx_t *msg_ctx) {
  return axis2_gimias_execute(env, node);
}

axiom_node_t *AXIS2_CALL gimias_on_fault(axis2_svc_skeleton_t *svc_skeli,
                                         const axutil_env_t *env, axiom_node_t *node) {
  axiom_node_t *error_node = NULL;
  axiom_element_t *error_ele = NULL;
  axiom_node_t *node1 = NULL, *node2 = NULL;

  error_ele = axiom_element_create(env, node, "gimiasServiceError", NULL, &error_node);
  axiom_element_create(env, error_node, "error", NULL, &node1);
  axiom_text_create(env, node1, "gimias service failed ", &node2);
  return error_node;
}

int AXIS2_CALL gimias_free(axis2_svc_skeleton_t *svc_skeleton, const axutil_env_t *env) {
  if (svc_skeleton->func_array) {
    axutil_array_list_free(svc_skeleton->func_array, env);
    svc_skeleton->func_array = NULL;
  }

  if (svc_skeleton) {
    AXIS2_FREE(env->allocator, svc_skeleton);
    svc_skeleton = NULL;
  }

  return AXIS2_SUCCESS;
}

extern "C" AXIS2_EXPORT int axis2_get_instance(axis2_svc_skeleton_t **inst,
                                               const axutil_env_t *env) {
  *inst = axis2_gimias_create(env);
  if (!(*inst)) {
    return AXIS2_FAILURE;
  }

  return AXIS2_SUCCESS;
}

extern "C" AXIS2_EXPORT int axis2_remove_instance(axis2_svc_skeleton_t *inst,
                                                  const axutil_env_t *env) {
  axis2_status_t status = AXIS2_FAILURE;
  if (inst) {
    status = AXIS2_SVC_SKELETON_FREE(inst, env);
  }
  return status;
}
