from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmFiltering = api.CreateCompiledProject("gmFiltering", "dll")

gmFiltering.AddProjects([ 
  gmDataHandling,
  slicer
])

gmFiltering.SetPrecompiledHeader("gmFilteringPCH.h")

gmFiltering.AddSources(["src/*.cxx" ], checkExists = False)
gmFiltering.AddSources(["src/*.txx" ], checkExists = False)
gmFiltering.AddSources(["src/*.h" ], checkExists = False)
gmFiltering.AddIncludeFolders(["src"])

if api.GetCompiler().TargetIsWindows():
    gmFiltering.AddDefinitions(["/bigobj"], private = 1)
