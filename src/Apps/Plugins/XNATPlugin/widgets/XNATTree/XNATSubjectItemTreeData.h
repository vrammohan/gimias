/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef XNATSubjectItemTreeData_H
#define XNATSubjectItemTreeData_H

#include <wx/treectrl.h>

/**
A class that holds some information about subject in the tree item

\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class SubjectItemTreeData : public wxTreeItemData {
public:
  //!
  SubjectItemTreeData(int id, std::string &subjectId, std::string &subjectName,
                      std::string &subjectBirthDate, std::string &subjectSex)
      : m_id(id), m_subjectId(subjectId), m_subjectName(subjectName),
        m_subjectBirthDate(subjectBirthDate), m_subjectSex(subjectSex) {}

  //!
  ~SubjectItemTreeData() {}

  //!
  int GetId() const { return m_id; }
  //!
  std::string GetSubjectId() const { return m_subjectId; }
  //!
  std::string GetSubjectName() const { return m_subjectName; }
  //!
  std::string GetSubjectBirthDate() const { return m_subjectBirthDate; }
  //!
  std::string GetSubjectSex() const { return m_subjectSex; }

private:
  //!
  int m_id;
  //!
  std::string m_subjectId;
  //!
  std::string m_subjectName;
  //!
  std::string m_subjectBirthDate;
  //!
  std::string m_subjectSex;
};

#endif // XNATSubjectItemTreeData_H
