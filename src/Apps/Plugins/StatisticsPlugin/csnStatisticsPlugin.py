from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

StatisticsPlugin = GimiasPluginProject("StatisticsPlugin", api)

projects = [
    gmCore,
]

StatisticsPlugin.AddProjects(projects)

StatisticsPlugin.AddSources(["*.cxx", "*.h"])
StatisticsPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
StatisticsPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "StatisticsToolbar",
  "StatisticsWorkingArea",
  "TableView"
]

StatisticsPlugin.AddWidgetModules(widgetModules, _useQt = 0)

StatisticsPlugin.SetPrecompiledHeader("StatisticsPluginPCH.h")

StatisticsPlugin.AddTests(["tests/*.*"], cxxTest)

StatisticsPlugin.AddFilesToInstall(StatisticsPlugin.Glob("resource"), "resource")

