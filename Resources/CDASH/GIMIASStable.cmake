
###################################################################################
CMAKE_MINIMUM_REQUIRED (VERSION 2.2)
include(GIMIASTestMacro.cmake)
include(GIMIASDoxyGenMacro.cmake)
include(GIMIASInstallerMacro.cmake)

###################################################################################
SET(CLIENT_BASE_DIRECTORY "C:/Code")
SET(CLIENT_CMAKE_PATH "C:/Program Files (x86)/CMake 2.8/bin")
SET(CLIENT_SITE "CDASHPC.UPF")

###################################################################################
SET(JOB_BUILDTYPE Experimental)
SET(JOB_OS_NAME "Windows")
SET(JOB_OS_VERSION "7")
SET(JOB_OS_BITS "64")
SET(JOB_COMPILER_NAME "msvc")
set(JOB_INITIAL_CACHE "\n")
set(CTEST_NOTES_FILES ${CTEST_SCRIPT_DIRECTORY}/${CTEST_SCRIPT_NAME})

###################################################################################
MACRO(JOB_FAILED)
  file(DOWNLOAD "${CTEST_DROP_METHOD}://${CTEST_DROP_SITE}/submit.php?siteid=1&jobfailed=1" "${CLIENT_BASE_DIRECTORY}/scriptfailed.txt")
  return()
ENDMACRO(JOB_FAILED)

###################################################################################
# GIMIAS
set(CTEST_PROJECT_NAME "GIMIAS-Stable")
set(JOB_BUILD_PROJECT_NAME "GIMIAS-Stable")
SET(JOB_REPOSITORY "https://svn.gimias.org/svn/toolkit/gimias/stable/GIMIAS_1_5/")

SET(JOB_COMPILER_VERSION "2009_32")
TEST_GIMIAS( )

SET(JOB_COMPILER_VERSION "2009_64")
TEST_GIMIAS( )

###################################################################################
# Extensions
set(CTEST_PROJECT_NAME "Extensions-Stable")
set(JOB_BUILD_PROJECT_NAME "GIMIAS-Stable") # Reuse same build folder
SET(JOB_REPOSITORY "https://svn.gimias.org/svn/toolkit/gimias/extensions/branches/1_5")

SET(JOB_COMPILER_VERSION "2009_32")
TEST_GIMIAS( )

SET(JOB_COMPILER_VERSION "2009_64")
TEST_GIMIAS( )

###################################################################################
# DoxyGen
set(CTEST_PROJECT_NAME "GIMIAS")

SET(PROJECT_NUMBER "1.6.r1")
set(CTEST_BINARY_NAME ${CTEST_PROJECT_NAME}_${PROJECT_NUMBER}_DoxyGen)
SET(CLIENT_DOXYGEN_EXE "C:/Program Files/doxygen/bin/doxygen.exe")
GENERATE_DOXYGEN()

###################################################################################
# Installer
set(CTEST_PROJECT_NAME "GIMIAS-Stable")

FIND_PACKAGE(Subversion) 
IF(Subversion_FOUND) 
  Subversion_WC_INFO("${CLIENT_BASE_DIRECTORY}/src/${JOB_BUILD_PROJECT_NAME}" GIMIAS) 
  MESSAGE("Current revision is ${GIMIAS_WC_REVISION}") 
ENDIF(Subversion_FOUND) 

SET(PROJECT_NUMBER "1.6.r1.${GIMIAS_WC_REVISION}")
SET(MAIN_PROJECT_NUMBER "1.6")
SET(ARCHITECTURE "win64" )
SET(JOB_COMPILER_VERSION "2009_64")
set(CTEST_BINARY_NAME ${CTEST_PROJECT_NAME}_Installer)
SET(CLIENT_INSTALLER_EXE "C:/Program Files (x86)/NSIS/makensis.exe")
GENERATE_INSTALLER()

SET(PROJECT_NUMBER "1.6.r1.${GIMIAS_WC_REVISION}")
SET(MAIN_PROJECT_NUMBER "1.6")
SET(ARCHITECTURE "win32" )
SET(JOB_COMPILER_VERSION "2009_32")
set(CTEST_BINARY_NAME ${CTEST_PROJECT_NAME}_Installer)
SET(CLIENT_INSTALLER_EXE "C:/Program Files (x86)/NSIS/makensis.exe")
GENERATE_INSTALLER()


