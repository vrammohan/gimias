/*
 *  msvWxVTKButtons.h porting of msvQVTKButtons in wxWidgets
 *
 *
 *  Created by Luigi Carotenuto on 9/27/12.
 *  Copyright 2012 Universitat Pompeu Fabra. All rights reserved.
 *
 *  See License at: http://tiny.cc/QXJ4D
 *
 */

#ifndef msvWxVTKButtons_H
#define msvWxVTKButtons_H

//#include "msvWxWidgetsExport.h"

#include <wx/window.h>
#include <wx/image.h>

#include "msvWxVTKAnimate.h"

// forward declarations
class vtkButtonWidget;
class vtkButtonCallback;
class vtkButtonHighLightCallback;
class vtkRenderer;
class vtkDataSet;
class msvWxVTKButtons;

/**
 Caller interface
\ingroup msvPlugin
*/
class wxCallerWidget {
public:
  virtual void ShowInformation(msvWxVTKButtons *button, bool show) = 0;
  virtual void ShowTooltip(msvWxVTKButtons *button, bool show) = 0;
  //! Return true if data is being loaded
  virtual bool BeginAnimation(bool isFlight, unsigned int btnId) = 0;
  virtual void EndAnimation(bool isFlight, unsigned int btnId) = 0;
};

/**
 This is the tool representing a VTK buttons.
\ingroup msvPlugin
*/
class msvWxVTKButtons : public wxWindow {

public:
  /// Allow to execute and update the pipeline when something change.
  /*virtual*/
  void update();

  void Update() { update(); }; // wxWindow uses this (upperCase)

  /// Object constructor.
  msvWxVTKButtons(wxCallerWidget *parent, unsigned int id);

  /// Allow to show/hide button
  void setShowButton(bool show);

  /// Return showLabel flag
  bool showButton() const;

  /// Allow to show/hide label
  void setShowLabel(bool show);

  /// set the icon path
  void setIconFileName(wxString iconFileName);

  /// set the icon size
  void setIconSize(int size);

  /// Return showLabel flag
  bool showLabel() const;

  /// set the text label
  void setLabel(wxString text);

  /// Allow to activate FlyTo animation
  void setFlyTo(bool active);

  /// Return FlyTo flag
  bool flyTo() const;

  /// Allow to set button position on center or on corner
  void setOnCenter(bool onCenter);

  /// Return OnCenter flag
  bool onCenter() const;

  /// set the tooltip string
  void setDescription(wxString text);

  /// get tooltip
  wxString getDescription() const;

  /// add vtk button to Renderer
  void setCurrentRenderer(vtkRenderer *renderer);

  /// add vtk button to Renderer
  vtkRenderer *getCurrentRenderer();

  /// set bounds
  void setBounds(double b[6]);

  /// set the show/hide signal
  void showTooltip(bool value);

  /// set the show/hide signal
  void showInformation(bool value);

  /// Object destructor.
  virtual ~msvWxVTKButtons();

  /// retrieve button pointer.
  vtkButtonWidget *button();

  /// save the button preview image in fname
  /// \note If filename already exists -> return true
  bool buildPreview(vtkSmartPointer< vtkDataSet > data, int width, int height);

  /// get the preview full file path
  wxString getPreviewFName();

  /// set the preview full file path
  void setPreviewFName(const wxString &fname);

  /// Return this button id
  unsigned int getId() { return m_id; };

  // signal launched when animation begins
  bool beginAnimation();

  // signal launched when animation ends
  void endAnimation();

  ///
  void Animate();

  ///
  msvWxVTKAnimate *getAnimateCamera();

private:
  /// calculate Position (center or corner)
  void calculatePosition();

  vtkButtonWidget *m_ButtonWidget;     ///< VTK button widget.
  vtkButtonCallback *m_buttonCallback; ///< Callback called by picking on vtkButton
  vtkButtonHighLightCallback
      *m_highlightCallback; ///< Callback called by hovering over the button.
  msvWxVTKAnimate *m_AnimateCamera;

  wxString m_Label;        ///< label of the button
  wxString m_IconFileName; ///< File name of the image to be applied to the button.
  wxString m_prevFName;    ///< The fullpath fname where to save the preview
  wxString m_Description;
  bool m_ShowButton; ///< Flag to show/hide button
  bool m_ShowLabel;  ///< Flag to show/hide label
  bool m_FlyTo;      ///< Flag to activate FlyTo animation
  bool m_OnCenter;   ///< Flag to set button position on center or on corner (can be
                     ///refactored with a enum??)
  unsigned int m_id; ///< the id of the button as assigned by the caller
  int m_IconSize;

  wxCallerWidget *m_caller; ///< the caller wxWindow
};

#endif // msvWxVTKButtons_H
