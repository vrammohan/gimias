# Used to configure gmCoreLight
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmCoreLight = api.CreateCompiledProject("gmCoreLight", "library")
gmCoreLight.AddProjects([ 
  gmCommonObjects,
  gmDataHandling,
  gmKernel,
  gmWidgets,
  gmProcessors,
  gmWorkflow
])

#gmCoreLight.SetPrecompiledHeader("gmCoreLightPCH.h")
gmCoreLight.GenerateWin32Header(False)

if not api.GetCompiler().TargetIsWindows():
    gmCoreLight.AddLibraries(["util"])

gmCoreLight.AddFilesToInstall(gmCoreLight.Glob("Copyrights/*.txt"), "Copyrights")
gmCoreLight.AddFilesToInstall(gmCoreLight.Glob("resource"), "resource")
gmCoreLight.AddFilesToInstall(gmCoreLight.Glob("resource/local.xml"))
gmCoreLight.AddFilesToInstall(gmCoreLight.Glob("Core/*.xml"), "")
gmCoreLight.AddFilesToInstall(gmCoreLight.Glob("Core/*.xsl"), "")

#gmCoreLight.AddTests(["Tests/*.*"], cxxTest)
#gmCoreLight.testProject.SetPrecompiledHeader("gmCoreLightPCH.h")

