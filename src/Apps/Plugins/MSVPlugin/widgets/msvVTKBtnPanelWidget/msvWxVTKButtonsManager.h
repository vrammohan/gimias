/*
* Copyright (c) 2012,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef msvWxVTKButtonsManager_H
#define msvWxVTKButtonsManager_H

#include "coreObject.h"

class msvMultiRenderWindow;
class msvWxVTKButtons;

/**
\brief VTK buttons manager

- VTK Button manager
- Update buttons properties when selected

\ingroup msvPlugin
\author Xavi Planes
\date Nov 2012
*/
class PLUGIN_EXPORT msvWxVTKButtonsManager : public Core::SmartPointerObject {
public:
  coreDeclareSmartPointerClassMacro(msvWxVTKButtonsManager, Core::SmartPointerObject);

  //! Set rendering window
  void SetRenderWindow(msvMultiRenderWindow *renderWindow);

  //!
  void AddVTKButton(Core::DataEntity::Pointer dataEntity);
  //!
  void RemoveVTKButton(Core::DataEntity::Pointer dataEntity);
  //!
  void ShowVTKButton(Core::DataEntity *dataEntity, bool show = true);

  //!
  void UpdateDataObject(Core::DataEntity::Pointer dataEntity);
  //!
  void UpdatePreview(Core::DataEntity::Pointer dataEntity);

  //!
  msvWxVTKButtons *GetButton(int id);

  //!
  void SelectButton(int id);

  //!
  void UpdateCameraParameters(Core::DataEntity::Pointer dataEntity,
                              blTagMap::Pointer view);

  //!
  void GetCameraParameters(blTagMap::Pointer cameraParams);

  //! Show/Hide buttons and dialog
  void ShowButtons(bool show, std::map< unsigned int, bool > &backupShow);

  //!
  void GetCameraBounds(blTagMap::Pointer metadata);

  //!
  std::string GetScaleLevel(Core::DataEntity::Pointer dataEntity);

  //!
  void UpdateVTKButtonProperties(int id);

protected:
  //!
  msvWxVTKButtonsManager();

  //!
  virtual ~msvWxVTKButtonsManager(void);

  //!
  bool GetMetadataTag(blTagMap::Pointer tagMap, const std::string &name, double *,
                      int size);

  //!
  vtkDataSet *GetVTKObject(Core::DataEntity::Pointer dataEntity);

private:
  //! vector of buttons
  std::map< unsigned int, msvWxVTKButtons * > m_Buttons;

  //!
  msvMultiRenderWindow *m_RenderWindow;
};

#endif // msvMultiRenderWindow_H
