# Used to configure lcore
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

# Used to configure gimias
def CreateGIMIAS():
    
    gimias = api.CreateStandardModuleProject("Gimias", "executable")
    gimias.AddSources(gimias.Glob("GUI/MainApp/src/*.h"), checkExists = 1)
    gimias.AddSources(gimias.Glob("GUI/MainApp/src/*.cxx"), checkExists = 1)
    gimias.SetPrecompiledHeader( "GUI/MainApp/gmMainAppPCH.h" )
    gimias.AddIncludeFolders([ gimias.GetBuildFolder() ])
    gimias.AddProperties(["WIN32_EXECUTABLE 1"])
    return gimias

commandLinePlugins = api.CreateStandardModuleProject(
	"CommandLinePlugins",
	"container",
	categories = ["Gimias", "CommandLinePlugins"] )


