/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _msvPlugin_H
#define _msvPlugin_H

#include "msvPluginProcessorCollective.h"
#include "msvPluginWidgetCollective.h"

// CoreLib
#include "coreFrontEndPlugin.h"

namespace msvPlugin {

/**
\brief Creates all objects of the plug-in and connect them.

\note Nobody can get access to this class. This class is only for
initialization of all components.

\note Try to make all processors reusable for other plug-ins. Be aware
of creating a dependency between the processor and any class of the rest
of the plug-in.

\ingroup msvPlugin
*/
class PLUGIN_EXPORT msvPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
  // TYPE DEFINITIONS
public:
  coreDeclareSmartPointerClassMacro(msvPlugin, Core::FrontEndPlugin::FrontEndPlugin);

  // OPERATIONS
protected:
  //!
  msvPlugin(void);

  //!
  virtual ~msvPlugin(void);

private:
  //! Purposely not implemented
  msvPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  ProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  WidgetCollective::Pointer m_Widgets;
}; // class msvPlugin

} // namespace msvPlugin

#endif // msvPlugin_H
