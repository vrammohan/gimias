/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SSHPluginModuleExecution_H
#define _SSHPluginModuleExecution_H

#include "DynLibWin32Header.h"
#include "dynModuleExecutionCLPExe.h"
#include "ModuleDescription.h"
#include "SshPluginNodeManager.h"
#include "sshExecuteCommandBackground.h"

/**
Executes a ModuleDescription using SSH

When a SSH command line plugin is executed, the following steps are performed:
- Save all input data to a local folder
- Create remote use case folder
- Transfer input data to remote directory
- Execute command
- Transfer output remote data to local folder
- Load output data
- Clean remote directory
- Clean local data

\ingroup SshPlugin
\author Xavi Planes
\date Mar 2011
*/
class SSHPluginModuleExecution : public dynModuleExecutionCLPExe {
public:
  typedef SSHPluginModuleExecution Self;
  typedef blSmartPointer< Self > Pointer;
  blNewMacro1param(Self, SshPluginNodeManager::Pointer);
  defineModuleFactory1param(SSHPluginModuleExecution, SshPluginNodeManager::Pointer);
  virtual dynModuleExecutionImpl::Pointer CreateAnother(void) const {
    dynModuleExecutionImpl::Pointer smartPtr;
    smartPtr = New(m_nodeManager).GetPointer();
    return smartPtr;
  }

  //!
  void BuildCommandLineLocation();

  //! Run the filter
  void RunFilter();

  //!
  void WaitForData();

  //! Remove the embedded XML from the stdout stream
  virtual void ProcessOutputInformation();

  //! Redefined
  virtual bool IsExecutedLocally();

protected:
  //!
  SSHPluginModuleExecution(SshPluginNodeManager::Pointer nodeManager);

  //!
  virtual ~SSHPluginModuleExecution();

private:
  //!
  SshPluginNodeManager::Pointer m_nodeManager;
  //!
  sshExecuteCommandBackground::Pointer m_ExecuteCommand;
};

#endif // _SSHPluginModuleExecution_H
