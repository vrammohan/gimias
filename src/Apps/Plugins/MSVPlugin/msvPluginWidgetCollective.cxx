/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvPluginWidgetCollective.h"
#include "msvVTKBtnPanelWidget.h"
#include "msvMultiRenderWindow.h"
#include "wxDataNavigationWidget.h"

namespace msvPlugin {

WidgetCollective::WidgetCollective() {
  // Panel widgets
  Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
      VTKBtnPanelWidget::Factory::NewBase(),
      Core::WindowConfig().Preferences().Caption("MSV"));

  Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
      wxDataNavigationWidget::Factory::NewBase(), Core::WindowConfig()
                                                      .Layer(3)
                                                      .Right()
                                                      .Show()
                                                      .Category("Windows")
                                                      .Id(wxID_TOOLTIP_WINDOW)
                                                      .Caption("Data Navigation"));

  // MultiRenderWindow
  Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
      msvMultiRenderWindow::Factory::NewBase(),
      Core::WindowConfig().RenderWindow().Caption("MSV Multi Render Window"));

  if (GetPluginTab() != NULL) {
    GetPluginTab()->ShowWindow("MSV");
  }
}

} // namespace msvPlugin
