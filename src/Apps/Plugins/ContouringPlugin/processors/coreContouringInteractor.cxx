/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

// CoreLib
#include "coreContouringInteractor.h"
#include "coreReportExceptionMacros.h"
#include "coreRenderingTreeMITK.h"
#include "coreRenDataFactory.h"
#include "coreSettings.h"

/**
 */
Core::ContouringInteractor::ContouringInteractor(
    Core::RenderingTree::Pointer renderingTree,
    Core::DataEntityHolder::Pointer selectedContour,
    Core::DataEntityHolder::Pointer selectedData, TOOLS_TYPE contourType = INVALID_TOOL) {
  m_toolType = contourType;

  try {
    SetSelectedContourHolder(selectedContour);
    SetSelectedData(selectedData);
    SetRenderingTree(renderingTree);
  }
  coreCatchExceptionsAddTraceAndThrowMacro(
      "Core::ContouringInteractor::ContouringInteractor");

  m_InteractorStateHolder = InteractorStateHolderType::New();
  m_InteractorStateHolder->SetSubject(INTERACTOR_DISABLED);
}

/**
 */
Core::ContouringInteractor::~ContouringInteractor() {
  if (m_selectedContourHolder.IsNotNull()) {
    m_selectedContourHolder->RemoveObserver< ContouringInteractor >(
        this, &Self::OnContourModified);
  }

  DisconnectFromDataTreeNode();

  DestroyInteractor();
}

Core::ContouringInteractor::InteractorStateHolderType::Pointer
Core::ContouringInteractor::GetInteractorStateHolder() const {
  return m_InteractorStateHolder;
}

void Core::ContouringInteractor::SetToolType(Core::ContouringInteractor::TOOLS_TYPE val) {
  m_toolType = val;
}

Core::ContouringInteractor::TOOLS_TYPE Core::ContouringInteractor::GetToolType() {
  return m_toolType;
}

void Core::ContouringInteractor::ConnectToDataTreeNode() {
  if (!IsConnectedToRenderingTree(GetSelectedContourHolder()->GetSubject())) {
    ConnectNodeToTree();
  }

  CreateInteractor();

  ConnectInteractors();

  // Call the subclass
  OnInteractorConnected();

  // add data entity to list
  OnContourModified();

  // Show the node after changing the size
  m_renderingTree->Show(GetSelectedContourDataEntity(), true);
}

void Core::ContouringInteractor::DisconnectFromDataTreeNode() {
  try {
    DisconnectInteractors();

    // Commented out. We don't want to disconnect the contour from the rendering node when
    // the interactor is destructed. MB
    // DisconnectNodeFromTree();

    DestroyInteractor();
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      "Core::ContouringInteractor::DisconnectFromDataTreeNode()");
}

void Core::ContouringInteractor::ConnectNodeToTree() {
  if (GetSelectedDataEntity()->GetType() != Core::ImageTypeId) {
    throw Core::Exceptions::Exception("ContourInteractor",
                                      "Input Data should be an image");
  }

  //! add a check before doing the connections of the interactor
  if (GetSelectedContourDataEntity().IsNull())
    return;

  // will be set by the interactor class
  m_renderingTree->Add(GetSelectedContourDataEntity(), false, false);
}

void Core::ContouringInteractor::DisconnectNodeFromTree() {
  if (GetSelectedContourDataEntity().IsNotNull())
    m_renderingTree->Remove(GetSelectedContourDataEntity(), false);
}

void Core::ContouringInteractor::ConnectInteractors() {
  // MITK checks if the interactor is in the list. It will not be added twice
  m_toolManager->RegisterClient();

  int toolId = GetCurrentToolId();
  m_toolManager->ActivateTool(toolId);

  // For future expansion, one can create several tools and distinguish one from each
  // other like this
  mitk::Tool *tool = GetContourTool();
  switch (m_toolType) {
  case MANUALCONTOURTOOL:
    // Not necessary
    break;
  case INVALID_TOOL:
    throw Core::Exceptions::Exception("ContourInteractor", "Selected Tool is invalid");
    break;
  }
}

void Core::ContouringInteractor::DisconnectInteractors() {
  if (m_toolManager) {
    m_toolManager->UnregisterClient();
    m_toolManager->SetWorkingData(NULL);
    //! TODO change SetReferenceData function in mitk::toolmanager in order not to accept
    //! null
    m_toolManager->SetReferenceData(NULL);
  }
}

void Core::ContouringInteractor::CreateInteractor() {
  if (m_toolManager.IsNull()) {
    RenderingTreeMITK *treeMITK =
        dynamic_cast< RenderingTreeMITK * >(m_renderingTree.GetPointer());
    m_toolManager = mitk::ToolManager::New(treeMITK->GetDataStorage());
  }
}

bool Core::ContouringInteractor::IsConnectedToRenderingTree(
    Core::DataEntity::Pointer dataEntity) {
  mitk::DataTreeIteratorClone itFound;
  bool bDataIsInRenderingTree = false;

  if (GetRenderingTree().IsNull())
    return false;

  bDataIsInRenderingTree = GetRenderingTree()->IsDataEntityRendered(dataEntity);

  return bDataIsInRenderingTree;
}

void Core::ContouringInteractor::DestroyInteractor() { m_toolManager = NULL; }

void Core::ContouringInteractor::OnInteractorConnected() {
  m_toolManager->SetReferenceData(GetSelectedDataEntityNode());
  m_toolManager->SetWorkingData(GetSelectedContourNode());
}

Core::DataEntity::Pointer Core::ContouringInteractor::GetSelectedContourDataEntity() {
  if (m_selectedContourHolder.IsNull()) {
    return NULL;
  }

  return m_selectedContourHolder->GetSubject();
}

Core::DataEntity::Pointer Core::ContouringInteractor::GetSelectedDataEntity() {
  return m_selectedData->GetSubject();
}

mitk::DataTreeNode::Pointer Core::ContouringInteractor::GetSelectedContourNode() {
  mitk::DataTreeNode::Pointer node;
  boost::any anyData = GetRenderingTree()->GetNode(GetSelectedContourDataEntity());
  Core::CastAnyProcessingData(anyData, node);
  return node;
}

mitk::DataTreeNode::Pointer Core::ContouringInteractor::GetSelectedDataEntityNode() {
  mitk::DataTreeNode::Pointer node;
  boost::any anyData = GetRenderingTree()->GetNode(GetSelectedDataEntity());
  Core::CastAnyProcessingData(anyData, node);
  return node;
}

mitk::Image::Pointer Core::ContouringInteractor::GetSelectedContourRenderingData() {
  if (GetSelectedContourDataEntity().IsNull()) {
    throw Core::Exceptions::Exception("ContourInteractor", "Input Contour is NULL");
  }
  mitk::BaseData::Pointer renderingData;
  renderingData =
      Core::RenDataFactory::GetBaseRenderingData(GetSelectedContourDataEntity());
  if (renderingData.IsNull()) {
    throw Core::Exceptions::Exception("ContourInteractor",
                                      "Input Contour rendering data is NULL");
  }
  mitk::Image *contour = dynamic_cast< mitk::Image * >(renderingData.GetPointer());
  if (contour == NULL) {
    throw Core::Exceptions::Exception("ContourInteractor",
                                      "Input Contour rendering data is not correct");
  }
  return contour;
}

Core::RenderingTree::Pointer Core::ContouringInteractor::GetRenderingTree() const {
  return m_renderingTree;
}

void Core::ContouringInteractor::SetRenderingTree(Core::RenderingTree::Pointer val) {
  m_renderingTree = val;
}

Core::DataEntityHolder::Pointer
Core::ContouringInteractor::GetSelectedContourHolder() const {
  return m_selectedContourHolder;
}

void Core::ContouringInteractor::SetSelectedContourHolder(
    Core::DataEntityHolder::Pointer val) {
  if (m_selectedContourHolder.IsNotNull()) {
    m_selectedContourHolder->RemoveObserver< ContouringInteractor >(
        this, &Self::OnContourModified);
  }

  // Observers to rendering data
  mitk::Image::Pointer mitkData = NULL;
  try {
    mitkData = GetSelectedContourRenderingData();
  } catch (...) {
  }

  m_selectedContourHolder = val;

  // Observers to processing data
  m_selectedContourHolder->AddObserver< ContouringInteractor >(this,
                                                               &Self::OnContourModified);
}

void Core::ContouringInteractor::OnContourModified() {
  if (GetSelectedContourHolder().IsNull())
    return;
  if (GetSelectedContourHolder()->GetSubject().IsNull())
    return;

  Core::DataEntityHelper::AddDataEntityToList(GetSelectedContourHolder(), false);
}

Core::DataEntityHolder::Pointer Core::ContouringInteractor::GetSelectedData() const {
  return m_selectedData;
}

void Core::ContouringInteractor::SetSelectedData(Core::DataEntityHolder::Pointer val) {
  m_selectedData = val;
}

mitk::Tool *Core::ContouringInteractor::GetContourTool() {
  if (!m_toolManager) {
    return NULL;
  }

  int toolId = GetCurrentToolId();

  mitk::Tool *tool = NULL;
  if (toolId >= 0) {
    tool = m_toolManager->GetToolById(toolId);
  }

  return tool;
}

int Core::ContouringInteractor::GetCurrentToolId() {
  int toolId = INVALID_TOOL;
  switch (m_toolType) {
  case MANUALCONTOURTOOL:
    toolId = m_toolManager->GetToolIdByToolType< mitk::ManualContouringTool >();
    break;
  }
  return toolId;
}
