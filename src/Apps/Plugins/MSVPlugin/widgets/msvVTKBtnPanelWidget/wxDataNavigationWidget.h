/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef wxDataNavigationWidget_H
#define wxDataNavigationWidget_H

// wxWidgets
#include <wx/dialog.h>
#include <wx/html/htmlwin.h>
#include <wx/wx.h>
#include <wx/sizer.h>
#include "wxNavigationDialog.h"
#include "msvViewManager.h"
#include "msvWxVTKButtonsManager.h"
#include "coreProcessingWidget.h"
#include "msvWxHtmlWindow.h"

class wxHtmlEventHandler;
class msvWxVTKButtons;
class msvVTKBtnEdit;
class msvMultiRenderWindow;
class msvVTKBtnEditView;
class msvWxHtmlWindow;

#define wxID_HTML_WINDOW wxID("wxID_HTML_WINDOW")
#define wxID_EDIT_DETAILS wxID("wxID_EDIT_DETAILS")
#define wxID_ADD_CHILD_SESSION wxID("wxID_ADD_CHILD_SESSION")
#define wxID_REMOVE_CURRENT wxID("wxID_REMOVE_CURRENT")
#define wxID_LOAD_CHILD_DATA wxID("wxID_LOAD_CHILD_DATA")
#define wxID_SAVE_DATA wxID("wxID_SAVE_DATA")
#define wxID_EDIT_VIEW wxID("wxID_EDIT_VIEW")
#define wxID_ADD_VIEW wxID("wxID_ADD_VIEW")
#define wxID_UNLOAD_DATA wxID("wxID_UNLOAD_DATA")
#define wxID_LOAD_DATA wxID("wxID_LOAD_DATA")

DECLARE_EXPORTED_EVENT_TYPE(PLUGIN_EXPORT, wxID_REFRESH_CONTENTS, wxID_ANY)

/**
\brief Dialog to show tooltips and information of a VTK button. Ii also handles
the events when clicking on the window.

It uses an HTML window (msvWxHtmlWindow) to show the contents and use hyperlinks to handle
interaction.

User events are managed by two strategies:
- hyperlink on the HTML page
- Context menu

\ingroup msvKPlugin
\author Xavi Planes
\date Nov 2010
*/
class wxDataNavigationWidget : public wxPanel, public Core::Widgets::ProcessingWidget {

public:
  coreDefineBaseWindowFactory(wxDataNavigationWidget);
  coreClassNameMacro(wxDataNavigationWidget)

      //!
      wxDataNavigationWidget(wxWindow *parent, wxWindowID id,
                             const wxPoint &pos = wxDefaultPosition,
                             const wxSize &size = wxDefaultSize,
                             long style = wxDEFAULT_DIALOG_STYLE,
                             const wxString &name = "dialogBox");

  ~wxDataNavigationWidget();

  //! Set active button
  void SetButton(msvWxVTKButtons *button);

  //! Set rendering tree
  void SetRenderWindow(msvMultiRenderWindow *renderWindow);

  //!
  void SetButtonsManager(msvWxVTKButtonsManager::Pointer buttonsManager);

  //! Get active button
  msvWxVTKButtons *GetButton();

  //! Load a specific page
  void LoadPage(wxString hPage);

  //! Get internal window
  wxHtmlWindow *GetHtmlWin();

  //! Show dialog
  bool Show(bool show = true);

  //! Hide dialog
  bool Hide();

  //!
  void SetMode(msvWxHtmlWindow::MODE_TYPE mode);

  //! Get dialog mode
  msvWxHtmlWindow::MODE_TYPE GetMode();

  //!
  Core::DataEntity::Pointer GetDataEntity();

  //! Add id as modified
  void AddModified(int id);

  //!
  bool CheckModified(int id);

  //! Move back to the last visualized object data
  int PopBack();

  //! Push back the current visualized object data
  void PushBack(int id);

  //!
  void ShowContextMenu(const wxPoint &position);

  //!
  msvVTKBtnEditView *GetEditView();

  //!
  msvViewManager::Pointer GetViewManager();

private:
  DECLARE_EVENT_TABLE()

  //!
  void OnLinkClicked(wxHtmlLinkEvent &link);

  //!
  void OnCellHover(wxHtmlCellEvent &event);

  //! Close edit dialog
  void OnCancelEdit(wxCommandEvent &event);

  //! Remove current view
  void OnRemoveView(wxCommandEvent &event);

  //!
  void OnModifiedDataEntity();

  //!
  void OnEditDetails(wxCommandEvent &event);

  //!
  void OnRemoveCurrent(wxCommandEvent &event);

  //!
  void OnLoadChildData(wxCommandEvent &event);

  //!
  void OnSaveData(wxCommandEvent &event);

  //!
  void OnEditView(wxCommandEvent &event);

  //!
  void OnAddChildSession(wxCommandEvent &event);

  //!
  void OnAddView(wxCommandEvent &event);

  //!
  void OnLoadData(wxCommandEvent &event);

  //!
  void OnUnLoadData(wxCommandEvent &event);

  //!
  void OnRefreshContents(wxCommandEvent &WXUNUSED(event));

  //! Call CaptureSnapshot( )
  void OnTakeSnapshot(wxCommandEvent &event);

  //! Capture view
  void OnCaptureView(wxCommandEvent &event);

private:
  //! drawing elements
  msvWxHtmlWindow *m_htmlWin;

  //!
  msvWxVTKButtons *m_Button;

  //!
  msvWxHtmlWindow::MODE_TYPE m_oldMode;

  //!
  msvWxHtmlWindow::MODE_TYPE m_Mode;

  //!
  msvVTKBtnEdit *m_msvVTKBtnEdit;

  //!
  msvVTKBtnEditView *m_msvVTKBtnEditView;

  //!
  msvMultiRenderWindow *m_RenderWindow;

  //!
  std::vector< int > m_History;

  //!
  wxMenu *m_ContextMenu;

  //!
  std::list< int > m_ModifiedList;

  //!
  msvViewManager::Pointer m_ViewManager;

  //!
  Core::DataEntityList::Pointer m_DataEntityList;

  //!
  msvWxVTKButtonsManager::Pointer m_ButtonsManager;
};

#endif // wxDataNavigationWidget_H