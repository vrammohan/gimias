/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef msvRenderingTree_H
#define msvRenderingTree_H

#include "coreRenderingTreeMITK.h"
#include "coreDataEntity.h"
#include "msvWxVTKButtons.h"

class msvMultiRenderWindow;

/**
\brief Specialization of RenderingTree for MSV

\ingroup msvPlugin
\author Xavi Planes
\date Oct 2010
*/
class PLUGIN_EXPORT msvRenderingTree : public Core::RenderingTreeMITK {
public:
  coreDeclareSmartPointerClassMacro(msvRenderingTree, Core::RenderingTreeMITK);

  //!
  void SetMultiRenderWindow(msvMultiRenderWindow *renderWindow);

  //! Redefined: Show VTK button
  boost::any Add(Core::DataEntity::Pointer dataEntity, bool bShow = true,
                 bool bInitializeViews = true);
  //! Redefined: Hide VTK button
  bool Remove(mitk::DataTreeNode::Pointer node, bool bInitializeViews = true);
  //! Redefined: Show/hide VTK button
  virtual void Show(Core::DataEntity::Pointer dataEntity, bool bShow,
                    bool initializeViews = true);

protected:
  //!
  msvRenderingTree(void);
  virtual ~msvRenderingTree(void);

  coreDeclareNoCopyConstructors(msvRenderingTree);

private:
  //!
  msvMultiRenderWindow *m_RenderWindow;
};

#endif // msvRenderingTree_H
