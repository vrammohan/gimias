/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef CLINICALREPORTPLUGINUTILITIES_H
#define CLINICALREPORTPLUGINUTILITIES_H

// std
#include <string>
#include <vector>

// core
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreDirectory.h"
#include "coreReportExceptionMacros.h"
#include "coreBaseExceptions.h"

#include "ClinicalReportProcessor.h"

// boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

// xml
#include "tinyxml.h"

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 10 Jan 2011
*/

namespace ClinicalReportPluginUtilities {
void CreateEmptyReport(std::string reportsFolder, std::string reportname);
void CreateReportsConfigFile();
std::string GetLastSelectedFromConfig();
void SetLastSelectedToConfig(std::string newSelected);
std::vector< std::string > GetReports(std::string basedir,
                                      Core::IO::Directory::Pointer reportsdir);
TiXmlElement *LoadReportXML(std::string filename);
std::pair< std::vector< std::string >, std::vector< std::vector< std::string > > >
GetTableFormat(std::string input);
void OpenClinicalReportDesigner(ClinicalReportProcessor::Pointer processor,
                                bool addReport = false);
}

#endif // CLINICALREPORTPLUGINUTILITIES_H
