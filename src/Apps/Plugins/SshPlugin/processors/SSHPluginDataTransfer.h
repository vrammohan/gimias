/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SSHPluginDataTransfer_H
#define _SSHPluginDataTransfer_H

// CoreLib
#include "coreDynDataTransferCLP.h"
#include "corePluginProvider.h"
#include "SshPluginNodeManager.h"
#include "sshConnection.h"
#include "sshDirectory.h"

/**
\brief The inputs/outputs passed to a remote machine using SSH

\ingroup SshPlugin
\author Xavi Planes
\date Mar 2011
*/
class SSHPluginDataTransfer : public Core::DynDataTransferCLP {
public:
  //!
  coreDeclareSmartPointerClassMacro1Param(SSHPluginDataTransfer, Core::DynDataTransferCLP,
                                          SshPluginNodeManager::Pointer)
      coreDefineFactoryClass1param(SSHPluginDataTransfer, SshPluginNodeManager::Pointer)
          coreDefineFactoryTagsBegin()
              coreDefineFactoryAddTag("ModuleType", std::string("SSHCommandLineModule"))
                  coreDefineFactoryTagsEnd() coreDefineFactoryClassEnd()

                      coreCreateAnother1Param(SSHPluginDataTransfer, m_nodeManager);

protected:
  //!
  SSHPluginDataTransfer(SshPluginNodeManager::Pointer nodeManager);

  //!
  virtual ~SSHPluginDataTransfer(void);

  //!
  void PreProcessData();

  //!
  void PostProcessData();

  //!
  void UpdateProcessorOutput(ModuleParameter *param, int num);

  //!
  void CleanTemporaryFiles();

  //!
  void CreateConnection();

  //! Save all inputs to local folder
  time_t ComputeLocalUseCaseTime(time_t *minTime = NULL, time_t *maxTime = NULL);

  //! Compute time of transfer input to remote computer and processing
  time_t ComputeRemoteUseCaseTime(sshDirectory &remoteDir);

private:
  //!
  SSHPluginDataTransfer(const Self &);

  //!
  void operator=(const Self &);

protected:
  //!
  SshPluginNodeManager::Pointer m_nodeManager;
  //! Remote working directory without the use-case subfolder
  std::string m_RemotePath;
  //! Local working directory without the use-case subfolder
  std::string m_LocalPath;
  //! Remote working directory with the use-case subfolder
  std::string m_RemoteUseCasePath;
  //! Local working directory with the use-case subfolder
  std::string m_LocalUseCasePath;
  //!
  sshConnection::Pointer m_Connection;
};

#endif // _SSHPluginDataTransfer_H
