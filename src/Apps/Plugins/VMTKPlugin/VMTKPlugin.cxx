/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "VMTKPlugin.h"

// CoreLib
#include "coreReportExceptionMacros.h"
#include "corePluginMacros.h"
#include "coreProfile.h"

// Declaration of the plugin
coreBeginDefinePluginMacro(VMTKPlugin) coreEndDefinePluginMacro()

    VMTKPlugin::VMTKPlugin(void)
    : FrontEndPlugin() {
  try {
    m_Processors = vmtkProcessorCollective::New();

    m_Widgets = vmtkWidgetCollective::New();
    m_Widgets->Init();
  }
  coreCatchExceptionsReportAndNoThrowMacro(VMTKPlugin::VMTKPlugin)
}

VMTKPlugin::~VMTKPlugin(void) {}
