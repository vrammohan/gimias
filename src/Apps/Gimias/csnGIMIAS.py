# Used to configure lcore
import glob
import string
import csnGIMIASCommon
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0")

# Used to configure gimias

# GIMIAS
gimias = api.RewrapProject(csnGIMIASCommon.CreateGIMIAS( ))

# Create a library that will hold all dependencies
# This allows to add tests to this project
gimiasLib = api.CreateCompiledProject("GIMIASLib", "library")

# Add Core dependency & MITKPlugin
gimiasLib.AddProjects([gmCoreLight])

# Collect all command line plugins with just importing csn files
CollectCommandLinePlugins()

#Add plugin dependencies
gimiasLib.AddProjects([
    ahePlugin,
    XNATPlugin,
    commandLinePlugins,
    dicomPlugin,
    genericSegmentationPlugin,
    imageToolsPlugin,
    manualSegmentationPlugin,
    meshEditorPlugin,
    mitkPlugin,
    msvPlugin,
    sandboxPlugin,
    sceneViewPlugin,
    securityPlugin,
    signalViewerPlugin,
    sshPlugin,
    tavernaPlugin,
    vmtkPlugin,
    webServicesPlugin,
    clinicalReportPlugin,
    contouringPlugin
], dependency = False)

# Add gimiasLib dependency to gimias executable project
gimias.AddProjects([gimiasLib,wxMitk])

def GetListOfSpuriousPluginDlls(gimiasProject):
    """
    Returns a list of filenames containing those GIMIAS plugin dlls which are not built by the current configuration.
    """
    result = []
    api = GetAPI("2.5.0-beta")
    gimiasProject = api.RewrapProject(gimiasProject)

    configuredPluginNames = [pluginProject.GetName().lower() for pluginProject in gimiasProject.GetProjects(recursive = True) ]
    for configuration in ("Debug", "Release"):
        pluginsFolder = "%s/plugins/*" % (gimiasProject.GetBuildResultsFolder(configurationName=configuration))
        for pluginFolder in glob.glob( pluginsFolder ):
            pluginName = os.path.basename(pluginFolder)
            if os.path.isdir(pluginFolder) and not (pluginName.lower() in configuredPluginNames):
                if api.GetCompiler().TargetIsWindows():
                    searchPath = string.Template("$folder/lib/$config/$name.dll").substitute(folder = pluginFolder, config = configuration, name = pluginName )
                else:
                    searchPath = string.Template("$folder/lib/lib$name.so").substitute(folder = pluginFolder, name = pluginName )
                if os.path.exists( searchPath ):
                    result.append( searchPath )
    return result

def RemoveSpuriousPluginDlls(gimiasProject, askUser):
    spuriousDlls = GetListOfSpuriousPluginDlls(gimiasProject)

    if len(spuriousDlls) == 0:
        return

    dllMessage = ""
    for x in spuriousDlls:
        dllMessage += ("%s\n" % x)

    message = "In the build results folder, CSnake found GIMIAS plugins that have not been configured.\nThe following plugin dlls may crash GIMIAS:\n%sDelete them?" % dllMessage
    askUser.SetType(askUser.QuestionYesNo())
    if askUser.Ask(message, askUser.AnswerYes()) == askUser.AnswerNo():
        return

    for dll in spuriousDlls:
        os.remove(dll)

#gimias.AddPostCMakeTasks([RemoveSpuriousPluginDlls])
