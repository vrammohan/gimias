// -*- C++ -*- generated by wxGlade 0.6.3 on Tue Oct 18 12:25:25 2011

#include <wx/wx.h>
#include <wx/image.h>

#ifndef WEBSERVICESPREFERENCESPANELWIDGETUI_H
#define WEBSERVICESPREFERENCESPANELWIDGETUI_H

// begin wxGlade: ::dependencies
#include <wx/tglbtn.h>
// end wxGlade

// begin wxGlade: ::extracode
#include "wxID.h"

#define wxID_BTN_START wxID("wxID_BTN_START")
#define wxID_SHOW_LOG wxID("wxID_SHOW_LOG")
#define wxID_CONFIGURATION wxID("wxID_CONFIGURATION")

// end wxGlade

class WebServicesPreferencesPanelWidgetUI : public wxScrolledWindow {
public:
  // begin wxGlade: WebServicesPreferencesPanelWidgetUI::ids
  // end wxGlade

  WebServicesPreferencesPanelWidgetUI(wxWindow *parent, int id,
                                      const wxPoint &pos = wxDefaultPosition,
                                      const wxSize &size = wxDefaultSize, long style = 0);

private:
  // begin wxGlade: WebServicesPreferencesPanelWidgetUI::methods
  void set_properties();
  void do_layout();
  // end wxGlade

protected:
  // begin wxGlade: WebServicesPreferencesPanelWidgetUI::attributes
  wxToggleButton *m_btnStart;
  wxStaticText *m_lblState;
  wxStaticText *m_lblStatus;
  wxTextCtrl *m_txtLog;
  wxButton *m_btnConfiguration;
  wxButton *m_btnLog;
  // end wxGlade

  DECLARE_EVENT_TABLE();

public:
  virtual void OnStart(wxCommandEvent &event);         // wxGlade: <event_handler>
  virtual void OnConfiguration(wxCommandEvent &event); // wxGlade: <event_handler>
  virtual void OnShowLog(wxCommandEvent &event);       // wxGlade: <event_handler>
};                                                     // wxGlade: end class

#endif // WEBSERVICESPREFERENCESPANELWIDGETUI_H
