# Used to configure SandboxPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

sandboxPlugin = GimiasPluginProject("SandboxPlugin", api)

projects = [
    gmCore, 
    guiBridgeLib, 
    baseLibVTK,
    guiBridgeLibWxWidgets
]
sandboxPlugin.AddProjects(projects)

sandboxPlugin.AddSources(["*.cxx", "*.h"])
sandboxPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
sandboxPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  "SandboxPluginShapeScalePanelWidget",
  "SandboxPluginSubtractPanelWidget",
  "SandboxPluginResamplePanelWidget"
  ]
sandboxPlugin.AddWidgetModules(widgetModules, _useQt = 0)

sandboxPlugin.SetPrecompiledHeader("SandboxPluginPCH.h")

