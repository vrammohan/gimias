#!/bin/bash

# create plugin install structure and copy/move all
# things needed
is_debug_or_release=$(pwd | grep -o 'Debug\|Release')

if [[ $is_debug_or_release != "" ]] ; then
    c_mode=$is_debug_or_release
    p_source_dir="/home/el1mc/src/git.gimias.org/gimias.git/src/Apps/Plugins"

    dirs=$(ls -1 plugins/)

    # for every built plugin
    for el in $dirs ; do
        plugin=plugins/$el
        plugin_name=$(echo $plugin | sed 's/plugins\///g')
        echo -n "Installing " $plugin_name "..."

        # delete previous c_mode folder
        plugin_lib="${plugin}/lib"
        plugin_lib_debug="${plugin}/lib/${c_mode}"
        if [[ -d $plugin_lib_debug ]] ; then
            rm -Rf $plugin_lib_debug
        fi

        # make and copy everything needed
        plugin_source_dir="${p_source_dir}/${plugin_name}"

        cp -f ${plugin_source_dir}/plugin.xml ${plugin_lib}/ &> /dev/null
        cp -fr ${plugin_source_dir}/Filters ${plugin_lib}/ &> /dev/null

        echo " done."
    done
fi

gimias_bin_dir=$(pwd)
gimias_src_dir="/home/el1mc/src/git.gimias.org/gimias.git/src"

[[ ! -d ${gimias_bin_dir}/resource ]] && mkdir ${gimias_bin_dir}/resource

resource_folders=$(cd $gimias_src_dir; find -type d -printf '%P\n' | grep resource | gawk -F '/resource' '{print $1"/resource/";}' | uniq)

echo $gimias_bin_dir
echo $gimias_src_dir

for el in $resource_folders ; do
    echo "copying ${el}"
    cd "${gimias_src_dir}/${el}"
    cp -fr * "${gimias_bin_dir}/resource"
done

echo cp -f ${gimias_src_dir}/Modules/TpExtLib/libmodules/tpExtMITK/src/StateMachine.xml ${gimias_bin_dir}
cp -f ${gimias_src_dir}/Modules/TpExtLib/libmodules/tpExtMITK/src/StateMachine.xml ${gimias_bin_dir}
cp -f ${gimias_src_dir}/Modules/TpExtLib/libmodules/tpExtMITK/src/StateMachine.xml ${gimias_bin_dir}/resource

echo cp -f ${gimias_src_dir}/Apps/Plugins/MITKPlugin/widgets/Interactors/GimiasStateMachines.xml ${gimias_bin_dir}
cp -f ${gimias_src_dir}/Apps/Plugins/MITKPlugin/widgets/Interactors/GimiasStateMachines.xml ${gimias_bin_dir}
cp -f ${gimias_src_dir}/Apps/Plugins/MITKPlugin/widgets/Interactors/GimiasStateMachines.xml ${gimias_bin_dir}/resource

echo cp -f ${gimias_src_dir}/Apps/Plugins/MITKPlugin/widgets/Interactors/mitkEventAndActionConstants.xml ${gimias_bin_dir}
cp -f ${gimias_src_dir}/Apps/Plugins/MITKPlugin/widgets/Interactors/mitkEventAndActionConstants.xml ${gimias_bin_dir}

echo cp -f ${gimias_bin_dir}/resource/local.xml ${gimias_bin_dir}
cp -f ${gimias_bin_dir}/resource/local.xml ${gimias_bin_dir}
