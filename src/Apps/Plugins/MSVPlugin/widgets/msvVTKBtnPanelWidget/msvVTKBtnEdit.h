/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef msvVTKBtnEdit_H
#define msvVTKBtnEdit_H

#include "msvVTKBtnEditUI.h"
#include "coreBaseWindow.h"
#include "msvViewManager.h"

#define wxID_msvVTKBtnEdit wxID("wxID_msvVTKBtnEdit")

/**
\brief Edit Data

\ingroup msvPlugin
\author Xavi Planes
\date Nov 2012
*/
class msvVTKBtnEdit : public msvVTKBtnEditUI, public Core::BaseWindow {
public:
  //!
  coreDefineBaseWindowFactory(msvVTKBtnEdit);
  //!
  msvVTKBtnEdit(wxWindow *parent, int id, const wxPoint &pos = wxDefaultPosition,
                const wxSize &size = wxDefaultSize, long style = 0);

  //!
  void SetDataEntity(Core::DataEntity::Pointer dataEntity);
  //!
  Core::DataEntity::Pointer GetDataEntity();
  //!
  void SetViewManager(msvViewManager::Pointer viewManager);

private:
  //!
  void OnTakeSnapshot(wxCommandEvent &event);
  //!
  void OnCleanSnapshot(wxCommandEvent &event);
  //!
  void OnLoadSnapshot(wxCommandEvent &event);
  //!
  void OnApply(wxCommandEvent &event);
  //!
  void OnCancel(wxCommandEvent &event);

protected:
  //!
  Core::DataEntity::Pointer m_DataEntity;
  //!
  msvViewManager::Pointer m_ViewManager;
};

#endif // msvVTKBtnEdit_H
