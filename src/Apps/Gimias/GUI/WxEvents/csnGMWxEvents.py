from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmWxEvents = api.CreateCompiledProject("gmWxEvents", "dll")
gmWxEvents.AddProjects([
  gmProcessors,
  gmWorkflow,
  wxWidgets
])

gmWxEvents.SetPrecompiledHeader("gmWxEventsPCH.h")

gmWxEvents.AddSources(["src/*.cpp" ], checkExists = 0)
gmWxEvents.AddSources(["src/*.cxx" ], checkExists = 0)
gmWxEvents.AddSources(["src/*.txx" ], checkExists = 0)
gmWxEvents.AddSources(["src/*.h" ], checkExists = 0)
gmWxEvents.AddIncludeFolders(["src"])

