# Used to configure meshEditorPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

meshEditorPlugin = GimiasPluginProject("MeshEditorPlugin", api)

projects = [
    gmCore,
    meshLib
]
meshEditorPlugin.AddProjects(projects)

meshEditorPlugin.AddSources(["*.cxx", "*.h"])
meshEditorPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
meshEditorPlugin.AddIncludeFolders(["processors"])

widgetModules = [
	"EdgeSwappingWidget",
	"ExtractScalarWidget",
	"ExtractSurfacesWidget",
	"LocalRefinerWidget",
	"LoopSubdivisionWidget",
	"MeshCreationWidget",
	"MeshStatisticsWidget",
	"NGOptimizationWidget",
	"RingCutWidget",
	"ShapeScaleWidget",
	"SkeletonCutWidget",
	"SkeletonizationWidget",
	"TaubinSmoothSurfaceWidget",
	"TetraGeneratorWidget",
	"ThresholdWidget",
	"VolumeClosingWidget",
    "CroppingWidget",
	"RadiographVisualization",
	"SurfaceSelectorWidget",
	"ToolbarMeshEditor",
	"BooleanOperationsWidget",
	"ManualNeckCuttingWidget",
	"MeshEditingWidget",
	"ReplaceScalarWidget"
  ]
meshEditorPlugin.AddWidgetModules(widgetModules, _useQt = 0)

meshEditorPlugin.SetPrecompiledHeader("MeshEditorPluginPCH.h")

meshEditorPlugin.AddFilesToInstall(meshEditorPlugin.Glob("resource"), "resource")

