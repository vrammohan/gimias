/*
 *  msvWxVTKAnimate.cpp derived from msvQVVTKAnimate.cpp
 *
 *  Created by Luigi Carotenuto on 25/09/12.
 *  Copyright Universitat Pompeu Fabra. All rights reserved.
 *
 *  See License at: http://tiny.cc/QXJ4D
 *
 */

#include "msvWxVTKAnimate.h"

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkCamera.h>
#include <vtkMath.h>

#include <vtkRendererCollection.h>

#include "blClock.h"

msvWxVTKAnimate::msvWxVTKAnimate() {
  for (int i = 0; i < 6; i++)
    m_Bounds[i] = 0;
  for (int i = 0; i < 3; i++)
    m_CameraPosition[i] = 0;
  m_CameraParallelScale = 0;
  for (int i = 0; i < 3; i++)
    m_CameraViewUp[i] = 0;
  for (int i = 0; i < 2; i++)
    m_ClippingRange[i] = 0;
  for (int i = 0; i < 3; i++)
    m_CameraFocalPoint[i] = 0;

  m_Renderer = NULL;
  m_flyToNSteps = 120;
  m_isFlight = false;
}

void msvWxVTKAnimate::setCurrentRenderer(vtkRenderer *renderer) { m_Renderer = renderer; }

vtkRenderer *msvWxVTKAnimate::getCurrentRenderer() { return m_Renderer; }

void msvWxVTKAnimate::flyTo() {
  // vtkRenderer *renderer = widget->renderer("tool");
  double center[3];
  center[0] = (m_Bounds[0] + m_Bounds[1]) / 2;
  center[1] = (m_Bounds[2] + m_Bounds[3]) / 2;
  center[2] = (m_Bounds[4] + m_Bounds[5]) / 2;

  vtkCamera *camera = m_Renderer->GetActiveCamera();

  double fly0[12]; // from
  double fly1[12]; // to
  double fly[12];  // interpolated position
  double distance;
  double vn[3];
  camera->GetViewPlaneNormal(vn);

  fly0[0] = camera->GetFocalPoint()[0];
  fly0[1] = camera->GetFocalPoint()[1];
  fly0[2] = camera->GetFocalPoint()[2];
  fly0[3] = camera->GetPosition()[0];
  fly0[4] = camera->GetPosition()[1];
  fly0[5] = camera->GetPosition()[2];
  fly0[6] = camera->GetParallelScale();
  fly0[7] = camera->GetViewUp()[0];
  fly0[8] = camera->GetViewUp()[1];
  fly0[9] = camera->GetViewUp()[2];
  fly0[10] = camera->GetClippingRange()[0];
  fly0[11] = camera->GetClippingRange()[1];

  if (m_CameraPosition[0] == 0 && m_CameraPosition[1] == 0 && m_CameraPosition[2] == 0) {
    /// new camera parameters found using code of vtkRenderer::ResetCamera(double
    /// m_Bounds[6])
    double w1 = m_Bounds[1] - m_Bounds[0];
    double w2 = m_Bounds[3] - m_Bounds[2];
    double w3 = m_Bounds[5] - m_Bounds[4];
    w1 *= w1;
    w2 *= w2;
    w3 *= w3;
    double radius = w1 + w2 + w3;

    radius = (radius == 0) ? (1.0) : (radius);
    radius = sqrt(radius) * 0.5;

    double angle = vtkMath::RadiansFromDegrees(camera->GetViewAngle());
    double parallelScale = radius;

    m_Renderer->ComputeAspect();
    double aspect[2];
    m_Renderer->GetAspect(aspect);

    if (aspect[0] >= 1.0) {
      // horizontal window, deal with vertical angle|scale
      if (camera->GetUseHorizontalViewAngle()) {
        angle = 2.0 * atan(tan(angle * 0.5) / aspect[0]);
      }
    } else {
      // vertical window, deal with horizontal angle|scale
      if (!camera->GetUseHorizontalViewAngle()) {
        angle = 2.0 * atan(tan(angle * 0.5) * aspect[0]);
      }

      parallelScale = parallelScale / aspect[0];
    }

    distance = radius / sin(angle * 0.5);

    fly1[0] = center[0];
    fly1[1] = center[1];
    fly1[2] = center[2];
    fly1[3] = center[0] + distance * vn[0];
    fly1[4] = center[1] + distance * vn[1];
    fly1[5] = center[2] + distance * vn[2];
    fly1[6] = fly0[6];
    fly1[7] = fly0[7];
    fly1[8] = fly0[8];
    fly1[9] = fly0[9];
    fly1[10] = fly0[10];
    fly1[11] = fly0[11];
  } else {
    // If m_CameraFocalPoint is 0, use bounds
    if (m_CameraFocalPoint[0] == 0 && m_CameraFocalPoint[1] == 0 &&
        m_CameraFocalPoint[2] == 0) {
      fly1[0] = center[0];
      fly1[1] = center[1];
      fly1[2] = center[2];
    } else {
      fly1[0] = m_CameraFocalPoint[0];
      fly1[1] = m_CameraFocalPoint[1];
      fly1[2] = m_CameraFocalPoint[2];
    }

    fly1[3] = m_CameraPosition[0];
    fly1[4] = m_CameraPosition[1];
    fly1[5] = m_CameraPosition[2];
    fly1[6] = m_CameraParallelScale;
    fly1[7] = m_CameraViewUp[0];
    fly1[8] = m_CameraViewUp[1];
    fly1[9] = m_CameraViewUp[2];
    fly1[10] = m_ClippingRange[0];
    fly1[11] = m_ClippingRange[1];
  }

  // int rate = 15;
  double pi = vtkMath::Pi();

  // flyTo only if camera parameters has changed
  if (fabs(fly0[0] - fly1[0]) < 0.0000001 && fabs(fly0[1] - fly1[1]) < 0.0000001 &&
      fabs(fly0[2] - fly1[2]) < 0.0000001 && fabs(fly0[3] - fly1[3]) < 0.0000001 &&
      fabs(fly0[4] - fly1[4]) < 0.0000001 && fabs(fly0[5] - fly1[5]) < 0.0000001 &&
      fabs(fly0[6] - fly1[6]) < 0.0000001) {
    return;
  }

  // loop
  blClock clock;
  clock.Start();
  double startTime = clock.Finish();
  int totalsteps = 100;
  int countsteps = 0;
  double totalDuration = float(m_flyToNSteps) / 1000;
  while (clock.Finish() < totalDuration) {
    double t = (countsteps * 1.0) / totalsteps;
    double t2 = 0.5 + 0.5 * sin(t * pi - pi / 2);

    for (int j = 0; j < 12; j++) {
      fly[j] = (1 - t2) * fly0[j] + t2 * fly1[j];
    }
    camera->SetFocalPoint(fly[0], fly[1], fly[2]);
    camera->SetPosition(fly[3], fly[4], fly[5]);
    camera->SetParallelScale(fly[6]);
    camera->SetViewUp(fly[7], fly[8], fly[9]);
    camera->SetClippingRange(fly[10], fly[11]);

    m_Renderer->GetRenderWindow()->Render();

    countsteps++;

    double finishTime = clock.Finish();
    double iterationTime = (finishTime - startTime) / countsteps;
    double timeLeft = totalDuration - finishTime > 0 ? totalDuration - finishTime : 0;
    if (iterationTime > 0) {
      totalsteps = int(countsteps + timeLeft / iterationTime);
    }
  }

  // Final render
  camera->SetFocalPoint(fly1[0], fly1[1], fly1[2]);
  camera->SetPosition(fly1[3], fly1[4], fly1[5]);
  camera->SetParallelScale(fly1[6]);
  camera->SetViewUp(fly1[7], fly1[8], fly1[9]);
  camera->SetClippingRange(fly1[10], fly1[11]);

  m_Renderer->GetRenderWindow()->Render();
}

msvWxVTKAnimate::~msvWxVTKAnimate() {}

void msvWxVTKAnimate::setCameraPosition(double val[3]) {
  m_CameraPosition[0] = val[0];
  m_CameraPosition[1] = val[1];
  m_CameraPosition[2] = val[2];
}

void msvWxVTKAnimate::setCameraParallelScale(double val) { m_CameraParallelScale = val; }

void msvWxVTKAnimate::setCameraViewUp(double val[3]) {
  m_CameraViewUp[0] = val[0];
  m_CameraViewUp[1] = val[1];
  m_CameraViewUp[2] = val[2];
}

void msvWxVTKAnimate::setCameraClippingRange(double val[2]) {
  m_ClippingRange[0] = val[0];
  m_ClippingRange[1] = val[1];
}

void msvWxVTKAnimate::setCameraFocalPoint(double val[3]) {
  m_CameraFocalPoint[0] = val[0];
  m_CameraFocalPoint[1] = val[1];
  m_CameraFocalPoint[2] = val[2];
}

void msvWxVTKAnimate::setBounds(double b[6]) {
  for (int i = 0; i < 6; i++)
    m_Bounds[i] = b[i];
}

double *msvWxVTKAnimate::getBounds() { return m_Bounds; }

void msvWxVTKAnimate::setFlyToNSteps(int nSteps) { m_flyToNSteps = nSteps; }

bool msvWxVTKAnimate::checkValidBounds() {
  return m_Bounds[0] != 0 || m_Bounds[1] != 0 || m_Bounds[2] != 0 || m_Bounds[3] != 0 ||
         m_Bounds[4] != 0 || m_Bounds[5] != 0;
}
