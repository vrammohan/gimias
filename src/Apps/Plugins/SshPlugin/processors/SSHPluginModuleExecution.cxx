/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SSHPluginModuleExecution.h"
#include "blTextUtils.h"
#include "dynModuleExecution.h"

#include "sshExecuteCommandBackground.h"
#include "sshFile.h"

SSHPluginModuleExecution::SSHPluginModuleExecution(
    SshPluginNodeManager::Pointer nodeManager) {
  m_nodeManager = nodeManager;
}

SSHPluginModuleExecution::~SSHPluginModuleExecution() {}

void SSHPluginModuleExecution::BuildCommandLineLocation() {
  m_CommandLineAsString.clear();

  // Remote executable
  m_CommandLineAsString.push_back(m_Module->GetLocation());
}

void SSHPluginModuleExecution::RunFilter() {
  std::string sshNodeName = m_Module->GetTarget();

  sshNode::Pointer node = m_nodeManager->Get(sshNodeName);
  if (node.IsNull()) {
    throw Core::Exceptions::Exception("SSHPluginModuleExecution::RunFilter",
                                      "Cannot find the SSH node");
  }

  // Connection
  sshConnection::Pointer connection = sshConnection::New();
  connection->SetNode(node);
  connection->Connect();

  // Execute
  std::ostringstream command;
  for (std::vector< std::string >::size_type i = 0; i < m_CommandLineAsString.size();
       ++i) {
    command << m_CommandLineAsString[i] << " ";
  }
  m_ExecuteCommand = sshExecuteCommandBackground::New(connection);
  m_ExecuteCommand->SetWorkingDirectory(GetWorkingDirectory());
  m_ExecuteCommand->SetTemporaryDirectory(GetUseCaseDirectory());
  m_ExecuteCommand->SetLibraryPath(sshFile::GetFilenamePath(m_Module->GetLocation()));
  m_ExecuteCommand->SetRequestShell(true);
  m_ExecuteCommand->Execute(command.str());
}

void SSHPluginModuleExecution::WaitForData() {
  while (m_ExecuteCommand->GetState() == sshExecuteCommand::State_Executing) {

    if (GetModule()->GetProcessInformation()->Abort) {
      if (!GetUpdateCallback()->GetDetachProcessing()) {
        // Close and remove all files
        m_ExecuteCommand->Abort();
      } else {
        m_ExecuteCommand->SetCleanRemoteFiles(false);
      }
      GetModule()->GetProcessInformation()->Progress = 0;
      GetModule()->GetProcessInformation()->StageProgress = 0;
      GetUpdateCallback()->Modified();
      break;
    }

    if (m_ExecuteCommand->ReadData()) {
      m_stdoutbuffer = m_ExecuteCommand->Getcout();
      m_stderrbuffer = m_ExecuteCommand->Getcerr();
      UpdateProgress();
    }

    // If there's no change in member variables, it will not update observers
    GetUpdateCallback()->Modified();
  }

  if (m_ExecuteCommand->ReadData()) {
    m_stdoutbuffer = m_ExecuteCommand->Getcout();
    m_stderrbuffer = m_ExecuteCommand->Getcerr();
    UpdateProgress();
  }

  m_ExecuteCommand->Close();
}

void SSHPluginModuleExecution::ProcessOutputInformation() {
  FilterOutput();

  // Check result
  if (!GetModule()->GetProcessInformation()->Abort) {
    int result = m_ExecuteCommand->GetState();
    if (result == sshExecuteCommand::State_Exited) {
      if (m_ExecuteCommand->GetExitValue() == 0) {
        std::stringstream information;
        information << GetModule()->GetTitle() << " completed without errors"
                    << std::endl;
        std::cout << information.str().c_str();
      } else {
        std::stringstream information;
        information << GetModule()->GetTitle() << " completed with errors" << std::endl;
        std::cerr << information.str().c_str();
        information << m_stderrbuffer;
        throw dynModuleExecution::Exception(information.str().c_str());
      }
    } else if (result == sshExecuteCommand::State_Expired) {
      std::stringstream information;
      information << GetModule()->GetTitle() << " timed out" << std::endl;
      std::cerr << information.str().c_str();
      throw dynModuleExecution::Exception(information.str().c_str());
    } else if (result == sshExecuteCommand::State_Connection_Failed) {
      std::stringstream information;
      information << GetModule()->GetTitle() << " connection failed" << std::endl;
      std::cerr << information.str().c_str();
      throw dynModuleExecution::Exception(information.str().c_str());
    } else {
      std::stringstream information;
      information << GetModule()->GetTitle()
                  << " unknown termination. Result = " << result << std::endl;
      std::cerr << information.str().c_str();
      throw dynModuleExecution::Exception(information.str().c_str());
    }
  }
}

bool SSHPluginModuleExecution::IsExecutedLocally() { return false; }