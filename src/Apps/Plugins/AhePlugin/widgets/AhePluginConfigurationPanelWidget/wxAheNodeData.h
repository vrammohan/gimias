#ifndef _wxAheNodeData_H
#define _wxAheNodeData_H

namespace ahePlugin {

/**
 * Helper class to store AHE node data into the list control shown in the GUI of the
 * panel.
 *
 * \ingroup AhePlugin
 * \author Ernesto Coto
 * \date May 2014
 */
class wxAheNodeData : public wxClientData {
public:
  //! Class constructor
  wxAheNodeData(aheAppInst::Pointer node) { m_Node = node; }

  //! AHE node pointer
  aheAppInst::Pointer m_Node;
};
}

#endif // _wxAheNodeData_H