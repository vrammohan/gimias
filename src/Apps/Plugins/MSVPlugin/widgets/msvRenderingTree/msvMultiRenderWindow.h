/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef msvMultiRenderWindow_H
#define msvMultiRenderWindow_H

#include "coreMultiRenderWindowMITK.h"
#include "msvRenderingTree.h"
#include "wxID.h"
#include <wx/html/htmlwin.h>
#include "coreProcessorThread.h"

#include "wxHtmlTooltipDialog.h"
#include "msvWxVTKButtonsManager.h"

#include <boost/thread/mutex.hpp>

#define wxID_TOOLTIP_WINDOW wxID("wxID_TOOLTIP_WINDOW")

DECLARE_EXPORTED_EVENT_TYPE(PLUGIN_EXPORT, wxID_DATA_ENTITY_SELECTED, wxID_ANY)
DECLARE_EXPORTED_EVENT_TYPE(PLUGIN_EXPORT, wxID_DATA_ENTITY_MODIFIED, wxID_ANY)
DECLARE_EXPORTED_EVENT_TYPE(PLUGIN_EXPORT, wxID_ANIMATE, wxID_ANY)
DECLARE_EXPORTED_EVENT_TYPE(PLUGIN_EXPORT, wxID_END_ANIMATION, wxID_ANY)

class wxHtmlTooltipDialog;
class wxDataNavigationWidget;

/**
\brief GIMIAS specific implementation of msvMultiRenderWindow.

- Handle events
- Show/hide dialogs
- Load/Unload data
- Capture preview
- Retrieve/Restore rendering properties
- Change working area

\ingroup msvPlugin
\author Xavi Planes
\date 25 Oct 2012
*/
class PLUGIN_EXPORT msvMultiRenderWindow : public Core::Widgets::MultiRenderWindowMITK,
                                           public wxCallerWidget {
public:
  coreDefineBaseWindowFactory(msvMultiRenderWindow)

      msvMultiRenderWindow(wxWindow *parent, wxWindowID id,
                           const wxPoint &pos = wxDefaultPosition,
                           const wxSize &size = wxDefaultSize, long style = 0,
                           const wxString &name = wxT("View"));
  virtual ~msvMultiRenderWindow(void);

  //! After initialization, create VTK buttons
  virtual void Init(Core::DataEntityHolder::Pointer selectedDataEntity,
                    bool enablePlanes = true);

  /** Get list of DataEntities to load for the input parameter.
  The children that are not session nodes
  */
  Core::DataEntity::ChildrenListType
  GetListOfDataToLoad(Core::DataEntity::Pointer dataEntity);

  //! Check if it contains any children data node
  bool IsZoomable(Core::DataEntity::Pointer dataEntity);

  /** Check if the DataEntity is loaded
  Session nodes are loaded if all data children are loaded
  */
  bool IsLoaded(Core::DataEntity::Pointer dataEntity);

  //!
  void AnimateFirstData();

  //! Check if dataEntity1 is parent of dataEntity2 recursivelly
  bool IsParent(Core::DataEntity::Pointer dataEntity1,
                Core::DataEntity::Pointer dataEntity2);

  //!
  msvWxVTKButtonsManager::Pointer GetButtonsManager();

  /** Load single DataEntity/children of session into memory
  \return true if data is loaded
  */
  bool LoadDataEntity(int id);

  //! UnLoad DataEntity from memory
  void UnloadDataEntity(int id);

  //! Change button size, refresh navigation window, update selected DataEntity and render
  //! window
  void SelectDataEntity(int id);

  /** Hide windows and vtkbuttons, take snapshot, save it to temp
  preview of current button and restore windows and vtkbuttons
  */
  void CaptureSnapshot();

  //!
  void CaptureView();

  //!
  bool IsReading();

protected:
  //! Show tooltip window
  void ShowTooltip(msvWxVTKButtons *button, bool show);

  //! Show complete information window
  void ShowInformation(msvWxVTKButtons *button, bool show);

  //! Add destination object data
  virtual bool BeginAnimation(bool flyTo, unsigned int btnId);

  //! Remove origin data
  virtual void EndAnimation(bool flyTo, unsigned int btnId);

  //! Refresh all button properties
  void OnModifiedMetadata();

  //! Call SelectDataEntity( )
  void OnSelectedDataEntity(Core::DataEntity::Pointer de);

  //!
  void OnSelectDataEntity(wxCommandEvent &event);

  //!
  void OnModifiedDataEntity(Core::SmartPointerObject *dataEntity);

  //!
  void OnModifiedDataEntity(wxCommandEvent &event);

  //!
  void OnAnimate(wxCommandEvent &event);

  //!
  void OnEndAnimation(wxCommandEvent &event);

  //! Change button size, refresh navigation window, update selected DataEntity and render
  //! window
  void RefreshSelectedDataEntity();

  //!
  void OnDataEntityAddedToList(Core::DataEntity::Pointer dataEntity);

  //!
  void OnDataEntityRemovedFromList(Core::DataEntity::Pointer dataEntity);

  //!
  void GetAllRenderingTreeProperties(blTagMap::Pointer view);

  //!
  void UpdateRenderingPropsFromView(Core::DataEntity::Pointer dataEntity);

  //! Get name of currently selected working area
  std::string GetCurrentWorkingAreaName();

  //!
  void ChangeWorkingArea(const std::string &name);

  //!
  blTagMap::Pointer GetWorkingAreaProps();

  //!
  void OnModifiedReader(Core::SmartPointerObject *object);

  //!
  void OnModifiedReaderOutput(Core::DataEntity::Pointer dataEntity);

  //! Add pending reader to m_PendingReader
  bool AddToPendingReader(int id);

  //! Execute pending readers
  void ProcessPendingReaderList();

  //! Remove reader from m_PendingReader if reading failed
  void OnReaderThreadStatus(Core::SmartPointerObject *object);

  //! Send animate envents when reading has finished
  void SendAnimateEvents();

  DECLARE_EVENT_TABLE();

private:
  //! Tooltip Dialog
  wxHtmlTooltipDialog *m_DlgTooltip;
  //! Info Dialog
  wxDataNavigationWidget *m_DlgInfo;
  //!
  msvWxVTKButtonsManager::Pointer m_ButtonsManager;
  //!
  boost::signals::connection m_ConnOnAdd;
  //!
  boost::signals::connection m_ConnOnRemove;
  //!
  Core::DataEntityList::Pointer m_DataEntityList;
  //! Protect access to m_PendingReader
  boost::mutex m_MutexPendingReader;
  //!
  std::map< Core::DataEntity::Pointer, Core::ProcessorThread::Pointer > m_PendingReader;
  //!
  bool m_IsReading;
};

#endif // msvMultiRenderWindow_H
