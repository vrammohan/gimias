/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "ClinicalReportWidgetCollective.h"
#include "ClinicalReportProcessor.h"
#include "ClinicalReportWidget.h"
#include "ClinicalReportWidgetTool.h"
#include "ClinicalReportPluginUtilities.h"
#include "ClinicalReportToolbar.h"
#include "NewClinicalReportTableDialog.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTabFactory.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSimpleProcessingWidget.h"
#include "coreWindowConfig.h"
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreDirectory.h"

ClinicalReportWidgetCollective::ClinicalReportWidgetCollective() {}

void ClinicalReportWidgetCollective::Init() {
  // Environment initialization
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  std::string reportsFolder =
      settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  Core::IO::Directory::Pointer reportsDir = Core::IO::Directory::New();
  reportsDir->SetDirNameFullPath(reportsFolder);

  if (!reportsDir->Exists()) {
    reportsDir->Create();
    ClinicalReportPluginUtilities::CreateReportsConfigFile();
  }

  Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();

  gIface->RegisterFactory(
      Core::Widgets::ClinicalReportToolbar::Factory::NewBase(),
      Core::WindowConfig().Toolbar().Top().Show().Caption("Clinical Report Toolbar"));

  gIface->RegisterFactory(Core::Widgets::NewClinicalReportTableDialog::Factory::NewBase(),
                          Core::WindowConfig().Free().Caption("Clinical Report Table"));

  gIface->RegisterFactory(
      ClinicalReportWidgetTool::Factory::NewBase(),
      Core::WindowConfig().ProcessingTool().Category("Report").Caption(
          "Clinical Report Tool"));
}
