/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "NewClinicalReportImageDialog.h"
#include "ClinicalReportWidget.h"
#include "ClinicalReportPluginUtilities.h"

// core
#include "coreSettings.h"
#include "coreDirectory.h"

// boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::NewClinicalReportImageDialog,
                  NewClinicalReportImageDialogUI)
END_EVENT_TABLE();

NewClinicalReportImageDialog::NewClinicalReportImageDialog()
    : NewClinicalReportImageDialogUI(NULL, wxID_ANY, "", wxPoint(280, 180)) {
  try {
  }
  coreCatchExceptionsReportAndNoThrowMacro(
      "NewClinicalReportImageDialog::NewClinicalReportImageDialog");
}

NewClinicalReportImageDialog::~NewClinicalReportImageDialog() {}

Core::BaseProcessor::Pointer Core::Widgets::NewClinicalReportImageDialog::GetProcessor() {
  return NULL;
}

void NewClinicalReportImageDialog::OnAddReport(wxCommandEvent &event) {
  ClinicalReportPluginUtilities::OpenClinicalReportDesigner(m_Processor, true);
  LoadReports();
}

void NewClinicalReportImageDialog::OnEditReport(wxCommandEvent &event) {
  ClinicalReportPluginUtilities::OpenClinicalReportDesigner(m_Processor);
}

void NewClinicalReportImageDialog::AddImage(std::string thumbnailPath) {
  // we save the image
  m_ThumbnailVector.push_back(thumbnailPath);

  size_t nElems = m_ThumbnailVector.size();
  if (nElems > 1) {
    m_CheckBoxMerge->Enable(true);
  }

  // clear this widget and...
  m_Panel->GetSizer()->Clear(true);
  m_CheckBoxVector.clear();
  m_CommentsTxtVector.clear();
  m_TitlesTxtVector.clear();
  m_BitmapVector.clear();

  //... show all the information including the new image
  wxBoxSizer *globalsizer = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_checkbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_bitmap = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_commentsTxt = new wxBoxSizer(wxVERTICAL);

  for (size_t i = 0; i < nElems; i++) {
    wxCheckBox *checkbox = new wxCheckBox(m_Panel, wxID_ANY, wxEmptyString);
    checkbox->SetValue(true);
    wxStaticBitmap *bitmap = new wxStaticBitmap(
        m_Panel, wxID_ANY, wxBitmap(m_ThumbnailVector[i], wxBITMAP_TYPE_ANY));
    wxTextCtrl *imageTitle = new wxTextCtrl(m_Panel, wxID_ANY, "[Image Title Here]");
    wxTextCtrl *commentsTxt =
        new wxTextCtrl(m_Panel, wxID_ANY, "[Image Comments Here]", wxDefaultPosition,
                       wxDefaultSize, wxTE_MULTILINE);

    sizer_checkbox->Add(checkbox, 1, wxEXPAND, 0);
    sizer_bitmap->Add(bitmap, 1, wxEXPAND, 0);
    sizer_commentsTxt->Add(imageTitle, 0, wxEXPAND, 0);
    sizer_commentsTxt->Add(commentsTxt, 1, wxEXPAND, 0);

    m_CheckBoxVector.push_back(checkbox);
    m_BitmapVector.push_back(bitmap);
    m_TitlesTxtVector.push_back(imageTitle);
    m_CommentsTxtVector.push_back(commentsTxt);
  }

  globalsizer->Add(20, 20, 0, 0, 0);
  globalsizer->Add(sizer_checkbox, 0, wxEXPAND, 0);
  globalsizer->Add(sizer_bitmap, 1, wxEXPAND, 0);
  globalsizer->Add(sizer_commentsTxt, 1, wxEXPAND, 0);
  globalsizer->Add(20, 20, 0, 0, 0);
  m_Panel->SetSizer(globalsizer, true);

  // finally cast resize events to avoid sizer problems.
  wxSizeEvent resEventGlobal(this->GetSize(), this->GetId());
  resEventGlobal.SetEventObject(this);
  this->GetEventHandler()->ProcessEvent(resEventGlobal);

  wxSizeEvent resEvent(m_Panel->GetSize(), m_Panel->GetId());
  resEvent.SetEventObject(m_Panel);
  m_Panel->GetEventHandler()->ProcessEvent(resEvent);
}

std::vector< std::string > NewClinicalReportImageDialog::GetTitles() {
  // copy from wxTextCtrl vector m_TitlesTxtVector to std::string vector m_TitlesVector
  m_TitlesVector = std::vector< std::string >(m_TitlesTxtVector.size(), "");

  for (int i = 0; i < m_TitlesTxtVector.size(); i++) {
    m_TitlesVector[i] = std::string(m_TitlesTxtVector[i]->GetValue().mb_str());
    ;
  }

  // and return it
  return m_TitlesVector;
}

std::vector< std::string > NewClinicalReportImageDialog::GetComments() {
  // copy from wxTextCtrl vector m_CommentsTxtVector to std::string vector
  // m_CommentsVector
  m_CommentsVector = std::vector< std::string >(m_CommentsTxtVector.size(), "");

  for (int i = 0; i < m_CommentsTxtVector.size(); i++) {
    m_CommentsVector[i] = std::string(m_CommentsTxtVector[i]->GetValue().mb_str());
    ;
  }

  // and return it
  return m_CommentsVector;
}

std::vector< bool > NewClinicalReportImageDialog::GetSelected() {
  std::vector< bool > ret;

  // fill a vector that indicates which images are selected
  for (int i = 0; i < m_CheckBoxVector.size(); i++) {
    ret.push_back(m_CheckBoxVector[i]->GetValue());
  }

  return ret;
}

bool NewClinicalReportImageDialog::GetMergeAllInOne() {
  std::vector< bool > selected = GetSelected();
  int nSelected = std::count(selected.begin(), selected.end(), true);

  // return weather images should be merged or not
  return (nSelected > 1) && m_CheckBoxMerge->GetValue();
}

void NewClinicalReportImageDialog::LoadReports() {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();
  m_BaseDir = settings->GetProjectHomePath() + Core::IO::SlashChar + "Reports";

  Core::IO::Directory::Pointer reportsDir = Core::IO::Directory::New();
  reportsDir->SetDirNameFullPath(m_BaseDir);

  // find subdirectories containing reports
  std::vector< std::string > subdirectories;

  try {
    subdirectories = ClinicalReportPluginUtilities::GetReports(m_BaseDir, reportsDir);
  } catch (...) {
    return;
  }

  m_ListReports->Clear();

  unsigned int lastSelectedPos = 0;
  std::string lastSelected = ClinicalReportPluginUtilities::GetLastSelectedFromConfig();

  for (int i = 0; i < subdirectories.size(); i++) {
    m_ListReports->Append(subdirectories[i]);

    if (lastSelected == subdirectories[i]) {
      lastSelectedPos = i;
    }
  }

  for (int i = int(subdirectories.size()) - 1; i >= 0; i--) {
    LoadReportInfo(subdirectories[i]);
  }

  if (lastSelectedPos != 0)
    LoadReportInfo(lastSelected);
  if (m_ListReports->GetCount() > lastSelectedPos)
    m_ListReports->Select(lastSelectedPos);
}

void NewClinicalReportImageDialog::LoadReportInfo(std::string reportName) {
  TiXmlElement *ClinicalReport;
  std::string file = m_BaseDir + Core::IO::SlashChar + reportName + Core::IO::SlashChar +
                     reportName + ".xml";

  try {
    ClinicalReport = ClinicalReportPluginUtilities::LoadReportXML(file);
  } catch (...) {
    return;
  }

  m_ReportName = ClinicalReport->Attribute("name");
  ;
  m_ReportAuthors = ClinicalReport->Attribute("authors");
  ;
  m_ReportPath = file;

  return;
}

void NewClinicalReportImageDialog::OnChangeReport(wxCommandEvent &event) {
  m_ReportName = std::string(m_ListReports->GetStringSelection().mb_str());
  ClinicalReportPluginUtilities::SetLastSelectedToConfig(m_ReportName);
  LoadReportInfo(m_ReportName);
}

void NewClinicalReportImageDialog::SetProcessor(
    ClinicalReportProcessor::Pointer processor) {
  m_Processor = processor;
}
