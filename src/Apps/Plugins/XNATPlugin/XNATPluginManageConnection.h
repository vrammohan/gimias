/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef XNATPLUGINMANAGECONECTIONS_H
#define XNATPLUGINMANAGECONECTIONS_H

#include "XNATPluginUtilities.h"

#include "corePreferencesPage.h"
#include "blLightObject.h"
#include "CILabExceptionMacros.h"

#include <map>

/**
\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class XNATPluginManageConnections : public Core::SmartPointerObject {

public:
  coreDeclareSmartPointerClassMacro(XNATPluginManageConnections,
                                    Core::SmartPointerObject);

  typedef std::map< std::string, XNATPluginDefineConetions > ConnectionsMapType;

  //! Removes all coonections and inits the connections
  void InitConnections(const ConnectionsMapType &connections);

  //! Creates a new node
  void AddNode(XNATPluginDefineConetions node);

  //! Removes an existing node
  void RemoveNode(std::string selectedNodeName);

  //! Edit an already existing node
  void EditNode(XNATPluginDefineConetions node);

  //! Returns connetions holder
  Core::DataHolder< ConnectionsMapType >::Pointer GetConnectionsHolder();

  //! Returns connetions holder
  ConnectionsMapType &GetConnectionsMap();

protected:
  //!
  XNATPluginManageConnections();

  //!
  ~XNATPluginManageConnections();

protected:
  //!
  Core::DataHolder< ConnectionsMapType >::Pointer m_ConnectionsMapHolder;
};

#endif // XNATPLUGINMANAGECONECTIONS_H
