/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _WebServerProcessor_H
#define _WebServerProcessor_H

#include "coreBaseProcessor.h"
#include "corePluginMacros.h"
#include "WebServerThread.h"
#include "corePluginProviderManager.h"

/**
Starts/stop the Web services server.

Two files will be created before starting the server:
- serfices.xml: List of all available services
- wsGimias.wsdl: interface for each service

The interface of the available services is created using all ModuleDescription
instances from all plugin providers of type CommandLineModule or ProcessorModule.

The server is implemented using a separate thread implemented in the class
WebServerThread.

A wxEvtHandler is used to receive events from the thread and update the UI.

\ingroup WebServicesPlugin
\author Xavi Planes
\date Sept 2011
*/
class PLUGIN_EXPORT WebServerProcessor : public Core::BaseProcessor {
public:
  //!
  coreProcessor(WebServerProcessor, Core::BaseProcessor);

  //! Start web server
  void Start();

  //! Stop web server
  void Stop();

  //!
  void SetStatusObserver(wxEvtHandler *handler);

  //! If list is empty, return 127.0.0.1
  static std::list< std::string > GetPublicIpAddressList();

  //!
  std::string GetServerPort() const;
  void SetServerPort(std::string val);

  //!
  std::string GetServiceProviderPort() const;
  void SetServiceProviderPort(std::string val);

  //!
  std::string GetServiceProviderIPAddress() const;
  void SetServiceProviderIPAddress(std::string val);

  //! Read settings from this plugin
  void ReadSettings();

  //!
  bool IsStarted();

  //! Get log when observer is not configured
  std::string GetLogStream();

  //!
  void OnPluginManagerModified(Core::Runtime::PluginProviderManager::STATE state);

private:
  //!
  WebServerProcessor();

  //!
  ~WebServerProcessor();

  //! Purposely not implemented
  WebServerProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

  //! Create serfices.xml file
  void CreateServicesXML(std::list< ModuleDescription > &moduleList);

  //!
  void CreateWSDL(std::list< ModuleDescription > &moduleList);

private:
  //!
  WebServerThread *m_Thread;

  //! Receive the status events
  wxEvtHandler *m_Observer;

  //!
  std::string m_ServerPort;

  //!
  std::string m_ServiceProviderPort;

  //!
  std::string m_ServiceProvideripAddress;
};

#endif //_WebServerProcessor_H
