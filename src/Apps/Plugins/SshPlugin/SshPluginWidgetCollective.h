/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SshPluginWidgetCollective_H
#define _SshPluginWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

#include "SshPluginProcessorCollective.h"

namespace SshPlugin {

/**

\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/
class WidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(SshPlugin::WidgetCollective, Core::WidgetCollective);

private:
  //! The constructor instantiates all the widgets and registers them.
  WidgetCollective();

  //!
  ~WidgetCollective();
};

} // namespace SshPlugin

#endif //_SshPluginWidgetCollective_H
