/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _msvPluginWidgetCollective_H
#define _msvPluginWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

#include "msvPluginProcessorCollective.h"

namespace msvPlugin {

/**
This class instantiates all widgets used in the plugin. The widgets are used to operate
the plugin processors
(see ProcessorCollective).
In the msvPlugin, there is currently only one widget, but when the number of widgets
grows, this class
ensures that the code remains maintainable.

\ingroup msvPlugin
*/

class WidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(WidgetCollective, Core::WidgetCollective);

private:
  //! The constructor instantiates all the widgets and registers them.
  WidgetCollective();
};

} // namespace msvPlugin

#endif //_msvPluginWidgetCollective_H
