/*==============================================================================

  Library: MSVTK

  Copyright (c) Kitware Inc.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// .NAME __MSV_WX_WIDGETSExport - manage Windows system differences
// .SECTION Description
// The __MSV_WX_WIDGETSExport captures some system differences between Unix
// and Windows operating systems.

#ifndef __MSV_WX_WIDGETSExport_h
#define __MSV_WX_WIDGETSExport_h

#if defined(WIN32) && !defined(msvWxWidgets_STATIC)
#if defined(msvWxWXWidgets_EXPORTS)
#define MSV_WX_WIDGETS_EXPORT __declspec(dllexport)
#else
#define MSV_WX_WIDGETS_EXPORT __declspec(dllimport)
#endif
#else
#define MSV_WX_WIDGETS_EXPORT
#endif

#endif
