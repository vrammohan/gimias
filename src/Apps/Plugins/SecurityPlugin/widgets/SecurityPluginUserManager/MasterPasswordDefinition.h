/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef MasterPasswordDefinition_H
#define MasterPasswordDefinition_H

// XNAT plugin GUI
#include "MasterPasswordDefinitionUI.h"

/**
master password window

\ingroup security plugin
\author H�ctor Fernandez
\date Jan 2012
*/

class MasterPasswordDefinition : public MasterPasswordDefinitionUI {

public:
  MasterPasswordDefinition(wxWindow *parent, int id, const wxString &title,
                           bool enable = true, const wxPoint &pos = wxDefaultPosition,
                           const wxSize &size = wxDefaultSize,
                           long style = wxDEFAULT_DIALOG_STYLE);

  //! add a new master password
  void OnBtnAdd(wxCommandEvent &event);

  //! cancel master password
  void OnBtnCancel(wxCommandEvent &event);

  //! On change Radio Box
  void OnBtnRdBoxChange(wxCommandEvent &event);

  //! actual master Pass
  std::string m_actualMP;

  //! value of changed master Pass
  std::string m_changedMP;

  //! returns value of radio box option
  int GetRdBoxOption();

  //! enables or disables radio box
  void HideRdBox();

  //! Hides change actual Master Password option
  void HideActualMP();

  //! returns value of txt control active master password
  std::string GetTxtActiveMasterPassword();

  //! returns value of txt control new master password
  std::string GetTxtNewMasterPassword();

  //! returns value of txt control repeat master password
  std::string GetTxtRepMasterPassword();
};

#endif // MasterPasswordDefinition_H