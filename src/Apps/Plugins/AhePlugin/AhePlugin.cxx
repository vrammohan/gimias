/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "AhePlugin.h"

// CoreLib
#include "coreReportExceptionMacros.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginTab.h"

// Declaration of the plugin
coreBeginDefinePluginMacro(ahePlugin::AhePlugin) coreEndDefinePluginMacro()

    namespace ahePlugin {

  AhePlugin::AhePlugin(void) : FrontEndPlugin() {
    try {
      m_Processors = ProcessorCollective::New();
      m_Widgets = WidgetCollective::New();
    }
    coreCatchExceptionsReportAndNoThrowMacro(AhePlugin::AhePlugin)
  }

  AhePlugin::~AhePlugin(void) {}

} // namespace ahePlugin
