/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ContouringPlugin_H
#define _ContouringPlugin_H

#include "ContouringPluginProcessorCollective.h"
#include "ContouringPluginWidgetCollective.h"

// CoreLib
#include "coreFrontEndPlugin.h"

namespace contouringPlugin {

/**
\brief Creates all objects of the plug-in and connect them.

\note Nobody can get access to this class. This class is only for
initialization of all components.

\note Try to make all processors reusable for other plug-ins. Be aware
of creating a dependency between the processor and any class of the rest
of the plug-in.

\ingroup ContouringPlugin
*/
class PLUGIN_EXPORT ContouringPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
  // TYPE DEFINITIONS
public:
  coreDeclareSmartPointerClassMacro(ContouringPlugin,
                                    Core::FrontEndPlugin::FrontEndPlugin);

  // OPERATIONS
protected:
  //!
  ContouringPlugin(void);

  //!
  virtual ~ContouringPlugin(void);

private:
  //! Purposely not implemented
  ContouringPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  ProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  WidgetCollective::Pointer m_Widgets;
}; // class ContouringPlugin

} // namespace contouringPlugin

#endif // ContouringPlugin_H
