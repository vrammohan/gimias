// -*- C++ -*- generated by wxGlade 0.6.3 on Thu Jan 19 16:56:07 2012

#include <wx/wx.h>
#include <wx/image.h>

#ifndef SECURITYPLUGINUSERMANAGERUI_H
#define SECURITYPLUGINUSERMANAGERUI_H

// begin wxGlade: ::dependencies
#include <wx/grid.h>
// end wxGlade

// begin wxGlade: ::extracode
#include "wxID.h"

// end wxGlade

class SecurityPluginUserManagerUI : public wxPanel {
public:
  // begin wxGlade: SecurityPluginUserManagerUI::ids
  // end wxGlade

  SecurityPluginUserManagerUI(wxWindow *parent, int id,
                              const wxPoint &pos = wxDefaultPosition,
                              const wxSize &size = wxDefaultSize, long style = 0);

private:
  // begin wxGlade: SecurityPluginUserManagerUI::methods
  void set_properties();
  void do_layout();
  // end wxGlade

protected:
  // begin wxGlade: SecurityPluginUserManagerUI::attributes
  wxStaticBox *sizer_2_staticbox;
  wxStaticBox *sizer_5_staticbox;
  wxCheckBox *m_checkBoxUseMasterPassword;
  wxButton *m_btnChangeMasterPassword;
  wxTextCtrl *m_txtCtrSearch;
  wxGrid *m_gridUserList;
  wxButton *m_btnAdd;
  wxButton *m_btnRemove;
  // end wxGlade

  DECLARE_EVENT_TABLE();

public:
  virtual void OnCheckBoxOK(wxCommandEvent &event); // wxGlade: <event_handler>
  virtual void
  OnBtnChangeMasterPassword(wxCommandEvent &event);  // wxGlade: <event_handler>
  virtual void OnBtntxtEnter(wxCommandEvent &event); // wxGlade: <event_handler>
  virtual void OnBtnLeftClick(wxGridEvent &event);   // wxGlade: <event_handler>
  virtual void OnBtnAdd(wxCommandEvent &event);      // wxGlade: <event_handler>
  virtual void OnBtnRemove(wxCommandEvent &event);   // wxGlade: <event_handler>
};                                                   // wxGlade: end class

#endif // SECURITYPLUGINUSERMANAGERUI_H
