/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SshPluginEditConfigurationDialog.h"
#include "sshConnection.h"
#include "SshPluginAskPasswordWx.h"
#include "itksys/SystemTools.hxx"
#include "coreDirectory.h"

SshPluginEditConfigurationDialog::SshPluginEditConfigurationDialog(
    wxWindow *parent, int id, const wxString &title,
    const wxPoint &pos /*=wxDefaultPosition*/, const wxSize &size /*=wxDefaultSize*/,
    long style /*=wxDEFAULT_DIALOG_STYLE*/)
    : SshPluginEditConfigurationDialogUI(parent, id, title, pos, size, style) {
  m_Node = sshNode::New();
  UpdateWidget();
}

void SshPluginEditConfigurationDialog::OnBtnTest(wxCommandEvent &event) {
  try {
    UpdateData();

    sshConnection::Pointer connection = sshConnection::New();
    connection->SetNode(m_Node);
    connection->SetPasswordMethod(new sshPluginAskPasswordWx());
    connection->Connect();

    wxMessageDialog dialog(NULL, "Connection tested successfully", "SSH Test");
    dialog.ShowModal();
  }
  coreCatchExceptionsReportAndNoThrowMacro(SshPluginEditConfigurationDialog::OnBtnTest)
}

sshNode::Pointer SshPluginEditConfigurationDialog::GetNode() const { return m_Node; }

void SshPluginEditConfigurationDialog::SetNodeData(sshNode::Pointer val) {
  m_Node->Copy(val);
  UpdateWidget();
}

void SshPluginEditConfigurationDialog::UpdateWidget() {
  m_txtHostname->SetValue(m_Node->GetHostname());
  m_txtUsername->SetValue(m_Node->GetUsername());
  m_txtPort->SetValue(wxString::Format("%d", m_Node->GetPort()));
  switch (m_Node->GetAuthMethod()) {
  case sshNode::AUTH_METHOD_PUBLICKEY:
    m_rdbAuthenticationMethod->SetSelection(0);
    break;
  case sshNode::AUTH_METHOD_INTERACTIVE:
    m_rdbAuthenticationMethod->SetSelection(1);
    break;
  case sshNode::AUTH_METHOD_PASSWORD:
    m_rdbAuthenticationMethod->SetSelection(2);
    break;
  }
}

void SshPluginEditConfigurationDialog::UpdateData() {
  m_Node->SetHostname(m_txtHostname->GetValue().ToStdString());
  m_Node->SetUsername(m_txtUsername->GetValue().ToStdString());
  long port;
  m_txtPort->GetValue().ToLong(&port);
  m_Node->SetPort(port);

  switch (m_rdbAuthenticationMethod->GetSelection()) {
  case 0:
    m_Node->SetAuthMethod(sshNode::AUTH_METHOD_PUBLICKEY);
    break;
  case 1:
    m_Node->SetAuthMethod(sshNode::AUTH_METHOD_INTERACTIVE);
    break;
  case 2:
    m_Node->SetAuthMethod(sshNode::AUTH_METHOD_PASSWORD);
    break;
  }
}

void SshPluginEditConfigurationDialog::OnOK(wxCommandEvent &event) {
  UpdateData();
  EndModal(wxID_OK);
}

void SshPluginEditConfigurationDialog::OnCancel(wxCommandEvent &event) {
  EndModal(wxID_CANCEL);
}

void SshPluginEditConfigurationDialog::OnImportKeyFile(wxCommandEvent &event) {
  std::string dataPath;
  wxFileDialog openFileDialog(this, wxT("Open key file"), wxT(""), wxT(""), wxT(""),
                              wxFD_OPEN | wxFD_FILE_MUST_EXIST);

  if (openFileDialog.ShowModal() == wxID_OK) {
    std::string fileName =
        itksys::SystemTools::GetFilenameName(openFileDialog.GetPath().ToStdString());

    // OpenSSL home path
    std::string homePath = Core::IO::DirectoryHelper::GetHomePath();
    std::string dstFile = homePath +=
        Core::IO::SlashChar + ".ssh" + Core::IO::SlashChar + fileName;
    itksys::SystemTools::CopyAFile(openFileDialog.GetPath(), dstFile.c_str());

    wxMessageDialog dialog(NULL, "Key file imported successfully", "Import key file");
    dialog.ShowModal();
  }
}
