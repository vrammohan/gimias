# Used to configure TavernaPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

tavernaPlugin = GimiasPluginProject("TavernaPlugin", api)

projects = [
    gmCoreLight,
    wflLib
]
tavernaPlugin.AddProjects(projects)

tavernaPlugin.AddSources(["*.cxx", "*.h"])
tavernaPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
tavernaPlugin.AddIncludeFolders(["processors"])

widgetModules = [
  'TavernaPluginWorkflowPanelWidget',
  'TavernaPluginWorkingArea',
  'TavernaPluginConfigurationPanelWidget',
  'TavernaPluginToolbar'
  ]
tavernaPlugin.AddWidgetModules(widgetModules, _useQt = 0)

tavernaPlugin.SetPrecompiledHeader("TavernaPluginPCH.h")

#tavernaPlugin.AddTests(["tests/*.*"], cxxTest)

tavernaPlugin.AddFilesToInstall(tavernaPlugin.Glob("resource"), "resource")
