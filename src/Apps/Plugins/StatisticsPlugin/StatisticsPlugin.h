/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _StatisticsPlugin_H
#define _StatisticsPlugin_H

#include "StatisticsProcessorCollective.h"
#include "StatisticsWidgetCollective.h"

// core
#include "coreFrontEndPlugin.h"

/**
\brief Clinical Report Plugin
\ingroup StatisticsPlugin
\author Albert Sanchez
\date 18 Oct 2011
*/

class PLUGIN_EXPORT StatisticsPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
public:
  coreDeclareSmartPointerClassMacro(StatisticsPlugin,
                                    Core::FrontEndPlugin::FrontEndPlugin);

protected:
  //!
  StatisticsPlugin(void);

  //!
  virtual ~StatisticsPlugin(void);

private:
  //! Purposely not implemented
  StatisticsPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  StatisticsProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  StatisticsWidgetCollective::Pointer m_Widgets;
};

#endif // _StatisticsPlugin_H
