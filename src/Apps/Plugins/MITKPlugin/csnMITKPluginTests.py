# Used to configure coreIO
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

mitkPlugin = api.RewrapProject(mitkPlugin)
mitkPlugin.AddTests(["tests/*.*"], cxxTest, enableWxWidgets = 1, dependencies = [gimiasHeader], pch = "MITKPluginPCH.h")

