/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SshPluginEditConfigurationDialog_H
#define _SshPluginEditConfigurationDialog_H

#include "SshPluginEditConfigurationDialogUI.h"
#include "sshNode.h"

/**
Configure a single SSH node

\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/
class SshPluginEditConfigurationDialog : public SshPluginEditConfigurationDialogUI {
public:
  //!
  SshPluginEditConfigurationDialog(wxWindow *parent, int id, const wxString &title,
                                   const wxPoint &pos = wxDefaultPosition,
                                   const wxSize &size = wxDefaultSize,
                                   long style = wxDEFAULT_DIALOG_STYLE);

  //!
  sshNode::Pointer GetNode() const;
  void SetNodeData(sshNode::Pointer val);

private:
  //!
  void OnBtnTest(wxCommandEvent &event);
  void OnOK(wxCommandEvent &event);
  void OnCancel(wxCommandEvent &event);
  void OnImportKeyFile(wxCommandEvent &event);

  //!
  void UpdateWidget();

  //!
  void UpdateData();

protected:
  //!
  sshNode::Pointer m_Node;
};

#endif // _SshPluginEditConfigurationDialog_H
