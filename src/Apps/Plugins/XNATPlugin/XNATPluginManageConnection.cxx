/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "XNATPluginManageConnection.h"

//-------------------------------------------------------------
void XNATPluginManageConnections::AddNode(XNATPluginDefineConetions node)
//-------------------------------------------------------------
{
  ConnectionsMapType::iterator it = GetConnectionsMap().find(node.GetName());
  if (it != GetConnectionsMap().end()) {
    throw Core::Exceptions::Exception("XNATPluginManageConnections::AddNode",
                                      "Connection already exist.");
  }

  GetConnectionsMap()[node.GetName()] = node;
  m_ConnectionsMapHolder->NotifyObservers();
}

//-------------------------------------------------------------
void XNATPluginManageConnections::RemoveNode(std::string selectedNodeName)
//-------------------------------------------------------------
{
  ConnectionsMapType::iterator it = GetConnectionsMap().find(selectedNodeName);
  if (it == GetConnectionsMap().end()) {
    throw Core::Exceptions::Exception("XNATPluginManageConnections::RemoveNode",
                                      "Connection doesn't exist.");
  }

  GetConnectionsMap().erase(it);
  m_ConnectionsMapHolder->NotifyObservers();
}

//-------------------------------------------------------------
void XNATPluginManageConnections::EditNode(XNATPluginDefineConetions node)
//-------------------------------------------------------------
{
  ConnectionsMapType::iterator it = GetConnectionsMap().find(node.GetName());
  if (it == GetConnectionsMap().end()) {
    throw Core::Exceptions::Exception("XNATPluginManageConnections::EditNode",
                                      "Connection doesn't exist.");
  }

  GetConnectionsMap()[node.GetName()] = node;
  m_ConnectionsMapHolder->NotifyObservers();
}

//-------------------------------------------------------------
void XNATPluginManageConnections::InitConnections(const ConnectionsMapType &connections)
//-------------------------------------------------------------
{
  GetConnectionsMap().clear();

  ConnectionsMapType::const_iterator it;
  for (it = connections.begin(); it != connections.end(); it++) {
    GetConnectionsMap()[it->first] = it->second;
  }

  m_ConnectionsMapHolder->NotifyObservers();
}

//-------------------------------------------------------------
Core::DataHolder< XNATPluginManageConnections::ConnectionsMapType >::Pointer
XNATPluginManageConnections::GetConnectionsHolder()
//-------------------------------------------------------------
{
  return m_ConnectionsMapHolder;
}

//-------------------------------------------------------------
XNATPluginManageConnections::XNATPluginManageConnections()
//-------------------------------------------------------------
{
  this->m_ConnectionsMapHolder = Core::DataHolder< ConnectionsMapType >::New();

  // XNATPluginDefineConetions dummy;
  // dummy.SetEncripted(false);
  // dummy.SetName("");
  // dummy.SetPassword("");
  // dummy.SetURL("");
  // dummy.SetUser("");

  // GetConnectionsMap( )[ "" ] = dummy;
}

//-------------------------------------------------------------
XNATPluginManageConnections::~XNATPluginManageConnections()
//-------------------------------------------------------------
{}

XNATPluginManageConnections::ConnectionsMapType &
XNATPluginManageConnections::GetConnectionsMap() {
  return m_ConnectionsMapHolder->GetSubject();
}
