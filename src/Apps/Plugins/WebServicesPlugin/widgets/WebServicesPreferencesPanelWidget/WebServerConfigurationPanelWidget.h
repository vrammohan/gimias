/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _WebServerConfigurationPanelWidget_H
#define _WebServerConfigurationPanelWidget_H

#include "WebServerConfigurationPanelWidgetUI.h"
#include "wx/hyperlink.h"

/**

\ingroup WebServicesPlugin
\author Xavi Planes
\date Oct 2011
*/
class WebServerConfigurationPanelWidget : public WebServerConfigurationPanelWidgetUI {
public:
  //!
  WebServerConfigurationPanelWidget(wxWindow *parent, int id, const wxString &title,
                                    const wxPoint &pos = wxDefaultPosition,
                                    const wxSize &size = wxDefaultSize,
                                    long style = wxDEFAULT_DIALOG_STYLE);

  //!
  ~WebServerConfigurationPanelWidget();
  ;

  //!
  void UpdateWidget();

  //!
  void UpdateData();

  //!
  virtual void OnCancel(wxCommandEvent &event);

  //!
  virtual void OnOK(wxCommandEvent &event);

  //!
  virtual void OnApply(wxCommandEvent &event);

  DECLARE_EVENT_TABLE();

private:
protected:
  //!
  wxHyperlinkCtrl *n_EndPointHyperLink;
};

#endif // _WebServerConfigurationPanelWidget_H
