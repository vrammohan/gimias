# Used to configure GenericSegmentationPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

genericSegmentationPlugin = GimiasPluginProject("GenericSegmentationPlugin", api)
genericSegmentationPlugin.AddProjects([
  gmCore,
  meshLib,
  baseLibVTK  ])

widgetModules = [
  "gspPhilipsSegmentedScarMeshPanelWidget",
  "gspPropagateLandmarksPanelWidget",
  "MaskImageToROIImageWidget",
  "ThresholdWidget", 
  "OtsuWidget", 
  "RegionGrowWidget", 
  "ConnectedThresholdWidget", 
  "VtkConnectedThresholdWidget", 
  "ManualCorrectionWidget", 
  "ThresholdPhilipsPanelWidget",
  "TransformationBoxWidget",
  "Generate3DRoiPanelWidget",
  "MoveDataPanelWidget"
]

genericSegmentationPlugin.AddWidgetModules(widgetModules, _holdingFolder = "widgets", _useQt = 0)


genericSegmentationPlugin.AddSources(["*.cpp", "*.cxx", "*.h"])
genericSegmentationPlugin.AddSources(["processors/*.cpp",
									"processors/*.cxx", 
									"processors/*.h"], sourceGroup = "processors")
									
genericSegmentationPlugin.AddIncludeFolders([".", "widgets", "processors"])
genericSegmentationPlugin.SetPrecompiledHeader("GenericSegmentationPluginPCH.h")

