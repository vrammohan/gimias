# Used to configure coreIO
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmProcessors = api.CreateCompiledProject("gmProcessors", "dll")

gmProcessors.AddProjects([ 
	gmDataHandling,
	gmKernel
])
	
gmProcessors.SetPrecompiledHeader("gmProcessorsPCH.h")

baseFolders = [
  "src"
]

for baseFolder in baseFolders:
        gmProcessors.AddSources(["%s/*.cxx" % baseFolder], checkExists = 0)
        gmProcessors.AddSources(["%s/*.txx" % baseFolder], checkExists = 0)
        gmProcessors.AddSources(["%s/*.h" % baseFolder], checkExists = 0)
        gmProcessors.AddIncludeFolders([baseFolder])

