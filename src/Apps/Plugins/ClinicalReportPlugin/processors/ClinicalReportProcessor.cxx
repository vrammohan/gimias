/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "ClinicalReportProcessor.h"
#include "coreDirectory.h"

#include "itksys/SystemTools.hxx"

ClinicalReportProcessor::ClinicalReportProcessor() {}

ClinicalReportProcessor::~ClinicalReportProcessor() {}

void ClinicalReportProcessor::SetParameters(
    ClinicalReportPatientInformation patientInfo,
    std::vector< ClinicalReportElement > clinicalReportElements, wxString reportTitleTxt,
    wxString reportAuthorsTxt, wxString comentsTxt, wxString reportsFolder,
    wxString reportName, std::string image) {
  m_PatientInfo = patientInfo;
  m_ClinicalReportElements = clinicalReportElements;
  m_ReportTitleTxt = reportTitleTxt;
  m_ReportAuthorsTxt = reportAuthorsTxt;
  m_ComentsTxt = comentsTxt;
  if (m_ReportsFolder.empty()) {
    m_ReportsFolder = reportsFolder;
  }
  m_ReportName = reportName;
  m_Image = image;
}

void ClinicalReportProcessor::Update() {

  // INITIALIZATIONS...
  if (wxImage::FindHandler(wxBITMAP_TYPE_PNG) == NULL) {
    wxImage::AddHandler(new wxPNGHandler());
  }
  wxSetWorkingDirectory(wxGetCwd() + wxT("/../samples"));
  // Set the font path
  wxString fontPath = wxGetCwd() + wxT("/../lib/fonts");

#ifndef __WXMAC__
  wxPdfDocumentModule pdfDocModule;
  pdfDocModule.OnInit();
#endif //__WXMAC__

  wxPdfFontManager::GetFontManager()->AddSearchPath(fontPath);

  ClinicalReportDocument pdf;

  //! Set the document parameters
  pdf.SetTitle(m_ReportTitleTxt);
  pdf.SetAuthors(m_ReportAuthorsTxt);
  pdf.SetImage(m_Image);
  pdf.SetComments(m_ComentsTxt);
  pdf.SetPatientInformation(m_PatientInfo);
  pdf.SetReportElements(m_ClinicalReportElements);

  pdf.SetCompression(false);
  pdf.AddPage();
  pdf.AliasNbPages(wxString(wxT("{nb}")));

  //! Write the document
  pdf.WritePatientInformation();
  pdf.WriteComments();
  pdf.WriteReportElements();

  std::string folder = std::string(m_ReportsFolder) + Core::IO::SlashChar +
                       std::string(m_ReportName) + Core::IO::SlashChar;
  itksys::SystemTools::MakeDirectory(folder.c_str());
  m_ReportFilename = folder + std::string(m_ReportTitleTxt) + ".pdf";

  //! Save it to disk
  pdf.SaveAsFile(m_ReportFilename);
}

std::string ClinicalReportProcessor::GetReportFilename() { return m_ReportFilename; }

void ClinicalReportProcessor::SetParameter(blTag::Pointer tag) {
  if (tag->GetName() == "reportsFolder") {
    std::string folder;
    tag->GetValue(folder);
    if (!folder.empty()) {
      m_ReportsFolder = itksys::SystemTools::GetFilenamePath(folder);
    }
  }
}

std::string ClinicalReportProcessor::GetReportsFolder() {
  return m_ReportsFolder.ToStdString();
}

void ClinicalReportProcessor::SetReportsFolder(std::string folder) {
  m_ReportsFolder = folder;
}
