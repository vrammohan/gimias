/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "XNATUploadProcessor.h"
#include "coreDataEntityReader.h"

#include "itksys/SystemTools.hxx"

XNATUploadProcessor::XNATUploadProcessor() {
  SetName("XNATUploadProcessor");

  BaseProcessor::SetNumberOfInputs(NUMBEROFINPUTS);
  BaseProcessor::SetNumberOfOutputs(NUMBEROFOUTPUTS);

  GetOutputPort(OUTPUT_DATA_ENTITY)->SetDataEntityType(Core::ImageTypeId);
  GetOutputPort(OUTPUT_DATA_ENTITY)->SetReuseOutput(false);
}

XNATUploadProcessor::~XNATUploadProcessor() {}

void XNATUploadProcessor::SetParameters(XNATUploadParameters params) {
  m_Params = params;
}

void XNATUploadProcessor::Update() {

  try {

    XNAT::Utils client(m_Params.usr, m_Params.pwd, m_Params.siteUrl);
    XNAT::Utils::FileFormat format;
    std::string contentDescription;
    Poco::URI destURL;

    if (m_Params.resourceExt == ".vtk") {
      format = XNAT::Utils::VTK;
      contentDescription = "VTK";
    } else if (m_Params.resourceExt == ".csv") {
      format = XNAT::Utils::CSV;
      contentDescription = "CSV";
    } else {
      // TODO: the format should be part of the parameters
      //       because the invoker already knows about it
      format = XNAT::Utils::VTK;
      contentDescription = "Image";
    }

    //! if vtk or csv files are uploaded (MySpine specific!)
    if (m_Params.resourceExt == ".vtk" || m_Params.resourceExt == ".csv") {
      destURL = client.sendResourceFileSubLvl(
          m_Params.uploadFileName, m_Params.resourceCollection, m_Params.resourceLabel,
          m_Params.project, m_Params.label, format, contentDescription, false);
    }

    //! if other files are uploaded (e.g. MR/CT image scans)
    else {
      destURL = client.sendResourceFileExpLvl(
          m_Params.uploadFileName, m_Params.resourceCollection, m_Params.resourceLabel,
          m_Params.project, m_Params.label, m_Params.experiment.label,
          m_Params.resourceExt, m_Params.resourceExt + m_Params.resourceCollection,
          false);
    }

    m_DataEntity = Core::DataEntity::New();
    m_DataEntity->GetMetadata()->AddTag("RemotePath", destURL);
    m_DataEntity->GetMetadata()->SetName("XNAT " + contentDescription);
    m_DataEntity->GetMetadata()->SetModality(Core::UnknownModality);
    m_DataEntity->SetType(Core::ImageTypeId);
    Core::DataEntityHelper::AddDataEntityToList(m_DataEntity, true);
  } catch (...) {
    throw;
  }
}
