/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ManualContouringTool_H
#define _ManualContouringTool_H

#include "mitkCommon.h"
#include "MitkExtExports.h"
#include "mitkFeedbackContourTool.h"
#include "mitkPointSet.h"

namespace mitk {

/**
 \brief Simple curve tool.

 This a copy of mitk::ContourTool with minor modifications:

 - Contour remains visible after OnMousePressed()
 - Contour is not proyected to a 2D slice
 - No mask image is produced
 - Contour can be set to be closed or not
 - Contour can be set to be visible or not

\ingroup gmInteractors
\author Ernesto Coto
\date Oct 2013
*/
class PLUGIN_EXPORT ManualContouringTool : public mitk::FeedbackContourTool {

public:
  mitkClassMacro(ManualContouringTool, FeedbackContourTool);
  itkNewMacro(ManualContouringTool);

  virtual const char **GetXPM() const;
  virtual const char *GetName() const;

  void SetContourVisible(bool b);
  mitk::Contour::Pointer GetContour();
  vtkSmartPointer< vtkPolyData > GetContourAsPolyData();
  void SetDistBetweenPoints(double dist);
  void SetContourClosed(bool value);

protected:
  ManualContouringTool(int paintingPixelValue = 1); // purposely hidden
  virtual ~ManualContouringTool();

  virtual void Activated();
  virtual void Deactivated();

  virtual bool OnMousePressed(Action *, const StateEvent *);
  virtual bool OnMouseMoved(Action *, const StateEvent *);
  virtual bool OnMouseReleased(Action *, const StateEvent *);

  // virtual bool OnInvertLogic  (Action*, const StateEvent*);

  bool OnDeletePressed(Action *, const StateEvent *);

private:
  //! Pointset
  mitk::PointSet::Pointer m_PointSet;
  //! Node in rendering tree
  mitk::DataTreeNode::Pointer m_PointSetNode;
  //! Holds the minimum distance between points
  double m_MinDistBetweenPoints;
  //! Holds the position of the last click
  mitk::PositionEvent m_LastPositionEvent;
  //! Flag that indicates the contour is beign drawn
  bool m_Drawing;
};

} // namespace

#endif // _ManualContouringTool_H
