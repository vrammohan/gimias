/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#include "vmtkVesselBifurcationsProcessor.h"

#include "coreBaseWindow.h"
#include "coreDataEntityHelper.h"
#include "blShapeUtils.h"

#include "vtkCleanPolyData.h"
#include "vtkvmtkPolyDataCenterlines.h"
#include "vtkvmtkCenterlineSplittingAndGroupingFilter.h"
#include "vtkvmtkCenterlineBranchExtractor.h"
#include "vtkvmtkCenterlineBifurcationVectors.h"
#include "vtkvmtkCenterlineBifurcationReferenceSystems.h"
#include "vtkIdList.h"
#include "vtkvmtkCenterlineSmoothing.h"
#include "vtkvmtkCapPolyData.h"
#include "vtkvmtkNeighborhoods.h"
#include "vtkvmtkNeighborhood.h"
#include "vtkTriangleFilter.h"
#include "vtkStringArray.h"
#include "vtkPointSet.h"
#include "vtkPoints.h"
#include "vtkDataSet.h"
#include "vtkCellData.h"
#include "coreDataEntityBuilder.h"
#include "coreLandmarkSelectorWidget.h"
#include "coreMultiRenderWindowEventHandler.h"
#include "coreDataTreeMITKHelper.h"
#include "wxMitkRenderWindow.h"
#include "mitkRenderingManager.h"
#include "vtkCellArray.h"
#include "blShapeUtils.h"
#include "vtkLine.h"
#include "vtkPolyLine.h"

VesselBifurcationsProcessor::VesselBifurcationsProcessor() {
  SetNumberOfInputs(NUMBER_OF_INPUTS);
  GetInputPort(INPUT_CENTERLINE)->SetName("Bifurcation Centerline");
  GetInputPort(INPUT_CENTERLINE)->SetDataEntityType(Core::SkeletonTypeId);
  SetNumberOfOutputs(NUMBER_OF_OUTPUTS);
  GetOutputPort(OUTPUT_BIFURCATION_VECTORS)->SetDataEntityType(Core::SurfaceMeshTypeId);
  GetOutputPort(OUTPUT_CENTERLINE_BRANCHES)
      ->SetDataEntityType(Core::SkeletonTypeId | Core::SurfaceMeshTypeId);
  GetOutputPort(OUTPUT_SPLIT_CENTERLINE_1)
      ->SetDataEntityType(Core::SkeletonTypeId | Core::SurfaceMeshTypeId);
  GetOutputPort(OUTPUT_SPLIT_CENTERLINE_2)
      ->SetDataEntityType(Core::SkeletonTypeId | Core::SurfaceMeshTypeId);
  GetOutputPort(OUTPUT_SPLIT_CENTERLINE_3)
      ->SetDataEntityType(Core::SkeletonTypeId | Core::SurfaceMeshTypeId);
  GetOutputPort(OUTPUT_SPLIT_CENTERLINE_4)
      ->SetDataEntityType(Core::SkeletonTypeId | Core::SurfaceMeshTypeId);
  GetOutputPort(OUTPUT_SPLIT_CENTERLINE_5)
      ->SetDataEntityType(Core::SkeletonTypeId | Core::SurfaceMeshTypeId);

  m_branchesExtracted = false;
  m_op = EXTRACT_BRANCHES;
}

VesselBifurcationsProcessor::~VesselBifurcationsProcessor() {}

void VesselBifurcationsProcessor::Update() {
  // the input bifurcation centerline
  Core::vtkPolyDataPtr inputMesh;
  Core::DataEntityHelper::GetProcessingData(GetInputDataEntityHolder(INPUT_CENTERLINE),
                                            inputMesh);

  if (m_op == EXTRACT_BRANCHES)
    ExtractBranches();
  else if (m_op == SPLIT)
    SplitCenterline();
  else if (m_op == BIFURCATION_VECTORS)
    ComputeBifurcationVectors();
}

void VesselBifurcationsProcessor::ExtractBranches() {
  vtkSmartPointer< vtkvmtkCenterlineBranchExtractor > branchExtractor =
      vtkvmtkCenterlineBranchExtractor::New();

  Core::vtkPolyDataPtr centerline;
  Core::CastAnyProcessingData(GetInputDataEntity(INPUT_CENTERLINE)->GetProcessingData(),
                              centerline);

  branchExtractor->SetInput(centerline);
  branchExtractor->SetBlankingArrayName("Blanking");
  branchExtractor->SetRadiusArrayName("MaximumInscribedSphereRadius");
  branchExtractor->SetGroupIdsArrayName("GroupIds");
  branchExtractor->SetCenterlineIdsArrayName("CenterlineIds");
  branchExtractor->SetTractIdsArrayName("TractIds");
  branchExtractor->Update();

  Core::vtkPolyDataPtr outputCenterline;
  outputCenterline = branchExtractor->GetOutput();
  UpdateOutput(OUTPUT_CENTERLINE_BRANCHES, branchExtractor->GetOutput(),
               "centerline branches", false, 1);
}
void VesselBifurcationsProcessor::SplitCenterline() {
  Core::vtkPolyDataPtr centerlineBranches;
  Core::CastAnyProcessingData(GetInputDataEntity(INPUT_CENTERLINE)->GetProcessingData(),
                              centerlineBranches);

  double range[2];
  if ((centerlineBranches == NULL) || (centerlineBranches->GetCellData() == NULL) ||
      (centerlineBranches->GetCellData()->GetArray("CenterlineIds") == NULL))
    return;
  centerlineBranches->GetCellData()->GetArray("CenterlineIds")->GetRange(range);

  if ((int)(range[1] - range[0]) < 1)
    return;

  for (int i = 0; (i <= (int)(range[1] - range[0]) && (i < NUMBER_OF_OUTPUTS)); i++) {
    double low = range[0] + i;
    double high = low + 0.5;
    wxString centerlineName = wxString::Format("centerline_%d", i);
    Core::vtkPolyDataPtr centerline;
    centerline.TakeReference(blShapeUtils::ShapeUtils::GetShapeRegion(
        centerlineBranches, low, high, "CenterlineIds"));

    vtkSmartPointer< vtkLine > firstLine = vtkLine::New();
    // firstLine->DeepCopy(centerline->GetCell(0));

    firstLine->GetPointIds()->SetNumberOfIds(centerline->GetNumberOfPoints());
    for (vtkIdType id = 0; id < centerline->GetNumberOfPoints(); id++)
      // in theory there could be an error here: none assures that the points should be
      // aligned in sequence!
      firstLine->GetPointIds()->SetId(id, id);

    vtkSmartPointer< vtkCellArray > lineArray = vtkCellArray::New();
    lineArray->InsertNextCell(firstLine);
    centerline->SetLines(lineArray);

    // clean centerline
    vtkSmartPointer< vtkCleanPolyData > cleanFilter = vtkCleanPolyData::New();
    cleanFilter->SetInput(centerline);
    cleanFilter->SetTolerance(0.0);
    cleanFilter->SetAbsoluteTolerance(1.0);
    cleanFilter->SetConvertLinesToPoints(false);
    cleanFilter->SetConvertPolysToLines(false);
    cleanFilter->SetConvertStripsToPolys(false);
    cleanFilter->Update();

    UpdateOutput(OUTPUT_SPLIT_CENTERLINE_1, cleanFilter->GetOutput(),
                 centerlineName.ToStdString(), false, 1);
  }
  if ((int)(range[1] - range[0]) > NUMBER_OF_OUTPUTS)
    throw Core::Exceptions::Exception("VesselBifurcationsProcessor::SplitCenterline",
                                      "Only 5 centerlines can be created");
}

void VesselBifurcationsProcessor::ComputeBifurcationVectors() {

  // takes in input the centerlines already split into branches
  Core::vtkPolyDataPtr centerlineBranches;
  Core::CastAnyProcessingData(GetInputDataEntity(INPUT_CENTERLINE)->GetProcessingData(),
                              centerlineBranches);

  if ((centerlineBranches->GetCellData()->GetArray("CenterlineIds")) == NULL)
    return;
  // compute reference systems for each bifurcation of a tree
  vtkSmartPointer< vtkvmtkCenterlineBifurcationReferenceSystems >
      bifurcationReferenceSystems = vtkvmtkCenterlineBifurcationReferenceSystems::New();
  bifurcationReferenceSystems->SetInput(centerlineBranches);
  bifurcationReferenceSystems->SetRadiusArrayName("MaximumInscribedSphereRadius");
  bifurcationReferenceSystems->SetBlankingArrayName("Blanking");
  bifurcationReferenceSystems->SetGroupIdsArrayName("GroupIds");
  bifurcationReferenceSystems->SetNormalArrayName("Normal");
  bifurcationReferenceSystems->SetUpNormalArrayName("UpNormal");
  bifurcationReferenceSystems->Update();
  // Core::vtkPolyDataPtr referenceSystems;
  // referenceSystems->DeepCopy(bifurcationReferenceSystems->GetOutput());

  vtkSmartPointer< vtkvmtkCenterlineBifurcationVectors > bifurcationVectors =
      vtkvmtkCenterlineBifurcationVectors::New();
  bifurcationVectors->SetInput(centerlineBranches);
  bifurcationVectors->SetReferenceSystems(bifurcationReferenceSystems->GetOutput());
  bifurcationVectors->SetRadiusArrayName("MaximumInscribedSphereRadius");
  bifurcationVectors->SetGroupIdsArrayName("GroupIds");
  bifurcationVectors->SetCenterlineIdsArrayName("CenterlineIds");
  bifurcationVectors->SetTractIdsArrayName("TractIds");
  bifurcationVectors->SetBlankingArrayName("Blanking");
  bifurcationVectors->SetReferenceSystemGroupIdsArrayName("GroupIds");
  bifurcationVectors->SetReferenceSystemNormalArrayName("Normal");
  bifurcationVectors->SetReferenceSystemUpNormalArrayName("UpNormal");
  bifurcationVectors->SetBifurcationVectorsArrayName("BifurcationVectors");
  bifurcationVectors->SetInPlaneBifurcationVectorsArrayName("InPlaneBifurcationVectors");
  bifurcationVectors->SetOutOfPlaneBifurcationVectorsArrayName(
      "OutOfPlaneBifurcationVectors");
  bifurcationVectors->SetInPlaneBifurcationVectorAnglesArrayName(
      "InPlaneBifurcationVectorAngles");
  bifurcationVectors->SetOutOfPlaneBifurcationVectorAnglesArrayName(
      "OutOfPlaneBifurcationVectorAngles");
  bifurcationVectors->SetBifurcationVectorsOrientationArrayName(
      "BifurcationVectorsOrientation");
  bifurcationVectors->SetBifurcationGroupIdsArrayName("BifurcationGroupIds");
  bifurcationVectors->SetNormalizeBifurcationVectors(0);
  bifurcationVectors->Update();

  UpdateOutput(OUTPUT_BIFURCATION_VECTORS, bifurcationVectors->GetOutput(),
               "BifurcationVectors", false, 1);
}
