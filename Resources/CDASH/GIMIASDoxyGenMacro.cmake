
include(CTestConfig.cmake)

MACRO(GENERATE_DOXYGEN)

#############################
# INIT
set(SOURCE_NAME ${CTEST_PROJECT_NAME})
set(CTEST_SOURCE_NAME ${SOURCE_NAME})
#set(CTEST_BINARY_NAME ${CTEST_PROJECT_NAME}_${JOB_COMPILER_NAME}_${JOB_COMPILER_VERSION})
set(CTEST_DASHBOARD_ROOT "${CLIENT_BASE_DIRECTORY}")
set(CTEST_SOURCE_DIRECTORY "${CTEST_DASHBOARD_ROOT}/src/${CTEST_SOURCE_NAME}")
set(CTEST_BINARY_DIRECTORY "${CTEST_DASHBOARD_ROOT}/bin/${CTEST_BINARY_NAME}")
set(CTEST_CMAKE_GENERATOR "${JOB_CMAKE_GENERATOR}")
set(CTEST_BUILD_CONFIGURATION "${JOB_BUILD_CONFIGURATION}")

set(CTEST_SITE "${CLIENT_SITE}")
set(CTEST_BUILD_NAME "DoxyGen")

file(WRITE "${CTEST_BINARY_DIRECTORY}/CMakeCache.txt" "${JOB_INITIAL_CACHE}")

#############################
# Start
ctest_start(${JOB_BUILDTYPE})

# Copy files to binary folder
SET(DOXYGEN_SOURCE "${CTEST_SOURCE_DIRECTORY}/Resources/documentation/DoxygenRoot")
SET(DOXYGEN_DEST "${CTEST_DASHBOARD_ROOT}/bin/${CTEST_BINARY_NAME}")
file(COPY ${DOXYGEN_SOURCE} DESTINATION ${DOXYGEN_DEST})

# Configure Doxygen file
SET(DOXYGEN_CONFIG_FILE "${DOXYGEN_DEST}/DoxygenRoot/gimias.doxy" )
SET(INPUT "${CTEST_SOURCE_DIRECTORY}/src/Apps ${CTEST_SOURCE_DIRECTORY}/src/Modules ${CTEST_SOURCE_DIRECTORY}/GIMIASExtensions_src/modules ${CTEST_SOURCE_DIRECTORY}/GIMIASExtensions_src/plugins")
configure_file(${DOXYGEN_SOURCE}/gimias.doxy ${DOXYGEN_CONFIG_FILE})

# Clean previous output
FILE(REMOVE_RECURSE "${DOXYGEN_DEST}/gimias-v${PROJECT_NUMBER}-doxygen")

# Generate documentation
set(CTEST_BUILD_COMMAND "${CLIENT_DOXYGEN_EXE} ${DOXYGEN_CONFIG_FILE}" )
ctest_build(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)

# The following lines are used to associate a build id with this job.
ctest_submit(RETURN_VALUE res)

message("DONE")

ENDMACRO(GENERATE_DOXYGEN)
