/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SecurityPluginProcessorCollective_H
#define _SecurityPluginProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

namespace SecurityPlugin {

/**
This class instantiates all processors used in the plugin and registers them.

\ingroup SecurityPlugin
\author H�ctor Fernandez
\date Jan 2012
*/

class ProcessorCollective : public Core::SmartPointerObject {
public:
  //!
  coreDeclareSmartPointerClassMacro(ProcessorCollective, Core::SmartPointerObject);

private:
  //! The constructor instantiates all the processors and connects them.
  ProcessorCollective();
};

} // namespace SecurityPlugin{

#endif //_SecurityPluginProcessorCollective_H
