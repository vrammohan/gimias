/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#ifndef XNATQueryProcessor_H
#define XNATQueryProcessor_H

#include "XNATPluginUtilities.h"

#include "gmProcessorsWin32Header.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreCommonDataTypes.h"
#include "corePluginMacros.h"
#include "coreBaseProcessor.h"

#include <map>
#include <vector>

/**
\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

class XNATQueryProcessor : public Core::BaseProcessor {
public:
  typedef enum { NUMBEROFINPUTS } INPUT_TYPE;

  typedef enum {
    OUTPUT_XNAT_ALL_SUBJECTS,
    OUTPUT_XNAT_QUERY_RESULTS,
    NUMBEROFOUTPUTS
  } OUTPUT_TYPE;

  //!
  coreProcessor(XNATQueryProcessor, Core::BaseProcessor);

  //!
  XNATQueryProcessor();
  //!
  ~XNATQueryProcessor();

  //!
  std::vector< std::map< std::string, std::string > > GetSubjects();

  //!
  std::vector< std::map< std::string, std::string > > GetFilteredSubjects();

  //!
  void SetParameters(XNATQueryParameters params);

  //!
  void Update();

private:
  //! Purposely not implemented
  XNATQueryProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

  //!
  void AddTagMap(Core::DataEntity::Pointer dataEntity,
                 std::vector< std::map< std::string, std::string > > subjects);

  //!
  std::vector< std::map< std::string, std::string > >
  FilterSubjects(std::map< std::string, std::string > restrictions,
                 std::vector< std::map< std::string, std::string > > subjects);

  //!
  void PrintSubjects(std::vector< std::map< std::string, std::string > > subjects);

private:
  XNATQueryParameters m_Params;

  std::vector< std::map< std::string, std::string > > m_Subjects;
  std::vector< std::map< std::string, std::string > > m_FilteredSubjects;

  Core::DataEntity::Pointer m_DataEntity;
  Core::DataEntity::Pointer m_FilteredDataEntity;
};

#endif // XNATQueryProcessor_H
