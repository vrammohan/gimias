// Copyright 2009 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona,
// Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.

#include "vmtkVesselBifurcationsPanelWidget.h"
#include "vmtkVesselBifurcationsProcessor.h"

#include "coreDataContainer.h"
#include "coreKernel.h"
#include "corePointInteractorPointSet.h"
#include "coreDataTreeMITKHelper.h"
#include "coreProcessorInputWidget.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreProcessorManager.h"

#include "wxMitkSelectableGLWidget.h"

#include "blMitkUnicode.h"

#include "mitkProperties.h"
#include "mitkMaterialProperty.h"

#include "vtkGlyph3D.h"
#include "vtkArrowSource.h"
#include "vtkActor.h"
#include "vtkMapper.h"
#include "vtkPolyDataMapper.h"
#include "vtkRenderWindow.h"
#include "vtkCellData.h"
#include "vtkDataSetAttributes.h"

VesselBifurcationsPanelWidget::VesselBifurcationsPanelWidget(wxWindow *parent, int id,
                                                             const wxPoint &pos,
                                                             const wxSize &size,
                                                             long style)
    : wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL) {

  btnExtract = new wxButton(this, wxID_btnExtract, wxT("Extract Branches"));
  btnSplit = new wxButton(this, wxID_btnSplit, wxT("Split Branches"));
  btnVectors = new wxButton(this, wxID_btnVectors, wxT("Bifurcation Vectors"));
  m_extractBranchesStaticBox =
      new wxStaticBox(this, -1, wxT("Extract Centerline Branches"));

  m_processor = VesselBifurcationsProcessor::New();

  do_layout();
  UpdateWidget();
}
VesselBifurcationsPanelWidget::~VesselBifurcationsPanelWidget() {}

BEGIN_EVENT_TABLE(VesselBifurcationsPanelWidget, wxPanel)
// begin wxGlade: ptRingCutPanelWidget::event_table
EVT_BUTTON(wxID_btnExtract, VesselBifurcationsPanelWidget::OnButtonExtract)
EVT_BUTTON(wxID_btnSplit, VesselBifurcationsPanelWidget::OnButtonSplit)
EVT_BUTTON(wxID_btnVectors, VesselBifurcationsPanelWidget::OnButtonVectors)
// end wxGlade
END_EVENT_TABLE();

void VesselBifurcationsPanelWidget::OnButtonExtract(wxCommandEvent &event) {
  m_processor->SetOpType(VesselBifurcationsProcessor::EXTRACT_BRANCHES);
  m_processor->SetMultithreading(false);
  Core::Runtime::Kernel::GetProcessorManager()->Execute(m_processor.GetPointer());
}

void VesselBifurcationsPanelWidget::OnButtonSplit(wxCommandEvent &event) {
  m_processor->SetOpType(VesselBifurcationsProcessor::SPLIT);
  m_processor->SetMultithreading(false);
  Core::Runtime::Kernel::GetProcessorManager()->Execute(m_processor.GetPointer());
}

void VesselBifurcationsPanelWidget::OnButtonVectors(wxCommandEvent &event) {
  m_processor->SetOpType(VesselBifurcationsProcessor::BIFURCATION_VECTORS);
  m_processor->SetMultithreading(false);
  Core::Runtime::Kernel::GetProcessorManager()->Execute(m_processor.GetPointer());

  if ((m_processor->GetOutputDataEntity(
           VesselBifurcationsProcessor::OUTPUT_BIFURCATION_VECTORS))
          .IsNull())
    return;

  Core::vtkPolyDataPtr bifurcationVectors;
  Core::DataEntityHelper::GetProcessingData(
      m_processor->GetOutputDataEntityHolder(
          VesselBifurcationsProcessor::OUTPUT_BIFURCATION_VECTORS),
      bifurcationVectors);

  vtkDataArray *vectorToMap =
      bifurcationVectors->GetPointData()->GetArray("BifurcationVectors");
  if (vectorToMap->GetNumberOfComponents() != 3)
    return;

  bifurcationVectors->GetPointData()->SetVectors(vectorToMap);

  vtkSmartPointer< vtkPolyData > glyph = vtkSmartPointer< vtkPolyData >::New();

  vtkSmartPointer< vtkArrowSource > arrowSource =
      vtkSmartPointer< vtkArrowSource >::New();

  vtkSmartPointer< vtkGlyph3D > glyph3D = vtkSmartPointer< vtkGlyph3D >::New();
  glyph3D->SetSource(arrowSource->GetOutput());
  glyph3D->SetVectorModeToUseVector();
  glyph3D->SetScaleModeToDataScalingOff();
  glyph3D->OrientOn();
  glyph3D->SetInput(bifurcationVectors);
  glyph3D->Update();

  vtkSmartPointer< vtkPolyDataMapper > mapper =
      vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInputConnection(glyph3D->GetOutputPort());

  vtkSmartPointer< vtkActor > actor = vtkSmartPointer< vtkActor >::New();
  actor->SetMapper(mapper);

  Core::Widgets::MultiRenderWindowMITK *mitkRenderWindow;
  mitkRenderWindow =
      dynamic_cast< Core::Widgets::MultiRenderWindowMITK * >(GetMultiRenderWindow());
  vtkRenderWindow *renWin = mitkRenderWindow->Get3D()->GetVtkRenderWindow();

  mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void VesselBifurcationsPanelWidget::do_layout() {
  wxBoxSizer *GlobalSizer = new wxBoxSizer(wxVERTICAL);

  wxBoxSizer *sizer_0 = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *extractBranchesStaticBox =
      new wxStaticBoxSizer(m_extractBranchesStaticBox, wxVERTICAL);
  wxBoxSizer *sizer_2 = new wxBoxSizer(wxHORIZONTAL);
  sizer_2->Add(btnExtract, 0, wxALL | wxEXPAND, 5);
  sizer_2->Add(btnSplit, 0, wxALL | wxEXPAND, 5);
  sizer_2->Add(btnVectors, 0, wxALL | wxEXPAND, 5);

  extractBranchesStaticBox->Add(sizer_2, 0, wxALL | wxALIGN_RIGHT, 5);

  sizer_0->Add(extractBranchesStaticBox, 1, wxALL | wxEXPAND, 5);
  GlobalSizer->Add(sizer_0);
  SetSizer(GlobalSizer);
  GlobalSizer->Fit(this);
  // end wxGlade
}
void VesselBifurcationsPanelWidget::OnInit() {
  GetInputWidget(VesselBifurcationsProcessor::INPUT_CENTERLINE)
      ->SetAutomaticSelection(false);
  // GetInputWidget( VesselBifurcationsProcessor::INPUT_CENTERLINE
  // )->SetDefaultDataEntityFlag( false );
}
Core::BaseProcessor::Pointer VesselBifurcationsPanelWidget::GetProcessor() {
  return m_processor.GetPointer();
}
