/*
* Copyright (c) 2012,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "PluginSecurityWidget.h"
#include "corePluginProviderManager.h"
#include <wx/wupdlock.h>

/** Plugin data associated to each tree item
*/
class wxTreePluginData : public wxTreeItemData {
public:
  wxTreePluginData(Core::Runtime::PluginProvider::Pointer provider,
                   Core::Runtime::BasePlugin::Pointer basePlugin) {
    m_Provider = provider;
    m_BasePlugin = basePlugin;
  }
  Core::Runtime::PluginProvider::Pointer m_Provider;
  Core::Runtime::BasePlugin::Pointer m_BasePlugin;
};

PluginSecurityWidget::PluginSecurityWidget(wxWindow *parent, int id, const wxPoint &pos,
                                           const wxSize &size, long style)
    : PluginSecurityWidgetUI(parent, id, pos, size, style) {
  m_Trial = slTrial::New();
}

void PluginSecurityWidget::OnLoadPublicKey(wxCommandEvent &event) {
  wxFileDialog openFileDialog(this, wxT("Open public key"), wxT(""), wxT(""), wxT(""),
                              wxFD_OPEN | wxFD_FILE_MUST_EXIST);
  openFileDialog.SetWildcard(wxString::FromUTF8("*.pem"));
  if (openFileDialog.ShowModal() == wxID_OK) {
    m_Trial->LoadPublicKey(openFileDialog.GetPath().ToStdString());
    wxMessageBox("Public key has been loaded");
  }
}

void PluginSecurityWidget::OnLoadPrivateKey(wxCommandEvent &event) {
  wxFileDialog openFileDialog(this, wxT("Open private key"), wxT(""), wxT(""), wxT(""),
                              wxFD_OPEN | wxFD_FILE_MUST_EXIST);
  openFileDialog.SetWildcard(wxString::FromUTF8("*.pem"));
  if (openFileDialog.ShowModal() == wxID_OK) {
    m_Trial->LoadPrivateKey(openFileDialog.GetPath().ToStdString());
    wxMessageBox("Private key has been loaded");
  }
}

void PluginSecurityWidget::OnGenerateKey(wxCommandEvent &event) {
  wxDirDialog openDirectoryDialog(this, wxT("Directory"), wxT(""));

  if (openDirectoryDialog.ShowModal() == wxID_OK) {
    m_Trial->GenerateKey();

    std::string privateKey =
        openDirectoryDialog.GetPath().ToStdString() + "keyPrivate.pem";
    m_Trial->SavePrivateKey(privateKey);

    std::string publicKey = openDirectoryDialog.GetPath().ToStdString() + "keyPublic.pem";
    m_Trial->SavePublicKey(publicKey);

    wxMessageBox("Public key has been stored in the file: " + publicKey +
                     "\nAnd private key in the file: " + privateKey,
                 "Key generation");
  }
}

void PluginSecurityWidget::OnBtnVerify(wxCommandEvent &event) {
  wxTreePluginData *data =
      dynamic_cast< wxTreePluginData * >(m_Tree->GetItemData(m_Tree->GetSelection()));
  if (!data) {
    return;
  }

  Core::Runtime::BasePlugin::Pointer plugin = data->m_BasePlugin;
  if (plugin.IsNull()) {
    return;
  }

  m_Trial->SetExpireTime(plugin->GetExpireDate());
  m_Trial->SetDescription(plugin->GetName());
  m_Trial->SetSignedData(plugin->GetSignature());
  if (m_Trial->Verify()) {
    wxMessageBox("Signature has been successfully verified");
  } else {
    wxMessageBox("Signature verification failed");
  }
}

void PluginSecurityWidget::OnBtnSign(wxCommandEvent &event) {
  wxTreePluginData *data =
      dynamic_cast< wxTreePluginData * >(m_Tree->GetItemData(m_Tree->GetSelection()));
  if (!data) {
    return;
  }

  Core::Runtime::BasePlugin::Pointer plugin = data->m_BasePlugin;
  if (plugin.IsNull()) {
    return;
  }

  m_Trial->SetExpireTime(plugin->GetExpireDate());
  m_Trial->SetDescription(plugin->GetName());
  m_Trial->Sign();

  data->m_BasePlugin->SetSignature(m_Trial->GetSignedData());
  m_txtSignature->ChangeValue(m_Trial->GetSignedData());
}

void PluginSecurityWidget::OnItemSelectedFromTree(wxTreeEvent &event) {
  wxWindowUpdateLocker lock(this);

  m_txtName->ChangeValue("");
  m_txtCaption->ChangeValue("");
  m_txtDate->ChangeValue("");
  m_txtSignature->ChangeValue("");

  wxTreeItemId item = event.GetItem();
  if (!item.IsOk()) {
    return;
  }

  wxTreePluginData *data = dynamic_cast< wxTreePluginData * >(m_Tree->GetItemData(item));
  if (!data) {
    return;
  }

  Core::Runtime::BasePlugin::Pointer plugin = data->m_BasePlugin;
  if (plugin.IsNull()) {
    return;
  }

  m_txtName->ChangeValue(plugin->GetName());
  m_txtCaption->ChangeValue(plugin->GetCaption());
  m_txtDate->ChangeValue(plugin->GetExpireDate());
  m_txtSignature->ChangeValue(plugin->GetSignature());
}

void PluginSecurityWidget::UpdateWidget() {
  wxWindowUpdateLocker lock(m_Tree);

  m_Tree->DeleteAllItems();
  m_Tree->AddRoot("Plugins");
  m_Tree->SetItemImage(m_Tree->GetRootItem(), 1);

  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

  Core::Runtime::PluginProviderManager::Pointer pluginProviderManager;
  pluginProviderManager = graphicalIface->GetPluginProviderManager();

  Core::Runtime::PluginProviderManager::PluginProvidersType pluginProviers;
  pluginProviers = pluginProviderManager->GetPluginProviders();
  Core::Runtime::PluginProviderManager::PluginProvidersType::iterator it;
  for (it = pluginProviers.begin(); it != pluginProviers.end(); ++it) {
    wxTreeItemId parentItem;
    wxTreePluginData *data = new wxTreePluginData(*it, NULL);
    parentItem =
        m_Tree->AppendItem(m_Tree->GetRootItem(), _U((*it)->GetName()), 1, 1, data);

    blTagMap::Pointer plugins;
    plugins = (*it)->GetProperties()->GetTagValue< blTagMap::Pointer >("Plugins");

    bool selected = UpdateTree(parentItem, plugins, (*it));

    m_Tree->SetItemImage(parentItem, selected + 1);

    m_Tree->Expand(parentItem);
  }

  m_Tree->Expand(m_Tree->GetRootItem());

  m_Tree->SetScrollPos(wxVERTICAL, 0);
}

void PluginSecurityWidget::UpdateData() {
  Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
  graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

  Core::Runtime::PluginProviderManager::Pointer pluginProviderManager;
  pluginProviderManager = graphicalIface->GetPluginProviderManager();

  UpdateProvider(m_Tree->GetRootItem());
}

void PluginSecurityWidget::UpdateProvider(wxTreeItemId item) {
  if (!item.IsOk())
    return;

  wxTreeItemIdValue cookie;
  wxTreeItemId itemId = m_Tree->GetFirstChild(item, cookie);
  while (itemId.IsOk()) {
    wxTreePluginData *data =
        dynamic_cast< wxTreePluginData * >(m_Tree->GetItemData(itemId));
    if (data) {
      // Update this item
      if (data->m_BasePlugin.IsNotNull()) {
        Core::Runtime::BasePlugin::Pointer providerPlugin;
        providerPlugin = data->m_Provider->GetPlugin(data->m_BasePlugin->GetName());
        if (providerPlugin.IsNotNull()) {
          providerPlugin->SetName(data->m_BasePlugin->GetName());
          providerPlugin->SetCaption(data->m_BasePlugin->GetCaption());
          providerPlugin->SetExpireDate(data->m_BasePlugin->GetExpireDate());
          providerPlugin->SetSignature(data->m_BasePlugin->GetSignature());
          providerPlugin->WriteXML();
        }
      }

      // Update child items
      UpdateProvider(itemId);
    }
    itemId = m_Tree->GetNextChild(item, cookie);
  }
}

bool PluginSecurityWidget::UpdateTree(wxTreeItemId parentItem, blTagMap::Pointer plugins,
                                      Core::Runtime::PluginProvider::Pointer provider) {
  bool allChildrenSelected = true;
  blTagMap::Iterator it;
  for (it = plugins->GetIteratorBegin(); it != plugins->GetIteratorEnd(); ++it) {
    std::string caption = it->second->GetName();

    // Get selected and iterate recursivelly
    bool selected = false;
    wxTreeItemId item;
    if (it->second->GetValue().type() == typeid(blTagMap::Pointer)) {
      item = m_Tree->AppendItem(parentItem, _U(caption));
      selected =
          UpdateTree(item, it->second->GetValueCasted< blTagMap::Pointer >(), provider);
      if (m_Tree->GetChildrenCount(item) == 0) {
        m_Tree->Delete(item);
      }
    } else {
      std::string name = provider->GetPluginName(caption);
      if (provider->GetPlugin(name)) {
        Core::Runtime::BasePlugin::Pointer plugin = Core::Runtime::BasePlugin::New();
        plugin->SetName(provider->GetPlugin(name)->GetName());
        plugin->SetCaption(provider->GetPlugin(name)->GetCaption());
        plugin->SetExpireDate(provider->GetPlugin(name)->GetExpireDate());
        plugin->SetSignature(provider->GetPlugin(name)->GetSignature());
        wxTreePluginData *data = new wxTreePluginData(provider, plugin);
        item = m_Tree->AppendItem(parentItem, _U(caption), 1, 1, data);
        selected = false;
      }
    }

    // Set checked state
    if (item.IsOk()) {
      m_Tree->SetItemImage(item, selected + 1);
    }

    // Return if all children are selected
    allChildrenSelected &= selected;
  }

  return allChildrenSelected;
}

void PluginSecurityWidget::OnText(wxCommandEvent &event) {
  wxTreeItemId item = m_Tree->GetSelection();
  if (!item.IsOk()) {
    return;
  }

  wxTreePluginData *data = dynamic_cast< wxTreePluginData * >(m_Tree->GetItemData(item));
  if (!data) {
    return;
  }

  Core::Runtime::BasePlugin::Pointer plugin = data->m_BasePlugin;
  if (plugin.IsNull()) {
    return;
  }

  plugin->SetName(m_txtName->GetValue().ToStdString().c_str());
  plugin->SetCaption(m_txtCaption->GetValue().ToStdString().c_str());
  plugin->SetExpireDate(m_txtDate->GetValue().ToStdString().c_str());
  plugin->SetSignature(m_txtSignature->GetValue().ToStdString().c_str());
}
