/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvVTKBtnEdit.h"

#include "resource/Trash.xpm"
#include "resource/Camera.xpm"
#include "resource/Folder.xpm"

msvVTKBtnEdit::msvVTKBtnEdit(wxWindow *parent, int id, const wxPoint &pos,
                             const wxSize &size, long style)
    : msvVTKBtnEditUI(parent, id, pos, size, style) {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();

  m_bmpClose->SetBitmapFocus(wxImage(settings->GetCoreResourceForFile("close.png")));
  m_bmpClose->SetBitmapDisabled(wxImage(settings->GetCoreResourceForFile("close.png")));
  m_bmpClose->SetBitmapLabel(wxImage(settings->GetCoreResourceForFile("close.png")));
  m_bmpClose->SetBitmapSelected(wxImage(settings->GetCoreResourceForFile("close.png")));

  m_btnTakeSnapshot->SetBitmapFocus(wxBitmap(Camera_xpm));
  m_btnTakeSnapshot->SetBitmapDisabled(wxBitmap(Camera_xpm));
  m_btnTakeSnapshot->SetBitmapLabel(wxBitmap(Camera_xpm));
  m_btnTakeSnapshot->SetBitmapSelected(wxBitmap(Camera_xpm));

  m_btnCleanSnapshot->SetBitmapFocus(Trash_xpm);
  m_btnCleanSnapshot->SetBitmapDisabled(Trash_xpm);
  m_btnCleanSnapshot->SetBitmapLabel(Trash_xpm);
  m_btnCleanSnapshot->SetBitmapSelected(Trash_xpm);

  m_btnLoadSnapshot->SetBitmapFocus(Folder_xpm);
  m_btnLoadSnapshot->SetBitmapDisabled(Folder_xpm);
  m_btnLoadSnapshot->SetBitmapLabel(Folder_xpm);
  m_btnLoadSnapshot->SetBitmapSelected(Folder_xpm);
}

void msvVTKBtnEdit::OnTakeSnapshot(wxCommandEvent &event) {
  if (m_DataEntity.IsNull()) {
    return;
  }

  // Get view for this object
  std::string name = m_DataEntity->GetMetadata()->GetName();
  blTagMap::Pointer view = m_ViewManager->GetView(name);
  if (view.IsNull()) {
    view = m_ViewManager->AddView();
    view->AddTag("Name", name);
    view->AddTag("Visible", false);
  }

  // Update parameters
  view->AddTag("SaveRenderingTree", m_chkVisualProps->GetValue());
  view->AddTag("SaveCamera", m_chkCamera->GetValue());

  // Processed by parent windows
  event.Skip();
}

void msvVTKBtnEdit::OnCleanSnapshot(wxCommandEvent &event) {
  if (m_DataEntity.IsNull()) {
    return;
  }

  // clean image file
  m_DataEntity->GetPreview()->SetImage(wxNullImage);
  m_DataEntity->GetPreview()->SaveTempFile();

  // Clean Bounds information
  blTag::Pointer boundsTag = m_DataEntity->GetMetadata()->GetTag("Bounds");
  if (boundsTag.IsNotNull())
    m_DataEntity->GetMetadata()->RemoveTag(boundsTag);

  // Remove view
  if (m_ViewManager->GetViews().IsNotNull()) {
    blTagMap::Pointer views = m_ViewManager->GetViews();
    blTag::Pointer viewTag = views->GetTag(m_DataEntity->GetMetadata()->GetName());
    if (viewTag.IsNotNull())
      views->RemoveTag(viewTag);
  }

  m_DataEntity->Modified();
}

void msvVTKBtnEdit::OnLoadSnapshot(wxCommandEvent &event) {
  wxFileDialog openFileDialog(this, wxT("Open a preview image"), wxT(""), wxT(""),
                              wxT(""), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
  if (openFileDialog.ShowModal() == wxID_OK) {
    wxImage image;
    image.LoadFile(openFileDialog.GetPath());
    if (image.IsOk()) {
      m_DataEntity->GetPreview()->SetImage(image.Scale(80, 80, wxIMAGE_QUALITY_HIGH));
      m_DataEntity->GetPreview()->SaveTempFile();
      m_DataEntity->Modified();
    }
  }
}

void msvVTKBtnEdit::SetDataEntity(Core::DataEntity::Pointer dataEntity) {
  m_DataEntity = dataEntity;
  if (m_DataEntity.IsNull()) {
    return;
  }

  // Extract values from DataEntity
  Core::DataEntityMetadata::Pointer metadata = m_DataEntity->GetMetadata();
  std::string desc;
  if (metadata->GetTag("Description").IsNotNull()) {
    desc = metadata->GetTag("Description")->GetValueAsString();
  }
  std::string name = metadata->GetName();

  // Set values
  m_txtName->SetValue(name);
  m_txtDescription->SetValue(desc);

  // Get Preview from DataEntity
  Core::DataEntityPreview::Pointer preview = m_DataEntity->GetPreview();
  if (preview->IsOk()) {
    wxImage image = boost::any_cast< wxImage >(preview->GetImage());
    wxIcon icon;
    icon.CopyFromBitmap(image);
    m_bmpSnapshot->SetIcon(icon);
  } else {
    m_bmpSnapshot->SetIcon(wxNullIcon);
  }
}

void msvVTKBtnEdit::OnApply(wxCommandEvent &event) {}

void msvVTKBtnEdit::OnCancel(wxCommandEvent &event) {
  if (m_DataEntity.IsNotNull()) {
    // Pass values to DataEntity
    m_DataEntity->GetMetadata()->SetName(std::string(m_txtName->GetValue().c_str()));
    m_DataEntity->GetMetadata()->AddTag(
        "Description", std::string(m_txtDescription->GetValue().c_str()));
    m_DataEntity->Modified();
  }

  event.Skip();
}

Core::DataEntity::Pointer msvVTKBtnEdit::GetDataEntity() { return m_DataEntity; }

void msvVTKBtnEdit::SetViewManager(msvViewManager::Pointer viewManager) {
  m_ViewManager = viewManager;
}
