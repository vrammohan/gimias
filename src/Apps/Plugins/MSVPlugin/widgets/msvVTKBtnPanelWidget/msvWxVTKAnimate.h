/*
 *  msvWxVTKAnimate.h derived from msvQAnimate class
 *
 *
 *  Created by Luigi Carotenuto on 21/02/12.
 *  Copyright 2012 Universitat Pompeu Fabra. All rights reserved.
 *
 *  See License at: http://tiny.cc/QXJ4D
 *
 */

#ifndef msvWxVTKAnimate_H
#define msvWxVTKAnimate_H

// Includes list

//#include "msvWxWidgetsExport.h"
#include <vtkRenderer.h>

/**
Class name: msvWxVTKAnimate
This is an utility class to animate VTKCamera.
\ingroup msvPlugin
*/
class msvWxVTKAnimate {

public:
  /// Object constructor.
  msvWxVTKAnimate();

  /// Object destructor.
  virtual ~msvWxVTKAnimate();

  /// Animate the camera to zoom on the passed bounding box.
  void flyTo();

  ///
  void setBounds(double b[6]);

  ///
  double *getBounds();

  ///
  void setCurrentRenderer(vtkRenderer *renderer);

  /// add vtk button to Renderer
  vtkRenderer *getCurrentRenderer();

  ///
  void setCameraPosition(double[3]);

  ///
  void setCameraParallelScale(double);

  ///
  void setCameraViewUp(double val[3]);

  ///
  void setCameraClippingRange(double val[2]);

  ///
  void setCameraFocalPoint(double val[3]);

  /// set number of steps for flyTo
  void setFlyToNSteps(int nSteps);

  /// get number of steps for flyTo
  int getFlyToNSteps() { return m_flyToNSteps; };

  /// is th button flight already?
  bool isFlight() { return m_isFlight; };

  ///
  bool checkValidBounds();

protected:
  int m_flyToNSteps;
  double m_Bounds[6]; ///< bounds of the data related to the button
  double m_CameraPosition[3];
  double m_CameraParallelScale;
  double m_CameraViewUp[3];
  double m_ClippingRange[2];
  double m_CameraFocalPoint[3];
  vtkRenderer *m_Renderer;
  bool m_isFlight; ///< is the button flight?
};

#endif // msvWxVTKAnimate_H
