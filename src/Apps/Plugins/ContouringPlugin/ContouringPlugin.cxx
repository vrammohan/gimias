/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "ContouringPlugin.h"

// CoreLib
#include "coreReportExceptionMacros.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginTab.h"

// Declaration of the plugin
coreBeginDefinePluginMacro(contouringPlugin::ContouringPlugin) coreEndDefinePluginMacro()

    namespace contouringPlugin {

  ContouringPlugin::ContouringPlugin(void) : FrontEndPlugin() {
    try {
      m_Processors = ProcessorCollective::New();
      m_Widgets = WidgetCollective::New();
    }
    coreCatchExceptionsReportAndNoThrowMacro(ContouringPlugin::ContouringPlugin)
  }

  ContouringPlugin::~ContouringPlugin(void) {}

} // namespace contouringPlugin
