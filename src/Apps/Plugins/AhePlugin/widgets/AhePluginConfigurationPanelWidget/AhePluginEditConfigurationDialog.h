/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _AhePluginEditConfigurationDialog_H
#define _AhePluginEditConfigurationDialog_H

#include "AhePluginEditConfigurationDialogUI.h"

// CoreLib
#include "coreRenderingTree.h"
#include "corePointInteractorPointSelect.h"
#include "coreProcessingWidget.h"
#include "aheAppInst.h"

namespace ahePlugin {

/**
 * GUI implementation class for Add/Edit AHE node panel.
 *
 * Defines GUI components for the panel.
 *
 * \ingroup AhePlugin
 * \author Ernesto Coto
 * \date May 2014
 */
class AhePluginEditConfigurationDialog : public AhePluginEditConfigurationDialogUI {

  // OPERATIONS
public:
  //! Class constructor
  AhePluginEditConfigurationDialog(wxWindow *parent, int id, const wxString &title,
                                   const wxPoint &pos = wxDefaultPosition,
                                   const wxSize &size = wxDefaultSize,
                                   long style = wxDEFAULT_DIALOG_STYLE);

  //! Class destructor
  ~AhePluginEditConfigurationDialog();

  //! Loads the dialog's initial content and configuration
  void OnInit();

  //! Gets the configured node
  aheAppInst::Pointer GetNode() const;

  //! Initializes the configured node. Used for editing the node.
  void SetNodeData(aheAppInst::Pointer val);

private:
  //! Update GUI from working data
  void UpdateWidget();

  //! Update working data from GUI
  void UpdateData();

  //! OK button event handler.
  void OnOK(wxCommandEvent &event);

  //! Cancel button event handler.
  void OnCancel(wxCommandEvent &event);

  //! Test button event handler.
  void OnTest(wxCommandEvent &event);

  // ATTRIBUTES
private:
  //! Working data

  // The node being added or edited
  aheAppInst::Pointer m_AppNode;

}; // class EditConfigurationPanelWidget

} // namespace ahePlugin

#endif //_AhePluginEditConfigurationDialog_H
