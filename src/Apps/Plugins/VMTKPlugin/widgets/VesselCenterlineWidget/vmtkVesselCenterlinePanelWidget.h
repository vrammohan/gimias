/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef VesselCenterlinePanelWidget_H
#define VesselCenterlinePanelWidget_H

#include <wx/wx.h>
#include <wx/image.h>

#include "corePluginMacros.h"
#include "coreProcessingWidget.h"
#include "vmtkVesselCenterlineProcessor.h"
#include "coreLandmarkSelectorWidget.h"

#define wxID_btnSources wxID_HIGHEST + 1
#define wxID_btnTargets wxID_HIGHEST + 2
#define wxID_btnCenterline wxID_HIGHEST + 3
#define wxID_btnOpenProfiles wxID_HIGHEST + 4
#define wxID_btnCenterlineOP wxID_HIGHEST + 5
#define wxID_btnSections wxID_HIGHEST + 6

/**
\brief Vessel Centerline
\ingroup MeshEditorPlugin
\author Valeria Barbarito
\date 24 Jan 2011
*/

class PLUGIN_EXPORT VesselCenterlinePanelWidget : public wxPanel,
                                                  public Core::Widgets::ProcessingWidget {
public:
  coreDefineBaseWindowFactory(VesselCenterlinePanelWidget)

      typedef enum {
        SOURCE_BUTTON,
        TARGET_BUTTON,
      } SEED_BUTTON;

  VesselCenterlinePanelWidget(wxWindow *parent, int id = wxID_ANY,
                              const wxPoint &pos = wxDefaultPosition,
                              const wxSize &size = wxDefaultSize, long style = 0);

  ~VesselCenterlinePanelWidget();

  virtual void OnButtonSources(wxCommandEvent &event);    // wxGlade: <event_handler>
  virtual void OnButtonTargets(wxCommandEvent &event);    // wxGlade: <event_handler>
  virtual void OnButtonCenterline(wxCommandEvent &event); // wxGlade: <event_handler>
  // virtual void OnButtonOpenProfiles(wxCommandEvent &event); // wxGlade: <event_handler>
  // virtual void OnButtonCenterlineWithOpenProfiles(wxCommandEvent &event); // wxGlade:
  // <event_handler>

protected:
  void DoCancel(int seedButton);

  void OnSelectedInput();
  void ConnectInteractor();
  void DisconnectInteractor();
  void OnModifiedSelectedPoint();
  void OnModifiedOutputCapCenters();

  void OnInit();
  void UpdateWidget();

  //!
  Core::BaseProcessor::Pointer GetProcessor();

private:
  // begin wxGlade: ptRingCutPanelWidget::methods
  void do_layout();
  // end wxGlade
  bool Enable(bool enable = true);

protected:
  wxButton *btnCenterline;
  wxToggleButton *toggleBtnSources;
  wxToggleButton *toggleBtnTargets;

  // wxButton* btnOpenProfiles;
  // wxButton* btnCenterlineOP;

  DECLARE_EVENT_TABLE();

private:
  //!
  VesselCenterlineProcessor::Pointer m_processor;
  //!
  bool m_sourceEnabled;
  bool m_targetEnabled;

  wxStaticBox *m_seedsStaticBox;

  wxCheckBox *m_chkAppendEndPoints;
  //
  // wxStaticBox* m_openProfilesStaticBox;
  // wxStaticText* m_staticTextInlet;
  // wxTextCtrl* m_textCtrlInlet;
  // wxStaticText* m_staticTextOutlet;
  // wxTextCtrl* m_textCtrlOutlet;

}; // wxGlade: end class

#endif // VesselCenterlinePanelWidget_H