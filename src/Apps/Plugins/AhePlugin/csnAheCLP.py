# CSNake configuration of the Template filters

# CSNake imports
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")
# Other dependencies
from csnToolkitOpen import *
from csnGIMIASDef import *

# Definition of the aheclp CLP
aheclpCLP = CommandLinePlugin("AheCLP")
aheclpCLP.AddSources([u'applications/AheCLP/AheCLP.cxx'])
aheclpCLP.AddProjects([ itk, aheAPI, boost, slicer, generateClp])
	
# Contained for all the CLP of this file
#everyCLP = api.CreateStandardModuleProject("aheclpCLP", "container")
#everyCLP.AddProjects([aheclpCLP])
