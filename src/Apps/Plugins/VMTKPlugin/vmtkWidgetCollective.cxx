/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "vmtkWidgetCollective.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTabFactory.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSimpleProcessingWidget.h"

#include "vmtkVesselBifurcationsPanelWidget.h"
#include "vmtkVesselCenterlinePanelWidget.h"

using namespace Core;

vmtkWidgetCollective::vmtkWidgetCollective() {}

void vmtkWidgetCollective::Init() {
  Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer gIface;
  gIface = Core::Runtime::Kernel::GetGraphicalInterface();

  Core::WindowConfig windowConfig = Core::WindowConfig().ProcessingTool();
  windowConfig.Category("VMTK Vasculature Analysis");
  gIface->RegisterFactory(VesselCenterlinePanelWidget::Factory::NewBase(),
                          windowConfig.Caption("Vessel Centerline"));
  gIface->RegisterFactory(VesselBifurcationsPanelWidget::Factory::NewBase(),
                          windowConfig.Caption("Centerline Bifurcations"));
}
