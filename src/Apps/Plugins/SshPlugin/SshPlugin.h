/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _SshPlugin_H
#define _SshPlugin_H

#include "SshPluginProcessorCollective.h"
#include "SshPluginWidgetCollective.h"

// CoreLib
#include "coreFrontEndPlugin.h"

namespace SshPlugin {

/**
\ingroup SshPlugin
\author Xavi Planes
\date Feb 2011
*/
class PLUGIN_EXPORT SshPlugin : public Core::FrontEndPlugin::FrontEndPlugin {
  // TYPE DEFINITIONS
public:
  coreDeclareSmartPointerClassMacro(SshPlugin, Core::FrontEndPlugin::FrontEndPlugin);

  // OPERATIONS
protected:
  //!
  SshPlugin(void);

  //!
  virtual ~SshPlugin(void);

private:
  //! Purposely not implemented
  SshPlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  ProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  WidgetCollective::Pointer m_Widgets;
};

} // namespace SshPlugin

#endif // SshPlugin_H
