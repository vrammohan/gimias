/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "SSHPluginProviderConfigurationDialog.h"
#include "sshConnection.h"
#include "sshDirectory.h"
#include "sshFile.h"
#include "SshPluginAskPasswordWx.h"

#include "wxID.h"
#include "coreSettingsIO.h"

#include "itksys/SystemTools.hxx"

#include "wx/wupdlock.h"

using namespace Core::Widgets;

#define wxID_CMB_SSH_NODES wxID("wxID_CMB_SSH_NODES")
#define wxID_CMB_REMOTE_GIMIAS wxID("wxID_CMB_REMOTE_GIMIAS")

/**
Store remote GIMIAS folders for each GIMIAS version: scan and working directory
*/
class wxRemoteGimiasData : public wxClientData {
public:
  wxRemoteGimiasData(std::string workingDirectory = "", std::string scanFolder = "") {
    m_WorkingDirectory = workingDirectory;
    m_ScanFolder = scanFolder;
  }
  //!
  std::string m_WorkingDirectory;
  //!
  std::string m_ScanFolder;
};

BEGIN_EVENT_TABLE(SSHPluginProviderConfigurationDialog,
                  Core::Widgets::PropertiesConfigurationDialog)
EVT_COMBOBOX(wxID_CMB_SSH_NODES,
             SSHPluginProviderConfigurationDialog::OnCmbSSHNodesChanged)
EVT_COMBOBOX(wxID_CMB_REMOTE_GIMIAS,
             SSHPluginProviderConfigurationDialog::OnCmbRemoteGIMIASChanged)
END_EVENT_TABLE()

SSHPluginProviderConfigurationDialog::SSHPluginProviderConfigurationDialog(
    SshPluginNodeManager::Pointer nodeManager, wxWindow *parent, int id,
    const wxString &title, const wxPoint &pos, const wxSize &size, long style)
    : PropertiesConfigurationDialog(parent, id, title, pos, size, style) {
  m_NodeManager = nodeManager;

  m_cmbSshNode =
      new wxComboBox(this, wxID_CMB_SSH_NODES, wxT(""), wxDefaultPosition, wxDefaultSize,
                     0, NULL, wxCB_DROPDOWN | wxCB_SIMPLE | wxCB_READONLY);

  m_cmbRemoteGimias =
      new wxComboBox(this, wxID_CMB_REMOTE_GIMIAS, wxT(""), wxDefaultPosition,
                     wxDefaultSize, 0, NULL, wxCB_DROPDOWN | wxCB_SIMPLE | wxCB_READONLY);
  m_SizerRemoteGimias = new wxBoxSizer(wxHORIZONTAL);
  m_LabelRemoteGimias = new wxStaticText(this, wxID_ANY, wxT("Remote GIMIAS versions"));
  m_SizerRemoteGimias->Add(m_LabelRemoteGimias, 1, wxALIGN_CENTER_VERTICAL, 0);
  m_SizerRemoteGimias->Add(m_cmbRemoteGimias, 1, 0, 0);
  m_cmbRemoteGimias->Hide();
  m_LabelRemoteGimias->Hide();

  set_properties();
  do_layout();
}

void SSHPluginProviderConfigurationDialog::set_properties() {}

void SSHPluginProviderConfigurationDialog::do_layout() {}

void SSHPluginProviderConfigurationDialog::UpdateData() {
  PropertiesConfigurationDialog::UpdateData();

  if (m_Properties.IsNull()) {
    return;
  }

  std::string selected = std::string(m_cmbSshNode->GetStringSelection().mb_str());
  blTag::Pointer tag = m_Properties->FindTagByName("SSH node");
  tag->SetValueAsString(tag->GetTypeName(), selected);
}

void SSHPluginProviderConfigurationDialog::UpdateWidget() {
  PropertiesConfigurationDialog::UpdateWidget();

  CreateSSHNodesCombo();

  UpdateGIMIASComboBox();

  GetSizer()->Fit(this);
  Layout();
}

void SSHPluginProviderConfigurationDialog::CreateSSHNodesCombo() {
  if (m_Properties.IsNull()) {
    return;
  }

  // Replace wxTextCtrl by a combo box
  wxWindow *win = FindWindow("SSH node");
  wxTextCtrl *textCtrl = wxDynamicCast(win, wxTextCtrl);
  if (textCtrl == NULL) {
    return;
  }
  sshNodeManager::NodeMapType nodes = m_NodeManager->GetNodeMap();
  sshNodeManager::NodeMapType::iterator it;
  for (it = nodes.begin(); it != nodes.end(); it++) {
    m_cmbSshNode->Append(it->first);
  }

  blTag::Pointer tag = m_Properties->FindTagByName("SSH node");
  m_cmbSshNode->SetStringSelection(tag->GetValueAsString());

  textCtrl->GetContainingSizer()->Replace(textCtrl, m_cmbSshNode);

  textCtrl->Destroy();
}

void SSHPluginProviderConfigurationDialog::UpdateGIMIASComboBox() {
  wxWindowUpdateLocker lock(this);

  std::string selected = std::string(m_cmbSshNode->GetStringSelection().mb_str());
  sshNode::Pointer node = m_NodeManager->Get(selected);
  if (node.IsNull()) {
    GetSizer()->Detach(m_SizerRemoteGimias);
    m_LabelRemoteGimias->Hide();
    m_cmbRemoteGimias->Hide();
    return;
  }

  // Connect
  sshConnection::Pointer connection = sshConnection::New();
  connection->SetNode(node);
  connection->SetPasswordMethod(new sshPluginAskPasswordWx());
  connection->Connect();

  // Load ".gimias" folder in Linux
  sshDirectory remoteDir(connection);
  remoteDir.SetPath(".gimias");
  remoteDir.Load();

  // Append items
  m_cmbRemoteGimias->Clear();
  for (int i = 0; i < remoteDir.GetNumberOfFiles(); i++) {
    std::string configFileName = remoteDir.GetFullFile(i);
    configFileName += "/config.xml";

    Core::IO::ConfigVars configVars;
    try {
      // Download file
      sshFile file(connection);
      file.SetRemoteFilename(configFileName);
      file.DownloadByReference("remoteConfig.xml");

      // Read configuration
      Core::IO::SettingsIO::Pointer settingsIO = Core::IO::SettingsIO::New();
      settingsIO->ReadConfigFromFile("remoteConfig.xml", configVars);
    } catch (...) {
    }
    itksys::SystemTools::RemoveFile("remoteConfig.xml");

    // Check if ApplicationPath is present
    blTag::Pointer appPathTag;
    appPathTag =
        configVars.m_MapPluginConfiguration["GIMIAS"].m_Properties->FindTagByName(
            "ApplicationPath");
    if (appPathTag.IsNull()) {
      continue;
    }

    wxRemoteGimiasData *data = new wxRemoteGimiasData();
    data->m_WorkingDirectory = appPathTag->GetValueAsString();
    data->m_ScanFolder = data->m_WorkingDirectory + "/commandLinePlugins";
    m_cmbRemoteGimias->Append(remoteDir.GetFile(i), data);
  }

  if (!m_cmbRemoteGimias->IsListEmpty()) {
    m_LabelRemoteGimias->Show();
    m_cmbRemoteGimias->Show();
    size_t count = GetSizer()->GetChildren().size();
    GetSizer()->Insert(count - 1, m_SizerRemoteGimias, 0, wxALL | wxEXPAND, 5);
  } else {
    GetSizer()->Detach(m_SizerRemoteGimias);
    m_LabelRemoteGimias->Hide();
    m_cmbRemoteGimias->Hide();
  }

  // Preserve dialog width
  int width = GetSize().x;
  GetSizer()->Fit(this);
  Layout();
  SetSize(width, wxDefaultCoord);
}

void SSHPluginProviderConfigurationDialog::OnCmbSSHNodesChanged(wxCommandEvent &event) {
  UpdateGIMIASComboBox();
}

void SSHPluginProviderConfigurationDialog::OnCmbRemoteGIMIASChanged(
    wxCommandEvent &event) {
  wxClientData *clientData;
  clientData = m_cmbRemoteGimias->GetClientObject(m_cmbRemoteGimias->GetSelection());

  wxRemoteGimiasData *data;
  data = static_cast< wxRemoteGimiasData * >(clientData);

  wxWindow *win = FindWindowByName("Scan folder");
  if (win) {
    wxTextCtrl *textCtrl = wxDynamicCast(win, wxTextCtrl);
    textCtrl->SetValue(data->m_ScanFolder);
  }

  win = FindWindowByName("Working directory");
  if (win) {
    wxTextCtrl *textCtrl = wxDynamicCast(win, wxTextCtrl);
    textCtrl->SetValue(data->m_WorkingDirectory);
  }
}
