/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef msvWxHtmlWindow_H
#define msvWxHtmlWindow_H

// wxWidgets
#include <wx/html/htmlwin.h>

class wxDataNavigationWidget;
class msvWxVTKButtons;
class msvMultiRenderWindow;

/**
\brief HTML window that builds HTML pages depending on the current mode

It contains different modes:
- Tooltip
- Navigation
- Description
- Complete information
- Edit
- Views

\ingroup msvKPlugin
\author Xavi Planes
\date Nov 2010
*/
class msvWxHtmlWindow : public wxHtmlWindow {
public:
  enum MODE_TYPE {
    //! Simple tooltip
    MODE_TOOLTIP,
    //! Navigation
    MODE_NAVIGATION,
    //! Brief information
    MODE_DESCRIPTION,
    //! Complete information
    MODE_COMPLETE_INFORMATION,
    //! Edit
    MODE_EDIT,
    //! Browse views
    MODE_BROWSE_VIEWS,
    //! Edit view
    MODE_EDIT_VIEW,
    MODE_NONE
  };

  msvWxHtmlWindow(wxWindow *parent, wxWindowID id = -1,
                  const wxPoint &pos = wxDefaultPosition,
                  const wxSize &size = wxDefaultSize, long style = wxHW_DEFAULT_STYLE,
                  const wxString &name = "htmlWindow");

  //! Overwrite default behavior
  void OnLinkClicked(const wxHtmlLinkInfo &link);

  //!
  void OnInternalIdle();

  //! Set active button
  void SetButton(msvWxVTKButtons *button);

  //!
  void SetRenderWindow(msvMultiRenderWindow *renderWindow);

  //!
  void BuildContents(MODE_TYPE mode);

protected:
  //!
  std::string BuildToolTip();

  //!
  std::string BuildNavigation();

  //!
  std::string BuildDescription();

  //!
  std::string BuildCompleteInformation();

  //!
  std::string BuildViews();

  //!
  std::string BuildGenericDataInformation();

  //!
  std::string BuildNavigationLink();

  //!
  std::string BuildHeader();

  //!
  std::string BuildMenu();

  //!
  std::string BuildNavigationContent();

  //!
  std::string BuildDescriptionMainContent();

  //!
  std::string BuildViewsMainContent();

private:
  //!
  wxDataNavigationWidget *m_Dialog;

  //!
  msvMultiRenderWindow *m_RenderWindow;

  //!
  msvWxVTKButtons *m_Button;
};

#endif // msvWxHtmlWindow_H