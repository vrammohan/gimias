/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#include "msvVTKBtnEditView.h"

#include "resource/Trash.xpm"
#include "resource/Camera.xpm"
#include "resource/Folder.xpm"

msvVTKBtnEditView::msvVTKBtnEditView(wxWindow *parent, int id, const wxPoint &pos,
                                     const wxSize &size, long style)
    : msvVTKBtnEditViewUI(parent, id, pos, size, style) {
  Core::Runtime::Settings::Pointer settings =
      Core::Runtime::Kernel::GetApplicationSettings();

  m_bmpClose->SetBitmapFocus(wxImage(settings->GetCoreResourceForFile("close.png")));
  m_bmpClose->SetBitmapDisabled(wxImage(settings->GetCoreResourceForFile("close.png")));
  m_bmpClose->SetBitmapLabel(wxImage(settings->GetCoreResourceForFile("close.png")));
  m_bmpClose->SetBitmapSelected(wxImage(settings->GetCoreResourceForFile("close.png")));

  m_btnTakeSnapshot->SetBitmapFocus(wxBitmap(Camera_xpm));
  m_btnTakeSnapshot->SetBitmapDisabled(wxBitmap(Camera_xpm));
  m_btnTakeSnapshot->SetBitmapLabel(wxBitmap(Camera_xpm));
  m_btnTakeSnapshot->SetBitmapSelected(wxBitmap(Camera_xpm));

  m_btnCleanSnapshot->SetBitmapFocus(Trash_xpm);
  m_btnCleanSnapshot->SetBitmapDisabled(Trash_xpm);
  m_btnCleanSnapshot->SetBitmapLabel(Trash_xpm);
  m_btnCleanSnapshot->SetBitmapSelected(Trash_xpm);

  m_btnLoadSnapshot->SetBitmapFocus(Folder_xpm);
  m_btnLoadSnapshot->SetBitmapDisabled(Folder_xpm);
  m_btnLoadSnapshot->SetBitmapLabel(Folder_xpm);
  m_btnLoadSnapshot->SetBitmapSelected(Folder_xpm);
}

void msvVTKBtnEditView::OnSelectedViewModified() { UpdateWidget(); }

void msvVTKBtnEditView::OnCancel(wxCommandEvent &event) {
  UpdateData();
  event.Skip();
}

void msvVTKBtnEditView::OnCaptureView(wxCommandEvent &event) {
  UpdateData();
  event.Skip();
}

void msvVTKBtnEditView::OnRemoveView(wxCommandEvent &event) { event.Skip(); }

void msvVTKBtnEditView::OnLoadSnapshot(wxCommandEvent &event) {
  blTagMap::Pointer view = m_ViewManager->GetSelectedView();
  if (view.IsNull())
    return;

  wxFileDialog openFileDialog(this, wxT("Open a preview image"), wxT(""), wxT(""),
                              wxT(""), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
  if (openFileDialog.ShowModal() == wxID_OK) {
    wxImage image;
    image.LoadFile(openFileDialog.GetPath());
    if (image.IsOk()) {
      view->AddTag("Image", image.Scale(80, 80, wxIMAGE_QUALITY_HIGH));
      UpdateWidget();
    }
  }

  event.Skip();
}

void msvVTKBtnEditView::UpdateWidget() {
  blTagMap::Pointer view = m_ViewManager->GetSelectedView();
  if (view.IsNull()) {
    m_txtName->SetValue("");
    m_chkVisualProps->SetValue(true);
    m_chkCamera->SetValue(true);
    m_bmpSnapshot->SetIcon(wxNullIcon);
  } else {
    m_txtName->SetValue(view->GetTag("Name")->GetValueAsString());
    m_chkVisualProps->SetValue(view->GetTagValue< bool >("SaveRenderingTree"));
    m_chkCamera->SetValue(view->GetTagValue< bool >("SaveCamera"));

    wxImage image;
    if (view->GetTag("Image").IsNotNull())
      image = view->GetTag("Image")->GetValueCasted< wxImage >();
    if (image.IsOk()) {
      wxIcon icon;
      icon.CopyFromBitmap(image);
      m_bmpSnapshot->SetIcon(icon);
    } else {
      m_bmpSnapshot->SetIcon(wxNullIcon);
    }
  }
}

void msvVTKBtnEditView::UpdateData() {
  blTagMap::Pointer view = m_ViewManager->GetSelectedView();
  if (view.IsNotNull()) {
    view->AddTag("Name", std::string(m_txtName->GetValue()));
    view->AddTag("SaveRenderingTree", m_chkVisualProps->GetValue());
    view->AddTag("SaveCamera", m_chkCamera->GetValue());
  }
}

void msvVTKBtnEditView::SetViewManager(msvViewManager::Pointer viewManager) {
  if (m_ViewManager.IsNotNull())
    m_ViewManager->GetSelectedViewHolder()->RemoveObserver(
        this, &msvVTKBtnEditView::OnSelectedViewModified);

  m_ViewManager = viewManager;

  if (m_ViewManager.IsNotNull())
    m_ViewManager->GetSelectedViewHolder()->AddObserver(
        this, &msvVTKBtnEditView::OnSelectedViewModified);
}
