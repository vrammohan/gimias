/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _VesselCenterlineProcessor_H
#define _VesselCenterlineProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseFilter.h"
#include "coreBaseProcessor.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include "coreVTKPolyDataHolder.h"
//#include "coreDirectionalPlane.h"

/*
compute vessel centerline using VMTK

\ingroup VMTKPlugin
\author Valeria Barbarito
\date 24 jan 2011
*/

class PLUGIN_EXPORT VesselCenterlineProcessor : public Core::BaseProcessor {
public:
  coreDeclareSmartPointerClassMacro(VesselCenterlineProcessor, Core::BaseProcessor);

  typedef enum { INPUT_SURFACE, INPUT_SEED_POINTS, NUMBER_OF_INPUTS } INPUT_TYPE;

  typedef enum {
    OUTPUT_CENTERLINE,
    OUTPUT_CAPPEDSURFACE,
    OUTPUT_CAPSCENTER_POINTS,
    NUMBER_OF_OUTPUTS
  } OUTPUT_TYPE;

  typedef enum {
    SEED_POINTLIST,
    SEED_OPENPROFILES,
  } SEEDSELECTOR_TYPE;

  //!
  void Update();
  void ComputeCenterline(int seedSelectorType);
  void ShowCapCenters();
  void SetInletOutletIDs(int inletID, int outletID);

  void Smoothing(Core::vtkPolyDataPtr centerline);

  //!
  void UpdatePlane();

  //!
  bool CheckInputs();

  //!
  void ComputePointIdLists();

  void GetCapCenterIDs();

  void NonManifoldSurfaceChecker(Core::vtkPolyDataPtr surface);

  int GetNumSource() const { return m_numSource; }
  void SetNumSource(int val) { m_numSource = val; }

  int GetSeedSelectorType() const { return m_seedSelectorType; }
  void SetSeedSelectorType(int val) { m_seedSelectorType = val; }

  int GetNumberOfIds();

  void SetAppendEndPoints(bool val);

private:
  /**
  */
  VesselCenterlineProcessor();
  //!
  ~VesselCenterlineProcessor();

  vtkSmartPointer< vtkIdList > m_targetPointsList;

  vtkSmartPointer< vtkIdList > m_sourcePointsList;

  vtkSmartPointer< vtkIdList > m_capCenterIDs;

  Core::vtkPolyDataPtr m_cappedSurfaceInput;

  std::vector< Core::vtkPolyDataPtr > m_landmarks;
  std::string m_radiusArrayName;
  std::string m_costFunction;
  int m_flipNormals;
  int m_appendEndPoints;
  int m_simplifyVoronoi;
  int m_resampling;
  double m_resamplingStepLength;
  int m_numSource;
  int m_seedSelectorType;

  int m_numTarget;

  int m_inletID;
  int m_outletID;
};

#endif //_VesselCenterlineProcessor_H
