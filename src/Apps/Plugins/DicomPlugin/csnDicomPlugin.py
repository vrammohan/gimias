# Used to configure DicomPlugin
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

dicomPlugin = GimiasPluginProject("DicomPlugin", api)

projects = [
    baseLibITK,
    tpExtLibMITK, 
    gmCore,  
    boost, 
    cilabMacros,
    dcmAPI,
    pacsAPI,
    wxMitk
]
widgetModules = ["DicomWorkingAreaWidget", "DicomConnectToPACSWidget", "DICOMTree", "BrowseTags"]

dicomPlugin.AddProjects(projects)
dicomPlugin.AddSources(["*.h", "*.cpp","*.cxx"])
#dicomPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
dicomPlugin.AddWidgetModules(widgetModules)
dicomPlugin.AddIncludeFolders(["processors"])
dicomPlugin.SetPrecompiledHeader("DicomPluginPCH.h")
if api.GetCompiler().TargetIsWindows():
    dicomPlugin.AddDefinitions(["/bigobj"])

dicomPlugin.AddFilesToInstall(dicomPlugin.Glob("widgets/resource/*.ico"), "resource")


