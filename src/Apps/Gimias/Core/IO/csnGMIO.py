# Used to configure coreIO
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmIO = api.CreateCompiledProject("gmIO", "dll")
gmIO.AddProjects([ 
  cilabMacros,
  boost,
  gmFiltering,
  tpExtLibUTF
])

gmIO.SetPrecompiledHeader("gmIOPCH.h")

gmIO.AddSources(["src/*.cxx" ], checkExists = 0)
gmIO.AddSources(["src/*.txx" ], checkExists = 0)
gmIO.AddSources(["src/*.h" ], checkExists = 0)
gmIO.AddIncludeFolders(["src"])

if api.GetCompiler().TargetIsWindows():
    gmIO.AddDefinitions(["-D_CRT_SECURE_NO_DEPRECATE"], private = 1)
    gmIO.AddDefinitions(["-D_SCL_SECURE_NO_WARNINGS"], private = 1)

IOFolders = gmIO.Glob("src/DataFormatObjects/*")
for IOFolder in IOFolders:
    if os.path.isdir(IOFolder):
        gmIO.AddSources(["%s/*.cxx" % IOFolder], checkExists = 0)
        gmIO.AddSources(["%s/*.txx" % IOFolder], checkExists = 0)
        gmIO.AddSources(["%s/*.h" % IOFolder], checkExists = 0)
        gmIO.AddIncludeFolders([IOFolder])
        

if api.GetCompiler().TargetIsWindows():
    gmIO.AddDefinitions(["/bigobj"], private = 1)

