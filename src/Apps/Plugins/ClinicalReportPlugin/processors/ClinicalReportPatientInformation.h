/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef ClinicalReportPatientInformation_H
#define ClinicalReportPatientInformation_H

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 26 Oct 2010
*/

class ClinicalReportPatientInformation {
public:
  ClinicalReportPatientInformation();

  //! Getters
  wxString GetId();
  wxString GetName();
  int GetAge();
  bool GetSex();
  wxDateTime GetAdquisitionDate();
  wxDateTime GetBirthDate();

  //! Setters
  void SetId(wxString);
  void SetName(wxString);
  void SetAge(int);
  void SetSex(bool);
  void SetAdquisitionDate(wxDateTime);
  void SetBirthDate(wxDateTime);

private:
  wxString m_Id;
  wxString m_Name;
  int m_Age;
  bool m_Sex; // female = true
  wxDateTime m_AdquisitionDate;
  wxDateTime m_BirthDate;
};

#endif // ClinicalReportPatientInformation_H
