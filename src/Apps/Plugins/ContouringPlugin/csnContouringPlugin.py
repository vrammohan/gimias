# CSNake configuration of the ContouringPlugin

# CSNake imports
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")
# Other dependencies
from csnGIMIASDef import *
from csnToolkitOpen import * 

# Definition of the template plugin
contouringPlugin = GimiasPluginProject("ContouringPlugin", api)
# plugin dependencies
projects = [
    gmCore, 
    guiBridgeLib, 
    baseLibVTK,
    guiBridgeLibWxWidgets
]
contouringPlugin.AddProjects(projects)
# plugin sources
contouringPlugin.AddSources(["*.cxx", "*.h"])
contouringPlugin.SetPrecompiledHeader("ContouringPluginPCH.h")
# plugin tests
contouringPlugin.AddTests(["tests/*.*"], cxxTest)

# plugin widgets
widgetModules = [
  "ContouringWidget",
]
contouringPlugin.AddWidgetModules(widgetModules, _useQt = 0)
contouringPlugin.AddSources(["processors/*.cxx", "processors/*.h"])
contouringPlugin.AddIncludeFolders(["processors",])