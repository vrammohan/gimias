// -*- C++ -*- generated by wxGlade 0.6.3 on Fri Feb 17 15:40:08 2012

#include "XNATWorkingAreaUI.h"

// begin wxGlade: ::extracode

// end wxGlade

XNATWorkingAreaUI::XNATWorkingAreaUI(wxWindow *parent, int id, const wxPoint &pos,
                                     const wxSize &size, long style)
    : wxScrolledWindow(parent, id, pos, size, style) {
  // begin wxGlade: XNATWorkingAreaUI::XNATWorkingAreaUI
  m_PanelDetailedInformation = new wxScrolledWindow(this, wxID_ANY, wxDefaultPosition,
                                                    wxDefaultSize, wxTAB_TRAVERSAL);
  notebook_1 = new wxNotebook(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0);
  notebook_1_pane_2 = new wxPanel(notebook_1, wxID_ANY);
  notebook_1_pane_1 = new wxPanel(notebook_1, wxID_ANY);
  notebook_1_pane_1_copy = new wxPanel(notebook_1_pane_1, wxID_ANY);
  sizer_5_copy_staticbox = new wxStaticBox(notebook_1_pane_1_copy, -1, wxEmptyString);
  sizer_5_copy_1_staticbox = new wxStaticBox(notebook_1_pane_2, -1, wxEmptyString);
  sizer_27_staticbox = new wxStaticBox(this, -1, wxT("Search Criteria"));
  sizer_15_staticbox = new wxStaticBox(this, -1, wxT("Local Files"));
  sizer_6_copy_staticbox = new wxStaticBox(this, -1, wxT("Detailed Information"));
  sizer_10_staticbox = new wxStaticBox(this, -1, wxT("Results"));
  sizer_2_staticbox = new wxStaticBox(this, -1, wxT("Login Credentials"));
  bitmap_CVRemod = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap);
  label_2 = new wxStaticText(this, wxID_ANY, wxT("Connection:"));
  const wxString *m_ComboConnections_choices = NULL;
  m_ComboConnections = new wxComboBox(
      this, wxID_XNAT_CONNECTIONS_COMBO, wxT(""), wxDefaultPosition, wxDefaultSize, 0,
      m_ComboConnections_choices, wxCB_DROPDOWN | wxCB_READONLY);
  m_ConnectionsBtn =
      new wxButton(this, wxID_XNAT_MANAGE_CONNECTIONS, wxT("Manage Connections"));
  label_SubjectID =
      new wxStaticText(notebook_1_pane_1_copy, wxID_ANY, wxT("Subject Label:"));
  m_edSubjectID = new wxTextCtrl(notebook_1_pane_1_copy, wxID_ANY, wxEmptyString);
  label_Gender = new wxStaticText(notebook_1_pane_1_copy, wxID_ANY, wxT("Gender:"));
  const wxString combo_Gender_choices[] = { wxEmptyString, wxT("female"), wxT("male"),
                                            wxT("other"), wxT("unknown") };
  combo_Gender = new wxComboBox(notebook_1_pane_1_copy, wxID_ANY, wxT(""),
                                wxDefaultPosition, wxDefaultSize, 5, combo_Gender_choices,
                                wxCB_DROPDOWN | wxCB_READONLY);
  label_Hand = new wxStaticText(notebook_1_pane_1_copy, wxID_ANY, wxT("Handedness:"));
  const wxString combo_Hand_choices[] = { wxEmptyString, wxT("ambidextrous"),
                                          wxT("right"), wxT("unknown"), wxT("left") };
  combo_Hand =
      new wxComboBox(notebook_1_pane_1_copy, wxID_ANY, wxT(""), wxDefaultPosition,
                     wxDefaultSize, 5, combo_Hand_choices, wxCB_DROPDOWN | wxCB_READONLY);
  label_YOB = new wxStaticText(notebook_1_pane_1_copy, wxID_ANY, wxT("YOB:"));
  m_edYOB = new wxTextCtrl(notebook_1_pane_1_copy, wxID_ANY, wxEmptyString);
  label_Project = new wxStaticText(notebook_1_pane_1_copy, wxID_ANY, wxT("Project:"));
  const wxString *combo_Projects_choices = NULL;
  combo_Projects = new wxComboBox(notebook_1_pane_1_copy, wxID_ANY, wxT(""),
                                  wxDefaultPosition, wxDefaultSize, 0,
                                  combo_Projects_choices, wxCB_DROPDOWN | wxCB_READONLY);
  m_btnQuery = new wxButton(notebook_1_pane_1_copy, wxID_XNAT_QUERY, wxT("Query"));
  label_Label = new wxStaticText(notebook_1_pane_2, wxID_ANY, wxT("Experiment label:"));
  m_LabelExperiment = new wxTextCtrl(notebook_1_pane_2, wxID_ANY, wxEmptyString);
  label_XSI_Type = new wxStaticText(notebook_1_pane_2, wxID_ANY, wxT("XSI type:"));
  const wxString *combo_XSI_Type_choices = NULL;
  combo_XSI_Type = new wxComboBox(notebook_1_pane_2, wxID_ANY, wxT(""), wxDefaultPosition,
                                  wxDefaultSize, 0, combo_XSI_Type_choices,
                                  wxCB_DROPDOWN | wxCB_READONLY);
  label_Date = new wxStaticText(notebook_1_pane_2, wxID_ANY, wxT("Date between:"));
  datepicker_ctrl_1 = new wxDatePickerCtrl(notebook_1_pane_2, wxID_ANY);
  m_DateAfter = new wxDatePickerCtrl(notebook_1_pane_2, wxID_ANY);
  label_Project_Experiments =
      new wxStaticText(notebook_1_pane_2, wxID_ANY, wxT("Project:"));
  const wxString *combo_Projects_Experiments_choices = NULL;
  combo_Projects_Experiments = new wxComboBox(
      notebook_1_pane_2, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0,
      combo_Projects_Experiments_choices, wxCB_DROPDOWN | wxCB_READONLY);
  m_btnQuery_Experiments =
      new wxButton(notebook_1_pane_2, wxID_XNAT_QUERY_EXPERIMENTS, wxT("Query"));
  m_btnOpenDirectoryForSavingPacsData =
      new wxButton(this, wxID_XNAT_OPEN_DIRECTORY_FOR_SAVING_DATA, wxT("Select Folder"));
  m_edSaveToDirectory = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
  // bitmap_Browse = new wxBitmapButton(this, wxID_XNAT_BROWSE_LOCAL , wxNullBitmap);
  bitmap_XNAT = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap);
  m_XNATTree = new XNATTree(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
                            wxTR_HAS_BUTTONS | wxTR_LINES_AT_ROOT | wxTR_MULTIPLE |
                                wxTR_MULTIPLE | wxTR_DEFAULT_STYLE | wxSUNKEN_BORDER);
  m_btnDownload = new wxButton(this, wxID_XNAT_DOWNLOAD, wxT("Download"));

  set_properties();
  do_layout();
  // end wxGlade
}

BEGIN_EVENT_TABLE(XNATWorkingAreaUI, wxScrolledWindow)
// begin wxGlade: XNATWorkingAreaUI::event_table
EVT_BUTTON(wxID_XNAT_MANAGE_CONNECTIONS, XNATWorkingAreaUI::OnManageConnection)
EVT_BUTTON(wxID_XNAT_QUERY, XNATWorkingAreaUI::OnQuery)
EVT_BUTTON(wxID_XNAT_QUERY_EXPERIMENTS, XNATWorkingAreaUI::OnQueryExperiments)
EVT_BUTTON(wxID_XNAT_OPEN_DIRECTORY_FOR_SAVING_DATA,
           XNATWorkingAreaUI::OpenDirectoryForSavingData)
EVT_BUTTON(wxID_XNAT_BROWSE_LOCAL, XNATWorkingAreaUI::OnBrowseLocal)
EVT_TREE_SEL_CHANGED(wxID_ANY, XNATWorkingAreaUI::OnTreeSelectionChanged)
EVT_BUTTON(wxID_XNAT_DOWNLOAD, XNATWorkingAreaUI::OnDownload)
EVT_COMBOBOX(wxID_XNAT_CONNECTIONS_COMBO, XNATWorkingAreaUI::OnConnectionItemSelect)
// end wxGlade
END_EVENT_TABLE();

void XNATWorkingAreaUI::OnConnectionItemSelect(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(
      wxT("Event handler (XNATWorkingAreaUI::OnConnectionItemSelect) not implemented "
          "yet")); // notify the user that he hasn't implemented the event handler yet
}

void XNATWorkingAreaUI::OnManageConnection(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT("Event handler (XNATWorkingAreaUI::OnManageConnection) not implemented "
                 "yet")); // notify the user that he hasn't implemented the event handler
                          // yet
}

void XNATWorkingAreaUI::OnQuery(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT(
      "Event handler (XNATWorkingAreaUI::OnQuery) not implemented yet")); // notify the
                                                                          // user that he
                                                                          // hasn't
                                                                          // implemented
                                                                          // the event
                                                                          // handler yet
}

void XNATWorkingAreaUI::OnQueryExperiments(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT("Event handler (XNATWorkingAreaUI::OnQueryExperiments) not implemented "
                 "yet")); // notify the user that he hasn't implemented the event handler
                          // yet
}

void XNATWorkingAreaUI::OpenDirectoryForSavingData(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(
      wxT("Event handler (XNATWorkingAreaUI::OpenDirectoryForSavingData) not implemented "
          "yet")); // notify the user that he hasn't implemented the event handler yet
}

void XNATWorkingAreaUI::OnBrowseLocal(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT(
      "Event handler (XNATWorkingAreaUI::OnBrowseLocal) not implemented yet")); // notify
                                                                                // the
                                                                                // user
                                                                                // that he
                                                                                // hasn't
                                                                                // implemented
                                                                                // the
                                                                                // event
                                                                                // handler
                                                                                // yet
}

void XNATWorkingAreaUI::OnTreeSelectionChanged(wxTreeEvent &event) {
  event.Skip();
  wxLogDebug(
      wxT("Event handler (XNATWorkingAreaUI::OnTreeSelectionChanged) not implemented "
          "yet")); // notify the user that he hasn't implemented the event handler yet
}

void XNATWorkingAreaUI::OnDownload(wxCommandEvent &event) {
  event.Skip();
  wxLogDebug(wxT(
      "Event handler (XNATWorkingAreaUI::OnDownload) not implemented yet")); // notify the
                                                                             // user that
                                                                             // he hasn't
                                                                             // implemented
                                                                             // the event
                                                                             // handler
                                                                             // yet
}

// wxGlade: add XNATWorkingAreaUI event handlers

void XNATWorkingAreaUI::set_properties() {
  // begin wxGlade: XNATWorkingAreaUI::set_properties
  SetSize(wxSize(1459, 939));
  SetScrollRate(10, 10);
  m_ComboConnections->SetToolTip("XNAT Server Selection");
  m_ConnectionsBtn->SetMinSize(wxSize(120, 30));
  m_ConnectionsBtn->SetToolTip("Manage XNAT Configuration");
  m_edSubjectID->SetMinSize(wxSize(160, 23));
  combo_Gender->SetSelection(0);
  combo_Hand->SetSelection(-1);
  m_edYOB->SetMinSize(wxSize(160, 23));
  m_btnQuery->SetMinSize(wxSize(120, 30));
  m_LabelExperiment->SetMinSize(wxSize(160, 23));
  m_btnQuery_Experiments->SetMinSize(wxSize(120, 30));
  m_btnOpenDirectoryForSavingPacsData->SetMinSize(wxSize(120, 30));
  m_btnDownload->SetMinSize(wxSize(120, 30));
  m_btnDownload->Enable(false);
  m_btnDownload->SetToolTip("Downloads a selected 'Experiment' or a 'Resource File' from "
                            "the Results shown above");
  m_PanelDetailedInformation->SetMinSize(wxSize(-1, 85));
  m_PanelDetailedInformation->SetScrollRate(10, 10);
  // end wxGlade
}

void XNATWorkingAreaUI::do_layout() {
  // begin wxGlade: XNATWorkingAreaUI::do_layout
  wxBoxSizer *sizer_7 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_4 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_19 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_21 = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *sizer_10 = new wxStaticBoxSizer(sizer_10_staticbox, wxHORIZONTAL);
  wxBoxSizer *sizer_8 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_22 = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *sizer_6_copy =
      new wxStaticBoxSizer(sizer_6_copy_staticbox, wxHORIZONTAL);
  wxBoxSizer *sizer_10_copy = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_3 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_1 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_20 = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *sizer_15 = new wxStaticBoxSizer(sizer_15_staticbox, wxHORIZONTAL);
  wxBoxSizer *sizer_16 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_17 =
      new wxBoxSizer(wxVERTICAL); // VR: changed from wxHORIZONTAL to wxVERTICAL
  wxStaticBoxSizer *sizer_27 = new wxStaticBoxSizer(sizer_27_staticbox, wxHORIZONTAL);
  wxBoxSizer *sizer_9 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_25 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_26 = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *sizer_5_copy_1 =
      new wxStaticBoxSizer(sizer_5_copy_1_staticbox, wxVERTICAL);
  wxBoxSizer *sizer_3_copy_copy_copy_1_copy_1_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_9_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_3_copy_copy_copy_1_copy_2_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_3_copy_copy_copy_1_copy_2 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_3_copy_copy_copy_2_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_5 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_13 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_18 = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *sizer_5_copy =
      new wxStaticBoxSizer(sizer_5_copy_staticbox, wxVERTICAL);
  wxBoxSizer *sizer_3_copy_copy_copy_1_copy_1 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_3_copy_copy_copy_2 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_3_copy_copy_copy_1_copy = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_3_copy_copy_copy_1 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_3_copy_copy = new wxBoxSizer(wxHORIZONTAL);
  wxStaticBoxSizer *sizer_2 = new wxStaticBoxSizer(sizer_2_staticbox, wxHORIZONTAL);
  wxBoxSizer *sizer_11 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_6 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_24 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_14 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *sizer_23 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *sizer_12 = new wxBoxSizer(wxHORIZONTAL);
  sizer_7->Add(20, 20, 0, 0, 0);
  sizer_19->Add(20, 20, 0, 0, 0);
  sizer_12->Add(20, 20, 1, 0, 0);
  sizer_12->Add(bitmap_CVRemod, 0, 0, 0);
  sizer_12->Add(20, 20, 1, 0, 0);
  sizer_20->Add(sizer_12, 0, wxEXPAND, 0);
  sizer_6->Add(20, 20, 0, 0, 0);
  sizer_23->Add(label_2, 0, wxALIGN_RIGHT, 0);
  sizer_23->Add(20, 20, 0, 0, 0);
  sizer_23->Add(m_ComboConnections, 1, 0, 0);
  sizer_14->Add(sizer_23, 0, wxEXPAND, 0);
  sizer_6->Add(sizer_14, 0, wxEXPAND, 0);
  sizer_6->Add(20, 20, 0, 0, 0);
  sizer_24->Add(m_ConnectionsBtn, 0, wxALIGN_RIGHT, 0);
  sizer_6->Add(sizer_24, 1, wxEXPAND, 0);
  sizer_6->Add(20, 20, 0, wxALIGN_RIGHT, 0);
  sizer_11->Add(sizer_6, 0, wxEXPAND, 0);
  sizer_2->Add(sizer_11, 0, wxEXPAND, 0);
  sizer_20->Add(sizer_2, 0, wxEXPAND, 0);
  sizer_20->Add(20, 20, 0, 0, 0);
  sizer_20->Add(20, 20, 0, 0, 0);
  sizer_13->Add(20, 20, 0, 0, 0);
  sizer_18->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy->Add(label_SubjectID, 1, wxALL | wxEXPAND, 5);
  sizer_3_copy_copy->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy->Add(m_edSubjectID, 1, wxALL | wxEXPAND, 0);
  sizer_5_copy->Add(sizer_3_copy_copy, 0, wxEXPAND, 5);
  sizer_3_copy_copy_copy_1->Add(label_Gender, 1, wxALL | wxEXPAND, 5);
  sizer_3_copy_copy_copy_1->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1->Add(combo_Gender, 1, 0, 0);
  sizer_5_copy->Add(sizer_3_copy_copy_copy_1, 0, wxEXPAND, 5);
  sizer_3_copy_copy_copy_1_copy->Add(label_Hand, 1, wxALL | wxEXPAND, 5);
  sizer_3_copy_copy_copy_1_copy->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1_copy->Add(combo_Hand, 1, 0, 0);
  sizer_5_copy->Add(sizer_3_copy_copy_copy_1_copy, 0, wxEXPAND, 5);
  sizer_3_copy_copy_copy_2->Add(label_YOB, 1, wxALL | wxEXPAND, 5);
  sizer_3_copy_copy_copy_2->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_2->Add(m_edYOB, 1, wxALL | wxEXPAND, 0);
  sizer_5_copy->Add(sizer_3_copy_copy_copy_2, 0, wxEXPAND, 5);
  sizer_5_copy->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1_copy_1->Add(label_Project, 1, wxALL | wxEXPAND, 5);
  sizer_3_copy_copy_copy_1_copy_1->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1_copy_1->Add(combo_Projects, 1, 0, 0);
  sizer_5_copy->Add(sizer_3_copy_copy_copy_1_copy_1, 0, wxEXPAND, 5);
  sizer_5_copy->Add(20, 20, 0, 0, 0);
  sizer_5_copy->Add(m_btnQuery, 0,
                    wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 5);
  sizer_18->Add(sizer_5_copy, 1, wxEXPAND, 0);
  sizer_18->Add(20, 20, 0, 0, 0);
  sizer_13->Add(sizer_18, 1, wxEXPAND, 0);
  sizer_13->Add(20, 20, 0, 0, 0);
  notebook_1_pane_1_copy->SetSizer(sizer_13);
  sizer_5->Add(notebook_1_pane_1_copy, 0, 0, 0);
  notebook_1_pane_1->SetSizer(sizer_5);
  sizer_25->Add(20, 20, 0, 0, 0);
  sizer_26->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_2_copy->Add(label_Label, 1, wxALL | wxEXPAND, 5);
  sizer_3_copy_copy_copy_2_copy->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_2_copy->Add(m_LabelExperiment, 1, wxALL | wxEXPAND, 0);
  sizer_5_copy_1->Add(sizer_3_copy_copy_copy_2_copy, 0, wxEXPAND, 5);
  sizer_3_copy_copy_copy_1_copy_2->Add(label_XSI_Type, 1, wxALL | wxEXPAND, 5);
  sizer_3_copy_copy_copy_1_copy_2->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1_copy_2->Add(combo_XSI_Type, 1, 0, 0);
  sizer_5_copy_1->Add(sizer_3_copy_copy_copy_1_copy_2, 0, wxEXPAND, 5);
  sizer_5_copy_1->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1_copy_2_copy->Add(label_Date, 1, wxALL | wxEXPAND, 5);
  sizer_3_copy_copy_copy_1_copy_2_copy->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1_copy_2_copy->Add(datepicker_ctrl_1, 0, 0, 0);
  sizer_5_copy_1->Add(sizer_3_copy_copy_copy_1_copy_2_copy, 0, wxEXPAND, 5);
  sizer_9_copy->Add(20, 20, 1, 0, 0);
  sizer_9_copy->Add(m_DateAfter, 0, 0, 0);
  sizer_5_copy_1->Add(sizer_9_copy, 1, wxEXPAND, 0);
  sizer_5_copy_1->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1_copy_1_copy->Add(label_Project_Experiments, 1,
                                            wxALL | wxEXPAND, 5);
  sizer_3_copy_copy_copy_1_copy_1_copy->Add(20, 20, 0, 0, 0);
  sizer_3_copy_copy_copy_1_copy_1_copy->Add(combo_Projects_Experiments, 1, 0, 0);
  sizer_5_copy_1->Add(sizer_3_copy_copy_copy_1_copy_1_copy, 0, wxEXPAND, 5);
  sizer_5_copy_1->Add(20, 20, 0, 0, 0);
  sizer_5_copy_1->Add(m_btnQuery_Experiments, 0,
                      wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 5);
  sizer_26->Add(sizer_5_copy_1, 1, wxEXPAND, 0);
  sizer_26->Add(20, 20, 0, 0, 0);
  sizer_25->Add(sizer_26, 1, wxEXPAND, 0);
  sizer_25->Add(20, 20, 0, 0, 0);
  sizer_9->Add(sizer_25, 1, wxEXPAND, 0);
  notebook_1_pane_2->SetSizer(sizer_9);
  notebook_1->AddPage(notebook_1_pane_1, wxT("Subjects"));
  notebook_1->AddPage(notebook_1_pane_2, wxT("Experiments"));
  sizer_27->Add(notebook_1, 1, wxEXPAND, 0);
  sizer_20->Add(sizer_27, 0, wxEXPAND, 0);
  sizer_17->Add(10, 10, 0, 0, 0);
  sizer_17->Add(m_edSaveToDirectory, 0, wxEXPAND, 0);
  sizer_17->Add(20, 20, 0, 0, 0);
  sizer_17->Add(m_btnOpenDirectoryForSavingPacsData, 0, wxALIGN_CENTRE, 0);
  sizer_17->Add(20, 20, 0, 0, 0);
  sizer_16->Add(sizer_17, 0, wxEXPAND, 0);
  sizer_15->Add(sizer_16, 1, wxEXPAND, 0);
  sizer_20->Add(sizer_15, 0, wxEXPAND, 0);
  sizer_19->Add(sizer_20, 0, wxEXPAND, 0);
  sizer_19->Add(20, 20, 0, 0, 0);
  sizer_1->Add(20, 20, 1, 0, 0);
  sizer_1->Add(bitmap_XNAT, 0, wxALIGN_CENTER_VERTICAL, 0);
  sizer_1->Add(20, 20, 1, 0, 0);
  sizer_21->Add(sizer_1, 0, wxEXPAND, 0);
  sizer_10->Add(20, 20, 0, 0, 0);
  sizer_22->Add(20, 20, 0, 0, 0);
  sizer_22->Add(m_XNATTree, 1, wxEXPAND, 0);
  sizer_22->Add(20, 20, 0, 0, 0);
  sizer_3->Add(20, 20, 1, 0, 0);
  sizer_3->Add(m_btnDownload, 0, wxEXPAND, 0);
  sizer_3->Add(20, 20, 1, 0, 0);
  sizer_22->Add(sizer_3, 0, wxEXPAND, 0);
  sizer_22->Add(20, 20, 0, 0, 0);
  m_PanelDetailedInformation->SetSizer(sizer_10_copy);
  sizer_6_copy->Add(m_PanelDetailedInformation, 1, wxEXPAND, 0);
  sizer_22->Add(sizer_6_copy, 1, wxEXPAND, 0);
  sizer_22->Add(20, 20, 0, 0, 0);
  sizer_8->Add(sizer_22, 1, wxEXPAND, 0);
  sizer_8->Add(20, 20, 0, 0, 0);
  sizer_10->Add(sizer_8, 1, wxEXPAND, 0);
  sizer_21->Add(sizer_10, 1, wxEXPAND, 0);
  sizer_19->Add(sizer_21, 1, wxEXPAND, 0);
  sizer_19->Add(20, 20, 0, 0, 0);
  sizer_7->Add(sizer_19, 1, wxEXPAND, 0);
  sizer_7->Add(sizer_4, 0, wxEXPAND, 0);
  sizer_7->Add(20, 20, 0, 0, 0);
  SetSizer(sizer_7);
  // end wxGlade
}
