/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef XNATPLUGINUTILITIES_H
#define XNATPLUGINUTILITIES_H

// std
#include <string>
#include <vector>
#include <map>

// core
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreDirectory.h"
#include "coreReportExceptionMacros.h"
#include "coreBaseExceptions.h"

// boost
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

// xml
#include "tinyxml.h"

// xnat
#include "xnatClient.h"
#include "Core.h"
#include "Utils.h"
//#include "XnatPluginConnectionPanelWidget.h"

/**
\ingroup XNATPlugin
\author Albert Sanchez
\date 19 Sep 2011
*/

struct XNATSubject {
  int id;
  std::map< std::string, std::string > attributes;

  std::map< std::string, std::vector< XNAT::Utils::Experiment > >
      experiments; // classified by project ids
  std::map< std::string, std::vector< XNAT::Utils::Scan > >
      scans; // classified by experiment label
  std::map< std::string, std::vector< XNAT::Utils::Resource > >
      resources; // classified by experiment label
  std::map< std::string, std::vector< XNAT::Utils::SubjectResourceFile > >
      subjectResourceFiles; // classified by subject+resource label
  XNAT::Utils::diagnosis
      diagnosisItems; // classified by the diagnosis name like MSP:SymptomaticSegment
};

struct XNATQueryParameters {
  std::string usr;
  std::string pwd;
  std::string siteUrl;

  std::map< std::string, std::string > restrictions;

  std::vector< std::string > GetColumns() {
    std::vector< std::string > ret;
    std::map< std::string, std::string >::iterator it;

    for (it = restrictions.begin(); it != restrictions.end(); it++) {
      ret.push_back(it->first);
    }

    return ret;
  }
};

struct XNATDownloadParameters {
  std::string usr;
  std::string pwd;
  std::string siteUrl;

  std::string project;
  std::string label;

  std::vector< XNAT::Utils::Scan > experimentScans;
  XNAT::Utils::Experiment experiment;
  XNAT::Utils::SubjectResourceFile
      subjectResourceFile; //!  Refer XNATWorkingArea::OnDownload(...) and
                           //!  XNATDownloadProcessor::Update() for changes

  std::string downloadFileName;
  std::string baseFolder;

  bool downloadScans;            //! TRUE if Scans under experiment have to be downloaded.
                                 //! previously it was bool scansEmpty;
  bool downloadSubjectResources; //! TRUE if Subject's resource file has to be downloaded.
                                 //! previously it was bool resourcesEmpty;
  //! Refer XNATWorkingArea::OnDownload(...) and XNATDownloadProcessor::Update() for
  //! changes
};

struct XNATUploadParameters {
  std::string usr;
  std::string pwd;
  std::string siteUrl;

  std::string project;
  std::string label;

  XNAT::Utils::Experiment experiment;
  std::string uploadFileName;

  std::string resourceCollection;
  std::string resourceLabel;

  std::string resourceExt;
};

struct XNATExportParameters {
  std::string user;
  std::string password;
  std::string url;
  std::string filename;
};

class XNATPluginDefineConetions {
public:
  std::string GetName() const { return m_Name; }

  void SetName(std::string str) { m_Name = str; }

  std::string GetURL() const { return m_URL; }

  bool GetEncripted() const { return m_Encripted; }

  void SetURL(std::string str) { m_URL = str; }

  std::string GetUser() const { return m_User; }

  void SetUser(std::string str) { m_User = str; }

  std::string GetPassword() const { return m_Password; }

  void SetPassword(std::string str) { m_Password = str; }

  void SetEncripted(bool bl) { m_Encripted = bl; }

  bool operator==(const XNATPluginDefineConetions &c) {
    return this->m_Name == c.m_Name && this->m_URL == c.m_URL &&
           this->m_User == c.m_User && this->m_Password == c.m_Password &&
           this->m_Encripted == c.m_Encripted;
  }

private:
  // Variables
  std::string m_Name, m_URL, m_User, m_Password;

  bool m_Encripted;
};

bool operator==(const XNATPluginDefineConetions &c1, const XNATPluginDefineConetions &c2);

class XNATPluginUtilities {
public:
  static std::string CreateBaseFolder(std::string path);
  static void CreateConfigFile(std::string xnatFolder);

  static std::string GetConfigFilePath();
  static void SetConfigFilePath(std::string str);

  static void AddNodeConfigFile();
  static void RemoveNodeConfigFile();
  static void EditNodeConfigFile();

  static void
  OverwriteConfigFile(std::map< std::string, XNATPluginDefineConetions > urlManager);
  static std::map< std::string, XNATPluginDefineConetions > ReadConfigFile();
  static std::string m_Path;

protected:
  ~XNATPluginUtilities();
};

#endif // XNATPLUGINUTILITIES_H
