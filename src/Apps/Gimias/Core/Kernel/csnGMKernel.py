# Used to configure coreIO
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

gmKernel = api.CreateCompiledProject("gmKernel", "dll")
gmKernel.AddProjects([
  cilabMacros,
  itk,
  boost,
  gmCommonObjects,
  gmDataHandling,
  gmIO,
  gmWorkflow,
  dynLib,
  tpExtLibBoost
])

gmKernel.SetPrecompiledHeader("gmKernelPCH.h")
#if not api.GetCompiler().TargetIsWindows():
#	gmKernel.AddDefinitions(["-ftemplate-depth-1024"], private = 1)

baseFolders = [
  "src"
]

for baseFolder in baseFolders:
        gmKernel.AddSources(["%s/*.cxx" % baseFolder], checkExists = 0)
        gmKernel.AddSources(["%s/*.txx" % baseFolder], checkExists = 0)
        gmKernel.AddSources(["%s/*.h" % baseFolder], checkExists = 0)
        gmKernel.AddIncludeFolders([baseFolder])

gmKernel.AddFilesToInstall(gmKernel.Glob("resource"), "resource")

#if api.GetCompiler().TargetIsUnix():
#    gmKernel.AddDefinitions(["-ftemplate-depth=80"])
