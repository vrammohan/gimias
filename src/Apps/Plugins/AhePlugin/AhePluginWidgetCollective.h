/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _AhePluginWidgetCollective_H
#define _AhePluginWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

#include "AhePluginProcessorCollective.h"

namespace ahePlugin {

/**
This class instantiates all widgets used in the plugin. The widgets are used to operate
the plugin processors
(see ProcessorCollective).
In the AhePlugin, there is currently only one widget, but when the number of widgets
grows, this class
ensures that the code remains maintainable.

\ingroup AhePlugin
*/

class WidgetCollective : public Core::WidgetCollective {
public:
  //!
  coreDeclareSmartPointerClassMacro(ahePlugin::WidgetCollective, Core::WidgetCollective);

private:
  //! The constructor instantiates all the widgets and registers them.
  WidgetCollective();
};

} // namespace ahePlugin

#endif //_AhePluginWidgetCollective_H
