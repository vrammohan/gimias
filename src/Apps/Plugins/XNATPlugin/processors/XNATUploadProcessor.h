/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* University of Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/
#ifndef XNATUploadProcessor_H
#define XNATUploadProcessor_H

#include "XNATPluginUtilities.h"

#include "gmProcessorsWin32Header.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreCommonDataTypes.h"
#include "corePluginMacros.h"
#include "coreBaseProcessor.h"

#include <map>
#include <vector>

#include <wx/event.h>
#include <wx/process.h>
#include <wx/dir.h>
#include <wx/zipstrm.h>
#include <wx/wfstream.h>

/**
\ingroup XNATPlugin
\author Shreyansh Jain
\date 15 Apr 2013
\brief This processor is responsible for uploading resources
       to the selected experiement
*/

class XNATUploadProcessor : public Core::BaseProcessor {
public:
  typedef enum { NUMBEROFINPUTS } INPUT_TYPE;

  typedef enum { OUTPUT_DATA_ENTITY, NUMBEROFOUTPUTS } OUTPUT_TYPE;

  //!
  coreProcessor(XNATUploadProcessor, Core::BaseProcessor);

  //!
  XNATUploadProcessor();
  //!
  ~XNATUploadProcessor();

  //!
  void SetParameters(XNATUploadParameters params);

  //!
  void Update();

private:
  //! Purposely not implemented
  XNATUploadProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  XNATUploadParameters m_Params;
  Core::DataEntity::Pointer m_DataEntity;
};

#endif // XNATUploadProcessor_H
