/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef StatisticsWorkingArea_H
#define StatisticsWorkingArea_H

#include "StatisticsWorkingAreaUI.h"
#include "StatisticsPluginUtilities.h"

#include "CILabNamespaceMacros.h"
#include "coreProcessingWidget.h"
#include "coreDICOMFileReader.h"

/**
A class that defines all the graphical interface for displaying table data and user
interaction for executing statistics

\ingroup StatisticsPlugin
\author Albert Sanchez
\date 18 Oct 2011
*/

class StatisticsWorkingArea : public StatisticsWorkingAreaUI,
                              public Core::Widgets::ProcessingWidget {
public:
  coreDefineBaseWindowFactory(StatisticsWorkingArea)

      //! constructor. initializes members.
      StatisticsWorkingArea(wxWindow *parent, int id,
                            const wxPoint &pos = wxDefaultPosition,
                            const wxSize &size = wxDefaultSize, long style = 0);

public:
  //!
  void OnInit();

  //!
  Core::BaseProcessor::Pointer GetProcessor();

private:
  //! Updates GUI
  void UpdateWidget();

private:
};

#endif // StatisticsWorkingArea_H
