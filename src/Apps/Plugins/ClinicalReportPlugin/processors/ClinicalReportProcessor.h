/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#ifndef ClinicalReportProcessor_H
#define ClinicalReportProcessor_H

#include "gmProcessorsWin32Header.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreCommonDataTypes.h"
#include "corePluginMacros.h"
#include "coreBaseProcessor.h"

#include <map>
#include <vector>

// wxpdf
#include "wx/pdfdoc.h"
#include "wx/pdffontmanager.h"
#include "wx/pdfdocdef.h"

#include "ClinicalReportDocument.h"
#include "ClinicalReportElement.h"
#include "ClinicalReportPatientInformation.h"

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 19 Oct 2010
*/

#ifndef __WXMAC__
class WXDLLIMPEXP_PDFDOC wxPdfDocumentModule : public wxModule {
  DECLARE_DYNAMIC_CLASS(wxPdfDocumentModule)
public:
  wxPdfDocumentModule();
  bool OnInit();
  void OnExit();
};
#endif //__WXMAC__

class ClinicalReportProcessor : public Core::BaseProcessor {
public:
  //!
  coreProcessor(ClinicalReportProcessor, Core::BaseProcessor);

  //!
  ClinicalReportProcessor();
  //!
  ~ClinicalReportProcessor();

  //!
  void SetParameters(ClinicalReportPatientInformation patientInfo,
                     std::vector< ClinicalReportElement > clinicalReportElements,
                     wxString reportTitleTxt, wxString reportAuthorsTxt,
                     wxString comentsTxt, wxString reportsFolder, wxString reportName,
                     std::string imageName);

  //!
  void Update();

  //!
  void SetParameter(blTag::Pointer tag);

  //!
  std::string GetReportsFolder();
  void SetReportsFolder(std::string folder);

  //!
  std::string GetReportFilename();

private:
  //! Purposely not implemented
  ClinicalReportProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

  ClinicalReportPatientInformation m_PatientInfo;
  std::vector< ClinicalReportElement > m_ClinicalReportElements;

  wxString m_ReportTitleTxt;
  wxString m_ReportAuthorsTxt;
  wxString m_ComentsTxt;
  wxString m_ReportsFolder;
  wxString m_ReportName;
  std::string m_ReportFilename;

  std::string m_Image;

private:
};

#endif // ClinicalReportProcessor_H
