/*==============================================================================

  Library: MSVTK

  Copyright (c) Kitware Inc.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

#ifndef __msvVTKBUTTONSMainWindow_h
#define __msvVTKBUTTONSMainWindow_h

// Qt includes
#include <QMainWindow>
class QListWidgetItem;

// CTK includes
//#include <ctkVTKObject.h>

// Export macro includes
#include "msvWxWidgetsExport.h"

class msvWxVTKButtonsMainWindowPrivate;

class MSV_WX_WIDGETS_EXPORT msvWxVTKButtonsMainWindow : public wxWindow {
public:
  typedef wxWindow Superclass;
  msvWxVTKButtonsMainWindow(wxWidget *parent = 0);
  virtual ~msvWxVTKButtonsMainWindow();

public: // slots:
  void openData();
  void closeData();
  void aboutApplication();
  void updateView();
  void setCurrentSignal(int pointId);
  void showTooltip(QString text);

  void on_checkBoxShowButtons_stateChanged(int state);
  void on_checkBoxShowLabels_stateChanged(int state);
  void on_checkBoxFlyTo_stateChanged(int state);
  void on_comboBoxPosition_currentIndexChanged(int index);

protected: // slots:
  void onPointSelected();
  void onCurrentTimeChanged(double);
  void onVTKButtonsSelectionChanged();
  void onCurrentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

protected:
  QScopedPointer< msvWxVTKButtonsMainWindowPrivate > d_ptr; /*TODO*/

private:
  //  Q_DECLARE_PRIVATE(msvQVTKButtonsMainWindow); /*TODO*/
  //  Q_DISABLE_COPY(msvQVTKButtonsMainWindow); /**/
};

#endif
