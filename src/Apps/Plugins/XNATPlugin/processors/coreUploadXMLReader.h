/*
* Copyright (c) 2011,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef coreUploadXMLReader_H
#define coreUploadXMLReader_H

#include "coreBaseDataEntityReader.h"
#include "coreDataEntityWriter.h"
#include "coreSettings.h"
#include "coreKernel.h"
#include "coreDirectory.h"
#include "coreDataEntityReader.h"

#include <wx/zipstrm.h>
#include <wx/wfstream.h>

namespace Core {
namespace IO {
  /**
  A specialization of the DataEntityReader class for reading a specific purpose XML used
  to Upload data from Unicore to XNAT.

  \ingroup XNATPlugin
  \author Albert Sanchez
  \date 14 Feb 2012
  */

  class UploadXMLReader : public BaseDataEntityReader {
  public:
    coreDeclareSmartPointerClassMacro(Core::IO::UploadXMLReader, BaseDataEntityReader);

    struct UploadElement {
      std::string baseURL;
      std::string usr;
      std::string pwd;
      std::string subject;
      std::string project;
      std::string experiment;
      std::string folder;
      std::string filename;
    };
    //!
    virtual void ReadData();

  protected:
    UploadXMLReader(void);
    virtual ~UploadXMLReader(void);

    //!
    virtual boost::any ReadSingleTimeStep(int iTimeStep, const std::string &filename);

  private:
    coreDeclareNoCopyConstructors(UploadXMLReader);

    std::string m_TmpDir;

    std::string FindAfter(std::string str, std::string pat);
    std::string FindBefore(std::string str, std::string pat);
  };
}
}

#endif // coreUploadXMLReader_H
