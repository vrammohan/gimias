/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef _AhePlugin_H
#define _AhePlugin_H

#include "AhePluginProcessorCollective.h"
#include "AhePluginWidgetCollective.h"

// CoreLib
#include "coreFrontEndPlugin.h"

namespace ahePlugin {

/**
\brief Creates all objects of the plug-in and connect them.

\note Nobody can get access to this class. This class is only for
initialization of all components.

\note Try to make all processors reusable for other plug-ins. Be aware
of creating a dependency between the processor and any class of the rest
of the plug-in.

\ingroup AhePlugin
*/
class PLUGIN_EXPORT AhePlugin : public Core::FrontEndPlugin::FrontEndPlugin {
  // TYPE DEFINITIONS
public:
  coreDeclareSmartPointerClassMacro(AhePlugin, Core::FrontEndPlugin::FrontEndPlugin);

  // OPERATIONS
protected:
  //!
  AhePlugin(void);

  //!
  virtual ~AhePlugin(void);

private:
  //! Purposely not implemented
  AhePlugin(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  //! Contains all the processors for the plugin
  ProcessorCollective::Pointer m_Processors;

  //! Contains all the widgets for the plugin
  WidgetCollective::Pointer m_Widgets;
}; // class AhePlugin

} // namespace ahePlugin

#endif // AhePlugin_H
