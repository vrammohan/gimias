/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef ClinicalReportDocument_H
#define ClinicalReportDocument_H

#include "ClinicalReportPatientInformation.h"
#include "ClinicalReportElement.h"

// wxpdf
#include "wx/pdfdoc.h"

/**
\ingroup ClinicalReportPlugin
\author Albert Sanchez
\date 21 Oct 2010
*/

class ClinicalReportDocument : public wxPdfDocument {
public:
  ClinicalReportDocument();

  void Header();
  void Footer();

  void SetTitle(wxString title);
  wxString GetTitle();
  void SetAuthors(wxString authors);
  wxString GetAuthors();
  void SetImage(std::string image);
  void SetComments(wxString comments);
  wxString GetComments();

  void SetPatientInformation(ClinicalReportPatientInformation patientInfo);
  void SetReportElements(std::vector< ClinicalReportElement > ReportElements);

  void WritePatientInformation();
  void WriteReportElements();
  void WriteComments();

private:
  void WriteTitle(wxString title);
  void WriteTable(std::vector< std::string > header,
                  std::vector< std::vector< std::string > > lines);
  void AddAnImage(std::string imagePath, int MaxHeight, int maxWidth,
                  bool checkPageEnd = true, bool advanceAfter = true);

private:
  wxString m_Title;
  wxString m_Authors;
  wxString m_Comments;

  wxString m_HeaderFont;
  wxString m_FooterFont;
  wxString m_AuthorsFont;
  wxString m_DateFont;
  wxString m_TitleFont;
  wxString m_RegularTextFont;

  int m_HeaderFontSize;
  int m_FooterFontSize;
  int m_AuthorsFontSize;
  int m_DateFontSize;
  int m_TitleFontSize;
  int m_RegularTextFontSize;

  //! Clinical data
  ClinicalReportPatientInformation m_PatientInformation;
  std::vector< ClinicalReportElement > m_ReportElements;

  std::string m_ResourcePath;

  std::string m_RightImage;
};

#endif // ClinicalReportDocument_H
