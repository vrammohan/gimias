/*
* Copyright (c) 2013,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield, Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#include "ContouringPluginProcessorCollective.h"

namespace contouringPlugin {

ProcessorCollective::ProcessorCollective() {}

} // namespace contouringPlugin{
