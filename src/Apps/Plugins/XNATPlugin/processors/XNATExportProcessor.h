/*
* Copyright (c) 2010,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/
#ifndef XNATExportProcessor_H
#define XNATExportProcessor_H

#include "XNATPluginUtilities.h"

#include "gmProcessorsWin32Header.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreCommonDataTypes.h"
#include "corePluginMacros.h"
#include "coreBaseProcessor.h"

#include <map>
#include <vector>

#include <wx/event.h>
#include <wx/process.h>
#include <wx/dir.h>
#include <wx/zipstrm.h>
#include <wx/wfstream.h>

/**
\ingroup XNATPlugin
\author Albert Sanchez
\date 17 Feb 2011
*/

class XNATExportProcessor : public Core::BaseProcessor {
public:
  typedef enum { NUMBEROFINPUTS } INPUT_TYPE;

  typedef enum { OUTPUT_DATA_ENTITY, NUMBEROFOUTPUTS } OUTPUT_TYPE;

  //!
  coreProcessor(XNATExportProcessor, Core::BaseProcessor);

  //!
  XNATExportProcessor();
  //!
  ~XNATExportProcessor();

  //!
  void SetParameters(XNATExportParameters params);

  //!
  void Update();

private:
  //! Purposely not implemented
  XNATExportProcessor(const Self &);

  //! Purposely not implemented
  void operator=(const Self &);

private:
  XNATExportParameters m_Params;
};

#endif // XNATExportProcessor_H
