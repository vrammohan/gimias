/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef _ClinicalReportToolbar_H
#define _ClinicalReportToolbar_H

#include "coreToolbarBase.h"
#include "coreWorkingAreaFactory.h"
#include "coreToolbarPluginTab.h"

#include "ClinicalReportProcessor.h"

#define wxID_ADD_SCREENSHOT_TO_CLINICAL_REPORT                                           \
  wxID("wxID_ADD_SCREENSHOT_TO_CLINICAL_REPORT")
#define wxID_ADD_COMMENT_TO_CLINICAL_REPORT wxID("wxID_ADD_COMMENT_TO_CLINICAL_REPORT")
#define wxID_OPEN_CLINICAL_REPORT_DESIGNER wxID("wxID_OPEN_CLINICAL_REPORT_DESIGNER")

namespace Core {
namespace Widgets {

  /**
  \brief Toolbar for ClinicalReport
  \ingroup ClinicalReportPlugin
  \author Albert Sanchez
  \date 25 Oct 2010
  */

  class ClinicalReportToolbar : public ToolbarBase {
  public:
    //!
    coreDefineBaseWindowFactory(ClinicalReportToolbar);

    //!
    ClinicalReportToolbar(wxWindow *parent, wxWindowID id = wxID_ANY,
                          const wxPoint &pos = wxDefaultPosition,
                          const wxSize &size = wxDefaultSize,
                          long style = wxAUI_TB_DEFAULT_STYLE,
                          const wxString &name = wxPanelNameStr);

    //!
    Core::BaseProcessor::Pointer GetProcessor();
    void SetProcessor(ClinicalReportProcessor::Pointer processor);

    //!
    void Init();

    //!
    void OnAddClinicalReportElement(wxCommandEvent &event);

    //!
    void OnAddClinicalReportComment(wxCommandEvent &event);

    //!
    void OnOpenClinicalReportDesigner(wxCommandEvent &event);

    //!
    void OpenClinicalReportDesigner();

  protected:
    //!
    void UpdateState();

    DECLARE_EVENT_TABLE();

  private:
    //!
    void AddTextElementToClinicalReport(std::string ReportPath, std::string comments);

    //!
    void AddElementsToClinicalReport(std::string ReportPath,
                                     std::vector< bool > _selectedV,
                                     std::vector< std::string > _commentsV,
                                     std::vector< std::string > _titlesV,
                                     std::vector< std::string > _thumbnailSmallV,
                                     std::vector< std::string > _thumbnailMediumV,
                                     std::vector< std::string > _screenshotV,
                                     bool overwrite = false, std::string name = "",
                                     std::string authors = "");

    //!
    std::string GetNextAvailableName(std::string basedir);

    //!
    std::string GetString(int i);

    //!
    std::string GetLastSelectedFromConfig();

    //!
    void SetLastSelectedToConfig(std::string newSelected);

  private:
    //! processor
    ClinicalReportProcessor::Pointer m_Processor;
  };

} // namespace Widgets
} // namespace Core

#endif // _ClinicalReportToolbar_H
