
echo Packing %PACKAGE_NAME%...
@echo off

set PACKAGE_NAME=%1
set VERSION=%2
set PLATFORM=%3
set BIN_FOLDER=%4
set OUTPUT_FOLDER=%5
set TEMP_FOLDER=%5\Temp
set ZIP="C:\Program Files\7-Zip\7z.exe"

md %TEMP_FOLDER%
for /f "tokens=*" %%a in (%PACKAGE_NAME%_Files.txt) do (
    copy "%BIN_FOLDER%\%%a" "%TEMP_FOLDER%\%%a"
)
xcopy %BIN_FOLDER%\plugins\%PACKAGE_NAME% %TEMP_FOLDER%\plugins\%PACKAGE_NAME% /S /I
del %VERSION%\Packages\%PACKAGE_NAME%-%VERSION%-%PLATFORM%.zip
cd %TEMP_FOLDER%
%ZIP% a %OUTPUT_FOLDER%\%VERSION%\Packages\%PACKAGE_NAME%-%VERSION%-%PLATFORM%.zip *.* -r
cd %~dp0
rmdir %TEMP_FOLDER% /S/Q
echo Packed %PACKAGE_NAME%